from app.main import db
from app.main.model.claimer_message_tracking import ClaimerMessageTracking as Tracking
from app.main.messaging.model.claimer_message import ClaimerMessage
from app.main.notification.model.claimer_notification import ClaimerNotification
from sqlalchemy.sql import text
from app.main.model.message_document import MessageDocument
from itertools import chain 
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.core.service.workflow_service import make_claimer_transition
from app.main.service.logging_service import loggingBDError
from app.main.core.service.report_notification_massive_service2 import ReportJasperMasivo

def verify_mail_status(tracking, claimer_message):
    #update a los campos de claimer_message
    complete_claimer_message(claimer_message, tracking)                        
    #hago los procesos en claimer message si existe data en claimer_notification
    if (tracking.current_message_state != 'BOUNCE'):   
        document_notify=claimer_workflow_update(claimer_message, tracking.current_message_state)
    if claimer_message and claimer_message.claimer_message_type == 'NOTIFICATION':
        notify= verify_message_notification(claimer_message, tracking)
        if document_notify == 'notify':
            ReportJasperMasivo.create_report(64, claimer_message.claimer_id, claimer_message.email, None, claimer_message.aa_id)


def verify_mail_status2(claimer_message):              
    #hago los procesos en claimer message si existe data en claimer_notification
    ReportJasperMasivo.create_report(64, claimer_message['claimer_id'], claimer_message['email'], None, claimer_message['aa_id'])
        

def verify_message_notification(data, mail):
    if mail.current_message_state == 'OPEN' or mail.current_message_state == 'DELIVERED':
        notification=ClaimerNotification.query.filter_by(claimer_id = data.claimer_id, aa_id=data.aa_id).first() 
        user_notify = 0    
        if not notification:
            notification = ClaimerNotification()
            notification.claimer_id = data.claimer_id
            notification.notification_document_id = data.document_tracking_id
            notification.aa_id = data.aa_id
            notification.claimer_notified = True
            notification.notification_type = 'ELECTRONICAMENTE'
            notification.notification_date = mail.tracking_date
            notification.claimer_message_id = mail.claimer_message_id
            notification.user_id = 10000
            user_notify = 1
        elif notification.claimer_notified == False:
            # notification.notification_document_id = data.document_tracking_id
            notification.claimer_notified = True
            notification.notification_type = 'ELECTRONICAMENTE'
            notification.notification_date = mail.tracking_date
            notification.claimer_message_id = mail.claimer_message_id
            notification.user_id = 10000
            user_notify = 1
        if user_notify == 1:
            save_changes(notification)
        return user_notify

def complete_claimer_message(claimer_message,mail):
    change = 0
    if mail.current_message_state == 'OPEN' or mail.current_message_state == 'DELIVERED':
        if claimer_message.delivery_date == None:
            claimer_message.delivery_date = mail.tracking_date
            save_changes(claimer_message)
    return True

def claimer_workflow_update(claimer_message, state):

    # favor verificar bien antes de agregar o cambiar workflow_keys
    indice = ''
    _workflow_keys={ 'old':[16,7,6,2,8,5],
                    'new_s':[20,11,12,3,11,10],
                    'new_f':[17,4,10,15,9,9] }
    k =''
    if state== 'DELIVERED':
        k='new_s'
    elif state== 'OPEN':
        k='new_s'
    elif state== 'COMPLAINT':
        k='new_f'
    elif state== 'OTTO':
        k='new_f'
    elif state== 'UNDELIVERED':
        k='new_f'
    claimer = ClaimerWorkflow.query.filter_by(claimer_id = claimer_message.claimer_id,end_date = None).first()
    if claimer and k != '':
        try:
            indice = _workflow_keys['old'].index(claimer.current_state)
            if indice:
                data = {
                    'claimerId':claimer.claimer_id,
                    'nextState':_workflow_keys[k][indice],
                    'nextUser':10000,
                    'logUser': 10000
                }
                make_claimer_transition(data)
        except:
            print('no se encontró indice')
    if k=='new_s' and indice == 3:
        return 'notify'
    return 'ok'       


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='verify_mail_status')
        db.session.rollback()
        newLogger.error(e)


        



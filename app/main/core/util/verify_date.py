from app.main import db
from app.main.citation.model.massive_publication import MassivePublication
from datetime import date, timedelta


def verify_date_publication(id):

    massive = MassivePublication.query.filter(MassivePublication.publication_id == id).first()
    start= massive.publication_begin_date
    end=massive.publication_end_date
    today= date.today()

    result = 0

    before=start- timedelta(days=5)

    if(today >= before):
        result = 1
    elif (today >= start and today < end):
        result = 2
    elif (today >= end):
        result = 3
        
    return result
from flask_restplus import Namespace, fields


class SerieSubserieDto:
    api = Namespace('serie_subserie', description='operaciones relacionadas con la tabla serie_subserie')

    serie_subserie = api.model('serie_subserie', {
        'ssId': fields.Integer(attribute='ss_id'),
        'serie': fields.String(attribute='serie'),
        'subserie': fields.String(attribute='subserie'),
        'documentName': fields.String(attribute='document_name'),
        'active': fields.Boolean(attribute='active')
    })

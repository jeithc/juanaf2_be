from flask_restplus import Namespace, fields


class FrameColumnDto:
    api = Namespace('frame_column', descripion='columnas de frame_list')

    column = api.model('frame_column', {
        'frameColumnId': fields.Integer(attribute='frame_column_id', description='id de la columna'),
        'title': fields.String(attribute='title', description='id de la columna'),
        'order': fields.Integer(attribute='order', description='id de la columna'),
        'field': fields.String(description='atributo del objeto consultado'),
    })

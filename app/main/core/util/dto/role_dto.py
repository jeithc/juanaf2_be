from flask_restplus import Namespace, fields

class RoleDto:
    api = Namespace('role', description='Operaciones relacionadas con la tabla de roles')

    role = api.model('role', {
        'roleId': fields.Integer(attribute='role_id',description='Id del role'),
        'shortDescription': fields.String(attribute='short_description', required=True, description='Nombre del role'),
        'longDescription': fields.String(attribute='long_description', description='Descripción del role'),
        'state': fields.String(description='Estado del role'),
        'startDate': fields.DateTime(attribute='start_date', description='Fecha de activación del role'),
        'endDate': fields.DateTime(attribute='end_date', description='Fecha de desactivación del role'),
    })

    role_id = api.model('role_id', {
        'roleId': fields.Integer(attribute='role_id', description='Id del role')
    })
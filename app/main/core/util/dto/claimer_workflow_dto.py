from flask_restplus import Namespace, fields


class ClaimerWorkflowDto:
    api = Namespace('claimer_workflow', description='Operaciones relacionadas con la tabla de flujo de trabajo del solicitante')

    claimer_workflow = api.model('claimer_workflow', {
        'claimerId': fields.Integer(attribute='claimer_id',description='Id del solicitante'),
        'destinationUserId': fields.Integer(attribute='user_id', description='Id del usuario de destino'),
        'startState': fields.Integer(description='Estado de partida'),
        'endState': fields.Integer(attribute='current_state', description='Estado de llegada')
    })

    claimer_workflow_children = api.model('claimer_workflow_children', {
        'workflowId': fields.Integer(attribute='workflow_id', description='Id del workflow'),
        'claimerId': fields.Integer(attribute='claimer_id', description='Id del solicitante'),
        'userId': fields.Integer(attribute='user_id', description='Id del usuario de destino'),
        'currentState': fields.Integer(attribute='current_state', description='Estado actual del solicitante'),
        'endDate': fields.DateTime(attribute='end_date', description='Fecha de cierre del estado')
    })

    claimer_transition = api.model('claimer_transition', {
        'claimerId': fields.Integer(attribute='claimer_id',description='Id del solicitante'),
        'nextState': fields.Integer(attribute='user_id', description='Id del usuario de destino'),
        'nextUser': fields.Integer(description='Estado de partida'),
        'logUser': fields.Integer(attribute='current_state', description='Estado de llegada')
    })

    claimer_state_procs = api.model('claimer_state_procs' , {
        'nodeId': fields.String(attribute='node_id', description='Id del estado'),
        'description' : fields.String(attribute="description" , description=" Nombre descriptivo del estado"),
        'state': fields.String(atribute="state" , description="Estado del proceso Activo/Inactivo") 
    })

from flask_restplus import Namespace, fields

from app.main.core.util.dto.serie_subserie_attributes_dto import SerieSubserieAttributesDto
from app.main.core.util.dto.serie_subserie_dto import SerieSubserieDto


class SerieSubserieMetadataDto:
    api = Namespace('serie_subserie_metadata', description='operaciones relacionadas con la tabla serie_subserie_metadata')

    serie_subserie_metadata = api.model('serie_subserie_metadata', {
        'ssmId': fields.Integer(attribute='ssm_id'),
        'ssId': fields.Integer(attribute='ss_id'),
        'ssDocName': fields.String(attribute='ss_doc_name'),
        'serie': fields.String(attribute='serie'),
        'subserie': fields.String(attribute='subserie'),
        'ssAttributeId': fields.Integer(attribute='ss_attribute_id'),
        'ssAttribute': fields.Nested(SerieSubserieAttributesDto.serie_subserie_attributes, attribute='serie_subserie_attributes'),
        'mandatory': fields.Boolean(attribute='mandatory')
    })

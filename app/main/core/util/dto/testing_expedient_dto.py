from flask_restplus import Namespace, fields


class TestingExpedientSetDto:
    api = Namespace('testing_expedient_set', description='Operaciones relacionadas con expedientes de prueba')
    testing_expedient_set = api.model('testing_expedient_set', {
        'testingExpedientId': fields.Integer(attribute='testing_expedient_id', description='id del recurso'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'personExpedientId': fields.Integer(attribute='person_expedient_id'),
        'userId': fields.Integer(attribute='user_id'),
        'testingState': fields.String(attribute='testing_state'),
        'creationDate': fields.Date(attribute='creation_date')
    })

from flask_restplus import Namespace, fields


class FrameActionsDto:
    api = Namespace('frame_actions', description='acciones de la lista')

    frame_actions = api.model('frame_actions', {
        'actionId': fields.Integer(attribute='action_id', description='id de la acccion'),
        'targetUrl': fields.String(attribute='target_url'),
        'icon': fields.String(),
        'color': fields.String(),
        'params': fields.String(),
        'tooltip': fields.String(description='mensaje tooltip ralacionado cada accion')
    })

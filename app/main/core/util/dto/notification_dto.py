from flask_restplus import Namespace, fields


class NotificationDto:
    api = Namespace(
        'notification', description='Operaciones relacionadas con notificaciones')

    notification = api.model('notification', {
        'url': fields.String(required=False, description='URL Report')
    })


class ClaimerMessageTrackingDto:
    api = Namespace('claimer_notification',
                    description='Operaciones relacionadas con notification')
    claimer_message_tracking = api.model('claimer_message_tracking', {
        'claimer_message_tracking_id': fields.Integer(required=True, description='claimer_message_tracking_id'),
        'claimer_id': fields.Integer(required=True, description='claimer_id'),
        'claimer_message_id': fields.Integer(required=False, description='claimer_message_id'),
        'registration_date': fields.DateTime(required=False, description='registration_date'),
        'current_message_state': fields.String(required=False, description='current_message_state'),
        'document_tracking_id': fields.Integer(required=False, description='document_tracking_id'),
        'tracking_date': fields.DateTime(required=True, description='tracking_date'),
        'tracking_detail': fields.String(required=True, description='tracking_detail')
    })

    def __repr__(self):
        return "<ClaimerMessageTrackingDto '{}'>".format(self.claimer_id)

from flask_restplus import Namespace, fields

from app.main.core.util.dto.role_dto import RoleDto


class UserRoleDto:
    api = Namespace('user_role', description='Operaciones relacionadas con la tabla de user_role')

    user_role = api.model('user_role', {
        'roleId': fields.Integer(attribute='role_id', description='Id del role'),
        'shortDescription': fields.String(attribute='role.short_description', required=True, description='Nombre del role'),
        'longDescription': fields.String(attribute='role.long_description', description='Descripción del role'),
        'state': fields.String(attribute='state', description='Estado del usuario'),
        'startDate': fields.DateTime(attribute='start_date', description='Fecha de activación del rol al usuario'),
        'endDate': fields.DateTime(attribute='end_date', description='Fecha de desactivación del rol al usuario'),
        'supervisorUserId': fields.Integer(attribute='supervisor_user_id', description='Supervisor del usuario')
    })

    role_id = api.model('role_id', {
        'roleId': fields.Integer(attribute='role_id', description='Id del role')
    })
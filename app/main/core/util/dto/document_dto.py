from flask_restplus import Namespace, fields

from app.main.messaging.util.dto.document_type_dto import DocumentTypeDto


class DocumentDTO:
    api = Namespace('document', description='Operaciones relacionadas con la tabla de claimer_documents')

    document = api.model('document', {
        'documentId': fields.Integer(attribute='document_id', description='Id del role'),
        'claimerId': fields.Integer(attribute='claimer_id', required=True, description='Nombre del role'),
        'radicateNumber': fields.String(attribute='radicate_number', description='Descripción del role'),
        'documentTypeId': fields.Integer(attribute='document_type_id'),
        'numberOfPages': fields.Integer(attribute='number_of_pages'),
        'originDocument': fields.String(attribute='origin_document'),
        'documentChecksum': fields.String(attribute='document_checksum'),
        'metadataServerDocument': fields.String(attribute='Estado del role'),
        'xmlDocument': fields.String(attribute='xml_document'),
        'documentSize': fields.Float(attribute='document_size'),
        'radicationDate': fields.DateTime(attribute='radication_date', description='Fecha de activación del role'),
        'radicationDateString': fields.String(attribute='radication_date', ),
        'urlDocument': fields.String(attribute='url_document', description='Fecha de activación del role'),
        'documentFormat': fields.String(attribute='document_format_initials', description='Formato de documento'),
        'flowDocument': fields.String(attribute='flow_document', description='Fecha de activación del role'),
        'expedientEntryDate': fields.DateTime(attribute='expedient_entry_date', description='Fecha de desactivación del role'),
        'documentType': fields.Nested(DocumentTypeDto.document_type, attribute='document_type')
    })

from flask_restplus import Namespace, fields


class AppParamsDto:
    api = Namespace('app_params', description='Operaciones relacionadas con la tabla parametros de aplicacion')

    app_params = api.model('app_params', {
        'environment': fields.String(description='Ambiente del parametro'),
        'variable': fields.String(description='Estado de partida'),
        'value': fields.String(description='Valor de parmetro'),
        'state': fields.Boolean(description='Valor de parmetro')
    })
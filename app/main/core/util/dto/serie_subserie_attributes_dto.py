from flask_restplus import Namespace, fields


class SerieSubserieAttributesDto:
    api = Namespace('serie_subserie_attributes', description='operaciones relacionadas con la tabla serie_subserie_attributes')

    serie_subserie_attributes = api.model('serie_subserie_attributes', {
        'ssAttributeId': fields.Integer(attribute='ss_attribute_id'),
        'attributeName': fields.String(attribute='attribute_name'),
        'attributeDescriptionRule': fields.String(attribute='attribute_description_rule'),
        'attributeDataType': fields.String(attribute='attribute_data_type'),
        'attributeMaxLength': fields.Integer(attribute='attribute_max_length'),
        'active': fields.Boolean(attribute='active')
    })

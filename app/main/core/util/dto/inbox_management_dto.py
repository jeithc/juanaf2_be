from flask_restplus import Namespace, fields


class InboxManagementDto:
    api = Namespace(
        'inbox_management', description='operaciones ralacionadas con la tabla inbox_management'
    )
    inbox_management = api.model('inbox_management', {
        'userId': fields.Integer(attribute='user_id', description='id del usuario registrado'),
        'currentState': fields.Integer(attribute='current_state', description='estado actual'),
        'claimerId': fields.Integer(attribute='claimer_id', description='id del claimer'),
        'radicationDate': fields.Date(attribute='radication_date', description='fecha radicacion'),
        'expedientDescription': fields.String(attribute='expedient_description', description='desc del exp'),
        'assignDate': fields.Date(attribute='assign_date', description='fecha asiganacion'),
        'personExpedientId': fields.Integer(attribute='person_expedient_id', description='id de mascara'),
        'roleDescription': fields.String(attribute='role_description', description='descripcion del rol'),
        'inboxName': fields.String(attribute='inboxName', description='id del usuario registrado'),
        'inboxCreationDate': fields.Date(attribute='inbox_creation_date', description='fecha de creacion bandeja'),
        'expedientState': fields.String(attribute='expedient_state', description='Estado actual del expediente'),
        'expedientAnnotation': fields.String(attribute='expedient_annotation', description='Anotación del estado del expediente'),
        'returned': fields.String(attribute='returned_from_approval_proof', description='Determina si el expediente es devuelto')
    })

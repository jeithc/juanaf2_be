from flask_restplus import Namespace, fields
from app.main.core.util.dto.frame_column_dto import FrameColumnDto
from app.main.core.util.dto.role_dto import RoleDto
from app.main.core.util.dto.frame_actions_dto import FrameActionsDto


class FrameListDto:
    api = Namespace('frame_list', description='User related operations')

    frame_list = api.model('frame_list', {
        'frameListId': fields.Integer(attribute='frame_list_id', description='id'),
        'title': fields.String(description='nombre del frame_list'),
        'style': fields.String(description='estilo del frame_list'),
        'order': fields.Integer(description='orden en que apareceran los frame_list'),
        'queryExec': fields.Integer(description='parametros de consulta '),
        'listStyle': fields.Integer(description='estilo del frame_list '),
        'columns': fields.Nested(FrameColumnDto.column, many=True, attribute='frames_column'),
        'roles': fields.Nested(RoleDto.role, many=True),
        'actions': fields.Nested(FrameActionsDto.frame_actions, many=True, attribute='frame_actions'),
    })

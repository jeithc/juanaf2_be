from flask_restplus import Namespace, fields



class TestMailDTO:
    api = Namespace('test_mail', description='Operaciones relacionadas con mail verification')
    test_mail = api.model('test_mail', {
        'claimer_message_id': fields.Integer(required=True, description='id'),
        'current_state': fields.String(required=True, description='stado mensaje'),
        'document_certificate': fields.String(description='documento de resultado certificado'),
        'document_tracking': fields.String(description='documento de resultado guiá'),
        'tracking_date': fields.DateTime(description='fecha de resultado'),
        'tracking_detail': fields.String(description='texto de resultado')
    })

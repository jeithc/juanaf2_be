from flask_restplus import Namespace, fields


class AuthenticationDto:
    api = Namespace('authentication', description='Operaciones relacionadas con la autenticación')
    user_authentication = api.model('auth_details', {
        'email': fields.String(required=True, description='Dirección de email'),
        'password': fields.String(required=True, description='Contraseña del usuario'),
        'module': fields.String(description='Módulo en el que se registra el usuario')
    })

from flask_restplus import Namespace, fields


class StageParametersDto:
    api = Namespace('stage_parameters', description='Operaciones relacionadas con la tabla de stage_parameters')

    stage_parameters = api.model('stage_parameters', {
        'stageParamId': fields.Integer(attribute='stage_param_id', description='Id del stage_parameters'),
        'stageProccess': fields.String(attribute='stage_proccess'),
        'stageParamType': fields.String(attribute='stage_param_type', description='Tipo de parametro'),
        'stageParam': fields.String(attribute='stage_param', description='Nombre del parámetro'),
        'stage_param_text_value': fields.String(attribute='stage_param_text_value', description='Valor texto'),
        'stage_param_number_value': fields.Integer(attribute='stage_param_number_value', description='Valor numérico'),
        'note': fields.String(description='Explicación anexa al parámetro')
    })

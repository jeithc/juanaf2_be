from flask_restplus import Namespace, fields

from app.main.core.util.dto.role_dto import RoleDto
from app.main.core.util.dto.user_role_dto import UserRoleDto


class UserDto:
    api = Namespace('user', description='Operaciones relacionadas con la tabla de usuarios')

    user = api.model('user', {
        'id': fields.Integer(description='Id del usuario'),
        'email': fields.String(required=True, description='Dirección de email del usuario'),
        'names': fields.String(required=True, description='Nombres del usuario'),
        'surnames': fields.String(required=True, description='Apellidos del usuario'),
        'password': fields.String(description='Contraseña del usuario'),
        'identificationType': fields.String(required=True, attribute='identification_type',
                                            description='Tipo de identificación del usuario'),
        'identificationNumber': fields.String(required=True, attribute='identification_number',
                                              description='Número de identificación del usuario'),
        'phone': fields.String(description='Número de teléfono del usuario'),
        'state': fields.String(description='Estado del usuario'),
        'startDate': fields.DateTime(attribute='start_date', description='Fecha de activación del usuario'),
        'endDate': fields.DateTime(attribute='end_date', description='Fecha de desactivación del usuario'),
        'workload': fields.Integer(description='Carga de trabajo del analista'),
        'roles': fields.Nested(UserRoleDto.user_role, many=True, description='Roles del usuario')
    })

    user2 = api.model('user', {
        'id': fields.Integer(description='Id del usuario'),
        'email': fields.String(required=True, description='Dirección de email del usuario'),
        'names': fields.String(required=True, description='Nombres del usuario'),
        'surnames': fields.String(required=True, description='Apellidos del usuario'),
        'identificationType': fields.String(required=True, attribute='identification_type',
                                            description='Tipo de identificación del usuario'),
        'identificationNumber': fields.String(required=True, attribute='identification_number',
                                              description='Número de identificación del usuario'),
        'phone': fields.String(description='Número de teléfono del usuario'),
        'state': fields.String(description='Estado del usuario'),
        'startDate': fields.DateTime(attribute='start_date', description='Fecha de activación del usuario'),
        'endDate': fields.DateTime(attribute='end_date', description='Fecha de desactivación del usuario'),
        'workload': fields.Integer(description='Carga de trabajo del analista'),
        'roles': fields.Nested(UserRoleDto.user_role, many=True, description='Roles del usuario')
    })

    user_insert = api.model('user_insert', {
        'id': fields.Integer(description='Id del usuario'),
        'email': fields.String(required=True, description='Dirección de email del usuario'),
        'names': fields.String(required=True, description='Nombres del usuario'),
        'surnames': fields.String(required=True, description='Apellidos del usuario'),
        'password': fields.String(description='Contraseña del usuario'),
        'identificationType': fields.String(required=True, attribute='identification_type',
                                            description='Tipo de identificación del usuario'),
        'identificationNumber': fields.String(required=True, attribute='identification_number',
                                              description='Número de identificación del usuario'),
        'phone': fields.String(description='Número de teléfono del usuario'),
        'state': fields.String(description='Estado del usuario'),
        'startDate': fields.DateTime(attribute='start_date', description='Fecha de activación del usuario'),
        'endDate': fields.DateTime(attribute='end_date', description='Fecha de desactivación del usuario'),
        'workload': fields.Integer(description='Carga de trabajo del analista'),
        'roles': fields.Nested(RoleDto.role_id, many=True, description='Roles del usuario')
    })

    user_password = api.model('user_password', {
        'email': fields.String(required=True, description='Dirección de email del usuario'),
        'passwords': fields.String(required=True, description='Contraseña del usuario')
    })
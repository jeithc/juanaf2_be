from app.main import db, flask_bcrypt
import datetime, traceback
import jwt
# from app.main.administration.model.blacklist import BlacklistToken
from app.main.config import key
from app.main import logger


class UserBase():
    """ User base Model for storing users related details """

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    names = db.Column(db.String(100))
    surnames = db.Column(db.String(100))
    identification_type = db.Column(db.String(10))
    identification_number = db.Column(db.String(50), nullable=False)
    password_hash = db.Column(db.String(100))
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime)
    start_date = db.Column(db.DateTime)
    last_updated_by = db.Column(db.String, nullable=False)
    state = db.Column(db.String)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        try:
            self.password_hash = flask_bcrypt.generate_password_hash(password).decode('utf-8')
        except Exception as e:
            logger.error("Error en UserBase#setpassword - Error al establecer la contraseña")
            raise

    def check_password(self, password):
        if not self.password_hash:
            return False
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    @staticmethod
    def encode_auth_token(user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                key,
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(authentication_token):
        """
        Decodes the auth token
        :param authentication_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(authentication_token, key)
            # is_blacklisted_token = BlacklistToken.check_blacklist(authentication_token)
            # if is_blacklisted_token:
            #     return 'El token está en la lista negra. Por favor regístrese nuevamente.'
            # else:
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Token expiró. Por favor regístrese nuevamente.'
        except jwt.InvalidTokenError:
            return 'Toke inválido. Por favor regístrese nuevamente.'

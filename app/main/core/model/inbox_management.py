from app.main import db


class InboxManagement(db.Model):
    """ inbox management """
    __tablename__ = "inbox_management"
    __table_args__ = {'schema': 'core'}

    user_id = db.Column(db.Integer)
    current_state = db.Column(db.Integer)
    claimer_id = db.Column(db.Integer)
    radication_date = db.Column(db.Date)
    expedient_description = db.Column(db.String)
    assign_date = db.Column(db.Date)
    person_expedient_id = db.Column(db.Integer, primary_key=True,)
    role_description = db.Column(db.String(100))
    inbox_name = db.Column(db.String(100))
    inbox_creation_date = db.Column(db.Date)
    expedient_annotation = db.Column(db.String)
    expedient_state = db.Column(db.String)
    returned_from_approval_proof = db.Column(db.Boolean)


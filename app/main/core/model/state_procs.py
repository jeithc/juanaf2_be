from app.main import db


class StateProcs(db.Model):
    """Model State Process """
    __tablename__ = "state_procs"
    __table_args__ = {'schema': 'core'}
    node_id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255))
    state = db.Column(db.String(1))
    role_id = db.Column(db.Integer)


from app.main import db


class BusinessDays(db.Model):
    """ BusinessDays Model for storing BusinessDays"""
    __tablename__ = "business_days"
    __table_args__ = {'schema': 'core'}

    day_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    business_day_date = db.Column(db.DateTime)
    business_day_type = db.Column(db.String)
    week_number = db.Column(db.Integer)
    day_of_week = db.Column(db.Integer)
    day_name = db.Column(db.String)

    def __repr__(self):
        return "<BusinessDays '{}'>".format(self.day_id)

from app.main import db
from app.main.core.model.frame_list import FrameList

role_frame_list = db.Table('role_frame_list', db.Model.metadata,
                           db.Column('frame_list_id', db.Integer, db.ForeignKey('core.frame_list.frame_list_id')),
                           db.Column('role_id', db.Integer, db.ForeignKey('core.role.role_id')),
                           schema='core'
                           )


class Role(db.Model):
    """ Role Model for storing role related details """
    __tablename__ = "role"
    __table_args__ = {'schema': 'core'}

    role_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    short_description = db.Column(db.String(32))
    long_description = db.Column(db.String(128))
    state = db.Column(db.String(1))
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    frames_list = db.relationship("FrameList", secondary=role_frame_list, backref='roles')

    def __repr__(self):
        return "<Role '{}'>".format(self.short_description)

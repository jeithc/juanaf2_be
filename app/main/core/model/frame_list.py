from sqlalchemy.orm import relationship

from app.main import db
from app.main.core.model.frame_column import FrameColumn
from app.main.core.model.frame_actions import FrameActions

frame_column_frame_list = db.Table('frame_column_frame_list', db.Model.metadata,
                                   db.Column('frame_list_id', db.Integer,
                                             db.ForeignKey('core.frame_list.frame_list_id')),
                                   db.Column('frame_column_id', db.Integer,
                                             db.ForeignKey('core.frame_column.frame_column_id')),
                                   schema='core'
                                   )
event_list = db.Table('event_list', db.Model.metadata,
                      db.Column('frame_list_id', db.Integer, db.ForeignKey('core.frame_list.frame_list_id')),
                      db.Column('action_id', db.Integer, db.ForeignKey('core.frame_actions.action_id')),
                      schema='core'
                      )


class FrameList(db.Model):
    """frame_list or panels """
    __tablename__ = "frame_list"
    __table_args__ = {'schema': 'core'}

    frame_list_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(256))
    style = db.Column(db.String(256))
    order = db.Column(db.Integer)
    query_exec = db.Column(db.String)
    list_style = db.Column(db.String(256))
    frames_column = db.relationship("FrameColumn", secondary=frame_column_frame_list)
    frame_actions = db.relationship("FrameActions", secondary=event_list, backref="frames_list")

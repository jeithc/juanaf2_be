from app.main import db


class FrameActions (db.Model):
    """Acciones relacionadas al frameList """
    __tablename__ = 'frame_actions'
    __table_args__ = {'schema': 'core'}

    action_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    target_url = db.Column(db.String(512))
    icon = db.Column(db.String(512))
    color = db.Column(db.String(512))
    params = db.Column(db.String(256))
    tooltip = db.Column(db.String(256))

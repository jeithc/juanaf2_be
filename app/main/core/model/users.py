from app.main import db
from app.main.core.model.user_base import UserBase


class Users(db.Model, UserBase):
    """ User Model for storing user related details """
    __tablename__ = "users"
    __table_args__ = {'schema': 'core'}

    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.String(1))
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    phone = db.Column(db.String(20))
    roles = db.relationship("UserRole")

    # workflow_user = db.relationship('AspirantWorkflow')

    workload = 0

    def __repr__(self):
        return "<User '{}'>".format(self.email)

from app.main import db


class Connection(db.Model):
    """ Connection Model for storing users connected  """
    __tablename__ = "connection"
    __table_args__ = {'schema': 'core'}

    email = db.Column(db.String, primary_key=True)
    start_event_date = db.Column(db.Date)
    ending_type = db.Column(db.String(32))

    def __repr__(self):
        return "<Connection '{}'>".format(self.email)

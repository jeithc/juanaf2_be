from app.main import db


class FrameColumn(db.Model):
    """Columnas de las listas o paneles"""
    __tablename__ = "frame_column"
    __table_args__ = {'schema': 'core'}

    frame_column_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(256))
    order = db.Column(db.Integer)
    field = db.Column(db.String(256))

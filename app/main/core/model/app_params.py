from app.main import db, flask_bcrypt
from sqlalchemy import Table, Column, Integer, ForeignKey, String


class AppParams(db.Model):
    """Model AppParams"""
    __tablename__ = "app_params"
    __table_args__ = {'schema': 'core'}

    id = db.Column(db.Integer,  primary_key=True)
    variable = db.Column(db.String(128))
    environment = db.Column(db.String(128))
    value = db.Column(db.String(1024))
    state = db.Column(db.Boolean)

from app.main import db


class SerieSubserie(db.Model):
    """SerieSubserie model"""
    __tablename__ = 'serie_subserie'
    __table_args__ = {'schema': 'core'}

    ss_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    serie = db.Column(db.String(4))
    subserie = db.Column(db.String(15))
    document_name = db.Column(db.String(512))
    active = db.Column(db.Boolean)

    @staticmethod
    def init_from_dto(data):
        serieSubserie = SerieSubserie()
        if ('ssId' in data and data['ssId']):
            serieSubserie.ss_id = data['ssId']
        if ('serie' in data and data['serie']):
            serieSubserie.serie = data['serie']
        if ('subserie' in data and data['subserie']):
            serieSubserie.subserie = data['subserie']
        if ('documentName' in data and data['documentName']):
            serieSubserie.document_name = data['documentName']

        return serieSubserie

from sqlalchemy.orm import relationship
from app.main import db


class SerieSubserieMetadata(db.Model):
    """SerieSubserieMetadata model"""
    __tablename__ = 'serie_subserie_metadata'
    __table_args__ = {'schema': 'core'}

    ssm_id = db.Column(db.Integer)
    ss_id = db.Column(db.Integer, db.ForeignKey(
        'core.serie_subserie.ss_id'), primary_key=True)
    ss_doc_name = db.Column(db.String(512))
    serie = db.Column(db.String(4))
    subserie = db.Column(db.String(15))
    ss_attribute_id = db.Column(db.Integer, db.ForeignKey(
        'core.serie_subserie_attributes.ss_attribute_id'), primary_key=True)
    serie_subserie_attributes = relationship('SerieSubserieAttributes')
    mandatory = db.Column(db.Boolean)

    @staticmethod
    def init_from_dto(data):
        serieSubserieMetadata = SerieSubserieMetadata()
        if ('ssmId' in data and data['ssmId']):
            serieSubserieMetadata.ssm_id = data['ssmId']
        if ('ssId' in data and data['ssId']):
            serieSubserieMetadata.ss_id = data['ssId']
        if ('serie' in data and data['serie']):
            serieSubserieMetadata.serie = data['serie']
        if ('subserie' in data and data['subserie']):
            serieSubserieMetadata.subserie = data['subserie']
        if ('ssAttributeId' in data and data['ssAttributeId']):
            serieSubserieMetadata.ss_attribute_id = data['ssAttributeId']

        return serieSubserieMetadata

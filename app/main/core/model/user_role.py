from app.main import db


class UserRole(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "user_role"
    __table_args__ = {'schema': 'core'}

    user_id = db.Column(db.Integer, db.ForeignKey('core.users.id'), primary_key=True)
    role_id = db.Column(db.Integer, db.ForeignKey('core.role.role_id'), primary_key=True)
    state = db.Column(db.String(1))
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    supervisor_user_id = db.Column(db.Integer)
    role = db.relationship("Role")


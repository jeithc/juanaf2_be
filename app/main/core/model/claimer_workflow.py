from app.main import db


class ClaimerWorkflow(db.Model):
    """ ClaimerWorkflow Model for storing ClaimerWorkflow related details """
    __tablename__ = "claimer_workflow"
    __table_args__ = {'schema': 'phase_2'}

    workflow_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_user_data.claimer_id'))
    user_id = db.Column(db.Integer, db.ForeignKey('core.users.id'))
    current_state = db.Column(db.Integer)
    start_date = db.Column(db.DateTime)
    end_date = db.Column(db.DateTime)
    triggered_event_user = db.Column(db.Integer)

    def __repr__(self):
        return "<ClaimerWorkflow '{}'>".format(self.workflow_id)

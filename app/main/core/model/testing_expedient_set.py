from app.main import db
from sqlalchemy.orm import relationship


class TestingExpedientSet(db.Model):
    """testing_expedient_set model"""
    __tablename__ = 'testing_expedient_set'
    __table_args__ = {'schema': 'tempwork'}

    testing_expedient_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    person_expedient_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    testing_state = db.Column(db.String(100))
    creation_date = db.Column(db.DateTime)

    def __repr__(self):
        return "<TestingExpedientSet '{}'>".format(self.testing_expedient_id)

    @staticmethod
    def init_from_dto(data):
        testing_expedient_set = TestingExpedientSet()
        if 'testingExpedientId' in data and data['testingExpedientId']:
            testing_expedient_set.testing_expedient_id = data['testingExpedientId']
        if 'claimerId' in data and data['claimerId']:
            testing_expedient_set.claimer_id = data['claimerId']
        if 'personExpedientId' in data and data['personExpedientId']:
            testing_expedient_set.person_expedient_id = data['personExpedientId']
        if 'userId' in data and data['userId']:
            testing_expedient_set.user_id = data['userId']
        if 'testingState' in data and data['testingState']:
            testing_expedient_set.testing_state = data['testingState']
        if 'creationDate' in data and data['creationDate']:
            testing_expedient_set.creation_date = data['creationDate']

        return testing_expedient_set

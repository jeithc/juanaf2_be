from app.main import db


class StageParameters(db.Model):
    "Model StageParameters"
    __tablename__ = "stage_parameters"
    __table_args__ = {'schema': 'core'}

    stage_param_id = db.Column(db.Integer, primary_key=True)
    stage_proccess = db.Column(db.String(100))
    stage_param_type = db.Column(db.String(100))
    stage_param = db.Column(db.String(100))
    stage_param_text_value = db.Column(db.String(256))
    stage_param_number_value = db.Column(db.Integer)
    note = db.Column(db.String(512))

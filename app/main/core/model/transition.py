from app.main import db


class Transition(db.Model):
    """ Transition Model for storing Transition related details """
    __tablename__ = "transition"
    __table_args__ = {'schema': 'core'}

    origin = db.Column(db.Integer, primary_key=True)
    destination = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(128))
    state = db.Column(db.String(1))

    def __repr__(self):
        return "<Transition '{}' - '{}' - '{}'>".format(self.origin, self.destination, self.description)

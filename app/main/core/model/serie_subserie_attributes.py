from app.main import db


class SerieSubserieAttributes(db.Model):
    """SerieSubserieAttributes model"""
    __tablename__ = 'serie_subserie_attributes'
    __table_args__ = {'schema': 'core'}

    ss_attribute_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    attribute_name = db.Column(db.String(100))
    attribute_description_rule = db.Column(db.String(512))
    attribute_data_type = db.Column(db.String(100))
    attribute_max_length = db.Column(db.Integer)
    active = db.Column(db.Boolean)

    @staticmethod
    def init_from_dto(data):
        serieSubserieAttributes = SerieSubserieAttributes()
        if ('ssAttributeId' in data and data['ssAttributeId']):
            serieSubserieAttributes.ss_attribute_id = data['ssAttributeId']
        if ('attributeName' in data and data['attributeName']):
            serieSubserieAttributes.attribute_name = data['attributeName']
        if ('attributeDescriptionRule' in data and data['attributeDescriptionRule']):
            serieSubserieAttributes.attribute_description_rule = data['attributeDescriptionRule']
        if ('attributeDataType' in data and data['attributeDataType']):
            serieSubserieAttributes.attribute_data_type = data['attributeDataType']
        if ('attributeMaxLength' in data and data['attributeMaxLength']):
            serieSubserieAttributes.attribute_max_length = data['attributeMaxLength']
        if ('active' in data and data['active']):
            serieSubserieAttributes.active = data['active']

        return serieSubserieAttributes
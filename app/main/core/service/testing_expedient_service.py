from app.main.core.model.testing_expedient_set import *
from app.main.core.util.dto.testing_expedient_dto import TestingExpedientSetDto

from flask_restplus import marshal
from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from datetime import datetime
from sqlalchemy import text


def find_all():
    try:

        result = TestingExpedientSet.query.all()

        return result
    except Exception as e:
        return manage_db_exception(
          "Error en third_person_service#find_by_filter - Error al consultar terceros",
          'Ocurrió un error al consultar los terceros.', e)


def save(data):
    logger.debug('Entro a testing_expedient_service#save')

    new_te = TestingExpedientSet.init_from_dto(data)
    new_te.testing_expedient_id = None
    new_te.creation_date = datetime.now()
    new_te.testing_state = 'PARA SUSTANCIAR'
    try:
        db.session.add(new_te)
        db.session.commit()

        db.session.refresh(new_te)

        # funcion de asignacion
        make_assignment()

        return marshal(new_te, TestingExpedientSetDto.testing_expedient_set), 201
    except Exception as e:
        return manage_db_exception("Error en testing_expedient_service#save - Error al guardar el expediente",
                                   'Ocurrió un error al guardar el expediente.', e)


def make_assignment():
    update_query = ' SELECT tempwork.f_manual_delivery_expedient() '
    db.engine.execute(text(update_query).execution_options(autocommit=True))

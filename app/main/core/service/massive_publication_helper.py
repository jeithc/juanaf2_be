from app.main import db
from app.main.service.logging_service import loggingBDError
from app.main.citation.model.massive_publication import MassivePublication
from app.main.model.claimer_documents import Publication_claimer
from app.main.model.message_document import MessageDocument
import datetime

def add_massive_publication(data):
    massive= MassivePublication (
            aa_id = 1,
            publication_begin_date = datetime.datetime.now(),
            generator_document_id= data['generator_document_id'],
            publication_type= data['publication_type'],
            publication_state=data['publication_state']
        )
    save_changes(massive)
    return massive.publication_id

def complete_massive_publication(data, publication):
    massive = MassivePublication.query.filter(MassivePublication.publication_id == publication).first()
    massive.generator_document_id=data['generator_document_id']
    massive.publication_state=data['publication_state']
    save_changes(massive)
    return massive.publication_id


def add_massive_publication_claimer(claimer_id,publication_id):
    #agrego un nuevo registro en claimer_massive
    add_claimer = Publication_claimer(
        publication_id=publication_id,
        claimer_id=claimer_id
    )
    save_changes(add_claimer)
    return 'ok'

def add_mail_message_document(document,message):
    message_document = MessageDocument()
    message_document.claimer_message_id = message
    message_document.document_id = document
    save_changes(message_document)
    return 'ok'


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='massive publication service')
        db.session.rollback()
        newLogger.error(e)
        print(e)




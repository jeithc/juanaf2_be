import json
import requests
from app.main.config import ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.messaging.model.document_type import Document_type


def create_report(report_type, params, report_format):
    auth = (usuario_jasper, contrasena_jasper)

    # Init session so we have no need to auth again and again:
    s = requests.Session()

    report_params = []

    for key, value in params.items():
        value_array = [value]
        report_params.append({'name': key, 'value': value_array})

    general_params = {'reportUnitUri': "/reports/{}".format(report_type),
                      'async': 'false',
                      'freshData': 'false',
                      'saveDataSnapshot': 'false',
                      'outputFormat': report_format,
                      'interactive': 'true',
                      'ignorePagination': 'false',
                      'parameters': {'reportParameter': report_params}
                      }

    url = ruta_jasper_server + "reportExecutions"

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
    }

    response = s.post(url, data=json.dumps(general_params), headers=headers, auth=auth)
    data_response = response.json()
    sessionid = s.cookies.get('JSESSIONID')

    try:
        request_id = data_response.get("requestId")
        export_id = data_response.get("exports")[0].get("id")
        number_of_pages = data_response.get("totalPages")
        file_name = data_response.get("exports")[0].get("outputResource").get("fileName")
        report_url = ruta_jasper_server + "reportExecutions/{request_id}/exports/{export_id}/outputResource".format(
            request_id=request_id, export_id=export_id)
        metadata_server_document = json.dumps(data_response)
        report = view_report(sessionid, report_url, auth)

        return {'number_of_pages': number_of_pages, 'report': report, 'file_name': file_name,
                'metadata_server_document': metadata_server_document}

    except Exception as e:
        print(e)
        print("An exception occurred invoke report jasper")
        return None


def view_report(session_id, url, auth):
    s1 = requests.Session()
    s1.cookies['JSESSIONID'] = session_id
    headers = {"Accept": "application/json",
               "Content-Type": "application/xml"}
    ret = s1.get(url, headers=headers, auth=auth)
    return ret.content


def get_report_route(document_type):
    doc_type = Document_type.query.filter_by(id=document_type).first()
    return doc_type.document_route


if __name__ == "__main__":
    params = {'param_doc_type': 'CC',
              'param_doc_number': '20312245',
              'param_radicate_number': '2019003030000215651'}
    create_report('acta_de_notificacion_personal', params, 'pdf')
    create_report('acta_de_notificacion_personal', params, 'xml')

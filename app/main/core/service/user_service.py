from datetime import datetime
import base64

from flask_restplus import marshal
from sqlalchemy import text, or_

from app.main import db
from app.main import logger
from app.main.core.model.users import Users
from app.main.core.model.role import Role
from app.main.core.model.user_role import UserRole
from app.main.core.util.dto.role_dto import RoleDto
from app.main.core.util.dto.user_dto import UserDto
from app.main.core.service.general_service import manage_db_exception
from app.main.core.util.dto.user_role_dto import UserRoleDto


def save_new_user(data):
    logger.debug('Entro a user_service#save_new_user')

    try:
        other_user_ced= Users.query.filter_by(identification_number=data['identificationNumber']).first()
        other_user_mail= Users.query.filter_by(email=data['email']).first()
    except Exception as e:
        return manage_db_exception("Error en user_service#save_new_user - " +
                                   "Error al consultar el usuario",
                                   'Ocurrió un error al consultar el usuario.', e)
    if not other_user_ced and not other_user_mail:
        new_user = Users(
            email=data['email'],
            names=data['names'],
            surnames=data['surnames'],
            identification_type=data['identificationType'],
            identification_number=data['identificationNumber'],
            state=data['state'],
            start_date=data['startDate'],
            created_at=datetime.now(),
            updated_at=datetime.now(),
            last_updated_by=data['lastUpdatedBy']
        )

        if 'password' in data and data['password']:
            new_user.password = data['password']
        if 'phone' in data and data['phone']:
            new_user.phone = data['phone']
        if 'endDate' in data and data['endDate']:
            new_user.end_date = data['endDate']

        try:
            db.session.add(new_user)
            db.session.commit()
            #guardamos los roles que se agregaron al usuario creado
            if 'roles' in data and data['roles']:
                save_roles(data['roles'], data['startDate'], new_user.id)
            response_object = {
                'status': 'success',
                'message': 'Usuario almacenado exitosamente.'
            }
            return response_object, 201
        except Exception as e:
            return manage_db_exception("Error en user_service#save_new_user - Error al guardar el usuario",
                                       'Ocurrió un error al guardar el usuario.', e)

    else:
        if other_user_ced:
            response_object = {
                'status': 'fail',
                'message': 'El documento ya fue registrado previamente. Por favor ingrese uno diferente.'
            }
        if other_user_mail:
            response_object = {
                'status': 'fail',
                'message': 'El correo ya fue registrado previamente. Por favor ingrese uno diferente.'
            }
        return response_object, 200


def update_user(data):
    logger.debug('Entro a user_service#update_user')

    try:
        # user = Users.query.filter_by(email=data['email']).first()
        user = Users.query.filter_by(id=data['id']).first()
        other_user_ced= Users.query.filter_by(identification_number=data['identificationNumber'])\
            .filter(Users.id!=data['id']).first()
        other_user_mail= Users.query.filter_by(email=data['email'])\
            .filter(Users.id!=data['id']).first()
    except Exception as e:
        return manage_db_exception("Error en user_service#update_user - " +
                                   "Error al consultar el usuario",
                                   'Ocurrió un error al consultar el usuario.', e)
    if not user:
        response_object = {
            'status': 'fail',
            'message': 'El usuario no existe. Por favor verifique el email.'
        }
        return response_object, 200
    
    if other_user_ced:
        response_object = {
            'status': 'fail',
            'message': 'Ya existe un usuario con el documento ingresado.'
        }
        return response_object, 200
    
    if other_user_mail:
        response_object = {
            'status': 'fail',
            'message': 'Ya existe un usuario con el correo ingresado.'
        }
        return response_object, 200

    user.updated_at = datetime.now()
    user.last_updated_by =data['lastUpdatedBy']

    if 'names' in data and data['names']:
        user.names = data['names']
    if 'surnames' in data and data['surnames']:
        user.surnames = data['surnames']
    if 'identificationType' in data and data['identificationType']:
        user.identification_type = data['identificationType']
    if 'identificationNumber' in data and data['identificationNumber']:
        user.identification_number = data['identificationNumber']
    if 'password' in data and data['password']:
        user.password = data['password']
    if 'state' in data and data['state']:
        user.state = data['state']
    if 'startDate' in data and data['startDate']:
        user.start_date = data['startDate']
    if 'endDate' in data and data['endDate']:
        user.end_date = data['endDate']
    if 'phone' in data and data['phone']:
        user.phone = data['phone']
    if 'email' in data and data['email']:
        user.email = data['email']

    try:
        db.session.commit()
        if 'roles' in data and data['roles']:
            save_roles(data['roles'], datetime.now(), user.id)
        response_object = {
            'status': 'success',
            'message': 'Usuario actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en user_service#update_user - " +
                                   "Error al actualizar el usuario",
                                   'Ocurrió un error al actualizar el usuario.', e)


def test(user_id):
    roles = [9,1,2,3]
    save_roles(roles, '2020-05-12', user_id)


def save_roles(roles, start_date, user_id):
    old_roles=get_roles_by_user(user_id)
    delete=[]
    add=[]
    if old_roles:
        old_roles = [old['roleId'] for old in old_roles]
        roles = [rol['roleId'] for rol in roles]
        print(old_roles)
        #se verifican los nuevos roles 
        for rol in roles:
            if rol not in old_roles:
                add.append(rol)
        for rol in old_roles:
            if rol not in roles:
                delete.append(rol)
        #se guardan los nuevos roles si los hay 
        for rol in add:
            new_rol = UserRole(
                user_id = user_id,
                role_id = rol,
                state = 'A',
                start_date = start_date)
            save_rol(new_rol)

        #se eliminan los roles a eliminar si los hay
        for rol in delete:
            rol_delete = UserRole.query.filter_by(role_id= rol, user_id= user_id).first()
            delete_rol(rol_delete)


    else:
        for rol in roles:
            print(rol)
            new_rol = UserRole(
                user_id = user_id,
                role_id = rol['roleId'],
                state = 'A',
                start_date = start_date)
            save_rol(new_rol)
    return 'ok'   


def save_rol(new_rol):
    try:
        db.session.add(new_rol)
        db.session.commit()
    except Exception as e:
        manage_db_exception("Error en user_service#save_new_user - Error al guardar el rol al usuario",
                                    'Ocurrió un error al guardar el rol del usuario.', e)


def delete_rol(rol):
    try:
        db.session.delete(rol)
        db.session.commit()
    except Exception as e:
        manage_db_exception("Error en user_service#save_new_user - Error al borrar el rol al usuario",
                                    'Ocurrió un error al borrar el rol del usuario.', e)


def delete_user(data):
    logger.debug('Entro a user_service#delete_user')

    try:
        user = Users.query.filter_by(id=data['id']).first()
        if user:
            db.session.delete(user)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'Usuario eliminado exitosamente.'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'success',
                'message': 'El usuario no existe, por favor verifique el id.'
            }
            return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en user_service#delete_user - " +
                                   "Error al eliminar el usuario",
                                   'Ocurrió un error al eliminar el usuario.', e)


def get_all_users():
    logger.debug('Entro a user_service#get_all_users')
    try:
        users = Users.query.all()

        # for user in users:
        #     user.workload = AspirantWorkflow.query.filter(AspirantWorkflow.user_id==user.id)\
        #         .filter(AspirantWorkflow.end_date.is_(None))\
        #         .filter(AspirantWorkflow.current_state.in_([2, 3])).count()

        return marshal(users, UserDto.user), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_all_users - " +
                                   "Error al obtener los usuarios",
                                   'Ocurrió un error al obtener los usuarios.', e)


def get_all_users_paged(page,perPage):
    logger.debug('Entro a user_service#get_all_users_paged')
    try:
        query = Users.query
        query = query.paginate(page, perPage).items
        return marshal(query, UserDto.user), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_all_users - " +
                                   "Error al obtener los usuarios",
                                   'Ocurrió un error al obtener los usuarios.', e)


def count_all_users_paged():
    logger.debug('Entro a user_service#count_all_users_paged')
    try:
        query = Users.query
        return query.count(), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#count_all_users_paged - " +
                                   "Error al obtener los usuarios",
                                   'Ocurrió un error al obtener los usuarios.', e)


def get_user_by_email(email):
    logger.debug('Entro a user_service#get_user_by_email')
    try:
        user = Users.query.filter_by(email=email).first()
        return marshal(user, UserDto.user), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_user_by_email - " +
                                   "Error al obtener usuario por email",
                                   'Ocurrió un error al obtener el usuario por email.', e)
                                

def get_user_by_filter(data):
    logger.debug('Entro a user_service#get_user_by_filter')
    try:
        query = Users.query
        if 'names' in data and data['names']:
            query = query.filter(Users.names.like("%{}%".format(data['names'])))
        if 'surnames' in data and data['surnames']:
            query = query.filter(Users.surnames.like("%{}%".format(data['surnames'])))
        if 'identificationNumber' in data and data['identificationNumber']:
            query = query.filter(Users.identification_number == data['identificationNumber'])
        if 'startDate' in data and data['startDate']:
            query = query.filter(Users.start_date >= data['startDate'])
        if 'endDate' in data and data['endDate']:
            query = query.filter(Users.start_date <= data['endDate'])
        if 'email' in data and data['email']:
            query = query.filter(Users.email.like("%{}%".format(data['email'])))
        if 'state' in data and data['state']:
            query = query.filter(Users.state  == data['state'])
        if 'roles' in data and data['roles']:
            query = query.join(UserRole,
            UserRole.user_id==Users.id).filter(UserRole.role_id == data['roles'][0]['roleId'])
        query = query.paginate(data['page'], data['perPage']).items
        return marshal(query, UserDto.user), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_user_by_filter - " +
                                   "Error al obtener usuario por email",
                                   'Ocurrió un error al obtener el usuario por email.', e)
                                
def count_user_filter(data):
    logger.debug('Entro a user_service#get_user_by_filter')
    try:
        query = Users.query
        if 'names' in data and data['names']:
            query = query.filter(Users.names.like("%{}%".format(data['names'])))
        if 'surnames' in data and data['surnames']:
            query = query.filter(Users.surnames.like("%{}%".format(data['surnames'])))
        if 'identificationNumber' in data and data['identificationNumber']:
            query = query.filter(Users.identification_number == data['identificationNumber'])
        if 'startDate' in data and data['startDate']:
            query = query.filter(Users.start_date >= data['startDate'])
        if 'endDate' in data and data['endDate']:
            query = query.filter(Users.start_date <= data['endDate'])
        if 'email' in data and data['email']:
            query = query.filter(Users.email.like("%{}%".format(data['email'])))
        if 'state' in data and data['state']:
            query = query.filter(Users.state  == data['state'])
        if 'roles' in data and data['roles']:
            query = query.join(UserRole,
            UserRole.user_id==Users.id).filter(UserRole.role_id == data['roles'][0]['roleId'])

        return query.count(), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_user_by_filter - " +
                                   "Error al obtener usuario por email",
                                   'Ocurrió un error al obtener el usuario por email.', e)

def change_password(data):
    logger.debug('Entro a user_service#change_password')
    try:
        user = Users.query.filter_by(email=data['email']).first()
    except Exception as e:
        return manage_db_exception("Error en user_service#change_password - " +
                                   "Error al consultar el usuario del sistema",
                                   'Ocurrió un error al consultar el usuario del sistema.', e)
    if not user:
        response_object = {
            'status': 'success',
            'message': 'El usuario no existe, por favor verifique el id.'
        }
        return response_object, 200

    user.updated_at = datetime.now()
    user.last_updated_by = user.email

    decode_pass = base64.b64decode(data['password'])

    user.password = decode_pass
    user.state = 'A'

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Contraseña modificada exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en user_service#change_password - " +
                                   "Error al guardar el usuario",
                                   'Ocurrió un error al actualizar el usuario.', e)


def get_user_by_role(role):
    logger.debug('Entro a user_service#get_user_by_role')
    try:
        users = Users.query\
            .join(UserRole)\
            .join(Role, UserRole.role_id == Role.role_id)\
            .filter(Role.short_description == role, Users.state == 'A').all()

        return marshal(users, UserDto.user), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_user_by_role - " +
                                   "Error al obtener usuario por role",
                                   'Ocurrió un error al obtener el usuario por role.', e)


def get_all_users_by_role(role):
    logger.debug('Entro a user_service#get_all_users_by_role')
    try:
        if(role == 'todos'):
            users = Users.query \
                .join(UserRole) \
                .join(Role, UserRole.role_id == Role.role_id) \
                .filter(Role.short_description == 'aj_auditor' or Role.short_description == 'aj_jefe_auditor' or Role.short_description == 'aj_analista') \
                .order_by(Users.surnames).all()
        else:
            users = Users.query\
                .join(UserRole)\
                .join(Role, UserRole.role_id == Role.role_id)\
                .filter(Role.short_description == role)\
                .order_by(Users.surnames).all()

        return marshal(users, UserDto.user2), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_all_users_by_role - " +
                                   "Error al obtener todos los usuarios por rol",
                                   'Ocurrió un error al obtener los usuario por rol.', e)


def get_all_users_by_supervisor(supervisorId):
    logger.debug('Entro a user_service#get_all_users_by_supervisor')
    try:
        users = Users.query\
            .join(UserRole)\
            .join(Role, UserRole.role_id == Role.role_id)\
            .filter(Role.short_description == 'aj_analista')\
            .filter(UserRole.supervisor_user_id == supervisorId)\
            .order_by(Users.surnames).all()

        return marshal(users, UserDto.user2), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_all_users_by_supervisor - " +
                                   "Error al obtener todos los usuarios por rol por supervisor",
                                   'Ocurrió un error al obtener los usuario por rol por supervisor.', e)


def get_user_by_id(userId):
    logger.debug('Entro a user_service#get_user_by_id')
    try:
        users = Users.query\
            .join(UserRole)\
            .join(Role, UserRole.role_id == Role.role_id)\
            .filter(Users.id == userId)\
            .order_by(Users.surnames).first()

        return marshal(users, UserDto.user2), 200
    except Exception as e:
        return manage_db_exception("Error en user_service#get_user_by_id - " +
                                   "Error al obtener el usuario por id",
                                   'Ocurrió un error al obtener el usuario por id.', e)


def get_roles_by_user(user_id):
    user = Users.query.get(user_id)
    if user:
        roles = marshal(user.roles, UserRoleDto.user_role)
        return roles
    else:
        return None


def get_all_roles():
    roles = Role.query.filter_by(state='A').order_by(Role.long_description.asc()).all()
    if roles:
        role_list = marshal(roles, RoleDto.role)
        return role_list
    else:
        return None


def validate_user_active(user_id):
    try:
        user = Users.query.get(user_id)
        print(user)
        if user:
            if user.state == 'A':
                return True
        return False
    except Exception as e:
        print(e)

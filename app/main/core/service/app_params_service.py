from app.main import db
from flask_restplus import marshal
from app.main.core.service.general_service import manage_db_exception
from app.main.core.model.app_params import AppParams


def get_app_params(variable):
    print('Entro a getParams')
    try:
        app_params = AppParams.query.filter(AppParams.variable == variable, AppParams.state == True).first()
        print(app_params.value)
        if not app_params:
            response_object = {
                'status': 'fail',
                'message': 'La variable no existe o no esta activa'
            }
            return response_object, 200
        return app_params, 200

    except Exception as e:
        return manage_db_exception("Error en workflow_service#get_citation - " +
                                    "Error el consultar paremtros",
                                    'Ocurrió un error al consultar el parametro.', e)
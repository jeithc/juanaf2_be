import datetime
import hashlib
import io
import json
import os
import webbrowser
import requests
from app.main import db
from sqlalchemy.sql import text
from app.main.model.claimer_documents import Claimer_documents, Publication_claimer
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User, InitialPassword
from app.main.citation.model.aa import Aa
from app.main.util.mail import send_mail_report
from app.main.mail.service.mail_service import build_message_user
from app.main.config import ruta_archivos, ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.service.logging_service import loggingBDError
from app.main.messaging.model.document_type import Document_type
from app.main.service.s3_service import upload_file, read_content_file
from app.main.core.service.massive_publication_helper import add_massive_publication, add_massive_publication_claimer, complete_massive_publication
from app.main.messaging.service.claimer_message_service import save_new_message
from app.main.service.logging_service import configLoggingError
from app.main.notification.model.claimer_notification import ClaimerNotification


class ReportJasperMasivo:
    def create_report_massive(report_request, subgroup=None, document_id=None, publication = None):
        body_mail = '<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'
        auth = (usuario_jasper, contrasena_jasper)

        docs_repetibles = [3, 5, 6, 7, 8, 9, 10, 21, 26, 33, 34, 35, 36,75]

        # Init session so we have no need to auth again and again:
        s = requests.Session()

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]}
            ]
        }
        }"""

        data_masiv = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_subgroup_id","value": [""]}
            ]
        }
        }"""

        data2 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_acto_id","value": [""]},
                {"name": "param_acto_descrip","value": [""]}
                
            ]
        }
        }"""

        dataNotify = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_publication_id","value": [""]}
            ]
        }
        }"""
       

        typeReport = report_request
        user = User.query.filter_by(claimer_id=(-3)).first()
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            if document_id:
                documentoEx = Claimer_documents.query.filter_by(
                    document_id=document_id).first()
                document = documentoEx
            else:
                documentoEx = Claimer_documents.query.filter_by(
                    claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()
                if not documentoEx or (tr in docs_repetibles):
                    document = Claimer_documents(
                        claimer_id=user.claimer_id,
                        document_type_id=int(tr),
                        flow_document="2",
                        origin_document=origin_document,
                        document_state=True
                    )
                ReportJasperMasivo.save_changes(document)

            if typeReport == 3:
                id_workflow = 9
                current_state = 12
                # CITACION MASIVA POR PUBLICACION' saligohe
                publication_type = 'CITACION MASIVA POR PUBLICACION'
                claimer_message_type = 'CONSTANCIA'  # CITATION'
                # data_add={
                # 'generator_document_id':document.document_id,
                # 'publication_state': True,
                # 'publication_type': 'CITACION MASIVA POR PUBLICACION'#CITACION MASIVA POR PUBLICACION PREDEFINIDA'
                # }
                jsonData = json.loads(data_masiv)
                # massive= add_massive_publication(data_add)
            elif typeReport == 9:
                id_workflow = 17
                current_state = 19
                publication_type = 'NOTIFICACION POR PUBLICACION'
                claimer_message_type = 'NOTIFICATION'
                data_add = {
                    'generator_document_id': document.document_id,
                    'publication_type': publication_type,
                    'publication_state': True
                }
                jsonData = json.loads(data2)
                massive = complete_massive_publication(data_add, publication)
            elif typeReport == 75:
                publication_type = 'NOTIFICACION POR PUBLICACION'
                claimer_message_type = 'NOTIFICATION'
                data_add = {
                    'generator_document_id': document.document_id,
                    'publication_type': publication_type,
                    'publication_state': True
                }
                jsonData = json.loads(dataNotify)
                massive = complete_massive_publication(data_add, publication)
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    str(publication)]

            url = ruta_jasper_server+"reportExecutions"
            # list_user = User.query.join(ClaimerWorkflow, ClaimerWorkflow.claimer_id==User.claimer_id)\
            #     .filter(ClaimerWorkflow.current_state==id_workflow,ClaimerWorkflow.end_date == None).all()

            # for user_list in list_user:
            # add_massive_publication_claimer(user_list.claimer_id, massive)
            # guardamos el mensaje
            # data= {
            #     'claimerId':user_list.claimer_id,
            #     'aaId': 1,
            #     'personExpedientId':user_list.person_expedient_id,
            #     'claimerMessageMode':'PUBLICACION',
            #     'claimerMessageType':claimer_message_type,
            #     'sendingDate':datetime.datetime.now()
            # }
            # save_new_message(data)

            # agrego un nuevo registro en workflow  de cada usuario
            # add_workflow = ClaimerWorkflow(
            #     claimer_id=user_list.claimer_id,
            #     current_state=current_state,
            #     user_id=1,
            #     start_date=datetime.datetime.now()
            # )
            # ReportJasperMasivo.save_changes(add_workflow)

            # se consulta el nombre del archivo en la tabla de documentos
            document_jasper = Document_type.query.filter_by(
                id=typeReport).first()
            jsonData['reportUnitUri'] = "/reports/{}".format(
                document_jasper.document_route)
            jsonData['outputFormat'] = "pdf"
            if typeReport == 9:
                act = Aa.query.filter(Aa.aa_id == 1).first()
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    str(act.aa_number)]
                jsonData['parameters']['reportParameter'][2]['value'] = [
                    act.aa_id]
                jsonData['parameters']['reportParameter'][3]['value'] = [
                    act.description]
            elif typeReport != 75 :
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    subgroup]
                # jsonData['parameters']['reportParameter'][0]['value'] = [str(document.radicate_number)]
                # jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]
            # user.registered_claimer = True
            # ReportJasperMasivo.save_changes(user)

            headers = {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
            response = s.post(url, data=json.dumps(
                jsonData), headers=headers, auth=auth)
            dataResponse = response.json()
            sessionid = s.cookies.get('JSESSIONID')
            request_id = ""
            export_id = ""
            number_of_pages = 0
            fileName = ""

            try:
                request_id = dataResponse.get("requestId")
                export_id = dataResponse.get("exports")[0].get("id")
                number_of_pages = dataResponse.get("totalPages")
                fileName = dataResponse.get("exports")[0].get(
                    "outputResource").get("fileName")
            except Exception as e:
                print(e)
                print("An exception occurred invoke report jasper")

            now = datetime.datetime.now()

            if typeReport == "5":
                user.authorize_electronic_notificati = True
                user.electronic_authorization_date = now

            report_url = ruta_jasper_server + \
                "reportExecutions/{request_id}/exports/{export_id}/outputResource".format(
                    request_id=request_id, export_id=export_id)
            url_document = report_url+"?"+sessionid
            document.metadata_server_document = json.dumps(dataResponse)
            document.number_of_pages = number_of_pages
            urlData = {"url": url_document}

            report = ReportJasperMasivo.view_report(urlData)

            base_dir = ruta_archivos
            store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(
                claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date=now.strftime('%Y%m%d'))
            pathDocument = base_dir+store_path

            if not os.path.exists(pathDocument):
                os.makedirs(pathDocument)

            ReportJasperMasivo.save_changes(document)

            name = "{documentId}-".format(
                documentId=document.document_id)+fileName
            document.url_document = store_path+name

            with open(pathDocument+name, "wb") as code:
                try:
                    code.write(report)
                    document.document_size = os.path.getsize(pathDocument+name)
                    document.document_checksum = hashlib.md5(
                        open(pathDocument+name, 'rb').read()).hexdigest()
                    upload_file(store_path, name, pathDocument+name)
                except:
                    print("An exception occurred write report to disk")

            document.url_document = store_path+name
            ReportJasperMasivo.save_changes(document)

            # update de anterior estado de workflow
            # t = text("update phase_2.claimer_workflow set end_date = :date where current_state = :state and end_date is null")
            # db.engine.execute(t, date=datetime.datetime.now(), state= id_workflow)
            # db.session.close()
            return store_path+name

    def create_report(report_request, claimer_id, email=None, subgroup_id=None, aa_id = None):
        auth = (usuario_jasper, contrasena_jasper)
        # Init session so we have no need to auth again and again:
        s = requests.Session()

        docs_repetibles = [3, 5, 6, 7, 8, 9, 10, 21, 26, 33, 34, 35, 36]

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_subgroup_id","value": [""]}
            ]
        }
        }"""

        data2 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_acto_id","value": [""]},
                {"name": "param_acto_descrip","value": [""]}
                
            ]
        }
        }"""

        data3 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_claimer_email","value": [""]}
            ]
        }
        }"""

        data4 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_aa_id","value": [""]}
            ]
        }
        }"""
        #typeReport = "5"
        typeReport = report_request

        user = User.query.filter_by(claimer_id=claimer_id).first()
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            documentoEx = Claimer_documents.query.filter_by(
                claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()
            if not documentoEx or (tr in docs_repetibles):
                state = True
                if typeReport == 30:
                    state = False

                document = Claimer_documents(
                    claimer_id=user.claimer_id,
                    document_type_id=int(tr),
                    flow_document="1",
                    origin_document=origin_document,
                    document_state=state
                )
                ReportJasperMasivo.save_changes(document)
                document_radicate = document.radicate_number
                if document_radicate == None:
                    document_radicate = 'xxxxxxxxxxxx'

                if typeReport == 6 or typeReport == 7:
                    jsonData = json.loads(data2)
                elif typeReport == 13:
                    # si es acta de notificacion
                    jsonData = json.loads(data3)
                elif typeReport == 64:
                    # si es notificación
                    jsonData = json.loads(data4)
                else:
                    jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                # se consulta el nombre del archivo en la tabla de documentos
                document_jasper = Document_type.query.filter_by(
                    id=typeReport).first()
                jsonData['reportUnitUri'] = "/reports/{}".format(
                    document_jasper.document_route)

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][1]['value'] = [
                    user.claimer_id]
                if typeReport == 6 or typeReport == 7:
                    act = Aa.query.filter(Aa.aa_id == 1).first()
                    jsonData['parameters']['reportParameter'][0]['value'] = [
                        str(act.aa_number)]
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        act.aa_id]
                    jsonData['parameters']['reportParameter'][3]['value'] = [
                        act.description]
                elif typeReport == 13:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        email]
                elif typeReport == 8:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 23:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 24:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 27:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 64:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        aa_id]
                    notify = ClaimerNotification.query.filter_by(aa_id=aa_id, claimer_id=user.claimer_id).first()
                    if notify:
                        notify.notification_document_id=document.document_id
                        ReportJasperMasivo.save_changes(notify)
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    str(document.radicate_number)]

                ReportJasperMasivo.save_changes(user)

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }
                response = s.post(url, data=json.dumps(
                    jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get(
                        "outputResource").get("fileName")
                except Exception as e:
                    print(e)
                    print("An exception occurred invoke report jasper")
                    newLogger = configLoggingError(
                        loggerName='report notification massive service- create report')
                    newLogger.error("error:{}, json: {}".format(e, jsonData))
                now = datetime.datetime.now()
                report_url = ruta_jasper_server + \
                    "reportExecutions/{request_id}/exports/{export_id}/outputResource".format(
                        request_id=request_id, export_id=export_id)
                url_document = report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData = {"url": url_document}

                report = ReportJasperMasivo.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(
                    claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date=now.strftime('%Y%m%d'))
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperMasivo.save_changes(document)

                name = "{documentId}-".format(
                    documentId=document.document_id)+fileName
                document.url_document = store_path+name

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(
                            pathDocument+name)
                        document.document_checksum = hashlib.md5(
                            open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except:
                        print("An exception occurred write report to disk")

                document.url_document = store_path+name

                ReportJasperMasivo.save_changes(document)

                return {'route': store_path+name, 'document': pathDocument, 'name': name, 'document_id': document.document_id}
            else:
                return {'route': "Ya registró un documento anteriormente"}

    def create_response(report_request, expedient):
        auth = (usuario_jasper, contrasena_jasper)
        # Init session so we have no need to auth again and again:
        s = requests.Session()

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [               
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_acto_id","value": [""]}
            ]
        }
        }"""
        typeReport = report_request
        user = User.query.filter_by(claimer_id=expedient['claimerId']).first()
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            act = Aa.query.filter(Aa.aa_id==expedient['aaId']).first()
            if act:
                # document = Claimer_documents.query.filter(Claimer_documents.document_id==act.aa_document_id).first()
                document = Claimer_documents(
                    claimer_id=expedient['claimerId'],
                    document_type_id=int(tr),
                    flow_document="1",
                    origin_document=origin_document,
                    document_state=False
                )
                ReportJasperMasivo.save_changes(document)
                #se guarda el radicado en el actas
                act.aa_number=document.radicate_number
                act.aa_document_id=document.document_id
                ReportJasperMasivo.save_changes(act)

                document_radicate = document.radicate_number
                if document_radicate == None:
                    document_radicate = 'xxxxxxxxxxxx'

                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                # se consulta el nombre del archivo en la tabla de documentos
                document_jasper = Document_type.query.filter_by(
                    id=typeReport).first()
                jsonData['reportUnitUri'] = "/reports/{}".format(
                    document_jasper.document_route)

                jsonData['outputFormat'] = "docx"
                # jsonData['parameters']['reportParameter'][0]['value'] = [
                # str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    expedient['claimerId']]
                jsonData['parameters']['reportParameter'][1]['value'] = [
                    expedient['aaId']]

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }
                print(json.dumps(jsonData))
                response = s.post(url, data=json.dumps(
                    jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get(
                        "outputResource").get("fileName")
                except Exception as e:
                    print(e)
                    print("An exception occurred invoke report jasper")
                    newLogger = configLoggingError(
                        loggerName='report notification massive service- create report')
                    newLogger.error("error:{}, json: {}".format(e, jsonData))
                now = datetime.datetime.now()
                report_url = ruta_jasper_server + \
                    "reportExecutions/{request_id}/exports/{export_id}/outputResource".format(
                        request_id=request_id, export_id=export_id)
                url_document = report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData = {"url": url_document}

                report = ReportJasperMasivo.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(
                    claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date=now.strftime('%Y%m%d'))
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperMasivo.save_changes(document)

                name = "{documentId}-".format(
                    documentId=document.document_id)+fileName
                document.url_document = store_path+name

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(pathDocument+name)
                        document.document_checksum = hashlib.md5(
                            open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except:
                        print("An exception occurred write report to disk")

                document.url_document = store_path+name

                ReportJasperMasivo.save_changes(document)

                return {'route': store_path+name, 'document': pathDocument, 'name': name, 'document_id': document.document_id}
            else:
                return {'error'}
        return {'error'}

    def update_report(report_request, user, email=None):
        auth = (usuario_jasper, contrasena_jasper)
        # Init session so we have no need to auth again and again:
        s = requests.Session()

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_claimer_email","value": [""]}
            ]
        }
        }"""
        #typeReport = "5"
        typeReport = report_request

        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            documentoEx = Claimer_documents.query.filter_by(
                claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()
            if documentoEx:
                document = documentoEx

                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                # se consulta el nombre del archivo en la tabla de documentos
                document_jasper = Document_type.query.filter_by(
                    id=typeReport).first()
                jsonData['reportUnitUri'] = "/reports/{}".format(
                    document_jasper.document_route)

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [
                    user.claimer_id]
                jsonData['parameters']['reportParameter'][2]['value'] = [email]

                ReportJasperMasivo.save_changes(user)

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }
                response = s.post(url, data=json.dumps(
                    jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get(
                        "outputResource").get("fileName")
                except Exception as e:
                    print(e)
                    print("An exception occurred invoke report jasper")
                    newLogger = configLoggingError(
                        loggerName='report notification massive service- create report')
                    newLogger.error("error:{}, json: {}".format(e, jsonData))
                    return {'route': 'error jasper'}
                now = datetime.datetime.now()
                report_url = ruta_jasper_server + \
                    "reportExecutions/{request_id}/exports/{export_id}/outputResource".format(
                        request_id=request_id, export_id=export_id)
                url_document = report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData = {"url": url_document}

                report = ReportJasperMasivo.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(
                    claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date=now.strftime('%Y%m%d'))
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperMasivo.save_changes(document)

                name = "{documentId}-".format(
                    documentId=document.document_id)+fileName
                document.url_document = store_path+name

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(
                            pathDocument+name)
                        document.document_checksum = hashlib.md5(
                            open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except:
                        print("An exception occurred write report to disk")

                document.url_document = store_path+name

                ReportJasperMasivo.save_changes(document)

                return {'route': store_path+name, 'document': pathDocument, 'name': name, 'document_id': document.document_id}
            else:
                return {'route': "Ya registró un documento anteriormente"}

    def create_report_or_id(report_request, claimer_id):
        response = ReportJasperMasivo.create_report(report_request, claimer_id)
        if response['route'] == 'Ya registró un documento anteriormente':
            document = Claimer_documents.query.filter_by(
                claimer_id=claimer_id, document_type_id=report_request).first()
            return {'document_id': document.document_id}
        else:
            return response

    def activate_report_massive():
        t = text("update phase_2.claimer_documents set document_state = :state where document_type_id = :type1 OR document_type_id = :type2")
        db.engine.execute(t,  state=True, type1=6, type2=30)
        db.session.close()
        return "se ha activado el reporte masivo en la plataforma"

    def view_report(report_download):
        auth = ('reporteador', 'Master01*')
        s1 = requests.Session()
        url = report_download.get('url').split("?")
        s1.cookies['JSESSIONID'] = url[1]
        headersS = {"Accept": "application/json",
                    "Content-Type": "application/xml"}
        ret = s1.get(url[0], headers=headersS, auth=auth)
        return ret.content

    def view_local_report(report_download):
        path = ruta_archivos+report_download
        bytesReport = open(path, 'rb').read()
        return bytesReport

    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(
                loggerName='report_notification_massive_service')
            db.session.rollback()
            newLogger.error(e)

    def calculate_checksum(ret, id):
        hash = ""
        name = "p"+str(id)+".pdf"
        with open(name, "wb") as code:
            code.write(ret)
            hash = hashlib.md5(open(name, 'rb').read()).hexdigest()
        os.remove(name)
        return hash

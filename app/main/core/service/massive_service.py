from app.main import db
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.notification.model.claimer_notification import ClaimerNotification
import datetime
from app.main.model.claimer_documents import Claimer_documents
from sqlalchemy.sql import text 
from app.main.core.model.claimer_workflow import ClaimerWorkflow 
from app.main.service.logging_service import loggingBDError

def claimer_notification_massive(id_workflow):
    t = text("SELECT DISTINCT CUD.claimer_ID FROM phase_2.expedient_person_search PS \
        INNER JOIN  phase_2.claimer_user_data CUD ON  PS.expedient_id = CUD.expedient_id\
        INNER JOIN phase_2.claimer_workflow CW ON CW.claimer_id = CUD.claimer_ID\
        WHERE    CW.current_state = :id AND CW.end_date IS NULL")
    list_user=db.engine.execute(t, id =id_workflow).fetchall()
    db.session.close()

    document = Claimer_documents.query.with_entities(Claimer_documents.document_id).filter_by(document_type_id = 6 , claimer_id = -3).first()

    total = 0
    #update de anterior estado de workflow
    t = text("update phase_2.claimer_workflow set end_date = :date where current_state = :state and end_date is null")
    db.engine.execute(t, date=datetime.datetime.now(), state= id_workflow)
    db.session.close()
    
    for user_list in list_user:
        notification = ClaimerNotification(
            claimer_id = user_list.claimer_id,
            notification_type = 'AVISO',
            has_attorney = False,
            notification_date = datetime.datetime.now(),
            notification_document_id = document[0],
            claimer_notified = True
        )
        result = save_changes(notification)
        total = total + result
        #agrego un nuevo registro en workflow  de cada usuario
        add_workflow = ClaimerWorkflow(
            claimer_id=user_list.claimer_id,
            current_state=20,
            user_id=1,
            start_date=datetime.datetime.now()
        )
        save_changes(add_workflow)

    if total == 0: 
        return 'ok'

def save_changes(data):
    db.session.add(data)
    try:
       db.session.commit()
       result = 0
    except Exception as e:
        newLogger = loggingBDError(loggerName='massive service')
        db.session.rollback()
        newLogger.error(e)
        result = 1
    finally:
        return result

from flask_restplus import marshal

from app.main import logger
from app.main.core.model.serie_subserie_attributes import SerieSubserieAttributes
from app.main.core.service.general_service import manage_db_exception
from app.main.core.util.dto.serie_subserie_attributes_dto import SerieSubserieAttributesDto


def get_all_serie_subserie_attributes():
    logger.debug('Ingresa en get_all_serie_subserie_attributes')

    try:
        serie_subserie_attributes_group = SerieSubserieAttributes.query.all()

        return marshal(serie_subserie_attributes_group, SerieSubserieAttributesDto.serie_subserie_attributes), 200
    except Exception as e:
        return manage_db_exception("Error en serie_subserie_attributes_service#get_all_serie_subserie_attributes - " +
                                   "Error al obtener los registros de serie subserie attributes",
                                   'Ocurrió un error al obtener las series subseries attributes.', e)
import traceback

from app.main import logger
from app.main import db


def manage_db_exception(message, message_response, exception):
    db.session.rollback()
    db.session.close()
    logger.error(message)
    logger.error(str(exception))
    logger.error(traceback.format_exc())
    response_object = {
        'status': 'fail',
        'message': message_response,
    }
    return response_object, 500


def print_log_exception(message, message_response, exception):
    logger.error(message)
    logger.error(str(exception))
    logger.error(traceback.format_exc())
    response_object = {
        'status': 'fail',
        'message': message_response,
    }
    return response_object, 500
from flask_restplus import marshal

from app.main import db
from app.main import logger
from app.main.core.util.dto.role_dto import RoleDto
from app.main.core.model.role import Role
from app.main.core.service.general_service import manage_db_exception


def save_new_role(data):
    logger.debug('Entro a role_service#save_new_role')

    try:
        role = Role.query.filter_by(short_description=data['shortDescription']).first()
    except Exception as e:
        return manage_db_exception("Error en role_service#save_new_role - " +
                                   "Error al consultar el role",
                                   'Ocurrió un error al consultar el role.', e)
    if not role:
        new_role = Role(
            short_description=data['shortDescription'],
            long_description=data['longDescription'],
            state=data['state'],
            start_date=data['startDate'],
            end_date=data['endDate']
        )

        try:
            db.session.add(new_role)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'Role almacenado exitosamente.'
            }
            return response_object, 201
        except Exception as e:
            return manage_db_exception("Error en role_service#save_new_role - Error al guardar el role",
                                       'Ocurrió un error al guardar el role.', e)

    else:
        response_object = {
            'status': 'fail',
            'message': 'El role ya fue registrado previamente. Por favor ingrese uno diferente.'
        }
        return response_object, 200


def update_role(data):
    logger.debug('Entro a role_service#update_role')

    try:
        role = Role.query.filter_by(short_description=data['shortDescription']).first()
    except Exception as e:
        return manage_db_exception("Error en role_service#update_role - " +
                                   "Error al consultar el role",
                                   'Ocurrió un error al consultar el role.', e)
    if not role:
        response_object = {
            'status': 'fail',
            'message': 'El role no existe. Por favor verifique el nombre.'
        }
        return response_object, 200

    if 'longDescription' in data and data['longDescription']:
        role.long_description = data['longDescription']
    if 'state' in data and data['state']:
        role.state = data['state']
    if 'startDate' in data and data['startDate']:
        role.start_date = data['startDate']
    if 'endDate' in data and data['endDate']:
        role.end_date = data['endDate']

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Role actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en role_service#update_role - " +
                                   "Error al actualizar el role",
                                   'Ocurrió un error al actualizar el role.', e)


def delete_role(data):
    logger.debug('Entro a role_service#delete_role')

    try:
        role = Role.query.filter_by(role_id=data['roleId']).first()
        if role:
            db.session.delete(role)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'Role eliminado exitosamente.'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'success',
                'message': 'El role no existe, por favor verifique el id.'
            }
            return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en role_service#delete_role - " +
                                   "Error al eliminar el role",
                                   'Ocurrió un error al eliminar el role.', e)


def get_all_roles():
    logger.debug('Entro a role_service#get_all_roles')
    try:
        roles = Role.query.all()

        return marshal(roles, RoleDto.role), 200
    except Exception as e:
        return manage_db_exception("Error en role_service#get_all_roles - " +
                                   "Error al obtener los roles",
                                   'Ocurrió un error al obtener los roles.', e)


def get_role_by_name(short_description):
    logger.debug('Entro a role_service#get_role_by_name')
    try:
        role = Role.query.filter_by(short_description=short_description).order_by(Role.role_id).all()
        return marshal(role, RoleDto.role), 200
    except Exception as e:
        return manage_db_exception("Error en role_service#get_role_by_name - " +
                                   "Error al obtener role por nombre",
                                   'Ocurrió un error al obtener el role por nombre.', e)

from flask_restplus import marshal

from app.main import logger
from app.main.core.model.serie_subserie import SerieSubserie
from app.main.core.service.general_service import manage_db_exception
from app.main.core.util.dto.serie_subserie_dto import SerieSubserieDto


def get_all_serie_subserie():
    logger.debug('Ingresa en get_all_serie_subserie')

    try:
        serie_subserie_group = SerieSubserie.query.all()

        return marshal(serie_subserie_group, SerieSubserieDto.serie_subserie), 200
    except Exception as e:
        return manage_db_exception("Error en serie_subserie_service#get_all_serie_subserie - " +
                                   "Error al obtener los registros de serie subserie",
                                   'Ocurrió un error al obtener las series subseries.', e)
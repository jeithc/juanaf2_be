from app.main import db
from app.main.core.model.blacklist import BlacklistToken
from app.main.core.service.general_service import manage_db_exception


def save_token(token):
    blacklist_token = BlacklistToken(token=token)
    try:
        # insert the token
        db.session.add(blacklist_token)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Salió del sistema exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en blacklist_service#save_token - Error al guardar el token",
                            'Ocurrió un error al guardar el token.', e)

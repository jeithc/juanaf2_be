from flask_restplus import marshal

from app.main import logger
from app.main.core.model.serie_subserie_metadata import SerieSubserieMetadata
from app.main.core.service.general_service import manage_db_exception
from app.main.core.util.dto.serie_subserie_metadata_dto import SerieSubserieMetadataDto


def get_all_serie_subserie_metadata():
    logger.debug('Ingresa en get_all_serie_subserie_metadata')

    try:
        serie_subserie_metadata_group = SerieSubserieMetadata.query.all()
        return marshal(serie_subserie_metadata_group, SerieSubserieMetadataDto.serie_subserie_metadata), 200
    except Exception as e:
        return manage_db_exception("Error en serie_subserie_metadata_service#get_all_serie_subserie_metadata - " +
                                   "Error al obtener los registros de serie subserie metadata",
                                   'Ocurrió un error al obtener las series subseries metadata.', e)


def get_serie_subserie_metadata_by_serie_subserie(serie, subserie):
    logger.debug('Ingresa en get_serie_subserie_metadata_by_serie_subserie')

    try:
        serie_subserie_metadata_group = SerieSubserieMetadata.query.filter_by(
            serie=serie, subserie=subserie).all()
        return marshal(serie_subserie_metadata_group, SerieSubserieMetadataDto.serie_subserie_metadata), 200
    except Exception as e:
        return manage_db_exception("Error en serie_subserie_metadata_service#get_serie_subserie_metadata_by_serie_subserie - " +
                                   "Error al obtener los registros de serie subserie metadata",
                                   'Ocurrió un error al obtener las series subseries metadata.', e)


def get_serie_subserie_metadata_by_ss_id(ssId):
    logger.debug('Ingresa en get_serie_subserie_metadata_by_ss_id')

    try:
        serie_subserie_metadata_group = SerieSubserieMetadata.query.filter_by(
            ss_id=ssId).all()
        return marshal(serie_subserie_metadata_group, SerieSubserieMetadataDto.serie_subserie_metadata), 200
    except Exception as e:
        return manage_db_exception("Error en serie_subserie_metadata_service#get_serie_subserie_metadata_by_serie_subserie - " +
                                   "Error al obtener los registros de serie subserie metadata",
                                   'Ocurrió un error al obtener las series subseries metadata.', e)

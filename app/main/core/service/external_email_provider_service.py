from flask import Flask
from flask_mail import Mail, Message
from threading import Thread
from app.main.service.logging_service import configLoggingError
from app.main.service.s3_service import download_bin_file, download_file
import datetime

app =Flask(__name__)

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'juanafase2@gmail.com'
app.config['MAIL_PASSWORD'] = 'Master01*'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_ASCII_ATTACHMENTS'] = False

# app.config['MAIL_SERVER']='smtp.office365.com'
# app.config['MAIL_PORT'] = 587 
# app.config['MAIL_USERNAME'] = 'notificaciones@defensoria.gov.co'
# app.config['MAIL_PASSWORD'] = 'Siaf.2016'
# app.config['MAIL_USE_TLS'] = True
# app.config['MAIL_USE_SSL'] = False
# app.config['MAIL_ASCII_ATTACHMENTS'] = False
#app.config['DEBUG'] = True

mail = Mail(app)
mail.init_app(app)

def send_mail(datamail,password):
    msg = Message(u"Doña Juana le responde - cambio de clave", sender = 'notificaciones_juana@defensoria.gov.co', recipients = [datamail])
    msg.html = "<p>Su clave ha sido modificada en el sistema Do&ntilde;a Juana le responde </p> <p>Su nueva contrase&ntilde;a es: </p>  <p> CONTRASE&Ntilde;A: {} </p>".format(password)
    thr = Thread(target=send_async_email, args=[app, msg])    
    thr.start()
    return thr

def send_mail_report(subject, message, email, attatchments):
    """Sends """
    msg = Message(subject, sender = 'notificaciones_juana@defensoria.gov.co', recipients = [email])
    msg.html = message.encode("utf-8")
    for filename, path, content_type in attatchments:
        with app.open_resource(path) as fp:
            msg.attach(filename, content_type, fp.read())
    thr = Thread(target=send_async_email, args=[app, msg])    
    thr.start()
    return thr

def send_async_email(app, msg):
    with app.app_context():
        newLogger = configLoggingError(loggerName='authLogger')
        try:
            mail.send(msg)
        except Exception as e:
            newLogger.error( "hour {}  []  error: {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") , str(e) ) )

def send_html_mail(sender, destionation, subject, body, body_params):
    message_body = body.format(*body_params)
    msg = Message(subject, sender = sender, recipients = [destionation])
    msg.html = message_body
    send_async_email(app, msg)

def send_attachment_mail(sender, recipient, subject, html_body, attatchments):
    msg = Message(subject, sender = sender, recipients = [recipient])
    msg.html = html_body.encode("utf-8")
    for attachment in attatchments:
        msg.attach(filename=attachment['filename'], content_type="application/pdf", data=attachment['content'])
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_bitbucket_mail(sender, recipient, subject, html_body, attatchments):
    """Sends bitbucket pdf files"""
    for attachment in attatchments:
        attachment['content'] = download_file( attachment['remote_path'], attachment['filename'])
    #WARN: TAB REQUIRED
    send_attachment_mail(sender, recipient, subject, html_body, attatchments)

import os
import datetime
from datetime import datetime as dtime
import requests
import uuid
from werkzeug.utils import secure_filename
from app.main.core.service.general_service import print_log_exception
from app.main.service.s3_service import upload_file, read_content_file
from app.main.config import temp_server_path
from app.main.service import climer_documents_service
from app.main.model.claimer_documents import Claimer_documents
from app.main.service.logging_service import configLoggingError
from app.main import db
from app.main.config import ruta_archivos
from app.main.service.jasper_serv import ReportJasper
import PyPDF2
import hashlib
from app.main import logger
import copy
from PyPDF2 import PdfFileReader


separator = '/'

def save_new_file(input_file, path, document_id):
    logger.debug('Entro a file_service#save_new_file')

    try:

        base_path = temp_server_path
        local_path = base_path + separator + path

        if not os.path.exists(local_path):
            os.makedirs(local_path)

        filename = secure_filename(input_file.filename)

        if len(input_file.filename) > 240:
            response_object = {
                'status': 'fail',
                'message': 'El nombre del archivo es demasiado largo, por favor verifique.'
            }
            return response_object, 200

        input_file.save(local_path + filename)

        if 'pdf' in input_file.content_type:
            with open(local_path + filename, "rb") as f:
                try:
                    pdf = PyPDF2.PdfFileReader(f)
                    n_pages = pdf.numPages
                except Exception as e:
                    logger.error(
                        'Error al calcular el numero de paginas del pdf')
                    n_pages = 1
        else:
            n_pages = 1

        url_document = upload_file(path, filename, local_path + filename)

        if document_id:
            document = climer_documents_service.document_by_id(document_id)[0]
            document.url_document = url_document
            document.number_of_pages = n_pages
            document.document_size = input_file.file_size
            document.document_checksum = hashlib.md5(
                input_file.read()).hexdigest()
            document.update(db.session)

        response_object = {
            'status': 'success',
            'message': 'Archivo almacenado exitosamente.',
            'url_document': url_document
        }
        return response_object, 201
    except Exception as e:
        return print_log_exception("Error en file_service#save_new_file - " +
                                   "Error al guardar el archivo",
                                   'Ocurrió un error al guardar el archivo.', e)


def save_report_file(report, document_id):
    logger.debug('Entro a file_service#save_new_file')

    try:
        if document_id:
            document = climer_documents_service.document_by_id(document_id)[0]

        input_file = report['report']
        path = generate_path(document)
        base_path = temp_server_path
        local_path = base_path + separator + path

        if not os.path.exists(local_path):
            os.makedirs(local_path)

        filename = '{}-{}'.format(document_id,
                                  secure_filename(report['file_name']))

        with open(local_path + filename, "wb") as code:
            try:
                code.write(input_file)
                document.document_size = os.path.getsize(local_path + filename)
                document.document_checksum = hashlib.md5(
                    open(local_path + filename, 'rb').read()).hexdigest()
                url_document = upload_file(
                    path, filename, local_path + filename)
            except Exception as e:
                return print_log_exception("Error en file_service#save_new_file - " +
                                           "Error al guardar el archivo",
                                           'Ocurrió un error al guardar el archivo.', e)

        document.url_document = url_document
        document.number_of_pages = report['number_of_pages']
        document.metadata_server_document = report['metadata_server_document']
        document.document_checksum = hashlib.md5(input_file).hexdigest()
        document.update(db.session)

        response_object = {
            'status': 'success',
            'message': 'Archivo almacenado exitosamente.',
            'url_document': url_document
        }
        return response_object, 201
    except Exception as e:
        return print_log_exception("Error en file_service#save_new_file - " +
                                   "Error al guardar el archivo",
                                   'Ocurrió un error al guardar el archivo.', e)


def generate_path(document):
    now = datetime.datetime.now()
    path = "/".join([str(document.claimer_id), document.origin_document.strip(), str(document.document_type_id),
                             now.strftime('%Y%m%d'), ''])
    return path


def get_filename_parts(document):
    full_path = document.url_document
    filename_start = full_path.rfind("/")
    return full_path[:filename_start], full_path[filename_start + 1:]


def verify_document_mail(document_id):
    try:
        document = Claimer_documents.query.filter(
            Claimer_documents.document_id == document_id).one()
        path, file_name = get_filename_parts(document)
        local_path = temp_server_path+separator
        if os.path.isfile(local_path+document.url_document):
            url = document.url_document
        else:
            local_path, url = s3_documents_download(
                path, file_name)
            local_path = local_path + separator
    except Exception as error:
        local_path = 'error'
        url = 'error'
    return local_path, url


def save_file_from_url(url, document_id, claimer_message_id, name='email_send_result'):
    try:
        print(url, document_id, claimer_message_id)
        document = climer_documents_service.document_by_id(document_id)
        r = requests.get(url)
        document = document[0]
        input_file = r.content
        path = generate_path(document)
        base_path = temp_server_path
        local_path = base_path + separator + path
        if not os.path.exists(local_path):
            os.makedirs(local_path)

        # with open("{}/{}_send_result_{}.pdf".format(local_path,name_document,document_id),'wb') as f:
        #     f.write(r.content)
        #     print('document creado')

        filename = "{}_{}.pdf".format(
            name, claimer_message_id)

        try:
            with open(local_path + filename, "wb") as code:
                code.write(input_file)
                document.document_size = os.path.getsize(local_path + filename)
                document.document_checksum = hashlib.md5(
                    open(local_path + filename, 'rb').read()).hexdigest()
                print(path, filename)
                url_document = upload_file(
                    path, filename, local_path + filename)
                document.url_document = url_document
                document.document_state = True
        except Exception as e:
            print(e)
            print('error al guardar archivo')
            return print_log_exception("Error en file_service#save_new_file - " +
                                       "Error al guardar el archivo",
                                       'Ocurrió un error al guardar el archivo.', e)
        try:
            pdf = PdfFileReader(open(local_path + filename, 'rb'))
            pages = pdf.getNumPages()
        except Exception as e:
            print(e)
            pages = 1
        document.number_of_pages = pages
        document.document_checksum = hashlib.md5(input_file).hexdigest()
        document.update(db.session)
        response_object = {
            'status': 'success',
            'message': 'Archivo almacenado exitosamente.',
            'url_document': url_document,
            'local': local_path+filename,
        }
        return response_object, 201
    except Exception as e:
        newLoggererror = configLoggingError(loggerName='save_file_from_url')
        newLoggererror.error('error: {},{},{},{},{} '.format(
            url, document_id, claimer_message_id, tipo, e))
        return print_log_exception("Error en file_service#save_new_file - " +
                                   "Error al guardar el archivo",
                                   'Ocurrió un error al guardar el archivo.', e)


def save_file_binary(binary, document_id):
    document = climer_documents_service.document_by_id(document_id)
    document = document[0]
    print(document)
    input_file = binary
    path = generate_path(document)
    base_path = temp_server_path
    local_path = base_path+"/"+path
    if not os.path.exists(local_path):
        os.makedirs(local_path)
    name = document.url_document
    name = name.split('/')
    name = name[-1]
    name = name.split('.')
    name = name[-2]
    filename = "{}.pdf".format(name)
    with open(local_path + filename, "wb") as code:
        try:
            code.write(input_file)
            document.document_size = os.path.getsize(local_path + filename)
            document.document_checksum = hashlib.md5(
                open(local_path + filename, 'rb').read()).hexdigest()
            url_document = upload_file(path, filename, local_path+filename)
            document.url_document = url_document
        except Exception as e:
            print_log_exception("Error en file_service#save_new_file - " +
                                       "Error al guardar el archivo",
                                       'Ocurrió un error al guardar el archivo.', e)
            return {
                'status':'error'
            } 
    try:
        pdf = PdfFileReader(open(local_path + filename, 'rb'))
        pages = pdf.getNumPages()
        document.number_of_pages = pages
        document.document_checksum = hashlib.md5(input_file).hexdigest()
        document.update(db.session)
        response_object = {
            'status': 'success',
            'message': 'Archivo almacenado exitosamente.',
            'url_document': url_document
        }
        return response_object, 201
    except Exception as e:
        print_log_exception("Error en file_service#save_new_file - " +
            "Error al leer el archivo",
            'Ocurrió un error al leer el archivo.', e)
        return {
            'status':'error'
        } 


def save_file_temp(input_file):

    uuid_key = uuid.uuid1()
    path = 'temp/' + str(uuid_key)

    try:

        base_path = temp_server_path
        local_path = base_path + separator + path + separator

        if not os.path.exists(local_path):
            os.makedirs(local_path)

        filename = secure_filename(input_file.filename)

        if len(input_file.filename) > 240:
            response_object = {
                'status': 'fail',
                'message': 'El nombre del archivo es demasiado largo, por favor verifique.'
            }
        input_file.save(local_path + filename)
        url_document = upload_file(
            path + separator, filename, local_path + filename)

        response_object = {
            'status': 'success',
            'message': 'Archivo almacenado exitosamente.',
            'url_document': path
        }
        return response_object, 201

    except Exception as e:
        return print_log_exception("Error en file_service#save_file_temp - " +
                                   "Error al guardar el archivo",
                                   'Ocurrió un error al guardar el archivo.', e)


def save_new_file_merge(path, document_id, local_path, filename):
    logger.debug('Entro a file_service#save_new_file_merge')
    n_pages = None
    with open(local_path + filename, "rb") as f:
        try:
            pdf = PyPDF2.PdfFileReader(f)
            n_pages = pdf.numPages

        except:
            logger.error('Error al calcular el numero de paginas del pdf')
            n_pages = 1

    url_document = upload_file(path, filename, local_path + filename)

    if document_id:
        document = climer_documents_service.document_by_id(document_id)[0]
        document.url_document = url_document
        document.number_of_pages = n_pages
        document.document_size = os.path.getsize(local_path + filename)
        document.document_checksum = hashlib.md5(
            open(local_path + filename, 'rb').read()).hexdigest()
        document.update(db.session)

    response_object = {
        'status': 'success',
        'message': 'Archivo almacenado exitosamente.',
        'url_document': url_document
    }
    return response_object, 201


def complete_document_data(path, filename):
    try:
        local_path, filename = s3_documents_download(path, filename)
        document_download = "{}{}{}".format(local_path, separator, filename)
        with open(document_download, "rb") as f:
            try:
                pdf = PyPDF2.PdfFileReader(f)
                n_pages = pdf.numPages
            except:
                logger.error('Error al calcular el numero de paginas del pdf')
                n_pages = 1
        return {
            'pages': n_pages,
            'size': os.path.getsize(document_download),
            'checksum': hashlib.md5(open(document_download, 'rb').read()).hexdigest()
        }
    except Exception as e:
        newLogger = configLoggingError(
            loggerName='file service-documents_download')
        newLogger.error(e)
        print(e)
        return 'error'


def s3_documents_download(path, filename):
    try:
        data = "{}/{}".format(path,   filename)  # pathReport
        input_file = ReportJasper.view_local_report(data)
        now = dtime.now()
        path = '-3/SISTEMA/DOWNLOAD/{}'.format(now.strftime('%Y%m%d'))
        base_path = temp_server_path
        local_path = base_path + separator + path
        if not os.path.exists(local_path):
            os.makedirs(local_path)
        with open("{}{}{}".format(local_path, separator, filename), 'wb') as f:
            f.write(input_file)
        return local_path, filename
    except Exception as e:
        newLogger = configLoggingError(
            loggerName='signature service-download s3 document')
        newLogger.error(e)
        print(e)
        return 'error'

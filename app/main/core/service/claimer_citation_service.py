from app.main import db
from sqlalchemy.sql import text
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User
from sqlalchemy import func
from app.main.mail.service.mail_service import email_metadata, send_email_user
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.mail.service.build_message_helper import build_message


class Claimer_citation:
    # def claimer_citation_massive():
    #     #verifico el claimer que esta en el estado deseado
    #     claimers = User.query.join(ClaimerWorkflow, ClaimerWorkflow.claimer_id==User.claimer_id)\
    #         .filter(ClaimerWorkflow.current_state==5,ClaimerWorkflow.end_date == None).all()
    #     result={
    #         'send':0,
    #         'no_send':0
    #     }
    #     if claimers:
    #         #saco los datos de metadata para el mail
    #         email_data = email_metadata(1)
    #         if email_data:
    #             #recorro el proceso para llamar a cada claimer
    #             for claimer in claimers:
    #                 #verifico que este registrado y que no haya aceptado la autorización de notificación personal electrónica.
    #                 verify_user_status = User.query\
    #                 .filter(User.claimer_id == claimer.claimer_id, User.registered_claimer.is_(True), User.email.like('%@%'))\
    #                 .filter((User.authorize_electronic_notificati.is_(False))|(User.authorize_electronic_notificati == None )).first()
    #                 if verify_user_status:
    #                     document_created=ReportJasperMasivo.create_report(1, claimer.claimer_id)
    #                     if document_created['route'] != 'Ya registró un documento anteriormente':
    #                         print(document_created)
    #                         print(claimer.claimer_id)
    #                         data_send_mail=email_data
    #                         if email_data['attachments']:
    #                             data_add = {
    #                                 'claimer': claimer,
    #                                 'document_id': document_created['document_id']
    #                             }
    #                         else:
    #                             data_add = {
    #                                 'claimer': claimer,
    #                             }

    #                         #se editan los espacios en el body para agregar los datos reales 
    #                         data_send_mail['body'] = data_send_mail['body'].format(
    #                             claimer.full_name,
    #                             'lugar',
    #                             'dirección',
    #                             'horario',
    #                             'no acto',
    #                             'nombre acto'
    #                             )

    #                         data_send_mail.update(data_add)
    #                         #SE ENVIA EL MAIL CON LOS DATOS DE data_send_mail
    #                         send_email_user(data_send_mail, document_created['document'] , document_created['name'])
    #                         result['send'] = result['send'] +1
    #                     else:
    #                         result['no_send'] = result['no_send'] +1
    #                 else:
    #                     print('no autorizo o no esta registrado user: {}'.format(claimer.claimer_id))
    #                     result['no_send'] = result['no_send'] +1               
    #             return result
    #         else:
    #             return "error al consultar datos de mail o adjuntar documentos"
    #     else: 
    #         return 'no se encontraron usuarios para envió'


    def claimer_massive():
        #se verifica los que están en estado 5
        return build_message( 5 ,1 , 5)


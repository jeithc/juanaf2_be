from flask_restplus import marshal
from datetime import datetime

from app.main import db
from app.main import logger
from app.main.core.util.dto.claimer_workflow_dto import ClaimerWorkflowDto
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.core.service.general_service import manage_db_exception
from app.main.core.model.state_procs import StateProcs
from app.main.core.model.transition import Transition
from app.main.core.model.users import Users


def advance_process(data):
    logger.debug('Entro a workflow_service#advance_process')

    try:
        claimer_workflow = ClaimerWorkflow.query \
          .filter(ClaimerWorkflow.claimer_id == data['claimerId']) \
          .filter(ClaimerWorkflow.current_state == data['startState']) \
          .filter(ClaimerWorkflow.end_date.is_(None)) \
          .first()

        if not claimer_workflow:
            response_object = {
              'status': 'success',
              'message': 'El claimer no está en el estado indicado.'
            }
            return response_object, 200

        claimer_workflow.end_date = datetime.now()
    except Exception as e:
        return manage_db_exception("Error en advance_process#advance_process - " +
                                 "Error al consultar el flujo de trabajo del claimer",
                                 'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)

    try:
        new_aspirant_workflow = ClaimerWorkflow(
          claimer_id=data['claimerId'],
          current_state=data['endState'],
          user_id=data['destinationUserId'],
          triggered_event_user=data['triggerUser'],
          start_date=datetime.now()
        )

        db.session.add(new_aspirant_workflow)
        db.session.commit()

        response_object = {
          'status': 'success',
          'message': 'El proceso se avanzó exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en workflow_service#advance_process - " +
                                 "Error al crear el nuevo flujo de trabajo del solicitante",
                                 'Ocurrió un error al crear el nuevo flujo de trabajo del solicitante.', e)


def get_process_track(claimer_id):
    logger.debug('Entro a workflow_service#get_process_track')

    try:
        claimer_workflow = ClaimerWorkflow.query \
          .filter(ClaimerWorkflow.claimer_id == claimer_id).order_by(ClaimerWorkflow.workflow_id).all()

        if not claimer_workflow:
          response_object = {
            'status': 'fail',
            'message': 'El solicitante no tiene traza disponible.'
          }
          return response_object, 200

        return marshal(claimer_workflow, ClaimerWorkflowDto.claimer_workflow_children), 200

    except Exception as e:
        return manage_db_exception("Error en workflow_service#advance_process - " +
                                 "Error al consultar el flujo de trabajo del solicitante",
                                 'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def get_current_state(claimer_id):
    logger.debug('Entro a workflow_service#get_current_state')

    try:
        claimer_workflow = ClaimerWorkflow.query \
          .filter(ClaimerWorkflow.claimer_id == claimer_id).filter(ClaimerWorkflow.end_date == None).first()

        if not claimer_workflow:
            return {}

        return marshal(claimer_workflow, ClaimerWorkflowDto.claimer_workflow_children)

    except Exception as e:
        return manage_db_exception("Error en workflow_service#advance_process - " +
                                 "Error al consultar el flujo de trabajo del solicitante",
                                 'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def get_posible_transitions(state):
    logger.debug('Entro a workflow_service#get_posible_transitions')

    transitions = Transition.query \
      .filter(Transition.origin == state) \
      .filter(Transition.state == 'A') \
      .all()

    return transitions


def make_claimer_transition(data):
    logger.debug('Entro a workflow_service#make_claimer_transition')

    claimer_id = data['claimerId']
    next_state = data['nextState']
    next_user = data['nextUser']
    log_user = data['logUser']

    try:

        next_user_obj = Users.query.filter_by(id=next_user).first()
        if next_user_obj:
            next_user = next_user_obj.id
        else:
            next_user = 0

        log_user_obj = Users.query.filter_by(id=log_user).first()
        if log_user_obj:
            log_user = log_user_obj.id
        else:
            log_user = 0

        claimer_current_workflow = get_current_state(claimer_id)

        if not claimer_current_workflow:
            response_object = {
              'status': 'fail',
              'message': 'El claimer no está registrado en ningun estado.'
            }
            return response_object, 200

        if claimer_current_workflow['currentState'] == next_state:
            response_object = {
              'status': 'success',
              'message': 'El claimer ya esta en ese estado.'
            }
            return response_object, 200

        transitions = get_posible_transitions(claimer_current_workflow['currentState'])

        tr = [x for x in transitions if x.destination == next_state]
        logger.debug(tr)
        if tr:
            data = {
              'claimerId': claimer_id,
              'startState': claimer_current_workflow['currentState'],
              'endState': next_state,
              'destinationUserId': next_user,
              'triggerUser': log_user
            }
            return advance_process(data)
        else:
            response_object = {
              'status': 'fail',
              'message': 'La transición no es permitida.'
            }
            return response_object, 200

    except Exception as e:
        return manage_db_exception("Error en workflow_service#advance_process - " +
                                  "Error al consultar el flujo de trabajo del solicitante",
                                  'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def get_state_procs():
    logger.debug('Entro en get_state_procs')

    try:
        states = StateProcs.query.all()
        return states
    except Exception as e:
        return manage_db_exception("Error en get_state_procs" +
                                 "Error al obtener los state_procs",
                                 "Ocurrio un error al obtener state_procs", e)


def get_suspend_state(claimer_id, suspend_type):
    logger.debug('Entro a workflow_service#get_suspend_state')

    try:

        claimer_current_workflow = get_current_state(claimer_id)

        transitions = get_posible_transitions(claimer_current_workflow['currentState'])

        idx = suspend_type.find('SUSPENDIDO')
        suspend_type = suspend_type[idx:]
        for x in transitions:
          idx_tr = x.description.find('SUSPENDIDO')
          if x.description[idx_tr:] == suspend_type:
            tr = x
            break
        logger.debug(tr)
        if tr:
            return tr.destination
        else:
            return None

    except Exception as e:
        return manage_db_exception("Error en workflow_service#get_suspend_state - " +
                                    "Error al consultar el flujo de trabajo del solicitante",
                                    'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def get_activate_state(claimer_id):
    logger.debug('Entro a workflow_service#get_activate_state')

    try:

        claimer_current_workflow = get_current_state(claimer_id)

        transitions = get_posible_transitions(claimer_current_workflow['currentState'])

        tr = transitions[0]
        if tr:
            return tr.destination
        else:
            return None

    except Exception as e:
        return manage_db_exception("Error en workflow_service#get_activate_state - " +
                                    "Error al consultar el flujo de trabajo del expediente",
                                    'Ocurrió un error al consultar el flujo de trabajo del expediente.', e)


def get_last_state_user(state, claimer_id):
    logger.debug('Entro en get_last_state_user')

    try:
        claimer_workflow = ClaimerWorkflow.query.filter(
          ClaimerWorkflow.claimer_id == claimer_id).filter(
          ClaimerWorkflow.end_date != None).filter(
          ClaimerWorkflow.current_state == state).order_by(ClaimerWorkflow.workflow_id.desc()).first()

        if not claimer_workflow:
            return {}

        return marshal(claimer_workflow, ClaimerWorkflowDto.claimer_workflow_children)

    except Exception as e:
        return manage_db_exception("Error en workflow_service#get_last_state_user - " +
                                 "Error al consultar el flujo de trabajo del solicitante",
                                 'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def validate_claimer_assign(user_id, claimer_id):
    logger.debug('Entro en validate_claimer_assign')

    try:
        claimer_workflow = ClaimerWorkflow.query.filter(
          ClaimerWorkflow.claimer_id == claimer_id).filter(
          ClaimerWorkflow.end_date == None).order_by(ClaimerWorkflow.workflow_id.desc()).first()

        if not claimer_workflow:
            return {}
        if claimer_workflow.user_id == int(user_id):
            return True
        else:
            return False

    except Exception as e:
        return manage_db_exception("Error en workflow_service#validate_claimer_assign - " +
                                 "Error al consultar el flujo de trabajo del solicitante",
                                 'Ocurrió un error al validar el flujo de trabajo del solicitante.', e)


def get_last_state_in_list(state_list, claimer_id):
    logger.debug('Entro en get_last_state_in_list')

    try:
        states = []
        for state in state_list.split(","):
            states.append(int(state))
        claimer_workflow = ClaimerWorkflow.query.filter(
          ClaimerWorkflow.claimer_id == claimer_id).filter(
          ClaimerWorkflow.current_state.in_(states)).order_by(ClaimerWorkflow.start_date.desc()).first()

        if not claimer_workflow:
            return {}

        return marshal(claimer_workflow, ClaimerWorkflowDto.claimer_workflow_children)

    except Exception as e:
        return manage_db_exception("Error en workflow_service#get_last_state_user - " +
                                 "Error al consultar el flujo de trabajo del solicitante",
                                 'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)
from app.main import db
from sqlalchemy.sql import text
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User
from sqlalchemy import func
from app.main.model.claimer_documents import Claimer_documents
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.citation.model.massive_publication import MassivePublication
from app.main.model.claimer_documents import Publication_claimer
from app.main.core.service.massive_publication_helper import add_massive_publication, add_massive_publication_claimer
import datetime
import os


class Administrative_act:
    def start_act():
        try:
            #result= ReportJasperMasivo.create_report(31,-3)
            document=Claimer_documents.query.filter(Claimer_documents.document_type_id == 31).filter(Claimer_documents.claimer_id == -3).first()
            if document:
                data_massive={
                    'generator_document_id':document.document_id,
                    'publication_type': 'ACTO ADMINISTRATIVO INICIAL',
                    'publication_state': True
                }
                massive= add_massive_publication(data_massive)
                newLogger2 = configLoggingError(loggerName='test service')
                print(massive)
                newLogger2.error('massive:{}'.format(massive))
                query = 'select * from phase_2.f_setting_administrative_act({})'.format(massive)
                data=db.engine.execute(text(query).execution_options(autocommit=True)).fetchone()
                print(data,'data')
                db.session.close()
                # data= db.session.query(func.phase_2.f_setting_administrative_act(massive)).first()
                return data[0]
            else:
                return {'state':"ya se hizo el acto administrativo inicial" }
        except Exception as e:
            print(e)
            newLogger2 = configLoggingError(loggerName='test service Exception')
            newLogger2.error(e)
            return {'state':"error al correr acto" }


    def add_workflow_state(data):
        #agrego un nuevo registro en workflow  de cada usuario
        add_workflow = ClaimerWorkflow(
            claimer_id=data['claimer_id'],
            current_state=data['next_state'],
            user_id=1,
            start_date=datetime.datetime.now()
        )
        Administrative_act.save_changes(add_workflow)
        return 'ok'

    def get_document_act():
        documents = Claimer_documents.query.filter_by(claimer_id=-3, document_type_id = 40 ).first()
        if documents:
            return documents.url_document
        else:
            result= ReportJasperMasivo.create_report(40,-3)
            return result['route']

    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(loggerName='massive service')
            db.session.rollback()
            newLogger.error(e)


def delete_aa(claimer_id, aa_id):
    update_query = 'SELECT phase_2.f_delete_administrative_act_per_dp_rejected(' + str(claimer_id) + ', ' + str(aa_id) + ')'
    db.engine.execute(text(update_query).execution_options(autocommit=True))


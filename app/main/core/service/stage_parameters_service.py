from app.main.core.util.dto.stage_parameters_dto import StageParametersDto
from app.main.core.model.stage_parameters import StageParameters
from app.main.core.service.general_service import manage_db_exception
from app.main import db


def get_stage_parameters(stage_proccess, stage_param_type):

    try:
        stage_parameters = StageParameters.query.filter_by(
            stage_proccess=stage_proccess, stage_param_type=stage_param_type).all()
        return stage_parameters
    except Exception as e:
        return manage_db_exception("Error en role_service#get_stage_parameters - " +
                                   "Error al consultar el stage_parameters",
                                   'Ocurrió un error al consultar el stage_parameters.', e)

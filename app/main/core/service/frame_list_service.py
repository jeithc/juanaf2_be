import json
from app.main import db
from app.main.core.model.role import Role
from app.main.core.model.users import Users
from app.main.model.user import User
from app.main.core.model.frame_list import FrameList
from app.main.core.model.frame_column import FrameColumn
from app.main.core.service import user_service
from flask_restplus import marshal
from app.main.core.util.dto.frame_list_dto import FrameListDto
from app.main.util.dto import UserDto
from app.main.core.service.general_service import manage_db_exception
from app.main.radication.model.claimer_resource_document import ClaimerResourceDocument
from app.main.core.model.inbox_management import InboxManagement
from app.main.core.util.dto.inbox_management_dto import InboxManagementDto


def get_frames_lists_for_user(user_id):
    roles_id = []
    try:
        roles = user_service.get_roles_by_user(user_id)
        if roles:
            for r in roles:
                roles_id.append(r['roleId'])

            frame_list = FrameList.query.join(FrameList.roles).filter(Role.role_id.in_(roles_id))\
                .order_by(FrameList.order.asc())\
                .all()

            return marshal(frame_list, FrameListDto.frame_list), 200

        else:
            response_object = {
                'status': 'success',
                'message': 'El usuario no existe, por favor verifique el id'
            }
            return response_object

    except Exception as e:
        return manage_db_exception("Error en frame_list_service#get_frames_lists_for_user - " +
                                   "Error al consultar las listas",
                                   'Ocurrió un error al consultar la lista.', e)


def get_count_frame_data(frame_id, user_id):

    frame_search = FrameList.query.get(frame_id).query_exec
    claimers = (frame_search + '.count()').format(user_id)
    list_data = eval(compile(claimers, '<>', 'eval'))
    response_object = {
        'count': list_data
    }

    return response_object


def get_frame_data(frame_id, page, per_page, user_id):

    frame_search = FrameList.query.get(frame_id).query_exec
    claimers = (frame_search + '.paginate({}, {})').format(user_id, page, per_page)
    list_data = eval(compile(claimers, '<>', 'eval'))
    return marshal(list_data.items, InboxManagementDto.inbox_management)

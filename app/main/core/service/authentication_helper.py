from flask_restplus import marshal
import base64
from datetime import datetime
from datetime import timedelta
from app.main import db

from app.main.core.model.user_base import UserBase
from app.main.core.model.users import Users
from app.main.core.util.dto.user_role_dto import UserRoleDto
from app.main.model.user_audit import UserAudit
from app.main.core.model.connection import Connection
from app.main.core.service.blacklist_service import save_token
from app.main.core.service.general_service import manage_db_exception
from app.main.core.util.dto.role_dto import RoleDto
from app.main.service.logging_service import loggingBDError
from app.main.util.dto import UserDto


class Authentication:

    @staticmethod
    def login_user(data):
        try:
            is_user_system = True

            user = Users.query.filter_by(email=data.get('email')).first()

            if not user:
                response_object = {
                    'status': 'fail',
                    'message': 'El usuario no existe.'
                }
                return response_object, 200

            if user.state == 'I':
                response_object = {
                    'status': 'fail',
                    'message': 'El usuario esta desactivado.'
                }
                return response_object, 200

            decode_pass = base64.b64decode(data['password'])

            # if user and user.check_password(decode_pass):
            if user:
                authentication_token = user.encode_auth_token(user.id)
                if authentication_token:
                    if is_user_system:
                        prev_user = Connection.query.filter_by(email=data.get('email')).first()

                        last_audit = UserAudit.query.filter_by(user=data.get('email')).order_by(
                            UserAudit.id.desc()).first()
                        if last_audit:
                            last_action = last_audit.created_at
                            valid_time = datetime.now() - timedelta(seconds=300)
                            user_idle = False

                            if last_action < valid_time:
                                user_idle = True

                            if prev_user and not user_idle:
                                response_object = {
                                'status': 'fail',
                                'message': 'El usuario ya tiene una sesión iniciada.'
                                }
                                return response_object, 200
                            else:
                                if prev_user and user_idle:
                                    prev_user.ending_type = 'IDLE_USER'
                                    Authentication.save_changes(prev_user)
                                    Connection.query.filter_by(email=prev_user.email).delete()
                                    db.session.commit()

                                conn = Connection(
                                    email=data.get('email'),
                                    start_event_date=datetime.now()
                                )
                                Authentication.save_changes(conn)

                        if user.state == 'R':
                            response_object = {
                                'status': 'recover',
                                'message': 'Debe actualizar el password',
                                # 'Authorization': authentication_token.decode(),
                                'email': user.email,
                                'id': user.id,
                                'names': user.names,
                                'surnames': user.surnames,
                                'roles': marshal(user.roles, UserRoleDto.user_role)
                            }
                            return response_object, 200
                        response_object = {
                            'status': 'success',
                            'message': 'Registrado exitosamente',
                            # 'Authorization': authentication_token.decode(),
                            'email': user.email,
                            'id': user.id,
                            'names': user.names,
                            'surnames': user.surnames,
                            'roles': marshal(user.roles, UserRoleDto.user_role)
                        }
                    else:
                        response_object = {
                            'status': 'success',
                            'message': 'Registrado exitosamente',
                            # 'Authorization': authentication_token.decode(),
                            'email': user.email,
                            'id': user.id,
                            'names': user.names,
                            'surnames': user.surnames
                        }
                    return response_object, 200
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'El correo y la contraseña no coinciden.'
                }
                return response_object, 200

        except Exception as e:
            return manage_db_exception("Error en authentication_helper#login_user - Error al registrar el usuario",
                                       'Ocurrió un error al registrar el usuario.', e)

    @staticmethod
    def logout_user(authentication_token):
        try:
            if authentication_token:
                resp = UserBase.decode_auth_token(authentication_token)
                if not isinstance(resp, str):
                    # mark the token as blacklisted
                    return save_token(token=authentication_token)
                else:
                    response_object = {
                        'status': 'fail',
                        'message': resp
                    }
                    return response_object, 401
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Por favor envíe un token de autenticación válido'
                }
                return response_object, 400
        except Exception as e:
            return manage_db_exception("Error en authentication_helper#logout_user - Error al salir del sistema",
                                       'Ocurrió un error al salir del sistema.', e)

    @staticmethod
    def get_logged_in_user(new_request):
        # get the auth token
        try:
            authentication_token = new_request.headers.get('Authorization')
            if authentication_token:
                resp = UserBase.decode_auth_token(authentication_token)
                if not isinstance(resp, str):
                    user = Users.query.filter_by(id=resp).first()
                    response_object = {
                        'status': 'success',
                        'data': {
                            'user_id': user.id,
                            'email': user.email,
                            'roles': marshal(user.roles, UserRoleDto.user_role)
                        }
                    }
                    return response_object, 200
                response_object = {
                    'status': 'fail',
                    'message': resp
                }
                return response_object, 401
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Por favor envíe un token válido.'
                }
                return response_object, 400
        except Exception as e:
            return manage_db_exception(
                "Error en authentication_helper#get_logged_in_user - Error al obtener el usuario",
                'Ocurrió un error al obtener el usuario.', e)

    @staticmethod
    def logout(email):
        prev_user = Connection.query.filter_by(email=email).first()
        prev_user.ending_type = 'LOGOUT'
        Authentication.save_changes(prev_user)

        Connection.query.filter_by(email=email).delete()
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'sesion cerrada exitosamente.'
        }
        return response_object, 200

    @staticmethod
    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            new_logger = loggingBDError(loggerName='authentication_helper')
            db.session.rollback()
            new_logger.error(e)

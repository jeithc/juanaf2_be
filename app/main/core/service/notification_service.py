from app.main import db
from app.main import logger
from app.main.notification.model.claimer_notification import ClaimerNotification
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.claimer_documents import Claimer_documents
from app.main.messaging.model.claimer_message import ClaimerMessage
from sqlalchemy.sql.expression import or_
from app.main.model.user import User
from app.main.service.s3_service import upload_file
from app.main.core.service.external_email_provider_service import send_bitbucket_mail
from app.main.model.claimer_message_tracking import ClaimerMessageTracking
from app.main.model.message_document import MessageDocument
from app.main.core.util.dto.notification_dto import ClaimerMessageTrackingDto
import datetime

STATE_PROCS__PARA_NOTIFICACION_ELECTRONICA = 2
STATE_PROCS__NOTIFICADO_ELECTRONICAMENTE = 3
NOTIFICATION_TYPE__ELECTRONICAMENTE = 50
DOMIAN__CLAIMER_MESSAGE_MODE_ELECTRONIC = 'ELECTRONICO'
DOMIAN__CLAIMER_MESSAGE_TYPE_NOTIFICATION = 'NOTIFICACION'
CLAIMER_DOCUMENTS__DOCUMENT_TYPE_ADMINISTRATIVE_ACT = 31
CLAIMER_DOCUMENTS__DOCUMENT_TYPE_ADMINISTRATIVE_RAI = 32

def save_message_tracking(claimer_id, claimer_message_tracking_dto):    
    print("claimer_message_tracking_dto " + str(claimer_message_tracking_dto))
    try:
        claimer_notification, claimer_message = db.session.query(ClaimerNotification, ClaimerMessage).filter(
            ClaimerNotification.claimer_id == ClaimerMessage.claimer_id, ClaimerNotification.claimer_id == claimer_id).one()
    except MultipleResultsFound as mrf:
        logger.error(
            f"Multple Notificacions found for user.claimer_id {claimer_id}")
        raise(mrf)
    except NoResultFound as nrf:
        logger.error(
            f"Notification not found for user.claimer_id {claimer_id}")
        raise(nrf)
    try:
        claimer_message_tracking = ClaimerMessageTracking()
        claimer_message_tracking.claimer_message_id = claimer_message.claimer_message_id
        claimer_message_tracking.registration_date = datetime.datetime.now()
        claimer_message_tracking.current_message_state = claimer_message_tracking_dto.current_message_state
        claimer_message_tracking.document_tracking_id = claimer_message_tracking_dto.document_tracking_id
        claimer_message_tracking.tracking_date = claimer_message_tracking_dto.tracking_date
        claimer_message_tracking.tracking_detail = claimer_message_tracking_dto.tracking_detail
        db.session.add(claimer_message_tracking)

        message_document = MessageDocument()
        message_document.claimer_message_id = claimer_message.claimer_message_id
        message_document.document_id = claimer_message_tracking_dto.document_tracking_id
        db.session.add(message_document)
        db.session.commit()
    except Exception as e:
        logger.error(e)

def get_attribute(obj, attrib):
    if hasattr(obj, attrib):
        print("found " + str(attrib))
        return obj[attrib]
    else:
        print("NOT found " + str(attrib))

def save_message_tracking_ws(data):
    
    print("save_message_tracking_ws()")
    try:
        claimer_message_tracking_dto = ClaimerMessageTrackingDto()    
        claimer_message_tracking_dto.claimer_message_id = get_attribute(data, "claimer_message_id")
        claimer_message_tracking_dto.registration_date = get_attribute(data, "registration_date")
        claimer_message_tracking_dto.current_message_state = get_attribute(data, "current_message_state")
        claimer_message_tracking_dto.document_tracking_id = get_attribute(data, "document_tracking_id")    
        claimer_message_tracking_dto.tracking_date = get_attribute(data, "tracking_date")    
        claimer_message_tracking_dto.tracking_detail = get_attribute(data, "tracking_detail")        
        claimer_message_tracking_dto.claimer_id = get_attribute(data, "claimer_id")   
        print(str(claimer_message_tracking_dto))
        claimer_id = claimer_message_tracking_dto.claimer_id
        save_message_tracking(claimer_id, claimer_message_tracking_dto)
    except Exception as e:
        print(e)

def get_claimer_documents_coordinates(user, document_types):
    documents = []
    db_documents = db.session.query(Claimer_documents).filter(Claimer_documents.claimer_id == user.claimer_id, or_(Claimer_documents.document_type_id ==
                                                                                                                   CLAIMER_DOCUMENTS__DOCUMENT_TYPE_ADMINISTRATIVE_ACT, Claimer_documents.document_type_id == CLAIMER_DOCUMENTS__DOCUMENT_TYPE_ADMINISTRATIVE_RAI)).all()
    for index, document in enumerate(db_documents):
        # TODO query claimer_documents, all documents are pdf
        path, filename = get_filename_parts(document)
        documents.append(dict(remote_path=path, filename=filename))
    # return documents
    return documents


def get_filename_parts(document):
    full_path = document.url_document
    filename_start = full_path.rfind("/")
    return full_path[:filename_start-1], full_path[filename_start+1:]


def get_annexed1_html_template(user, notification):
    subject = "Asunto: Doña Juana le Responde - Copia íntegra, auténtica y gratuita del acto administrativo"
    template = f"Hola {user.first_name} {user.middle_name} {user.last_name},\n\
    Adjunto a este mensaje encontrará el copia íntegra, auténtica, y gratuita, en los términos del artículo 56 de la Ley 1437 de 2011, del acto administrativo por el que se pone en conocimiento el resultado de su solicitud para hacerse parte del grupo de afectados por el derrumbe del relleno Sanitario Doña Juana, Resolución No {notification.aa_id}, expedida por la Defensoría del Pueblo, según la cual “ENCABEZADO DEL ACTO ADMINISTRATIVO” junto al Resultado de su análisis individualizado NÚMERO MÁSCARA.\
    Lo anterior teniendo en cuenta el correo electrónico indicado por usted {user.email}, {user.document_type}, {user.document_number}, {user.first_name} {user.middle_name} {user.last_name} para la recepción de las notificaciones personales electrónicas derivadas del presente trámite. \
    De igual forma se advierte que si decide consultar la totalidad de los Resultados individualizados de análisis puede hacerlo ingresando con su usuario y contraseña a la plataforma “DOÑA JUANA LE RESPONDE” en la que encontrará el enlace correspondiente (https://donajuanaleresponde.co/#/home).\
    Cordialmente,\
    DOÑA JUANA LE RESPONDE."
    return subject, template


def get_annexed3_html_template(user, notification):
    subject = "Asunto: Doña Juana le Responde - Acta de notificación personal electrónica"
    template = f"Hola NOMBRE DEL USUARIO,\
    Adjunto a este mensaje encontrará el acta que acredita la notificación personal electrónica surtida a TIPO DE ID, NÚMERO DE DOCUMENTO DE IDENTIDAD, NOMBRE DEL SOLICITANTE respecto a la Resolución No NÚMERO DEL ACTO ADMINISTRATIVO,expedida por la Defensoría del Pueblo, según la cual “ENCABEZADO DEL ACTO ADMINISTRATIVO” en el que funge como SOLICITANTE para para hacerse parte del grupo de afectados por el derrumbe del relleno Sanitario Doña Juana.\
    Lo anterior de conformidad con el artículo 56 de la Ley 1437 de 2011 y teniendo en cuenta la autorización de notificación personal electrónica que otorgó, mantuvo hasta este momento y el certificado de correo recibido en la cuenta indicada por usted ELSOLICITANTE@DOMINIODECORREO para la recepción de las notificaciones personales electrónicas derivadas del presente trámite. \
    De igual forma se advierte que puede consultar el documento referenciado en el asunto ingresando con su usuario y contraseña a la plataforma “DOÑA JUANA LE RESPONDE” (www………..).\
    Cordialmente,\
    DOÑA JUANA LE RESPONDE.\
    \
    Por favor, no responda a este mensaje, ya que este buzón de correo electrónico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribución. Si requiere más información, acuda a los canales de atención de la Defensoría del Pueblo.\
    La Defensoría del pueblo nunca le solicitará sus datos personales o sus credenciales de acceso a la plataforma DOÑA JUANA LE RESPONDE mediante vínculos de correo electrónico. En caso de recibir alguno, repórtelo de inmediato a través de los canales de atención de la Defensoría del Pueblo.\
    Aviso de confidencialidad: Este mensaje está dirigido para ser usado por su destinatario(a) exclusivamente y puede contener información confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribución o reproducción de este, o de cualquiera de sus anexos,está estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notifíquenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducción de este."
    return subject, template


def e_notification():
    try:
        to_notify_users = db.session.query(ClaimerWorkflow, User).filter(
            ClaimerWorkflow.claimer_id == User.claimer_id, ClaimerWorkflow.current_state == STATE_PROCS__PARA_NOTIFICACION_ELECTRONICA).all()
        # claimers_dict = dict()
        for index, list_ in enumerate(to_notify_users):
            # print("type" + str(type(to_notify_users)))
            workflow = list_[0]
            user = list_[1]
            claimer_notification = None
            try:
                print(">>>>>>>>>>>>>>>>>>>>>>>" + str(index))
                claimer_notification = db.session.query(ClaimerNotification).filter(
                    ClaimerNotification.claimer_id == user.claimer_id).one()
            except MultipleResultsFound as mrf:
                logger.error(
                    f"Multple Notificacions found for user.claimer_id {user.claimer_id}")
            except NoResultFound as nrf:
                logger.debug(
                    f"Creating notification for user.claimer_id {user.claimer_id}")
                claimer_notification = ClaimerNotification()
                # claimer_notification.aa_id = user.aa_id
                claimer_notification.claimer_id = user.claimer_id
                claimer_notification.notification_type = NOTIFICATION_TYPE__ELECTRONICAMENTE
                claimer_notification.has_attorney = False
                db.session.add(claimer_notification)

            documents_data = get_claimer_documents_coordinates(
                user, [CLAIMER_DOCUMENTS__DOCUMENT_TYPE_ADMINISTRATIVE_ACT, CLAIMER_DOCUMENTS__DOCUMENT_TYPE_ADMINISTRATIVE_RAI])
            # for doc in documents_data:
            #      print(doc)

            # print(user.email)
            subject, template = get_annexed1_html_template(
                user, claimer_notification)
            send_bitbucket_mail('notificaciones_juana@defensoria.gov.co',
                                user.email, subject, template, documents_data)
            try:
                claimer_message = db.session.query(ClaimerMessage).filter(
                    ClaimerMessage.claimer_id == user.claimer_id, ClaimerMessage.claimer_message_type == DOMIAN__CLAIMER_MESSAGE_TYPE_NOTIFICATION, ClaimerMessage.claimer_message_mode == DOMIAN__CLAIMER_MESSAGE_MODE_ELECTRONIC ).one()
            except MultipleResultsFound as mrf:
                logger.error(
                    f"Multple Messages found for user.claimer_id {user.claimer_id}")
            except NoResultFound as nrf:
                logger.debug(
                    f"Creating message for user.claimer_id {user.claimer_id}")                                
                claimer_message = ClaimerMessage()
                claimer_message.claimer_id = user.claimer_id
                # claimer_message.aa_id = user.aa_id
                # claimer_message.person_expedient_id = user.person_expedient_id
                claimer_message.claimer_message_mode = DOMIAN__CLAIMER_MESSAGE_MODE_ELECTRONIC
                claimer_message.claimer_message_type = DOMIAN__CLAIMER_MESSAGE_TYPE_NOTIFICATION
                db.session.add(claimer_message)

            workflow.current_state = STATE_PROCS__NOTIFICADO_ELECTRONICAMENTE
            db.session.add(workflow)
            subject, template = get_annexed3_html_template(
                user, claimer_notification)
            send_bitbucket_mail('notificaciones_juana@defensoria.gov.co',
                                user.email, subject, template, documents_data)
            db.session.commit()

    except Exception as e:
        logger.error(e)

import datetime
import hashlib
import io
import json
import os
import webbrowser
import requests
from app.main import db
from sqlalchemy.sql import text
from app.main.model.claimer_documents import Claimer_documents, Publication_claimer
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User, InitialPassword
from app.main.citation.model.aa import Aa
from app.main.util.mail import send_mail_report
from app.main.config import ruta_archivos, ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.service.logging_service import loggingBDError
from app.main.messaging.model.document_type import Document_type
from app.main.service.s3_service import upload_file, read_content_file
from app.main.core.service.massive_publication_helper import add_massive_publication, add_massive_publication_claimer, complete_massive_publication
from app.main.messaging.service.claimer_message_service import save_new_message
from app.main.service.logging_service import configLoggingError
from app.main.notification.model.claimer_notification import ClaimerNotification


class ReportJasperMasivo:
    def create_report(report_request, claimer_id, email=None, subgroup_id=None, aa_id = None):
        auth = (usuario_jasper, contrasena_jasper)
        # Init session so we have no need to auth again and again:
        s = requests.Session()

        docs_repetibles = [3, 5, 6, 7, 8, 9, 10, 21, 26, 33, 34, 35, 36,64]

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_subgroup_id","value": [""]}
            ]
        }
        }"""

        data2 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_acto_id","value": [""]},
                {"name": "param_acto_descrip","value": [""]}
                
            ]
        }
        }"""

        data3 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_claimer_email","value": [""]}
            ]
        }
        }"""

        data4 = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_aa_id","value": [""]}
            ]
        }
        }"""
        #typeReport = "5"
        typeReport = report_request

        user = User.query.filter_by(claimer_id=claimer_id).first()
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            documentoEx = Claimer_documents.query.filter_by(
                claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()
            if not documentoEx or (tr in docs_repetibles):
                state = True
                if typeReport == 30:
                    state = False

                document = Claimer_documents(
                    claimer_id=user.claimer_id,
                    document_type_id=int(tr),
                    flow_document="1",
                    origin_document=origin_document,
                    document_state=state
                )
                ReportJasperMasivo.save_changes(document)
                document_radicate = document.radicate_number
                if document_radicate == None:
                    document_radicate = 'xxxxxxxxxxxx'

                if typeReport == 6 or typeReport == 7:
                    jsonData = json.loads(data2)
                elif typeReport == 13:
                    # si es acta de notificacion
                    jsonData = json.loads(data3)
                elif typeReport == 64:
                    # si es notificación
                    jsonData = json.loads(data4)
                else:
                    jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                # se consulta el nombre del archivo en la tabla de documentos
                document_jasper = Document_type.query.filter_by(
                    id=typeReport).first()
                jsonData['reportUnitUri'] = "/reports/{}".format(
                    document_jasper.document_route)

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][1]['value'] = [
                    user.claimer_id]
                if typeReport == 6 or typeReport == 7:
                    act = Aa.query.filter(Aa.aa_id == 1).first()
                    jsonData['parameters']['reportParameter'][0]['value'] = [
                        str(act.aa_number)]
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        act.aa_id]
                    jsonData['parameters']['reportParameter'][3]['value'] = [
                        act.description]
                elif typeReport == 13:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        email]
                elif typeReport == 8:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 23:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 24:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 27:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        subgroup_id]
                elif typeReport == 64:
                    jsonData['parameters']['reportParameter'][2]['value'] = [
                        aa_id]
                    notify = ClaimerNotification.query.filter_by(aa_id=aa_id, claimer_id=user.claimer_id).first()
                    if notify:
                        notify.notification_document_id=document.document_id
                        ReportJasperMasivo.save_changes(notify)
                jsonData['parameters']['reportParameter'][0]['value'] = [
                    str(document.radicate_number)]

                ReportJasperMasivo.save_changes(user)

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }
                response = s.post(url, data=json.dumps(
                    jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get(
                        "outputResource").get("fileName")
                except Exception as e:
                    print(e)
                    print("An exception occurred invoke report jasper")
                    newLogger = configLoggingError(
                        loggerName='report notification massive service- create report')
                    newLogger.error("error:{}, json: {}".format(e, jsonData))
                now = datetime.datetime.now()
                report_url = ruta_jasper_server + \
                    "reportExecutions/{request_id}/exports/{export_id}/outputResource".format(
                        request_id=request_id, export_id=export_id)
                url_document = report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData = {"url": url_document}

                report = ReportJasperMasivo.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(
                    claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date=now.strftime('%Y%m%d'))
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperMasivo.save_changes(document)

                name = "{documentId}-".format(
                    documentId=document.document_id)+fileName
                document.url_document = store_path+name

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(
                            pathDocument+name)
                        document.document_checksum = hashlib.md5(
                            open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except:
                        print("An exception occurred write report to disk")

                document.url_document = store_path+name

                ReportJasperMasivo.save_changes(document)

                return {'route': store_path+name, 'document': pathDocument, 'name': name, 'document_id': document.document_id}
            else:
                return {'route': "Ya registró un documento anteriormente"}


    def view_report(report_download):
        auth = ('reporteador', 'Master01*')
        s1 = requests.Session()
        url = report_download.get('url').split("?")
        s1.cookies['JSESSIONID'] = url[1]
        headersS = {"Accept": "application/json",
                    "Content-Type": "application/xml"}
        ret = s1.get(url[0], headers=headersS, auth=auth)
        return ret.content

    def view_local_report(report_download):
        path = ruta_archivos+report_download
        bytesReport = open(path, 'rb').read()
        return bytesReport

    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(
                loggerName='report_notification_massive_service')
            db.session.rollback()
            newLogger.error(e)

    def calculate_checksum(ret, id):
        hash = ""
        name = "p"+str(id)+".pdf"
        with open(name, "wb") as code:
            code.write(ret)
            hash = hashlib.md5(open(name, 'rb').read()).hexdigest()
        os.remove(name)
        return hash

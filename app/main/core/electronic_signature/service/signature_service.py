"""import to signature service"""
import os
import json
from datetime import datetime, timedelta
import adal
import requests
from msrestazure.azure_active_directory import AADTokenCredentials
from flask import  session
from requests_toolbelt.multipart.encoder import MultipartEncoder
from sqlalchemy import text
from app.main.service.logging_service import configLoggingError, loggingBDError
from app.main.service.jasper_serv import ReportJasper
from app.main.model.claimer_documents import Claimer_documents as cdv
from app.main.config import temp_server_path
from app.main.core.service.file_service import save_file_binary
from app.main import db
from app.main.config import environment
from app.main.citation.model.aa import Aa
from app.main.substantiation.model.res_auto_proof_document import ResAutoProofDocument
from app.main.core.electronic_signature.model.aa_per_pckg import Aa_per_pckg
from app.main.core.electronic_signature.model.res_auto_proof_document_per_package import Auto_per_pckg
from app.main.core.electronic_signature.model.aa_state import Aa_state
from app.main.core.electronic_signature.model.res_auto_proof_document_sign_state import Rapdse_state
from app.main.core.electronic_signature.model.aa_sending_pckg import Aa_sending_pckg
from app.main.core.electronic_signature.model.res_auto_proof_document_sending_dp_p import Auto_sending_pckg
from app.main.core.service.workflow_service import make_claimer_transition
from app.main.mail.service.build_message_helper import build_message_response, verify_user_for_mail
from app.main.core.service.general_service import manage_db_exception
from app.main import logger
from app.main.util.mail import send_mail_general
from app.main.config import test_data_defensoria, data_defensoria, ruta_archivos
from app.main.core.model.users import Users
from app.main.documental.model.request import Request
from pathlib import Path
from app.main.core.service.app_params_service import get_app_params
from app.main.substantiation.model.claimer_auto_proof_document import ClaimerAutoProofDocument


class Signature:
    """clase para manejo de firma electronica"""
    def __init__(self, array=False, user_id=False, document_type=0):
        super().__init__()
        self.now = datetime.now()
        self.array = array
        self.user_id = user_id
        self.document_type = document_type
        if environment != 'prod':
            self.defensoria = test_data_defensoria
        else:
            self.defensoria = data_defensoria
        if (session.get('tokenAAD') and (self.now < session['tokenExpire'])):
            self.data_construct = self.construct_data()
        else:
            self.data_construct = self.get_azure_token()
        self.content_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        self.header = {
            'Content-Type': 'application/json',
            'content-type': 'application/json',
            'Authorization': '{} {}'.format(session['schema'], session['tokenAAD'])
        }


    @staticmethod
    def construct_data():
        """se construye la data para consumir el api de defensoria"""
        print('{} {}'.format(session['schema'], session['tokenAAD']))
        return {
            'Content-Type': 'multipart/form-data',
            'Authorization': '{} {}'.format(session['schema'], session['tokenAAD'])
        }


    @staticmethod
    def construct_data_multipart(data):
        """se construye el header para el envió de documento"""
        print('{} {}'.format(session['schema'], session['tokenAAD']))
        return {
            'Content-Type': 'multipart/form-data',
            'content-type': data.content_type,
            'Authorization': '{} {}'.format(session['schema'], session['tokenAAD'])
        }


    def get_azure_token(self):
        """
        Authenticate using service principal w/ key.
        """
        session.clear()
        authority_host_uri = self.defensoria['authority_host_uri']
        tenant = self.defensoria['tenant']
        authority_uri = '{}/{}'.format(authority_host_uri, tenant)
        resource_uri = self.defensoria['resource_uri']
        client_id = self.defensoria['client_id']
        client_secret = self.defensoria['client_secret']
        context = adal.AuthenticationContext(authority_uri, api_version=None)
        mgmt_token = context.acquire_token_with_client_credentials(
            resource_uri, client_id, client_secret)
        try:
            credentials = AADTokenCredentials(mgmt_token, client_id)
            session['tokenAAD'] = credentials.token['access_token']
            session['schema'] = credentials.scheme
            session['tokenExpire'] = self.now + \
                timedelta(seconds=credentials.token['expires_in'])
            return self.construct_data()
        except Exception as error:
            new_logger = configLoggingError(
                loggerName='signature service-get token')
            new_logger.error(error)
            print(error)
            return 'error'


    def upload_file(self):
        """proceso para subir el documento con el formato necesario"""
        if self.data_construct != "error":
            list_aa = []
            for document in self.array:
                #verificamos si existe el archivo
                try:
                    filename = open(ruta_archivos+document["route"], 'rb')
                except IOError:
                    self.documents_s3(document['document_id'])
                    filename = open(ruta_archivos+document["route"], 'rb')
                base = os.path.basename(document["route"])
                nombre, extension = base.split('.')
                multipart_data = MultipartEncoder(
                    fields={
                        # a file upload field
                        'upload': ('filename', filename, 'Content-Type: ' + self.content_type),
                        # plain text fields
                        "nombre": nombre,
                        "extension":  extension,
                        "tipoCaso": "1000",
                        "caso": "Caso",
                        "propietario": "Unal",
                        "analizar": "true",
                        "indexar": "true",
                        "asegurar": "true",
                        "adicional": "resolucion, defensoria",
                    }
                )
                result = requests.post(
                    self.defensoria['url_file'],
                    headers=self.construct_data_multipart(multipart_data),
                    data=multipart_data)
                if (result.status_code == 200):
                    try:
                        ref = result.json()['referencia']['rel']
                        print('referencia:', ref)
                        if self.document_type==0:
                            administrative_a = Aa.query.filter_by(aa_id=document["aa_id"]).first()
                            administrative_a.aa_id_dp = ref
                            list_aa.append(administrative_a.aa_id)
                            self.save_changes(administrative_a)
                            self.update_state(document["aa_id"], "PARA EMPAQUETAR")
                        else:
                            auto1 = ClaimerAutoProofDocument.query.filter_by(auto_proof_id=document["aa_id"]).first()
                            autos = ResAutoProofDocument.query.filter_by(document_id=auto1.auto_proof_document_id).all()
                            for auto in autos:
                                auto.rapd_id_dp = ref
                                auto.sign_auto_proof_document_user_id = self.now
                                auto.sign_auto_proof_document_user_id = self.user_id
                                self.save_changes(auto)
                            list_aa.append(auto1.auto_proof_id)
                            auto1.auto_proof_document_id_dp = ref
                            self.save_changes(auto1)                 
                            self.update_state(document["aa_id"], "PARA EMPAQUETAR")
                    except Exception as error:
                        print(error)
                else:
                    return 'error al cargar documentos'

            self.signature_process(list_aa)


    def get_file(self, administrative_a):
        """proceso para descargar el documento firmado"""
        if self.data_construct != "error":
            if self.document_type == 0:
                url = self.defensoria['url_file']+"/{}".format(administrative_a.aa_id_dp)
                result = requests.get(url, headers=self.header)
                if result.status_code == 200:
                    document = cdv.query.filter(cdv.document_id == administrative_a.aa_document_id).first()
                    document.document_state = True
                    self.save_changes(document)
                    save_file_binary(result.content, document.document_id)
            else:
                url = self.defensoria['url_file']+"/{}".format(administrative_a.auto_proof_document_id_dp)
                result = requests.get(url, headers=self.header)
                if result.status_code == 200:
                    document = cdv.query.filter(cdv.document_id == administrative_a.auto_proof_document_id).first()
                    document.document_state = True
                    self.save_changes(document)
                    result = save_file_binary(result.content, document.document_id)
                    try:
                        return result[0]
                    except Exception as e:
                        return {
                            'status': 'error'
                        }
        return {
            'status': 'ok'
        }

    def signature_process(self, list_aa):
        """proceso de firma"""
        days = timedelta(days=30)
        limit = self.now+days
        print(limit.strftime("%Y-%m-%d"))
        user = Users.query.filter_by(id=self.user_id).first()
        document = user.identification_number
        if self.data_construct != "error":
            archivos = []
            for act in list_aa:
                if self.document_type==0:
                    administrative_a = Aa.query.filter_by(aa_id=act).first()
                    archivo = dict(rel=administrative_a.aa_id_dp.strip(), 
                        href=self.defensoria['url_file']+"/{}".format(administrative_a.aa_id_dp.strip()),
                                method="GET", asegurar="true",
                                name="Documento")
                else:
                    auto = ClaimerAutoProofDocument.query.filter_by(auto_proof_id=act).first()
                    archivo = dict(rel=auto.auto_proof_document_id_dp.strip(), 
                        href=self.defensoria['url_file']+"/{}".format(auto.auto_proof_document_id_dp.strip()),
                                method="GET", asegurar="true",
                                name="Documento")
                archivos.append(archivo)
            print(archivos)
            data = {
                "tipoDocumento": "TIPO_DOCUMENTO.CC",
                "documento": document,
                "marcadorSello": "firma",
                "callback": {
                    "URI": "http://prueba.unal.edu.co"
                },
                "archivos": archivos,
                "fechaLimite": limit.strftime("%Y-%m-%d")
            }
            result = requests.post(
                self.defensoria['url_signature'], headers=self.header, data=json.dumps(data))

            if (result.status_code == 200):
                # SE DEBE GUARDAR PARA VERIFICAR DESPUES
                cod = result.text
                cod = cod.replace('"', '')
                self.save_package_info(cod, list_aa)
                return (cod)
            else:
                self.cancel_documents(list_aa)
                print(result.reason)
                print(result.status_code)
                return {
                    'status': result.status_code,
                    'reason': result.reason
                }
        else:
            return {
                    'status': 'error',
                    'reason': 'error al construir header'
                }


    def cancel_documents(self, list_aa):
        try:
            for aa_id in list_aa:
                if self.document_type==0:
                    acto=Aa.query.filter_by(aa_id=aa_id).first()
                    acto.aa_id_dp = None
                    self.save_changes(acto)
                else:
                    auto = ResAutoProofDocument.query.filter(
                        ResAutoProofDocument.res_apd_id == aa_id).first()
                    auto.rapd_id_dp = None
                    self.save_changes(auto)

        except Exception as error:
            return manage_db_exception("Error en signature_service#cancel_documents - " +
                                    "Error al eliminar proceso de firma",
                                    'Ocurrió un error al cancelar firma.', error)
        return 'cancelado'


    def send_mail_to_sign(self):
        """se enviá el mail con links para firma"""
        user_sign=[get_app_params('userVice')[0],get_app_params('userDNRAJ')[0]]
        mail=[get_app_params('buzonCorreoVice')[0],get_app_params('buzonCorreoDNRAJ')[0]]
        j=0
        for i in user_sign:
            self.user_id=i.value
            result_list = self.list_linkSignature(self.document_type, i.value)
            if result_list != "error":
                user = Users.query.filter(Users.id==i.value).first()
                total = len(result_list)
                if total > 0:
                    links = ["<br>"+x["link"]+" " for x in result_list]
                    links_l = "".join(links)
                    if self.document_type==0:
                        subject = 'Solicitud de firma electronica para notificación de Actos Administrativos'
                        html_body = 'Buen dia {} {},<br>Adjunto a este mensaje encontrará los links para realizar la firma \
                            electronica de los Actos Administrativos con el fin de ser notificados :\
                            <br>{}<br><br>'.format(user.names, user.surnames, links_l)
                    else:
                        subject = 'Solicitud de firma electronica para notificación de Autos de prueba'
                        html_body = 'Buen dia {} {},<br>Adjunto a este mensaje encontrará los links para realizar la firma \
                            electronica de los Autos de prueba con el fin de ser notificados :\
                            <br>{}<br><br>'.format(user.names, user.surnames, links_l)
                    send_result = send_mail_general(mail[j].value, "Doña Juana le responde - {}".format(subject), html_body)
            else:
                print("error al consultar listado para envió de firma autos de prueba")
            j+=1
        return 'ok'
  
    def signature_verify_massive_aa(self):
        """proceso para recorrer los paquetes sin firmar"""
        # BUSCO Y RECORRO LOS PAQUETES SIN FIRMAR
        logger.debug('Entro a signature_service#signature_verify_massive_aa')
        packages = Aa_sending_pckg.query.filter_by(package_state='EN PROCESO FIRMA').all()
        for pack in packages:
            result = self.signature_verify(pack.aasdp_id)
            if (result['status'] == "ok"):
                # VERIFICO LOS DOCUMENTOS RELACIONADOS AL PAQUETE
                actos = Aa.query.join(Aa_per_pckg, Aa.aa_id == Aa_per_pckg.aa_id).filter(Aa_per_pckg.aasdp_id == pack.aasdp_id).all()
                for administrative_a in actos:
                    # DESCARGO LOS ARCHIVOS Y ACTUALIZO EL AA
                    self.document_type = 0
                    self.user_id = administrative_a.approve_administrative_act_user_id
                    self.get_file(administrative_a)
                    self.update_state(administrative_a.aa_id, "FIRMADO", result['date'])
                    data_transition = {
                        'claimerId': administrative_a.claimer_id,
                        'nextState': 120,
                        'nextUser': 10000,
                        'logUser': 1
                    }
                    make_claimer_transition(data_transition)
        try:
            logger.debug('Entro a signature_service#f_send_aa_to_notification_or_vicedp')
            query = text('select * from phase_2.f_send_aa_to_notification_or_vicedp()')
            db.engine.execute(query.execution_options(autocommit=True))
            # data = db.session.query(
            #    func.phase_2.f_send_aa_to_notification_or_vicedp()).all()
            db.session.close()
        except Exception as error:
            logger.debug(error)
            print(error)
        self.notify_users()
        return "OK"


    def signature_download_document(self, id, document):
        """proceso para recorrer los paquetes sin firmar"""
        # BUSCO Y RECORRO LOS PAQUETES SIN FIRMAR
        logger.debug('Entro a signature_service#signature_verify_massive_aa')
        pack = Aa_sending_pckg.query.filter_by(aasdp_id=id).first()

        result = self.signature_verify(pack.aasdp_id)
        if (result['status'] == "ok"):
            # VERIFICO LOS DOCUMENTOS RELACIONADOS AL PAQUETE
            administrative_a = Aa.query.filter(Aa.aa_id_dp == document).first()
            # DESCARGO LOS ARCHIVOS Y ACTUALIZO EL AA
            self.document_type = 0
            self.user_id = administrative_a.approve_administrative_act_user_id
            self.get_file(administrative_a)
            self.update_state(administrative_a.aa_id, "FIRMADO", result['date'])
            data_transition = {
                'claimerId': administrative_a.claimer_id,
                'nextState': 120,
                'nextUser': 10000,
                'logUser': 1
            }
            result = make_claimer_transition(data_transition)
        try:
            logger.debug('Entro a signature_service#f_send_aa_to_notification_or_vicedp')
            query = text('select * from phase_2.f_send_aa_to_notification_or_vicedp()')
            db.engine.execute(query.execution_options(autocommit=True))
            # data = db.session.query(
            #    func.phase_2.f_send_aa_to_notification_or_vicedp()).all()
            db.session.close()
        except Exception as error:
            logger.debug(error)
            print(error)
        return "OK"

    
    def signature_verify_massive_auto(self):
        """proceso para recorrer los paquetes sin firmar"""
        # BUSCO Y RECORRO LOS PAQUETES SIN FIRMAR
        logger.debug('Entro a signature_service#signature_verify_massive_auto')
        packages = Auto_sending_pckg.query.filter_by(package_state = 'EN PROCESO FIRMA').all()
        for pack in packages:
            result = self.signature_verify(pack.rapdsdp_id)
            if (result['status'] == "ok"):
                # VERIFICO LOS DOCUMENTOS RELACIONADOS AL PAQUETE
                autos = ClaimerAutoProofDocument.query.join(
                Auto_per_pckg, ClaimerAutoProofDocument.auto_proof_id == Auto_per_pckg.auto_proof_id).filter(
                Auto_per_pckg.rapdsdp_id == pack.rapdsdp_id).all() 
                date=result['date']
                for auto in autos:      
                    # DESCARGO LOS ARCHIVOS Y ACTUALIZO EL AA
                    self.document_type = 1
                    self.user_id = pack.user_id
                    result = self.get_file(auto)
                    if result['status']!='error':
                        self.update_state(auto.auto_proof_id, "FIRMADO", date)
        return "OK"

    # def signature_verify_massive_auto1(self):
    #     """proceso para recorrer los paquetes sin firmar"""
    #     # BUSCO Y RECORRO LOS PAQUETES SIN FIRMAR
    #     logger.debug('Entro a signature_service#signature_verify_massive_auto_date')
    #     query = text( """select sdp.rapdsdp_id, sdp.user_id from phase_2.res_auto_proof_document rapd 
    #         inner join phase_2.res_auto_proof_document_per_package rpp on rpp.res_apd_id =rapd.res_apd_id
    #         inner join phase_2.res_auto_proof_document_sign_state ss on ss.res_apd_id = rapd.res_apd_id 
    #         inner join phase_2.res_auto_proof_document_sending_dp_p sdp on sdp.rapdsdp_id = rpp.rapdsdp_id
    #         where sdp.package_state = 'FIRMADO' and ss.end_date is null and ss.auto_proof_sign_state != 'FIRMADO' 
    #         group by sdp.rapdsdp_id, sdp.user_id
    #         order by sdp.rapdsdp_id""")
    #     packages=db.engine.execute(query).fetchall()
    #     i=0
    #     for pack in packages:
    #         try:
    #             result = self.signature_verify(pack.rapdsdp_id)
    #             if (result['status'] == "ok"):
    #                 # VERIFICO LOS DOCUMENTOS RELACIONADOS AL PAQUETE
    #                 autos = ResAutoProofDocument.query.join(
    #                 Auto_per_pckg, ResAutoProofDocument.res_apd_id == Auto_per_pckg.res_apd_id).filter(
    #                 Auto_per_pckg.rapdsdp_id == pack.rapdsdp_id).all() 
    #                 date=result['date']
    #                 for auto in autos:      
    #                     # DESCARGO LOS ARCHIVOS Y ACTUALIZO EL AA
    #                     self.document_type = 1
    #                     self.user_id = pack.user_id
    #                     result = self.get_file(auto)
    #                     self.update_state(auto.res_apd_id, "FIRMADO", date)              
    #                 i+=1
    #                 print(i)
    #         except Exception as error:
    #             print(error)

    
  
    # def signature_verify_massive_auto2(self):
    #     """proceso para recorrer los paquetes sin firmar"""
    #     # BUSCO Y RECORRO LOS PAQUETES SIN FIRMAR
    #     offset = (2 - 1) * 5
    #     logger.debug('Entro a signature_service#signature_verify_massive_auto')
    #     packages = Auto_sending_pckg.query.filter_by(package_state = 'EN PROCESO FIRMA').order_by(
    #         Auto_sending_pckg.rapdsdp_id).offset(offset).limit(5).all()
    #     for pack in packages:
    #         result = self.signature_verify(pack.rapdsdp_id)
    #         if (result['status'] == "ok"):
    #             # VERIFICO LOS DOCUMENTOS RELACIONADOS AL PAQUETE
    #             autos = ResAutoProofDocument.query.join(
    #             Auto_per_pckg, ResAutoProofDocument.res_apd_id == Auto_per_pckg.res_apd_id).filter(
    #             Auto_per_pckg.rapdsdp_id == pack.rapdsdp_id).all() 
    #             date=result['date']
    #             for auto in autos:      
    #                 # DESCARGO LOS ARCHIVOS Y ACTUALIZO EL AA
    #                 self.document_type = 1
    #                 self.user_id = pack.user_id
    #                 result = self.get_file(auto)
    #                 self.update_state(auto.res_apd_id, "FIRMADO", date)
                
    def notify_users(self):
        """proceso de notificación de usuarios"""
        # SE BUSCAN LOS USUARIOS Y SE VERIFICA SI SE PUEDE NOTIFICAR POR ALGUNO DE LOS CASOS
        # se verifica los que están en estado 5
        logger.debug('Entro a signature_service#notify_users')
        query = text('select cud.* from phase_2.claimer_user_data cud \
        inner join phase_2.claimer_workflow cw on cw.claimer_id = cud.claimer_id \
        where cw.current_state = :state and cw.end_date is null')
        claimers = db.engine.execute(query, state=2).fetchall()
        db.session.close()
        for claimer in claimers:
            print(claimer.claimer_id)
            aa = Aa.query.filter(Aa.claimer_id == claimer.claimer_id).first()
            # if verify_user_for_mail(claimer.email, claimer.claimer_id, True):
            #         data_transition = {
            #             'claimerId': claimer.claimer_id,
            #             'nextState': 2,
            #             'nextUser': 10000,
            #             'logUser': 1
            #         }
            #         result = make_claimer_transition(data_transition)
            #         if result:
            if aa:
                build_message_response(claimer.claimer_id, 2, 7, aa.aa_document_id, aa.aa_id)
        return 'proceso terminado'


    def signature_verify(self, id_search):
        """Verificar firma realizada"""
        if self.data_construct != "error":
            try:
                url = self.defensoria['url_signature'] + \
                    "/{}/resultado".format(id_search)
                response = requests.get(url, headers=self.header)
                if response.text != '':
                    result = response.json()
                    date = result['resultados'][0]['fecha']
                    pack = Aa_sending_pckg.query.filter_by(aasdp_id=id_search).first()
                    if pack:
                        pack.package_state = 'FIRMADO'
                        self.save_changes(pack)
                    else: 
                        pack = Auto_sending_pckg.query.filter_by(rapdsdp_id=id_search).first()
                        pack.package_state = 'FIRMADO'   
                        self.save_changes(pack)
                    return {
                        'status':'ok',
                        'date':date
                    }
                else:
                    return {
                        'status':'fail'
                    }
            except Exception as error:
                return {
                        'status':'fail'
                    }


    def documents_s3(self, document_id):
        db_documents = cdv.query.filter(cdv.document_id == document_id).first()
        print(db_documents)
        path, filename = self.get_filename_parts(db_documents)
        document = self.s3_documents_download(path, filename)
        if document != 'error':
            return document
        else:
            return 'error'


    @staticmethod
    def get_filename_parts(document):
        full_path = document.url_document
        filename_start = full_path.rfind("/")
        return full_path[:filename_start], full_path[filename_start + 1:]


    @staticmethod
    def s3_documents_download(path, filename):
        try:
            data = "{}/{}".format(path,   filename)  # pathReport
            print(data)
            input_file = ReportJasper.view_local_report(data)
            now = datetime.now()
            base_path = temp_server_path
            local_path = base_path+'/'+path
            if not os.path.exists(local_path):
                os.makedirs(local_path)
            with open("{}/{}".format(local_path, filename), 'wb') as f:
                f.write(input_file)
            return {local_path+'/'+filename,
                    filename
                    }
        except Exception as e:
            newLogger = configLoggingError(
                loggerName='signature service-download s3 document')
            newLogger.error(e)
            print(e)
            return 'error'


    # def login_test(user, password):
    #     """
    #     Authenticate the end-user using device auth.
    #     """
    #     authority_host_uri = 'https://login.microsoftonline.com'
    #     tenant = 'd1355907-e07f-4918-90d2-7384a7c1c1ea'
    #     authority_uri = authority_host_uri + '/' + tenant
    #     resource_uri = 'https://management.core.windows.net/'
    #     client_id = '0736fbc8-e036-4492-bd02-99606697b747'

    #     context = adal.AuthenticationContext(authority_uri, api_version=None)
    #     code = context.acquire_user_code(resource_uri, client_id)
    #     codigo = code['user_code']
    #     print(codigo)
    #     mgmt_token = context.acquire_token_with_device_code(
    #         resource_uri, code, client_id)
    #     credentials = AADTokenCredentials(mgmt_token, client_id)
    #     return credentials


    @staticmethod
    def save_changes(data):
        """guardar cambios en bd"""
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as error:
            print(error)
            new_logger = loggingBDError(loggerName='jasper_serv')
            db.session.rollback()
            new_logger.error(error)
            


    def save_package_info(self, package_id, list_aa):
        try:
            if self.document_type==0:
                package = Aa_sending_pckg(
                    aasdp_id=package_id,
                    processing_date=datetime.now(),
                    user_id=self.user_id,
                    package_state='SIN FIRMAR'
                )
            else:
                package = Auto_sending_pckg(
                    rapdsdp_id=package_id,
                    processing_date=datetime.now(),
                    user_id=self.user_id,
                    package_state='SIN FIRMAR'
                )
                
            self.save_changes(package)
        except Exception as error:
            return manage_db_exception("Error en signature_service#save_package_info - " +
                                    "Error al guardar el paquete de envio",
                                    'Ocurrió un error al guardar el paquete de envio.', error)
        try:
            for aa_id in list_aa:
                if self.document_type==0:
                    aa_per_pckg = Aa_per_pckg(
                        aa_id=aa_id,
                        aasdp_id=package_id
                    )
                else:
                    aa_per_pckg = Auto_per_pckg(
                        rapdsdp_id = package_id,
                        auto_proof_id = aa_id
                    )
                self.save_changes(aa_per_pckg)

                self.update_state(aa_id, "PARA FIRMAR")
        except Exception as error:
            return manage_db_exception("Error en signature_service#save_package_info - " +
                                    "Error al guardar la relación aa-paquete",
                                    'Ocurrió un error al guardar  aa-paquete.', error)
        return 'ok'


    def update_state(self, aa_id, state, date = datetime.now()):

        try:
            if self.document_type==0:
                aa_state_before = Aa_state.query.filter_by(
                aa_id=aa_id).filter_by(end_date=None).first()

                if aa_state_before:
                    aa_state_before.end_date = date
                    self.save_changes(aa_state_before)

                aa_state = Aa_state(
                    aa_id=aa_id,
                    aa_state=state,
                    user_id=self.user_id,
                    starrt_date=date
                )
                self.save_changes(aa_state)
            else:
                ra_state_before = Rapdse_state.query.filter_by(
                auto_proof_id=aa_id).filter_by(end_date=None).first()

                if ra_state_before:
                    ra_state_before.end_date = date
                    self.save_changes(ra_state_before)

                ra_state = Rapdse_state(
                    auto_proof_id=aa_id,
                    auto_proof_sign_state=state,
                    user_id=self.user_id,
                    start_date=date
                )
                self.save_changes(ra_state)
            
        except Exception as error:
            print(error)


    def list_linkSignature(self, document_type, user_id):
        try:
            retorno = []
            if document_type==0:
                aa_packages = Aa_sending_pckg.query.filter(
                Aa_sending_pckg.user_id == user_id).filter(Aa_sending_pckg.package_state == 'SIN FIRMAR').all()
                for aa_p in aa_packages:
                    retorno.append({"link": self.defensoria['url_to_signature']+
                                aa_p.aasdp_id, "fecha": aa_p.processing_date})
                    aa_p.package_state='EN PROCESO FIRMA'
                    self.save_changes(aa_p)
            else:
                aa_packages = Auto_sending_pckg.query.filter(
                Auto_sending_pckg.user_id == user_id).filter(Auto_sending_pckg.package_state == 'SIN FIRMAR').all()
                for auto_p in aa_packages:
                    retorno.append({"link": self.defensoria['url_to_signature']+
                                auto_p.rapdsdp_id, "fecha": auto_p.processing_date})
                    auto_p.package_state='EN PROCESO FIRMA'
                    self.save_changes(auto_p)
            return retorno
        except Exception as e:
            return "error"

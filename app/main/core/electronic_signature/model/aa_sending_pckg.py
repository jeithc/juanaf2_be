from app.main import db


class Aa_sending_pckg(db.Model):
    """Place model"""
    __tablename__ = 'administrative_act_sending_dp_p'
    __table_args__ = {'schema': 'phase_2'}

    aasdp_id = db.Column(db.String, primary_key=True)
    processing_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)
    package_state = db.Column(db.String)

    def __repr__(self):
        return "<aa_sending_pckg '{}'>".format(self.description)

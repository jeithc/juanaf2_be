from app.main import db


class Auto_per_pckg(db.Model):
    """res_auto_proof_document_per_package model"""
    __tablename__ = 'res_auto_proof_document_per_package'
    __table_args__ = {'schema': 'phase_2'}

    rapdsdp_id = db.Column(db.Integer,  db.ForeignKey(
        'phase_2.res_auto_proof_document_sending_dp_p.rapdsdp_id'), primary_key=True)
    auto_proof_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.claimer_auto_proof_document.auto_proof_id'), primary_key=True)
    claimer_id = db.Column(db.Integer)
    

    def __repr__(self):
        return "<Auto_per_pckg '{}'>".format(self.description)

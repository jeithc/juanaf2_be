from app.main import db


class Auto_sending_pckg(db.Model):
    """res_auto_proof_document_sending_dp_p model"""
    __tablename__ = 'res_auto_proof_document_sending_dp_p'
    __table_args__ = {'schema': 'phase_2'}

    rapdsdp_id = db.Column(db.String, primary_key=True)
    processing_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)
    package_state = db.Column(db.String)

    def __repr__(self):
        return "<Auto_sending_pckg '{}'>".format(self.description)

from app.main import db

class Rapdse_state(db.Model):
    """res_auto_proof_document_sign_state model"""
    __tablename__ = 'res_auto_proof_document_sign_state'
    __table_args__ = {'schema': 'phase_2'}

    apds_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    auto_proof_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_auto_proof_document.auto_proof_id'))
    auto_proof_sign_state = db.Column(db.String(100))
    user_id = db.Column(db.Integer)
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)

    def __repr__(self):
        return "<rapdse '{}'>".format(self.description)

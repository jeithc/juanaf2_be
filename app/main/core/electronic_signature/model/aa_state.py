from app.main import db


class Aa_state(db.Model):
    """Place model"""
    __tablename__ = 'administrative_act_state'
    __table_args__ = {'schema': 'phase_2'}

    aas_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aa_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.administrative_act.aa_id'))
    aa_state = db.Column(db.String)
    user_id = db.Column(db.Integer)
    starrt_date = db.Column(db.Date)
    end_date = db.Column(db.Date)

    def __repr__(self):
        return "<aa_state '{}'>".format(self.description)

from app.main import db


class Aa_per_pckg(db.Model):
    """Place model"""
    __tablename__ = 'administrative_act_per_package'
    __table_args__ = {'schema': 'phase_2'}

    aa_id = db.Column(db.Integer,  db.ForeignKey(
        'phase_2.administrative_act.aa_id'), primary_key=True)
    aasdp_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.administrative_act_sending_dp_p.aasdp_id'), primary_key=True)

    def __repr__(self):
        return "<aa_per_pckg '{}'>".format(self.description)

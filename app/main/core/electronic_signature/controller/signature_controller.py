from flask import request
from flask_restplus import Resource
from app.main.core.electronic_signature.service.signature_service import *
from flask_restplus import Namespace
from app.main.core.electronic_signature.service.signature_service import Signature
from app.main.genereate_resolution.util.dto.response_dto import ResponseDTO
import threading
import time


api = Namespace('electronic_signature',
                description='Operaciones relacionadas con firma electronica')


# @api.route('/upload_file/<document_id>/<document_path>')
# @api.param('document_id', 'ID document')
# @api.param('document_path', 'PATH document')
# class RolesList(Resource):
#     @api.doc('get token from ADD azure')
#     # @auth.login_required
#     def get(self, document_id, document_path):
#         """get token defensoria azure server"""
#         signature = Signature()
#         return signature.upload_file(document_id, document_path)


@api.route('/create_signature/<document>/<file_search>')
@api.param('document', 'ID person')
@api.param('file_search', 'file to verify')
class RolesList(Resource):
    @api.doc('petition process signature')
    # @auth.login_required
    def get(self, document, file_search):
        """create petition process signature"""
        signature = Signature()
        return signature.signature_process(document, file_search)


@api.route('/verify_signature/<id_search>')
@api.param('id_search', 'ID signature')
class RolesList(Resource):
    @api.doc('petition process signature verify')
    # @auth.login_required
    def get(self, id_search):
        """petition process signature verify"""
        signature = Signature()
        return signature.signature_verify(id_search)


@api.route('/verify_signature_massive_auto')
class RolesList(Resource):
    @api.doc('petition process signature verify massive auto')
    # @auth.login_required
    def get(self):
        """petition process signature verify"""
        signature = Signature()
        return signature.signature_verify_massive_auto()


@api.route('/verify_signature_aa/<id>/<document>')
@api.param('id', 'ID pack')
@api.param('document', 'ID document')
class RolesList(Resource):
    @api.doc('petition process')
    # @auth.login_required
    def get(self, id, document):
        """petition process signature verify ct"""
        signature = Signature()
        return signature.signature_download_document(id, document)


@api.route('/verify_signature_massive_aa')
class RolesList(Resource):
    @api.doc('petition process signature verify massive2')
    # @auth.login_required
    def get(self):
        """petition process signature verify administrative act"""
        signature = Signature()
        return signature.signature_verify_massive_aa()


@api.route('/download_file/<file_search>')
@api.param('file_search', 'file to verify')
class RolesList(Resource):
    @api.doc('download file from server')
    # @auth.login_required
    def get(self, file_search):
        """get file from server"""
        signature = Signature()
        return signature.get_file(file_search)


# @api.route('/login_test/<user>/<password>')
# @api.param('user', 'ID person')
# @api.param('password', 'file to verify')
# class RolesList(Resource):
#     @api.doc('test login with defensoria')
#     # @auth.login_required
#     def get(self, user, password):
#         """get file from server"""
#         signature = Signature()
#         return signature.login_test(user, password)


@api.route('/list_links_signature/<user_id>')
@api.param('user_id', 'the user to filter list')
class listToSignature(Resource):
    @api.marshal_with(ResponseDTO.listLinks)
    @api.doc('Listar links para firma')
    def get(self, user_id):
        """Listar links para firma"""
        signature = Signature()
        return signature.list_linkSignature(user_id)

from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.core.util.verify_mail_status import verify_mail_status 
from app.main.util.decorator import audit_init, audit_finish, login_app

#from app.main.general.controller.general_controller import auth

api = Namespace('mail_verify', description='prueba de servicio para verificar estado de  mail enviado')


@api.route('/verify_mail_status')
class RolesList(Resource):
    @api.doc('verify mail status')
    #@login_app
    @audit_init
    @audit_finish  
    def get(self):
        return verify_mail_status()




from flask import request
from flask_restplus import Resource

from app.main.core.service.authentication_helper import Authentication
from app.main.core.util.dto.authentication_dto import AuthenticationDto
from app.main.util.decorator import audit_init, audit_finish, login_app

api = AuthenticationDto.api
user_authentication = AuthenticationDto.user_authentication


@api.route('/login')
class UserLogin(Resource):
    """
        User Login Resource
    """
    @audit_finish
    @api.response(200, 'Registrado exitosamente')
    @api.response(200, 'El correo y la contraseña no coinciden.')
    @api.response(500, 'Fallo en método authentication_helper#Authetication#login_user')
    @api.doc('user login')
    @api.expect(user_authentication, validate=True)
    @login_app
    def post(self):
        # get the post data
        post_data = request.json
        return Authentication.login_user(data=post_data)


@api.route('/user/logout')
class LogoutAPI(Resource):
    """
    Logout Resource
    """
    @audit_init
    @audit_finish
    @api.response(200, 'Registro exitoso')
    @api.response(400, 'Por favor envíe un token de autenticación válido')
    @api.response(401, 'Falla al decodificar el token')
    @api.doc('logout a user')
    def post(self):
        # get authentication token
        # authentication_header = request.headers.get('Authorization')
        email = request.json['email']
        return Authentication.logout(email)

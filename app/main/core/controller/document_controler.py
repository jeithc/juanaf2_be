import io
import os
import PyPDF2
import datetime

from flask import request
from flask_restplus import Resource
from app.main.core.util.dto.document_dto import DocumentDTO
from app.main.service import climer_documents_service
from app.main.core.service import file_service
from app.main.util.decorator import audit_init, audit_finish
from app.main.radication.service import radicates_table_service
from app.main.config import ruta_archivos



# from app.main.general.controller.general_controller import auth


api = DocumentDTO.api


@api.route('/')
class ClaimerDocuments(Resource):
    @api.doc('list_of_registered_users')
    @audit_init
    @audit_finish 
    # @auth.login_required
    def get(self):
        """List all registered users"""

        #return user_service.get_all_users()

    @api.response(200, 'El Documento ya existe. Por favor verifique.')
    @api.response(201, 'Documento almacenado exitosamente.')
    @api.response(500, 'Ocurrió un error al guardar el Documento.')
    @api.doc('create a new user')
    @api.expect(DocumentDTO.document, validate=False)
    # @auth.login_required
    def post(self):
        """Creates a new Document """
        data = request.json
        user_email = request.headers.environ['HTTP_USER_ID']
        return climer_documents_service.save_new_claimer(data=data, user_email=user_email)

    @api.response(200, 'documento actualizado exitosamente.')
    @api.response(200, 'El documento no existe. Por favor verifique el email.')
    @api.response(500, 'Ocurrió un error al actualizar el documento.')
    @api.doc('udapte role')
    @api.expect(DocumentDTO.document, validate=False)
    # @auth.login_required
    def put(self):
        """Update document """
        data = request.json
        return climer_documents_service.claimer_update(data=data)


@api.route('/<id>')
@api.param('email', 'The email user')
class DocumentById(Resource):
    @audit_init
    @audit_finish 
    @api.doc('claimer_document')
    # @auth.login_required
    @api.marshal_with(DocumentDTO.document, skip_none=True)
    def get(self, id):
        """get document by id"""
        return climer_documents_service.document_by_id(id)


@api.route('/find_by_radicate/<radicate_number>')
@api.param('email', 'The email user')
class DocumentByRadicate(Resource):
    @audit_init
    @audit_finish
    @api.doc('claimer_document')
    # @auth.login_required
    @api.marshal_with(DocumentDTO.document, skip_none=True)
    def get(self, radicate_number):
        """get document by id"""
        return climer_documents_service.document_by_radicate(radicate_number)


@api.route('/get_by_claimer_type/<doc_type>/<claimer_id>')
@api.param('role', 'The role user')
class DocumentByTypeClaimerId(Resource):
    @api.doc('get_user_by_role')
    # @auth.login_required
    def get(self, doc_type, claimer_id):
        """get user by role"""
        return climer_documents_service.document_by_type_claimer(doc_type, claimer_id)


@api.route('/file')
class File(Resource):
    @audit_init
    @audit_finish 
    @api.response(201, 'Archivo almacenado exitosamente.')
    @api.response(500, 'Ocurrió un error al guardar el archivo.')
    @api.doc('create a new file')
    # @auth.login_required
    def post(self):
        """Creates a new file """
        input_file = request.files['inputFile']

        if 'fileName' in request.form:
            input_file.filename = request.form['fileName']

        if 'fileSize' in request.form:
            input_file.file_size = request.form['fileSize']

        path = request.args.get('path')
        document_id = request.args.get('documentId')

        return file_service.save_new_file(input_file, path, document_id)

@api.route('/file_to_merge')
class FileToMerge(Resource):
    @audit_init
    @audit_finish 
    @api.response(201, 'Archivo almacenado exitosamente.')
    @api.response(500, 'Ocurrió un error al guardar el archivo.')
    @api.doc('create a new file')
    # @auth.login_required
    def post(self):
        """Creates a new file """
        input_file = request.files['inputFile']
        if 'fileName' in request.form:
            input_file.filename = request.form['fileName']
        if 'fileSize' in request.form:
            input_file.file_size = request.form['fileSize']
        base_dir = ruta_archivos
        pathDocument = ""
        if 'urlSticker' in request.form:
            urlStickerL = str(request.form['urlSticker'])
            urlSticker = urlStickerL.split('/')
            now = datetime.datetime.now()
            sticker = radicates_table_service.generate_sticker_document(urlSticker[7], urlSticker[8]+"/"+urlSticker[9]+"/"+urlSticker[10], urlSticker[11],urlSticker[12], urlSticker[13])
            store_path = "{claimerId}/USUARIO/22/{date}/".format(claimerId=urlSticker[13], date= now.strftime('%Y%m%d') )
            pathDocument = base_dir+store_path
            if not os.path.exists(pathDocument):
                os.makedirs(pathDocument)

            with open(pathDocument+'sticker.pdf', "wb") as code:
                try:
                    code.write(sticker)
                except:
                    print("An exception occurred write report to disk")

            input_file.save(pathDocument + input_file.filename)

            paths = [pathDocument + input_file.filename, pathDocument+'sticker.pdf']

            pdf_writer = PyPDF2.PdfFileWriter()

            for path in paths:
                pdf_reader = PyPDF2.PdfFileReader(path)
                for page in range(pdf_reader.getNumPages()):
                    pdf_writer.addPage(pdf_reader.getPage(page))
                with open(pathDocument +"2"+ input_file.filename, 'wb') as out:
                    pdf_writer.write(out)

        path = request.args.get('path')
        document_id = request.args.get('documentId')
        return file_service.save_new_file_merge(path, document_id,pathDocument +"2",input_file.filename)

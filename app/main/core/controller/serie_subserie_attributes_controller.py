from flask_restplus import Resource

from app.main.core.service import serie_subserie_attributes_service
from app.main.core.util.dto.serie_subserie_attributes_dto import SerieSubserieAttributesDto

api = SerieSubserieAttributesDto.api


@api.route('/serie_subserie_attributes')
class SerieSubserieAttributesList(Resource):

    @api.doc('get list serie_subserie_attributes')
    def get(self):
        """ get list serie_subserie_attributes """
        return serie_subserie_attributes_service.get_all_serie_subserie_attributes()

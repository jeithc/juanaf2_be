from flask import request, Flask, send_file
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.util.decorator import audit_init, audit_finish, login_app, audit_front

api = Namespace('audit')

@login_app
@api.route('/audit_claimer_document')
class AuditClaimerDocument(Resource):
    @audit_init
    @audit_finish
    @api.doc('audit open document')
    def post(self):
        """audit open Documents """


@api.route('/audit_front')
class AuditFrontDocument(Resource):
    @audit_front
    @api.doc('save audit front errors')
    def post(self):
        """audit front errors"""
        

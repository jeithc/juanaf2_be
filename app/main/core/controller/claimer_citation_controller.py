#from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace


from app.main.core.service.claimer_citation_service import Claimer_citation
from app.main.core.service.mail_notification_service import send_notifications
from app.main.util.decorator import audit_init, audit_finish, login_app

#from app.main.general.controller.general_controller import auth


api = Namespace('claimer_citation', description='Operaciones relacionadas con citación de reclamantes')



@api.route('/by_mail')
class RolesList(Resource):
    @api.doc('mail citation')
    #@login_app
    @audit_init
    @audit_finish  
    def get(self):
        """mail citation"""
        return Claimer_citation.claimer_massive()


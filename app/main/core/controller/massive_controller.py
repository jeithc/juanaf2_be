from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.core.util.dto.notification_dto import NotificationDto
from app.main.core.util.verify_date import verify_date_publication
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.core.service.mail_notification_service import send_notifications
from app.main.core.service.massive_service import claimer_notification_massive
from app.main.util.decorator import audit_init, audit_finish, login_app

#from app.main.general.controller.general_controller import auth


api = Namespace('massive_notification', description='Operaciones relacionadas con notificaciones masivas')

@api.route('/by_publication/<id_publication>')
@api.param('id_publication','id_publication')
class RolesList(Resource):
    @api.doc('citación by publication')
    #@login_app
    @audit_init
    @audit_finish  
    def get(self,id_publication):
        """notification by publication"""
        #verifico tiempo de publicación
        result =verify_date_publication(id_publication)
        if result ==1:
            doc_fijacion= ReportJasperMasivo.create_report(6, -3)
            masivo = ReportJasperMasivo.create_report_massive(75 ,None, None, id_publication)
            return {
                    'doc_fijacion': doc_fijacion['route'],
                    'doc_masivo' : masivo
                }
        elif result ==2:
            return  ReportJasperMasivo.activate_report_massive()
        elif result ==3:
            #proceso para desfijar noticia
            result= claimer_notification_massive(7)
            if result =='ok':
                route= ReportJasperMasivo.create_report(7,-3)
                return  route['route']
            else:
                return 'se ha producido un error al despublicar, por favor intente nuevamente mas tarde'
            
        elif result == 0:
            return "Fecha no valida para fijación o desfijación"



@api.route('/generateMassive/<subgroup>/<document_id>')
@api.param('subgroup', 'subgroup claimers')
@api.param('document_id', 'document')
class GenerateReportAPI(Resource):
    """Generate Masivo"""
    @api.doc('generate masivo')
    #@login_app
    @audit_init
    @audit_finish    
    def get(self, subgroup,document_id):
        return  ReportJasperMasivo.create_report_massive(3, subgroup, document_id)



@api.route('/notify_by_mail')
class Mail_notification (Resource):
    """notification Massive by mail"""
    @api.doc('send massive mail')
    #@login_app
    @audit_init
    @audit_finish    
    def get(self):
        return  send_notifications(-3)   




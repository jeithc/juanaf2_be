from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace

from app.main.core.service.administrative_act_service import Administrative_act
from app.main.core.service.administrative_act_service import delete_aa

from app.main.core.service.mail_notification_service import send_notifications
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.service.user_service import  get_user_documents

#from app.main.general.controller.general_controller import auth


api = Namespace('administrative_act', description='expedición de acto administrativo')



@api.route('/issue_act')
class RolesList(Resource):
    @api.doc('Acto administrativo')
    #@login_app
    @audit_init
    @audit_finish
    def get(self):
        """issue administrative act"""
        return Administrative_act.start_act()


@api.route('/issue_document')
class RolesList(Resource):
    @api.doc('documento para previsualizar acto administrativo')
    #@login_app
    @audit_init
    @audit_finish
    def get(self):
        """issue administrative act"""
        return Administrative_act.get_document_act()


@api.route('/delete_administrative_act/<user_id>/<aa_id>')
class DeleteAA(Resource):
    @api.doc('Elimina los datos asociados al acto administrativo')
    #@login_app
    @audit_init
    @audit_finish
    def delete(self, user_id, aa_id):
        """delete administrative act"""
        return delete_aa(user_id, aa_id)



from flask_restplus import Resource

from app.main.core.service import serie_subserie_metadata_service
from app.main.core.util.dto.serie_subserie_metadata_dto import SerieSubserieMetadataDto

api = SerieSubserieMetadataDto.api


@api.route('/serie_subserie_metadata')
class SerieSubserieMetadataList(Resource):

    @api.doc('get list serie_subserie_metadata')
    def get(self):
        """ get list serie_subserie_metadata """
        return serie_subserie_metadata_service.get_all_serie_subserie_metadata()

@api.route('/serie_subserie_metadata/<serie>/<subserie>')
@api.param('serie', 'The serie')
@api.param('subserie', 'The subserie')
class SerieSubserieMetadataBySerieSubserie(Resource):
    @api.doc('get serie subserie metadata by serie subserie')
    def get(self, serie, subserie):
        """get serie subserie metadata by serie subserie"""
        return serie_subserie_metadata_service.get_serie_subserie_metadata_by_serie_subserie(serie, subserie)

@api.route('/serie_subserie_metadata/<ssId>')
@api.param('ssId', 'Serie Subserie Id')
class SerieSubserieMetadataBySerieSubserieId(Resource):
    @api.doc('get serie subserie metadata by serie subserie id')
    def get(self, ssId):
        """get serie subserie metadata by serie subserie id"""
        return serie_subserie_metadata_service.get_serie_subserie_metadata_by_ss_id(ssId)

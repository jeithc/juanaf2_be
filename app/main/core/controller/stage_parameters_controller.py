from flask import request
from flask_restplus import Resource
from app.main.core.service import stage_parameters_service
from app.main.core.util.dto.stage_parameters_dto import StageParametersDto
from app.main.util.decorator import audit_init, audit_finish

api = StageParametersDto.api

@api.route('/stage_parameters/<stage_proccess>/<stage_param_type>')
class getList(Resource):
    @audit_init
    @audit_finish 
    @api.doc('get stage_parameters')
    @api.marshal_with(StageParametersDto.stage_parameters)
    def get(self, stage_proccess, stage_param_type):
        """get stage_paramaters"""
        return stage_parameters_service.get_stage_parameters(stage_proccess, stage_param_type)

from flask import request
from flask_restplus import Resource

from app.main.core.util.dto.notification_dto import ClaimerMessageTrackingDto
from app.main.core.service.notification_service import save_message_tracking_ws
from app.main.notification.service import claimer_notification_service

api = ClaimerMessageTrackingDto.api


@api.route('/save_message_tracking')
class AdvanceProcess(Resource):
    @api.response(200, 'La traza fue almacenada exitosamente.')
    @api.response(500, 'Ocurrió un error a registrar la traza.')
    @api.doc('registrar traza')
    @api.expect(ClaimerMessageTrackingDto.claimer_message_tracking, validate=False)
    # @auth.login_required
    def post(self):
        """Saves Message Tracking """
        data = request.json
        print("save_message_tracking")
        return save_message_tracking_ws(data=data)        

@api.route('/exceptional_counting')
class ExceptionalCountingProcess(Resource):
    @api.doc('registrar traza')
    def post(self):
        """Saves Message Tracking """
        data = request.json
        return claimer_notification_service.save_exceptional(data)
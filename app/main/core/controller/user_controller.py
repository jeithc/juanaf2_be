from flask import request
from flask_restplus import Resource

from app.main.core.util.dto.user_dto import UserDto
from app.main.core.service import user_service
from app.main.util.decorator import audit_init, audit_finish, login_app


api = UserDto.api


@api.route('/users')
class RolesList(Resource):
    @api.doc('list_of_registered_users')
    # @auth.login_required
    def get(self):
        """List all registered users"""

        return user_service.get_all_users()

    @api.response(200, 'El usuario ya existe. Por favor verifique.')
    @api.response(201, 'Usuario almacenado exitosamente.')
    @api.response(500, 'Ocurrió un error al guardar el usuario.')
    @api.doc('create a new user')
    @api.expect(UserDto.user_insert, validate=False)
    # @auth.login_required
    def post(self):
        """Creates a new Role """
        data = request.json
        return user_service.save_new_user(data=data)

    @api.response(200, 'Usuario actualizado exitosamente.')
    @api.response(200, 'El usuario no existe. Por favor verifique el email.')
    @api.response(500, 'Ocurrió un error al actualizar el usuario.')
    @api.doc('udapte role')
    @api.expect(UserDto.user, validate=False)
    # @auth.login_required
    def put(self):
        """Update role """
        data = request.json
        return user_service.update_user(data=data)


@api.route('/user/<email>')
@api.param('email', 'The email user')
class UserByEmail(Resource):
    @api.doc('get_user_by_email')
    # @auth.login_required
    def get(self, email):
        """get user by email"""
        return user_service.get_user_by_email(email)


@api.route('/users_by_role/<rol>')
@api.param('rol', 'The rol user')
class UserByRol(Resource):
    @api.doc('users_by_role')
    def get(self, rol):
        """get user by rol"""
        return user_service.get_user_by_role(rol)


@api.route('/all_users_by_role/<rol>')
@api.param('rol', 'The rol user')
class UsersByRol(Resource):
    @api.doc('all_users_by_role')
    def get(self, rol):
        """get user by rol"""
        return user_service.get_all_users_by_role(rol)


@api.route('/all_users_by_supervisor/<supervisorId>')
@api.param('supervisorId', 'The id of supervisor user')
class UserBySuperisor(Resource):
    @api.doc('all_users_by_supervisorId')
    def get(self, supervisorId):
        """get user by supervisorId"""
        return user_service.get_all_users_by_supervisor(supervisorId)


@api.route('/user_by_id/<userId>')
@api.param('userId', 'The id of user')
class UserById(Resource):
    @api.doc('get_user_by_id')
    def get(self, userId):
        """get user by userId"""
        return user_service.get_user_by_id(userId)


@api.route('/user/change_password')
class ChangePassword(Resource):
    @api.response(200, 'Contraseña modificada exitosamente.')
    @api.response(404, 'El usuario no existe. Por favor verifique el email.')
    @api.response(500, 'Ocurrió un error al actualizar el usuario.')
    @api.doc('change password')
    @api.expect(UserDto.user_password, validate=False)
    @audit_init
    @audit_finish
    @login_app
    def put(self):
        """Change password"""
        data = request.json
        return user_service.change_password(data=data)


@api.route('/user_active/<user_id>')
@api.param('user_id', 'The rol user')
class UserActive(Resource):
    @api.doc('user_active')
    def get(self, user_id):
        """validate user_active"""
        return user_service.validate_user_active(user_id)


@api.route('/roles_by_user/<user_id>')
@api.param('user_id', 'The rol user')
class RolesByUser(Resource):
    @api.doc('roles by user')
    def get(self, user_id):
        """roles by user"""
        return user_service.get_roles_by_user(user_id)


@api.route('/get_all_roles')
class GetAllRoles(Resource):
    @api.doc('all roles')
    def get(self):
        """all roles"""
        return user_service.get_all_roles()


@api.route('/users_paged/<int:page>/<int:perPage>')
@api.param('page', 'pagina a mostrar')
@api.param('perPage', 'cuantos por pagina')
class RolesList(Resource):
    @api.doc('list_of_registered_users_paged')
    # @auth.login_required
    def get(self,page, perPage):
        """List all registered users"""
        return user_service.get_all_users_paged(page,perPage)


@api.route('/users_paged_count')
class CountFilter(Resource):
    @api.doc('count_list_of_registered_users')
    def get(self):
        """Get count subgroups by some filters """
        return user_service.count_all_users_paged()


@api.route('/user_filter')
class UserByFilter(Resource):
    @api.doc('get_user_by_filter')
    # @auth.login_required
    def post(self):
        """get user by filter"""
        data = request.json
        return user_service.get_user_by_filter(data)


@api.route('/user_filter_count')
class CountFilter(Resource):
    @api.doc('count_user_by_filters')
    # @auth.login_required
    def post(self):
        """count_user_by_filters"""
        data = request.json
        return user_service.count_user_filter(data)

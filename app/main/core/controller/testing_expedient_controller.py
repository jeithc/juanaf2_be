from flask_restplus import Resource
from flask import request
from app.main.core.util.dto.testing_expedient_dto import TestingExpedientSetDto
from app.main.core.service import testing_expedient_service
from app.main.util.decorator import audit_init, audit_finish

api = TestingExpedientSetDto.api


@api.route('/testing_expedient_set/all')
class TestingExpedientSetAll(Resource):
    @audit_init
    @audit_finish
    @api.doc('testing_expedient_set  all')
    @api.marshal_with(TestingExpedientSetDto.testing_expedient_set)
    def get(self):
        """ testing_expedient_set all """
        data = request.json
        return testing_expedient_service.find_all()


@api.route('/testing_expedient_set/save')
class TestingExpedientSetSave(Resource):
    @api.doc('save TestingExpedientSetDto')
    def post(self):
        """ save TestingExpedientSet """
        data = request.json
        return testing_expedient_service.save(data)


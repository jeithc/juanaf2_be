from flask import request
from flask_restplus import Resource, Namespace
from app.main.core.service import frame_list_service
from app.main.core.util.dto.frame_list_dto import FrameListDto

api = FrameListDto.api


@api.route('/frame_list/<user_id>')
class FrameList(Resource):
    @api.doc('get frame list for User')
    def get(self, user_id):
        """"get frame list for user """
        return frame_list_service.get_frames_lists_for_user(user_id=user_id)


@api.route('/frame_count_data/<frame_id>/<user_id>')
class FrameListCount(Resource):
    @api.doc('get frame data count for User')
    def get(self, frame_id, user_id):
        """"get frame data count for User """
        return frame_list_service.get_count_frame_data(frame_id=frame_id, user_id=user_id)


@api.route('/frame_data/<frame_id>/<user_id>')
@api.param('page', 'Number of page', type='integer')
@api.param('perPage', 'Results per page', type='integer')
class FrameListData(Resource):
    @api.doc('get frame data for User')
    def get(self, frame_id, user_id):
        """"get frame list for user """
        perPage = int(request.args.get('perPage'))
        page = int(request.args.get('page'))
        return frame_list_service.get_frame_data(frame_id=frame_id, user_id=user_id, page=page, per_page=perPage)

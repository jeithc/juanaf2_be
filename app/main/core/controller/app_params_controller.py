from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.core.util.dto.app_params_dto import AppParamsDto
from app.main.core.service import app_params_service

api = AppParamsDto.api

@api.route('/app_param/<variable>')
class AppParam(Resource):
    @api.marshal_with(AppParamsDto.app_params, skip_none=False)
    @api.doc('get app_param data')
    def get(self, variable):
        """get app_param """
        return app_params_service.get_app_params(variable=variable)

from flask import request
from flask_restplus import Resource

from app.main.core.util.dto.role_dto import RoleDto
from app.main.core.service import role_service

#from app.main.general.controller.general_controller import auth


api = RoleDto.api


@api.route('/roles')
class RolesList(Resource):
    @api.doc('list_of_registered_roles')
    #@auth.login_required
    def get(self):
        """List all registered roles"""

        return role_service.get_all_roles()

    @api.response(500, 'Ocurrió un error al guardar el role.')
    @api.doc('create a new role')
    @api.expect(RoleDto.role, validate=False)
    #@auth.login_required
    def post(self):
        """Creates a new Role """
        data = request.json
        return role_service.save_new_role(data=data)


    @api.response(500, 'Ocurrió un error al actualizar el role.')
    @api.doc('udapte role')
    @api.expect(RoleDto.role, validate=False)
    #@auth.login_required
    def put(self):
        """Update role """
        data = request.json
        return role_service.update_role(data=data)


@api.route('/role/<name>')
@api.param('name', 'The name role')
class RoleByName(Resource):
    @api.doc('get_role_by_name')
   # @auth.login_required
    def get(self, name):
        """get role by name"""
        return role_service.get_role_by_name(name)

from flask_restplus import Resource

from app.main.core.service import serie_subserie_service
from app.main.core.util.dto.serie_subserie_dto import SerieSubserieDto

api = SerieSubserieDto.api


@api.route('/serie_subserie')
class SerieSubserieList(Resource):

    @api.doc('get list serie_subserie')
    def get(self):
        """ get list serie_subserie """
        return serie_subserie_service.get_all_serie_subserie()

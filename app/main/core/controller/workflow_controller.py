from flask import request
from flask_restplus import Resource

from app.main.core.service import workflow_service
from app.main.core.util.dto.claimer_workflow_dto import ClaimerWorkflowDto

# from app.main.general.controller.general_controller import auth


api = ClaimerWorkflowDto.api


@api.route('/advance_process')
class AdvanceProcess(Resource):
    @api.response(200, 'El proceso se avanzo exitosamente.')
    @api.response(500, 'Ocurrió un error al avanzar el proceso.')
    @api.doc('advance process')
    @api.expect(ClaimerWorkflowDto.claimer_workflow, validate=False)
    # @auth.login_required
    def post(self):
        """Advance process """
        data = request.json
        return workflow_service.advance_process(data=data)


@api.route('/get_last_state_user/<state>/<claimer_id>')
class AdvanceProcess(Resource):
    @api.response(200, 'flujo de trabajo del solicitante.')
    @api.response(500, 'Error al consultar el flujo de trabajo del solicitante.')
    @api.doc('advance process')
    def get(self, state, claimer_id):
        """Advance process """
        return workflow_service.get_last_state_user(state, claimer_id)


@api.route('/claimer_process_track/<claimer_id>')
@api.param('claimer_id', 'Id del solicitante')
class ClaimerTrack(Resource):
    @api.response(500, 'Ocurrió al obtener la traza del solicitante.')
    @api.doc('obtener traza del solicitante')
    # @auth.login_required
    def get(self, claimer_id):
        """Obtener traza del solicitante """
        return workflow_service.get_process_track(claimer_id)


@api.route('/make_claimer_transition')
class MakeTransition(Resource):
    @api.response(500, 'Ocurrió un error al realizar la transicion.')
    @api.doc('Realizar transicion del solicitante')
    @api.expect(ClaimerWorkflowDto.claimer_transition, validate=False)
    # @auth.login_required
    def post(self):
        """Realizar transicion del solicitante """
        data = request.json
        return workflow_service.make_claimer_transition(data=data)


@api.route('/state_procs')
class StateProcs(Resource):
    @api.response(500, 'Ocurrio un error al realizar al obtener los estados de proceso')
    @api.doc('Obtiene todos los estados de procceso correspondientes al workflow')
    @api.marshal_with(ClaimerWorkflowDto.claimer_state_procs, code=200)
    def get(sekf):
        """Obtiene todos los estados de procceso correspondientes al workflow"""
        return workflow_service.get_state_procs()


@api.route('/validate_claimer_assign/<user_id>/<claimer_id>')
class AdvanceProcess(Resource):
    @api.response(200, 'flujo de trabajo del solicitante.')
    @api.response(500, 'Error al validar flujo de trabajo del solicitante.')
    @api.doc('validate_claimer_assign')
    def get(self, user_id, claimer_id):
        """validate_claimer_assign """
        return workflow_service.validate_claimer_assign(user_id, claimer_id)


@api.route('/get_last_state_in_list/<state_list>/<claimer_id>')
class LastStateInList(Resource):
    @api.response(200, 'Encontrado último estado.')
    @api.response(500, 'Error al consultar el último estado.')
    @api.doc('advance process')
    def get(self, state_list, claimer_id):
        """ Last State In State List """
        return workflow_service.get_last_state_in_list(state_list, claimer_id)
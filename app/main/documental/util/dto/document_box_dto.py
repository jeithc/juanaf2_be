from flask_restplus import Namespace, fields


class DocumentBoxDto:
    api = Namespace('document_box', description='operaciones relacionadas con la tabla document_box')

    document_box = api.model('aa', {
        'documentBoxId': fields.Integer(attribute='document_box_id', description="id del registro de la caja donde se guarda un documento"),
        'boxCode': fields.String(attribute='box_code'),
        'boxDescription': fields.String(attribute='box_description')
    })

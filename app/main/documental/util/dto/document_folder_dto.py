from flask_restplus import Namespace, fields

from app.main.documental.util.dto.document_box_dto import DocumentBoxDto


class DocumentFolderDto:
    api = Namespace('document_folder', description='operaciones relacionadas con la tabla document_folder')

    document_folder = api.model('document_folder', {
        'documentFolderId': fields.Integer(attribute='document_folder_id', description="id del registro del folder donde se guarda un documento"),
        'documentBoxId': fields.Integer(attribute='document_box_id'),
        'folderCode': fields.String(attribute='folder_code'),
        'documentBox': fields.Nested(DocumentBoxDto.document_box, description="caja de la carpeta", attribute='document_box')
    })

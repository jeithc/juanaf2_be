from flask_restplus import Namespace, fields

from app.main.documental.util.dto.document_folder_dto import DocumentFolderDto
from app.main.util.dto import UserDto, DocumentDTO


class RequestDto:
    api = Namespace('request', description='Operaciones relacionadas con request')
    request = api.model('request', {
        'requestId': fields.Integer(attribute='request_id'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'thirdPersonId': fields.Integer(attribute='third_person_id'),
        'requestType': fields.String(attribute='request_type'),
        'documentId': fields.Integer(attribute='document_id'),
        'document': fields.Nested(DocumentDTO.documents, description="documento", attribute='document'),
        'requestDocumentName': fields.String(attribute='request_document_name'),
        'requestClaimerDate': fields.DateTime(attribute='request_claimer_date'),
        'userId': fields.Integer(attribute='user_id')
    })
from flask_restplus import Namespace, fields


class ThirdPersonDto:
    api = Namespace('third_person', description='Operaciones relacionadas con terceros')
    third_person = api.model('third_person', {
        'thirdPersonId': fields.Integer(attribute='third_person_id', description='id del recurso'),
        'documentType': fields.String(attribute='document_type'),
        'documentNumber': fields.String(attribute='document_number'),
        'firstName': fields.String(attribute='first_name'),
        'middleName': fields.String(attribute='middle_name'),
        'lastName': fields.String(attribute='last_name'),
        'maidenName': fields.String(attribute='maiden_name'),
        'legalName': fields.String(attribute='legal_name'),
        'email': fields.String(attribute='email'),
        'phone': fields.String(attribute='phone'),
        'idNatgeo': fields.Integer(attribute='id_natgeo'),
        'idBogLocNeig': fields.Integer(attribute='id_bog_loc_neig'),
        'urbanRuralLocation': fields.String(attribute='urban_rural_location'),
        'address': fields.String(attribute='address'),
        'creationDate': fields.Date(attribute='creation_date')
    })

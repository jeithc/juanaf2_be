from flask_restplus import Namespace, fields
from app.main.util.dto import DocumentDTO


class AnswerToRequestDto:
    api = Namespace('answer_to_request', description='operaciones relacionadas con la tabla answer_to_request')

    answer_to_request = api.model('answer_to_request', {
        'answerToRequestId': fields.Integer(attribute='answer_to_request_id'),
        'requestId': fields.Integer(attribute='request_id'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'thirdPersonId': fields.Integer(attribute='third_person_id'),
        'requestDocumentName': fields.String(attribute='request_document_name'),
        'documentId': fields.Integer(attribute='document_id'),
        'document': fields.Nested(DocumentDTO.documents, attribute='document'),
        'requestClaimerDate': fields.Date(attribute='request_claimer_date'),
        'userId': fields.Integer(attribute='user_id'),
        'requestType': fields.String(attribute='request_type')

    })

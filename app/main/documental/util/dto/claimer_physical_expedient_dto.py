from flask_restplus import Namespace, fields

from app.main.documental.util.dto.document_folder_dto import DocumentFolderDto
from app.main.util.dto import UserDto


class ClaimerPhysicalExpedientDto:
    api = Namespace('claimer_physical_expedient', description='Operaciones relacionadas con expedientes fisicos')
    claimer_physical_expedient = api.model('claimer_physical_expedient', {
        'claimerId': fields.Integer(attribute='claimer_id'),
        'claimer': fields.Nested(UserDto.user, description="solicitante", attribute='claimer_user_data'),
        'claimerPhysicalExpedientId': fields.Integer(attribute='claimer_physical_expedient_id'),
        'documentFolderId': fields.Integer(attribute='document_folder_id'),
        'numberOfSheets': fields.Integer(attribute='number_of_sheets'),
        'userId': fields.Integer(attribute='user_id')
    })

    filter = api.model('filter', {
        'documentType': fields.String(attribute='document_type'),
        'documentNumber': fields.String(attribute='document_number'),
        'firstName': fields.String(attribute='first_name'),
        'middleName': fields.String(attribute='middle_name'),
        'lastName': fields.String(attribute='last_name'),
        'maidenName': fields.String(attribute='maiden_name'),
        'radicateNumber': fields.String(attribute='radicate_number'),
        'box': fields.Integer(attribute='document_box_id'),
        'folder': fields.Integer(attribute='document_folder_id'),
        'loan': fields.Boolean(attribute='loan')
    })
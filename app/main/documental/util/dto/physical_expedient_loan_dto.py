from flask_restplus import Namespace, fields
from flask_restplus.fields import Nested

from app.main.documental.util.dto.third_person_dto import ThirdPersonDto


class PhysicalExpedientLoanDto:
    api = Namespace('physical_expedient_loan', description='Operaciones relacionadas con prestamos de documentos')
    physical_expedient_loan = api.model('physical_expedient_loan', {
        'loanExpedientId': fields.Integer(attribute='loan_expedient_id'),
        'loanRequestedExpedientDate': fields.Date(attribute='loan_requested_expedient_date'),
        'loanDeliveryExpedientDate': fields.Date(attribute='loan_expedient_date'),
        'loanReturnedExpedientDate': fields.Date(attribute='loan_returned_expedient_date'),
        'loanExtensionExpedientDate': fields.Date(attribute='loan_extension_expedient_date'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'thirdPersonId': fields.Integer(attribute='third_person_id'),
        'thirdPerson': fields.Nested(ThirdPersonDto.third_person, description="tercero", attribute='third_person'),
        'loanExpedientResponsiblePerson': fields.String(attribute='loan_expedient_responsible_person'),
        'loanDeliveryFormat': fields.String(attribute='loan_delivery_format'),
        'copiesDelivered': fields.Integer(attribute='copies_delivered'),
        'loanExpedientAnnotation': fields.String(attribute='loan_expedient_annotation'),
        'loanClassification': fields.String(attribute='loan_classification')
    })

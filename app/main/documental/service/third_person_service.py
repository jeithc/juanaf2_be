from app.main.documental.model.third_person import *
from app.main.documental.util.dto.third_person_dto import ThirdPersonDto
from app.main.model.user import User
from sqlalchemy import func

from flask_restplus import marshal
from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from datetime import datetime
from app.main.util import db_helper


def find_by_filter(data, page, per_page, count):
    try:

        special_filter = ThirdPerson()

        if 'firstName' in data and data['firstName']:
            special_filter.first_name = data['firstName']
            del data['firstName']
        if 'middleName' in data and data['middleName']:
            special_filter.middle_name = data['middleName']
            del data['middleName']
        if 'lastName' in data and data['lastName']:
            special_filter.last_name = data['lastName']
            del data['lastName']
        if 'maidenName' in data and data['maidenName']:
            special_filter.maiden_name = data['maidenName']
            del data['maidenName']
        if 'legalName' in data and data['legalName']:
            special_filter.legal_name = data['legalName']
            del data['legalName']

        filter_data = ThirdPerson.init_from_dto(data)

        mapped_filter = db_helper.process_obj_fields(filter_data)

        query = ThirdPerson.query

        query = query.filter_by(**mapped_filter)

        if special_filter.first_name:
            query = query.filter(
                func.lower(ThirdPerson.first_name) == func.lower(special_filter.first_name))
        if special_filter.middle_name:
            query = query.filter(
                func.lower(ThirdPerson.middle_name) == func.lower(special_filter.middle_name))
        if special_filter.last_name:
            query = query.filter(
                func.lower(ThirdPerson.last_name) == func.lower(special_filter.last_name))
        if special_filter.maiden_name:
            query = query.filter(
                func.lower(ThirdPerson.maiden_name) == func.lower(special_filter.maiden_name))
        if special_filter.legal_name:
            query = query.filter(
                func.lower(ThirdPerson.legal_name) == func.lower(special_filter.legal_name))

        if count:
            result = query.count()
        else:
            result = query.paginate(int(page), int(per_page)).items

        return result
    except Exception as e:
        return manage_db_exception(
          "Error en third_person_service#find_by_filter - Error al consultar terceros",
          'Ocurrió un error al consultar los terceros.', e)


def get_by_id(third_person_id):
    try:
        third_person = ThirdPerson.query.filter_by(
            third_person_id=third_person_id).all()

        return third_person
    except Exception as e:
        return manage_db_exception(
            "Error en third_person_service#get_by_id - Error al consultar terceros",
            'Ocurrió un error al consultar los terceros.', e)


def save(data):
    logger.debug('Entro a radicate_table_service#save_new_resource')

    new_third = ThirdPerson.init_from_dto(data)
    new_third.third_person_id = None
    new_third.creation_date = datetime.now()

    if exist_third(new_third):
        response_object = {
          'status': 'fail',
          'message': 'Ya existe un tercero con esta información.'
        }
        return response_object, 200

    if exist_claimer(new_third):
        response_object = {
          'status': 'fail',
          'message': 'Ya existe un solicitante con esta información.'
        }
        return response_object, 200

    try:
        db.session.add(new_third)
        db.session.commit()

        db.session.refresh(new_third)

        return marshal(new_third, ThirdPersonDto.third_person), 201
    except Exception as e:
        return manage_db_exception("Error en third_person_service#save - Error al guardar el recurso",
                                   'Ocurrió un error al guardar el recurso.', e)


def update(data):
    logger.debug('Entro a radicates_table_service#update_resource')

    try:
        third_person = ThirdPerson.query.filter_by(
            third_person_id=data['thirdPersonId']).first()
    except Exception as e:
        return manage_db_exception("Error en third_person_service#update - " +
                                   "Error al consultar el recurso",
                                   'Ocurrió un error al consultar el recurso.', e)
    if not third_person:
        response_object = {
            'status': 'fail',
            'message': 'El tercero no existe. Por favor verifique el id.'
        }
        return response_object, 200

    third_person = ThirdPerson.fill_from_data(data, third_person)

    if exist_third(third_person):
        response_object = {
          'status': 'fail',
          'message': 'Ya existe un tercero con esta información.'
        }
        return response_object, 200

    try:
        db.session.add(third_person)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Tercero actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en third_person_service#update - " +
                                   "Error al actualizar el tercero",
                                   'Ocurrió un error al actualizar el tercero.', e)


def delete_third_person(third_person_id):
    logger.debug('Entro a third_person_service#delete_third_person')

    try:
        third_person = ThirdPerson.query.filter_by(third_person_id=third_person_id).first()
        if third_person:
            db.session.delete(third_person)
            db.session.commit()
            response_object = {
              'status': 'success',
              'message': 'tercero eliminado exitosamente.'
            }
            return response_object, 200
        else:
            response_object = {
              'status': 'success',
              'message': 'El tercero no existe, por favor verifique el id.'
            }
            return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en third_person_service#delete_third_person - " +
                                 "Error al eliminar el tercero",
                                 'Ocurrió un error al eliminar el tercero.', e)


def exist_third(third):
    third_person = ThirdPerson.query.filter_by(
        document_type=third.document_type).filter_by(
        document_number=third.document_number).first()

    if third_person:
        if third_person.third_person_id == third.third_person_id:
            return False
        return True
    else:
        return False


def exist_claimer(third):
    claimer = User.query.filter_by(
      document_type=third.document_type).filter_by(
      document_number=third.document_number).first()

    if claimer:
        return True
    else:
        return False

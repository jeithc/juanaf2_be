from app.main import logger
from app.main.documental.model.document_folder import DocumentFolder
from app.main.core.service.general_service import manage_db_exception


def get_all_document_folder():
    logger.debug('Entro a document_folder_service#get_all_document_folder')
    try:
        document_folders = DocumentFolder.query.all()

        if not document_folders:
            response_object = {
                'status': 'success',
                'message': 'No existen carpetas.'
            }
            return response_object, 200
        return document_folders, 200

    except Exception as e:
        return manage_db_exception("Error en document_folder_service#get_all_document_folder - " +
                                   "Error al obtener los registros de carpetas",
                                   'Ocurrió un error al obtener las carpetas.', e)


def get_document_folder_by_document_box_id(document_box_id):
    logger.debug('Entro a document_folder_service#get_document_folder_by_document_box_id')
    try:
        document_folders = DocumentFolder.query.filter_by(document_box_id=document_box_id).order_by(DocumentFolder.folder_code).all()

        if not document_folders:
            response_object = {
                'status': 'success',
                'message': 'No existen carpetas asociadas a la caja .' + document_box_id
            }
            return response_object, 200
        return document_folders, 200

    except Exception as e:
        return manage_db_exception("Error en document_folder_service#get_document_folder_by_document_box_id - " +
                                   "Error al obtener los registros de carpetas",
                                   'Ocurrió un error al obtener las carpetas.', e)


def get_document_folder_by_id(document_folder_id):
    logger.debug('Entro a document_folder_service#get_document_folder_by_id')
    try:
        document_folder = DocumentFolder.query.filter_by(document_folder_id=document_folder_id).first()

        if not document_folder:
            response_object = {
                'status': 'success',
                'message': 'No existe la carpeta  .' + document_folder_id
            }
            return response_object, 200
        return document_folder, 200

    except Exception as e:
        return manage_db_exception("Error en document_folder_service#get_document_folder_by_id - " +
                                   "Error al obtener los registros de carpetas",
                                   'Ocurrió un error al obtener las carpetas.', e)
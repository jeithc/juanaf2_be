from operator import and_

import dateutil

from app.main.core.model.business_days import BusinessDays
from app.main.documental.model.physical_expedient_loan import *
from app.main.documental.util.dto.physical_expedient_loan_dto import PhysicalExpedientLoanDto
from app.main.util.mail import send_mail_general
from flask_restplus import marshal
from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from datetime import datetime, date, timedelta
from app.main.util import db_helper


def find_by_filter(data, page, per_page, count):
    try:

        filter_data = PhysicalExpedientLoan.init_from_dto(data)

        mapped_filter = db_helper.process_obj_fields(filter_data)

        query = PhysicalExpedientLoan.query

        query = query.filter_by(**mapped_filter)
        if count:
            result = query.count()
        else:
            result = query.paginate(int(page), int(per_page)).items
        return result
    except Exception as e:
        return manage_db_exception(
          "Error en third_person_service#find_by_filter - Error al consultar terceros",
          'Ocurrió un error al consultar los prestamos.', e)


def get_by_id(loan_expedient_id):
    try:
        expedient_loan = PhysicalExpedientLoan.query.filter_by(
            loan_expedient_id=loan_expedient_id).all()

        return expedient_loan
    except Exception as e:
        return manage_db_exception(
            "Error en physical_expedient_loan_service#get_by_id - Error al consultar prestamos de documentos",
            'Ocurrió un error al consultar el prestamo.', e)


def get_by_claimer_id(claimer_id):
    try:
        expedient_loan = PhysicalExpedientLoan.query.filter_by(
            claimer_id=claimer_id).order_by(PhysicalExpedientLoan.loan_requested_expedient_date).all()

        if expedient_loan:
            return marshal(expedient_loan, PhysicalExpedientLoanDto.physical_expedient_loan)
        else:
            response_object = {
                'status': 'no_data_found',
                'message': 'No se encontraron resultados para esta búsqueda',
            }
            return response_object
    except Exception as e:
        return manage_db_exception(
            "Error en physical_expedient_loan_service#get_by_claimer_id - Error al consultar prestamos de documentos",
            'Ocurrió un error al consultar el prestamo.', e)


def save(data):
    logger.debug('Entro a physical_expedient_loan_service#save')

    expedient_loan = PhysicalExpedientLoan.init_from_dto(data)
    expedient_loan.loan_expedient_id = None

    try:
        db.session.add(expedient_loan)
        db.session.commit()
        db.session.refresh(expedient_loan)
        sendMessage(expedient_loan)

        return marshal(expedient_loan, PhysicalExpedientLoanDto.physical_expedient_loan), 201
    except Exception as e:
        return manage_db_exception("Error en physical_expedient_loan_service#save - Error al guardar el prestamo",
                                   'Ocurrió un error al guardar el prestamo.', e)


def update(data):
    logger.debug('Entro a physical_expedient_loan_service#update')

    try:
        expedient_loan = PhysicalExpedientLoan.query.filter_by(
            loan_expedient_id=data['loanExpedientId']).first()
    except Exception as e:
        return manage_db_exception("Error en physical_expedient_loan_service#update - " +
                                   "Error al consultar el prestamo",
                                   'Ocurrió un error al consultar el prestamo.', e)
    if not expedient_loan:
        response_object = {
            'status': 'fail',
            'message': 'El prestamo no existe. Por favor verifique el id.'
        }
        return response_object, 200

    expedient_loan = PhysicalExpedientLoan.fill_from_data(data, expedient_loan)

    try:
        db.session.add(expedient_loan)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Prestamo actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en physical_expedient_loan_service#update - " +
                                   "Error al actualizar el tercero",
                                   'Ocurrió un error al actualizar el tercero.', e)


def get_next_business_days(start_date):
    logger.debug('Entro a physical_expedient_loan_service#get_next_business_days ')
    limit = None
    try:
        business_days = BusinessDays.query \
            .filter(and_(BusinessDays.business_day_date > start_date, BusinessDays.business_day_type == 'DIA_HABIL')) \
            .order_by(BusinessDays.business_day_date.asc()).limit(8).all()

        if business_days:
            limit = business_days[len(business_days) - 1]
            limit = limit.business_day_date

        if not limit:
            start_date = dateutil.parser.parse(start_date)
            limit = start_date + timedelta(days=8)

        return limit.isoformat()
    except Exception as e:
        return manage_db_exception("Error en physical_expedient_loan_service#get_next_business_days - " +
                                   "Error al obtener los días de préstamo",
                                   'Ocurrió un error al obtener.', e)


def sendMessage(expedient_loan):

    logger.debug('Entro a physical_expedient_loan_service#sendMessage ')
    """se enviá el mail con links para firma"""
    subject = 'Prestamo de expedientes '
    html_body = 'Buen dia {} {},<br>Adjunto a este mensaje encontrará los links para realizar la firma \
            electronica de los Actos Administrativos con el fin de ser notificados :\
            <br>{}<br><br>'.format()
    send_result = send_mail_general('Pruebaxx@gmail.com', "Doña Juana le responde - {}".format(subject), html_body)
    return 'ok'
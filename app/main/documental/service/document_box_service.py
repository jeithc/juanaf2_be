import datetime
from flask_restplus import marshal
from app.main import logger, db
from app.main.documental.util.dto.document_box_dto import DocumentBoxDto
from app.main.documental.model.document_box import DocumentBox
from app.main.documental.model.document_folder import DocumentFolder
from app.main.documental.model.claimer_physical_expedient import ClaimerPhysicalExpedient
from app.main.core.service.general_service import manage_db_exception
from app.main.util.mail import send_mail_general
from app.main.core.service.app_params_service import get_app_params


def get_all_document_box():
    logger.debug('Entro a document_box_service#get_all_document_box')
    try:
        document_boxes = DocumentBox.query.all()

        return marshal(document_boxes, DocumentBoxDto.document_box), 200
    except Exception as e:
        return manage_db_exception("Error en document_box_service#get_all_document_box - " +
                                   "Error al obtener los registros de cajas",
                                   'Ocurrió un error al obtener las cajas.', e)



def document_box_mail():
    """se hacen las consultas para envió de mail"""

    #consulto total x cajas
    MAX_F_BOX = get_app_params('totalFoldersByBox')[0].value
    
    #consulto total x folders
    MAX_E_FOLDERS = get_app_params('totalPagesByFolder')[0].value

    #consulta total cajas
    total_cajas = DocumentFolder.query.distinct(DocumentFolder.document_box_id).join(
        ClaimerPhysicalExpedient, ClaimerPhysicalExpedient.document_folder_id == DocumentFolder.document_folder_id
    ).count()

    #consulta cajas totales creadas
    totales_cajas = DocumentFolder.query.distinct(DocumentFolder.document_box_id).count()
    total_cajas_vacias = totales_cajas - total_cajas

    #total folders asignados
    total_folder = ClaimerPhysicalExpedient.query.distinct(ClaimerPhysicalExpedient.document_folder_id).count()

    #total folders creados
    totales_folder = DocumentFolder.query.distinct(DocumentFolder.document_folder_id).count()
    total_folders_vacios = totales_folder - total_folder

    #total expedientes asignados
    total_expedients = ClaimerPhysicalExpedient.query.distinct(
        ClaimerPhysicalExpedient.claimer_physical_expedient_id
        ).filter(ClaimerPhysicalExpedient.document_folder_id.isnot(None)).count()


    #total expedientes asignados
    total_expedients_vacios = ClaimerPhysicalExpedient.query.distinct(
        ClaimerPhysicalExpedient.claimer_physical_expedient_id
        ).filter(ClaimerPhysicalExpedient.document_folder_id.is_(None)).count()
        

    # # min, max  cajas 
    # query = "select min(document_box_id ), max(document_box_id ) from phase_2.document_box"
    # all_box = db.engine.execute(query).fetchone()

    # min, max  folders 
    query = "select min(document_folder_id ), max(document_folder_id ) from phase_2.document_folder"
    all_folders = db.engine.execute(query).fetchone()

    # # min, max  cajas asignadas
    # query = """select min(db.document_box_id ), max(db.document_box_id ) from phase_2.document_box db 
    # inner join phase_2.document_folder df on df.document_box_id  = db.document_box_id 
    # inner join phase_2.claimer_physical_expedient cpe on cpe.document_folder_id =df.document_folder_id"""
    # result = db.engine.execute(query).fetchone()
    # db.session.close()
    
    # #total cajas intermedias sin asignación
    # box_mid = DocumentBox.query.distinct(DocumentBox.document_box_id).join(
    #     DocumentFolder, DocumentBox.document_box_id == DocumentFolder.document_box_id, isouter=True
    # ).join( 
    #     ClaimerPExp,  ClaimerPExp.document_folder_id == DocumentFolder.document_folder_id, isouter=True
    # ).filter(ClaimerPExp.document_folder_id == None,
    #     ~DocumentBox.document_box_id.in_([all_box['min'],all_box['max']])).count()
    # query = text("""select count(*) as total from (
    #     select  document_box_id from phase_2.document_folder df
    #     where document_box_id != :id_max
    #     group by document_box_id having count(*) < :total )b""")
    # box_mid = db.engine.execute(query, id_max=all_box['max'], total=int(MAX_F_BOX)).fetchone()



    # #total carpetas intermedias sin asignación
    # folder_mid = DocumentFolder.query.distinct(DocumentFolder.document_folder_id).join( 
    #     ClaimerPExp,  ClaimerPExp.document_folder_id == DocumentFolder.document_folder_id, isouter=True
    # ).filter(ClaimerPExp.document_folder_id == None,
    #     ~DocumentFolder.document_folder_id.in_([all_folders['min'],all_folders['max']])).count()
    # query = text("""select count(*)as total from (
    #     select  document_folder_id  from phase_2.claimer_physical_expedient df
    #     where document_folder_id != :id_max
    #     group by document_folder_id having count(*) < :total )b""")
    # folder_mid = db.engine.execute(query, id_max=all_folders['max'], total=int(MAX_E_FOLDERS)).fetchone()
   
    #listado cajas intermedias sin asignar
    # list_box_mid = DocumentBox.query.distinct(DocumentBox.document_box_id).join(
    #     DocumentFolder, DocumentBox.document_box_id == DocumentFolder.document_box_id, isouter=True
    # ).join( 
    #     ClaimerPExp,  ClaimerPExp.document_folder_id == DocumentFolder.document_folder_id, isouter=True
    # ).filter(
    #     ClaimerPExp.document_folder_id == None, 
    #     ~DocumentBox.document_box_id.in_([all_box['min'],all_box['max']])).all()
    # query = text("""select  document_box_id from phase_2.document_folder df
    #     where document_box_id != :id_max
    #     group by document_box_id having count(*) < :total""")
    # list_box_mid = db.engine.execute(query, id_max=all_box['max'], total=int(MAX_F_BOX)).fetchall()

    #listado carpetas intermedias sin asignar
    # list_folders_mid = DocumentFolder.query.distinct(DocumentFolder.document_folder_id).join( 
    #     ClaimerPExp,  ClaimerPExp.document_folder_id == DocumentFolder.document_folder_id, isouter=True
    # ).filter(
    #     ClaimerPExp.document_folder_id == None, 
    #     ~DocumentFolder.document_folder_id.in_([all_folders['min'],all_folders['max']])).all()
    # query = text("""select cpe.document_folder_id, df.document_box_id  
    #     from phase_2.claimer_physical_expedient cpe
    #     inner join phase_2.document_folder df on cpe.document_folder_id = df.document_folder_id 
    #     where df.document_folder_id != :id_max
    #     group by cpe.document_folder_id, df.document_box_id 
    #     having count(cpe.document_folder_id) < :total """)
    # list_folders_mid = db.engine.execute(query, id_max=all_folders['max'], total=int(MAX_E_FOLDERS)).fetchall()


    now = datetime.datetime.now()
    subject = 'Informe de asignación física de expedientes {}'.format(now.strftime('%Y/%m/%d'))

    html_body = """<!DOCTYPE html><html><head>

    
    Se adjunta informe de asignación de cajas y carpetas de expedientes.<br><br>
    <table border = "1" style ="border-collapse: collapse; border: 1px solid black;">
        <thead><tr><th colspan="2">CONSOLIDADO DE CARPETAS</th></tr></thead>
        <tbody>
            <tr><td><b>Cajas asignadas</b></td><td>{}</td></tr>
            <tr><td><b>Carpetas asignadas</b></td><td>{}</td></tr>
            <tr><td><b>Total expedientes asignadas</b></td><td>{}</td></tr>
        </tbody>
    </table><br>
    <table border = "1" style ="border-collapse: collapse; border: 1px solid black;">
        <thead><tr><th colspan="2">CONSOLIDADO DE CAJAS Y CARPETAS</th></tr></thead>
        <tbody>
            <tr><td><b>Cajas sin asignar</b></td><td>{}</td></tr>
            <tr><td><b>Carpetas sin asignar</b></td><td>{}</td></tr>
            <tr><td><b>Total expedientes sin asignar</b></td><td>{}</td></tr>
        </tbody>
    </table> <br>""".format(
        total_cajas,
        total_folder,
        total_expedients,
        total_cajas_vacias,
        total_folders_vacios,
        total_expedients_vacios
    )

    print(html_body)
    send_result = send_mail_general(get_app_params('buzonCorreoCooGesdoc')[0].value, "Doña Juana le responde - {}".format(subject), html_body)
    return 'proceso realizado'
    
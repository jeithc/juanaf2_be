from flask_restplus import marshal

from app.main import logger, db
from app.main.core.service import report_service
from app.main.documental.model.claimer_physical_expedient import ClaimerPhysicalExpedient
from app.main.core.service.general_service import manage_db_exception
from app.main.documental.model.document_box import DocumentBox
from app.main.documental.model.document_folder import DocumentFolder
from app.main.documental.util.dto.claimer_physical_expedient_dto import ClaimerPhysicalExpedientDto
from app.main.model.claimer_documents import Claimer_documents
from app.main.model.user import User
from app.main.service import user_service
from app.main.util import db_helper


def get_claimer_physical_expedient(claimers):
    logger.debug('Entro a claimer_physical_expedient_service#get_claimer_physical_expedient')

    try:
        claimers_list = []
        for claimer in claimers.split(","):
            claimers_list.append(int(claimer))
        response_object = ClaimerPhysicalExpedient.query.filter(ClaimerPhysicalExpedient.claimer_id.in_(claimers_list)).all()

        if response_object:
            response_object = marshal(response_object, ClaimerPhysicalExpedientDto.claimer_physical_expedient)

        else:
            response_object = {
                'status': 'no_data_found',
                'message': 'No hay relaciones.'
            }

        return response_object

    except Exception as e:
        return manage_db_exception("Error en claimer_physical_expedient_service#get_claimer_physical_expedient - " +
                                   "Error al obtener los registros de claimer_physical_expedient",
                                   'Ocurrió un error al obtener los claimer_physical_expedient.', e)


def find_claimer_physical_expedient_by_filter(page, per_page, data):
    logger.debug('come in #find_claimer_physical_expedient_by_filter')

    try:
        filter_data = ClaimerPhysicalExpedient.init_from_dto(data)
        query = ClaimerPhysicalExpedient.query

        if 'claimer' in data and data['claimer']:
            filter_claimer_data = User.init_from_dto(data['claimer'])
            mapped_filter_claimer = db_helper.process_obj_fields(filter_claimer_data)
            query_claimer = User.query
            query_claimer = query_claimer.filter_by(**mapped_filter_claimer)
            claimers = query_claimer.all()
            if claimers:
                for claimer in claimers:
                    query = query.filter(ClaimerPhysicalExpedient.has(claimer_id=claimer.claimer_id))
        if 'document' in data and data['document']:
            query_document = Claimer_documents.query
            query_document = query_claimer.filter_by(radicate_number=data['document']['radicateNumber'])
            documents = query_document.all()
            if documents:
                for document in documents:
                    query = query.filter(ClaimerPhysicalExpedient.has(claimer_id=document.claimer_id))
        if 'documentFolder' in data and data['documentFolder']:
            if 'documentFolderId' in data['documentFolder'] and data['documentFolder']['documentFolderId']:
                query = query.filter(ClaimerPhysicalExpedient.document_folder.has(documetn_folder_id=data['documentFolder']['documentFolderId']))
            if 'documentBoxId' in data['documentFolder']['documentBox'] and data['documentFolder']['documentBox']['documentBoxId']:
                query = query.filter(ClaimerPhysicalExpedient.document_folder.document_box.has(box_code=data['documentBox']['documentBoxId']))

        claimer_physical_expedient_list = query.paginate(page, per_page).items

        for claimerPhysicalExpedient in claimer_physical_expedient_list:
            claimer = user_service.get_a_user(ClaimerPhysicalExpedient.claimer_expedient.claimer_id)
            if claimer:
                claimerPhysicalExpedient.claimer_expedient.claimer = claimer

        return marshal(claimer_physical_expedient_list, ClaimerPhysicalExpedientDto.claimer_physical_expedient), 200

    except Exception as e:
        return manage_db_exception("Error en claimer_physical_expedient_service#find_claimer_physical_expedient_by_filter - " +
                                   "Error al obtener las relaciones de expediente-documento-carpeta por filtro",
                                   'Ocurrió un error al obtener las relaciones de expediente-documento-carpeta con filtros.', e)


def count_claimer_physical_expedient_by_filter(data):
    logger.debug('come in #count_claimer_physical_expedient_by_filter')

    try:
        filter_data = ClaimerPhysicalExpedient.init_from_dto(data)
        mapped_filter = db_helper.process_obj_fields(filter_data)
        query = ClaimerPhysicalExpedient.query
        query = query.filter_by(**mapped_filter)

        if 'claimer' in data and data['claimer']:
            filter_claimer_data = User.init_from_dto(data['claimer'])
            mapped_filter_claimer = db_helper.process_obj_fields(filter_claimer_data)
            query_claimer = User.query
            query_claimer = query_claimer.filter_by(**mapped_filter_claimer)
            claimers = query_claimer.all()
            if claimers:
                for claimer in claimers:
                    query = query.filter(ClaimerPhysicalExpedient.has(claimer_id=claimer.claimer_id))

        query = query.filter(ClaimerPhysicalExpedient.document.has(radicate_number=data['document']['radicateNumber']))

        count_value = query.count()
        return count_value, 200
    except Exception as e:
        return manage_db_exception("Error en claimer_physical_expedient_service#count_claimer_physical_expedient_by_filter - " +
                                   "Error al obtener la cuenta de las relaciones de expediente-documento-carpeta por filtro",
                                   'Ocurrió un error al obtener la cuenta de las relaciones de expediente-documento-carpeta por filtro.', e)


def get_count_claimer_physical_expedient_by_filters(dataIn):
    try:

        query = "select count(*) as count from (select distinct " \
                "cud.claimer_id, cud.document_type, cud.document_number, cud.full_name, cud.person_expedient_id, df.folder_code, db.box_code, cpel.claimer_id as taken, cpe.claimer_physical_expedient_id, FALSE AS selected " \
                "from phase_2.claimer_user_data cud " \
                "left join phase_2.claimer_physical_expedient cpe on cpe.claimer_id = cud.claimer_id " \
                "left join phase_2.document_folder df on df.document_folder_id = cpe.document_folder_id " \
                "left join phase_2.document_box db ON db.document_box_id = df.document_box_id " \
                "left join phase_2.claimer_documents cd on cd.claimer_id = cud.claimer_id " \
                "left join (select claimer_id FROM phase_2.claimer_physical_expedient_loan WHERE loan_returned_expedient_date IS NULL) cpel on cpel.claimer_id = cud.claimer_id " \
                "where 1 = 1 "

        if dataIn:
            if 'documentType' in dataIn and dataIn['documentType']:
                query += " and cud.document_type = '" + dataIn['documentType'] + "'"
            if 'documentNumber' in dataIn and dataIn['documentNumber']:
                query += " and cud.document_number like '%%" + dataIn['documentNumber'] + "%%'"
            if 'firstName' in dataIn and dataIn['firstName']:
                query += " and cud.first_name like '%%" + dataIn['firstName'].upper() + "%%'"
            if 'middleName' in dataIn and dataIn['middleName']:
                query += " and cud.middle_name like '%%" + dataIn['middleName'].upper() + "%%'"
            if 'lastName' in dataIn and dataIn['lastName']:
                query += " and cud.last_name like '%%" + dataIn['lastName'].upper() + "%%'"
            if 'maidenName' in dataIn and dataIn['maidenName']:
                query += " and cud.maiden_name like '%%" + dataIn['maidenName'].upper() + "%%'"
            if 'radicateNumber' in dataIn and dataIn['radicateNumber']:
                query += " and cd.radicate_number = '" + dataIn['radicateNumber'] + "'"
            if 'documentBoxId' in dataIn and dataIn['documentBoxId']:
                query += " and db.document_box_id = " + str(dataIn['documentBoxId'])
            if 'documentFolderId' in dataIn and dataIn['documentFolderId']:
                query += " and df.document_folder_id = " + str(dataIn['documentFolderId'])
            if 'loan' in dataIn and dataIn['loan']:
                query += ' and cpe.claimer_physical_expedient_id is not null'
                if dataIn['loan'] == 'true':
                    query += " and cpel.claimer_id is not null"
                else:
                    query += " and cpel.claimer_id is null"
            query += " )total"
        try:
            total = db.engine.execute(query).fetchall()
            db.session.close()
            return total[0][0]
        except Exception as e:
            return manage_db_exception('Error filtro', e)
    except Exception as e:
        return manage_db_exception("Error en claimer_expedients_service#get_expedients_by_filters - " +
                                   "Error al obtener los registros de expedientes por filtro",
                                   'Ocurrió un error al obtener los expedientes por filtro.', e)



def get_claimer_physical_expedient_by_filters(dataIn):
    try:

        query = "select distinct " \
                "cud.claimer_id, cud.document_type, cud.document_number, cud.full_name, cud.person_expedient_id, df.folder_code, db.box_code, cpel.claimer_id as taken, cpe.claimer_physical_expedient_id, FALSE AS selected " \
                "from phase_2.claimer_user_data cud " \
                "left join phase_2.claimer_physical_expedient cpe on cpe.claimer_id = cud.claimer_id " \
                "left join phase_2.document_folder df on df.document_folder_id = cpe.document_folder_id " \
                "left join phase_2.document_box db ON db.document_box_id = df.document_box_id " \
                "left join phase_2.claimer_documents cd on cd.claimer_id = cud.claimer_id " \
                "left join (select claimer_id FROM phase_2.claimer_physical_expedient_loan WHERE loan_returned_expedient_date IS NULL) cpel on cpel.claimer_id = cud.claimer_id " \
                "where 1 = 1 "

        if dataIn:
            if 'documentType' in dataIn and dataIn['documentType']:
                query += " and cud.document_type = '" + dataIn['documentType'] + "'"
            if 'documentNumber' in dataIn and dataIn['documentNumber']:
                query += " and cud.document_number like '%%" + dataIn['documentNumber'] + "%%'"
            if 'firstName' in dataIn and dataIn['firstName']:
                query += " and cud.first_name like '%%" + dataIn['firstName'].upper() + "%%'"
            if 'middleName' in dataIn and dataIn['middleName']:
                query += " and cud.middle_name like '%%" + dataIn['middleName'].upper() + "%%'"
            if 'lastName' in dataIn and dataIn['lastName']:
                query += " and cud.last_name like '%%" + dataIn['lastName'].upper() + "%%'"
            if 'maidenName' in dataIn and dataIn['maidenName']:
                query += " and cud.maiden_name like '%%" + dataIn['maidenName'].upper() + "%%'"
            if 'radicateNumber' in dataIn and dataIn['radicateNumber']:
                query += " and cd.radicate_number = '" + dataIn['radicateNumber'] + "'"
            if 'documentBoxId' in dataIn and dataIn['documentBoxId']:
                query += " and db.document_box_id = " + str(dataIn['documentBoxId'])
            if 'documentFolderId' in dataIn and dataIn['documentFolderId']:
                query += " and df.document_folder_id = " + str(dataIn['documentFolderId'])
            if 'loan' in dataIn and dataIn['loan']:
                query += ' and cpe.claimer_physical_expedient_id is not null'
                if dataIn['loan'] == 'true':
                    query += " and cpel.claimer_id is not null"
                else:
                    query += " and cpel.claimer_id is null"
            if 'perPage' in dataIn and dataIn['perPage']:
                query += " LIMIT " + str(dataIn['perPage'])
            if 'page' in dataIn and dataIn['page']:
                query += " OFFSET " + str(dataIn['page'] - 1)
        try:
            dataOut = db.engine.execute(query).fetchall()
            db.session.close()
        except Exception as e:
            return manage_db_exception('Error filtro', e)

        if dataOut:
            dataOut = [dict(row) for (row) in dataOut]
            return dataOut
        else:
            response_object = {
                'status': 'no_data_found',
                'message': 'No se encontraron resultados para esta búsqueda',
            }
            return response_object

    except Exception as e:
        return manage_db_exception("Error en claimer_expedients_service#get_expedients_by_filters - " +
                                   "Error al obtener los registros de expedientes por filtro",
                                   'Ocurrió un error al obtener los expedientes por filtro.', e)


def save_physical_location(claimer_physical_expedients):

    try:
        query = "select count(*) " \
                "from phase_2.claimer_physical_expedient " \
                "where document_folder_id = " + str(claimer_physical_expedients[0]['documentFolderId'])
        data = db.engine.execute(query).fetchall()
        db.session.close()
        total_expedients = data[0][0]
        if total_expedients > 200:
            folder = DocumentFolder.query.filter_by(document_folder_id=claimer_physical_expedients[0]['documentFolderId']).first()
            box = DocumentBox.query.filter_by(
                document_box_id=folder.document_box_id).first()
            db.session.close()
            response_object = {
                'status': 'folder_full',
                'message': 'La caja ' + str(box.box_code) + ', carpeta ' + str(folder.folder_code) + ' tiene ' + str(total_expedients) + ' expedientes. Solo se permite máximo 200 expedientes.',
            }
        else:
            for physical_expedient in claimer_physical_expedients:
                physical_expedient_to_save = ClaimerPhysicalExpedient.query.filter_by(claimer_physical_expedient_id=physical_expedient['claimerPhysicalExpedientId']).first()
                physical_expedient_to_save.document_folder_id = physical_expedient['documentFolderId']
            db.session.commit()

            response_object = {
                'status': 'success',
                'message': 'Asignadas correctamente las ubicaciones',
            }

        return response_object, 200

    except Exception as e:
        db.session.rollback()
        return manage_db_exception("Error en claimer_physical_expedient_service#save_physical_location - " +
                                   "Error al guardar las ubicaciones de los documentos",
                                   'Ocurrió un error al guardar las ubicaciones de los documentos.', e)


def generate_control_sheet(user_fullname, claimers):
    logger.debug('Entro a claimer_physical_expedient_service#generate_control_sheet')
    document_id = 78
    params = {
        'param_claimers': claimers,
        'param_user_fullname': user_fullname
    }

    try:
        report = report_service.create_report(report_service.get_report_route(
            document_id), params, 'pdf')

    except Exception as e:
        return manage_db_exception(
            "Error en claimer_physical_expedient_service#generate_control_sheet - Error al generar documento",
            'Ocurrió un error al generar el documento.', e)

    return report['report']


def generate_inventory_control():
    logger.debug('Entro a claimer_physical_expedient_service#generate_inventory_control')
    document_id = 79

    try:
        report = report_service.create_report(report_service.get_report_route(
            document_id), {}, 'pdf')

    except Exception as e:
        return manage_db_exception(
            "Error en claimer_physical_expedient_service#generate_inventory_control - Error al generar documento",
            'Ocurrió un error al guardar el recurso.', e)

    return report['report']
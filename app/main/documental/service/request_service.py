import datetime
from flask_restplus import marshal

from app.main import logger
from app.main.documental.model.request import Request
from app.main.documental.model.answer_request import AnswerRequest
from app.main.core.service.general_service import manage_db_exception
from app.main.model.claimer_documents import Claimer_documents
from app.main.service.logging_service import loggingBDError
from app.main.documental.util.dto.answer_to_request_dto import AnswerToRequestDto
from app.main import db
from sqlalchemy.orm import aliased


def get_radicates_expedient_requisition(claimer_id):
    logger.debug('Entro a expedient_service#get_radicates_expedient_requisition')
    try:
        response_object = Request.query.filter_by(claimer_id=claimer_id).all()
        if response_object:
            return response_object
        else:
            response_object = {
                'status': 'success',
                'message': 'Almacenamiento ok'
            }
            return response_object
    except Exception as e:
        return manage_db_exception(" - " +
                                   " ",
                                   'Ocurrió un error.', e)


def save_expedient_requisition(data):
    logger.debug('Entro a expedient_service#save_expedient_requisition')

    response_object = Request.query.filter_by(claimer_id=int(data['claimerId']), request_document_name=str(data['documentName'])).all()
    if response_object:
        return True
    else:
        document = Claimer_documents.query.filter_by(document_id=int(data['documentId'])).first()
        sDocumentTopology = None
        if document :
            sDocumentTopology = document.document_type.document_typology.document_typology
        thirdPersonId = None
        if 'thirdPersonId' in data and data['thirdPersonId']:
            thirdPersonId = data['thirdPersonId']

        request = Request(
            claimer_id = int(data['claimerId']),
            third_person_id = thirdPersonId,
            request_document_name = str(data['documentName']),
            document_id = int(data['documentId']),
            request_claimer_date = datetime.datetime.now(),
            request_type = sDocumentTopology,
            user_id = int(data['userId'])
        )
        
        try:
            save_changes(request)    
            response_object = {
                'status': 'success',
                'message': 'Almacenamiento ok'
            }
            return response_object
        except Exception as e:
            return manage_db_exception(" - " +
                                    " ",
                                    'Ocurrió un error.', e)


def get_expedient_requisition_by_claimer_id(claimer_id):
    logger.debug('Entro a expedient_service#get_expedient_requisition_by_claimer_id')

    try:
        response_object = Request.query.filter_by(claimer_id=claimer_id).all()
        if not response_object:
            response_object = {
                'status': 'no_data_found',
                'message': 'No hay relaciones.'
            }

        return response_object

    except Exception as e:
        return manage_db_exception("Error en request_service#get_expedient_requisition_by_claimer_id - " +
                                   "Error al obtener los registros de get_expedient_requisition_by_claimer_id",
                                   'Ocurrió un error al obtener los get_expedient_requisition_by_claimer_id.', e)


def save_expedient_response(data):
    logger.debug('Entro a expedient_service#save_expedient_response')

    response_object = AnswerRequest.query.filter_by(claimer_id=int(data['claimerId']), request_document_name=str(data['documentName'])).all()
    if response_object:
        return True
    else:
        document = Claimer_documents.query.filter_by(document_id=int(data['documentId'])).first()
        sDocumentTopology = None
        if document :
            sDocumentTopology = document.document_type.document_typology.document_typology

        thirdPersonId = None
        if 'thirdPersonId' in data and data['thirdPersonId']:
            thirdPersonId = data['thirdPersonId']
        answer = AnswerRequest(
            request_id = int(data['requestId']),
            claimer_id = int(data['claimerId']),
            third_person_id = thirdPersonId,
            request_document_name = str(data['documentName']),
            document_id = int(data['documentId']),
            request_claimer_date = datetime.datetime.now(),
            request_type = sDocumentTopology,
            user_id = int(data['userId'])
        )

        try:
            save_changes(answer)    
            response_object = {
                'status': 'success',
                'message': 'Almacenamiento ok'
            }
            return response_object
        except Exception as e:
            return manage_db_exception(" - " +
                                    " ",
                                    'Ocurrió un error.', e)


def get_response_by_claimer_type(claimer_id, response_type, radicate_number):
    logger.debug('Entro a expedient_service#get_radicates_expedient_requisition')

    doc_auto = aliased(Claimer_documents)
    doc_response = aliased(Claimer_documents)

    try:
        response_object = AnswerRequest.query.filter_by(claimer_id=claimer_id).join(
            doc_response, doc_response.document_id == AnswerRequest.document_id
        ).join(
            Request, Request.request_id == AnswerRequest.request_id
        ).join(
            doc_auto, doc_auto.document_id == Request.document_id
        ).filter(
            doc_response.document_type_id == response_type, doc_auto.radicate_number == radicate_number
        ).all()

        return marshal(response_object, AnswerToRequestDto.answer_to_request), 200

    except Exception as e:
        return manage_db_exception("Error en request_service#get_response_by_claimer_type - " +
                                   "Error al obtener los registros de answer_to_request",
                                   'Ocurrió un error al obtener las answer_to_request.', e)


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='expedient_service')
        db.session.rollback()
        newLogger.error(e)
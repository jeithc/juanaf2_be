from sqlalchemy.orm import relationship

from app.main import db


class ClaimerPhysicalExpedient(db.Model):
    """claimer_physical_expedient model"""
    __tablename__ = 'claimer_physical_expedient'
    __table_args__ = {'schema': 'phase_2'}

    claimer_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_user_data.claimer_id'))
    claimer = relationship("User")
    claimer_physical_expedient_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_folder_id = db.Column(db.Integer)
    created_at = db.Column(db.DateTime)
    number_of_sheets = db.Column(db.Integer)
    user_id = db.Column(db.Integer)

    @staticmethod
    def init_from_dto(data):
        claimer_physical_expedient = ClaimerPhysicalExpedient()
        if ('claimerId' in data and data['claimerId']):
            claimer_physical_expedient.claimer_id = data['claimerId']
        if ('claimerPhysicalExpedientId' in data and data['claimerPhysicalExpedientId']):
            claimer_physical_expedient.claimer_id = data['claimerPhysicalExpedientId']
        if ('documentFolderId' in data and data['documentFolderId']):
            claimer_physical_expedient.document_folder_id = data['documentFolderId']
        if ('numberOfSheets' in data and data['numberOfSheets']):
            claimer_physical_expedient.number_of_sheets = data['numberOfSheets']
        if ('userId' in data and data['userId']):
            claimer_physical_expedient.user_id = data['userId']
        return claimer_physical_expedient


    def __repr__(self):
        return "<ClaimerPhysicalExpedient '{}'>".format(self.claimer_physical_expedient_id)


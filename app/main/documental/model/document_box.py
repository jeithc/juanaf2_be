from app.main import db


class DocumentBox(db.Model):
    """DocumentBox model"""
    __tablename__ = 'document_box'
    __table_args__ = {'schema': 'phase_2'}

    document_box_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    box_code = db.Column(db.String(50))
    box_description = db.Column(db.String(100))

    @staticmethod
    def init_from_dto(data):
        documentBox = DocumentBox()
        if ('documentBoxId' in data and data['documentBoxId']):
            documentBox.document_box_id = data['documentBoxId']
        if ('boxCode' in data and data['boxCode']):
            documentBox.box_code = data['boxCode']
        if ('boxDescription' in data and data['boxDescription']):
            documentBox.box_description = data['boxDescription']

        return documentBox

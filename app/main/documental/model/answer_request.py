from app.main import db
from sqlalchemy.orm import relationship


class AnswerRequest(db.Model):
    """AnswerRequest model"""
    __tablename__ = 'answer_to_request'
    __table_args__ = {'schema': 'phase_2'}

    answer_to_request_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    request_id = db.Column(db.Integer, db.ForeignKey('phase_2.request.request_id'), primary_key=True)
    request = relationship("Request")
    claimer_id = db.Column(db.Integer)
    third_person_id = db.Column(db.Integer)
    request_document_name = db.Column(db.String)
    document_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_documents.document_id'), primary_key=True)
    document = relationship("Claimer_documents")
    request_claimer_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)
    request_type = db.Column(db.String) 
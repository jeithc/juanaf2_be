from app.main import db
from sqlalchemy.orm import relationship


class Request(db.Model):
    """Request model"""
    __tablename__ = 'request'
    __table_args__ = {'schema': 'phase_2'}

    request_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    third_person_id = db.Column(db.Integer)
    request_type = db.Column(db.String)
    document_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_documents.document_id'))
    document = relationship("Claimer_documents")
    request_document_name = db.Column(db.String)
    request_claimer_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)

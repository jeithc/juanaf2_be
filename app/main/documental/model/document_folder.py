from app.main import db
from sqlalchemy.orm import relationship


class DocumentFolder(db.Model):
    """DocumentFolder model"""
    __tablename__ = 'document_folder'
    __table_args__ = {'schema': 'phase_2'}

    document_folder_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_box_id = db.Column(db.Integer, db.ForeignKey('phase_2.document_box.document_box_id'))
    document_box = relationship("DocumentBox")
    folder_code = db.Column(db.String(50))

    def __repr__(self):
        return "<DocumentFolder '{}'>".format(self.document_folder_id)

    @staticmethod
    def init_from_dto(data):
        documentFolder = DocumentFolder()
        if ('documentFolderId' in data and data['documentFolderId']):
            documentFolder.document_folder_id = data['documentFolderId']
        if ('documentBoxId' in data and data['documentBoxId']):
            documentFolder.document_box_id = data['documentBoxId']
        if ('folderCode' in data and data['folderCode']):
            documentFolder.folder_code = data['folderCode']

        return documentFolder
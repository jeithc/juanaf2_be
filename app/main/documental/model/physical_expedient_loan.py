from app.main import db
from sqlalchemy.orm import relationship


class PhysicalExpedientLoan(db.Model):
    """PhysicalExpedientLoan model"""
    __tablename__ = 'claimer_physical_expedient_loan'
    __table_args__ = {'schema': 'phase_2'}

    loan_expedient_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    loan_requested_expedient_date = db.Column(db.DateTime)
    loan_expedient_date = db.Column(db.DateTime)
    loan_returned_expedient_date = db.Column(db.DateTime)
    loan_extension_expedient_date = db.Column(db.DateTime)
    claimer_id = db.Column(db.Integer)
    third_person_id = db.Column(db.Integer, db.ForeignKey('phase_2.third_person.third_person_id'))
    third_person = relationship("ThirdPerson")
    loan_expedient_responsible_person = db.Column(db.String(128))
    loan_delivery_format = db.Column(db.String(100))
    copies_delivered = db.Column(db.Integer)
    loan_expedient_annotation = db.Column(db.String(512))
    loan_classification = db.Column(db.String(512))

    def __repr__(self):
        return "<PhysicalExpedientLoan '{}'>".format(self.loan_expedient_id)

    @staticmethod
    def init_from_dto(data):
        physical_expedient_loan = PhysicalExpedientLoan()
        return PhysicalExpedientLoan.fill_from_data(data, physical_expedient_loan)

    @staticmethod
    def fill_from_data(data, physical_expedient_loan):

        if 'loanExpedientId' in data and data['loanExpedientId']:
            physical_expedient_loan.loan_expedient_id = data['loanExpedientId']
        if 'loanRequestedExpedientDate' in data and data['loanRequestedExpedientDate']:
            physical_expedient_loan.loan_requested_expedient_date = data['loanRequestedExpedientDate']
        if 'loanDeliveryExpedientDate' in data and data['loanDeliveryExpedientDate']:
            physical_expedient_loan.loan_expedient_date = data['loanDeliveryExpedientDate']
        if 'loanReturnedExpedientDate' in data and data['loanReturnedExpedientDate']:
            physical_expedient_loan.loan_returned_expedient_date = data['loanReturnedExpedientDate']
        if 'loanExtensionExpedientDate' in data and data['loanExtensionExpedientDate']:
            physical_expedient_loan.loan_extension_expedient_date = data['loanExtensionExpedientDate']
        if 'claimerId' in data and data['claimerId']:
            physical_expedient_loan.claimer_id = data['claimerId']
        if 'thirdPersonId' in data and data['thirdPersonId']:
            physical_expedient_loan.third_person_id = data['thirdPersonId']
        if 'loanExpedientResponsiblePerson' in data and data['loanExpedientResponsiblePerson']:
            physical_expedient_loan.loan_expedient_responsible_person = data['loanExpedientResponsiblePerson']
        if 'loanDeliveryFormat' in data and data['loanDeliveryFormat']:
            physical_expedient_loan.loan_delivery_format = data['loanDeliveryFormat']
        if 'copiesDelivered' in data and data['copiesDelivered']:
            physical_expedient_loan.copies_delivered = data['copiesDelivered']
        if 'loanExpedientAnnotation' in data and data['loanExpedientAnnotation']:
            physical_expedient_loan.loan_expedient_annotation = data['loanExpedientAnnotation']
        if 'loanClassification' in data and data['loanClassification']:
            physical_expedient_loan.loan_classification = data['loanClassification']

        return physical_expedient_loan

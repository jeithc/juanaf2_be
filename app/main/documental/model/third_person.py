from app.main import db
from sqlalchemy.orm import relationship


class ThirdPerson(db.Model):
    """ThirdPerson model"""
    __tablename__ = 'third_person'
    __table_args__ = {'schema': 'phase_2'}

    third_person_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_type = db.Column(db.String(100))
    document_number = db.Column(db.String(100))
    first_name = db.Column(db.String(100))
    middle_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    maiden_name = db.Column(db.String(100))
    legal_name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    phone = db.Column(db.String(100))
    country = db.Column(db.String(100))
    id_natgeo = db.Column(db.Integer)
    id_bog_loc_neig = db.Column(db.Integer)
    urban_rural_location = db.Column(db.String(100))
    address = db.Column(db.String(100))
    creation_date = db.Column(db.DateTime)

    def __repr__(self):
        return "<ThirdPerson '{}'>".format(self.third_person_id)

    @staticmethod
    def init_from_dto(data):
        third_person = ThirdPerson()
        return ThirdPerson.fill_from_data(data, third_person)

    @staticmethod
    def fill_from_data(data, third_person):
        if 'thirdPersonId' in data and data['thirdPersonId']:
            third_person.third_person_id = data['thirdPersonId']
        if 'documentType' in data and data['documentType']:
            third_person.document_type = data['documentType']
        if 'documentNumber' in data and data['documentNumber']:
            third_person.document_number = data['documentNumber']
        if 'firstName' in data and data['firstName']:
            third_person.first_name = data['firstName'].upper()
        if 'middleName' in data and data['middleName']:
            third_person.middle_name = data['middleName'].upper()
        if 'lastName' in data and data['lastName']:
            third_person.last_name = data['lastName'].upper()
        if 'maidenName' in data and data['maidenName']:
            third_person.maiden_name = data['maidenName'].upper()
        if 'legalName' in data and data['legalName']:
            third_person.legal_name = data['legalName'].upper()
        if 'email' in data and data['email']:
            third_person.email = data['email']
        if 'phone' in data and data['phone']:
            third_person.phone = data['phone']
        if 'country' in data and data['country']:
            third_person.country = data['country']
        if 'idNatgeo' in data and data['idNatgeo']:
            third_person.id_natgeo = data['idNatgeo']
        if 'idBogLocNeig' in data and data['idBogLocNeig']:
            third_person.third_person_id = data['idBogLocNeig']
        if 'urbanRuralLocation' in data and data['urbanRuralLocation']:
            third_person.urban_rural_location = data['urbanRuralLocation']
        if 'address' in data and data['address']:
            third_person.address = data['address']
        if 'creationDate' in data and data['creationDate']:
            third_person.creation_date = data['creationDate']

        return third_person

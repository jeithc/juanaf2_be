import io

from flask import request, send_file
from flask_restplus import Resource

from app.main.documental.service import claimer_physical_expedient_service
from app.main.documental.service.claimer_physical_expedient_service import generate_control_sheet, \
    generate_inventory_control
from app.main.documental.util.dto.claimer_physical_expedient_dto import ClaimerPhysicalExpedientDto
from app.main.util.decorator import audit_init, audit_finish

api = ClaimerPhysicalExpedientDto.api


@api.route('/claimer_physical_expedient/<claimers>')
@api.param('claimers', 'lista elegida de expedientes')
class ClaimerPhysicalExpedientList(Resource):
    @api.doc('get list claimer_physical_expedient')
    @audit_init
    @audit_finish
    def get(self, claimers):
        """ get list claimer_physical_expedient """
        return claimer_physical_expedient_service.get_claimer_physical_expedient(claimers=claimers)


@api.route('/claimer_physical_expedient/filter')
@api.param('page', 'Number of page', type='integer')
@api.param('perPage', 'Results per page', type='integer')
class ClaimerPhysicalExpedientByFilter(Resource):
    @api.expect(ClaimerPhysicalExpedientDto.claimer_physical_expedient, validate=False)
    @api.doc('find_claimer_physical_expedient_by_filter')
    def post(self):
        """Get ClaimerPhysicalExpedient by some filters """
        page = int(request.args.get('page'))
        per_page = int(request.args.get('perPage'))
        data = request.json

        return claimer_physical_expedient_service.find_claimer_physical_expedient_by_filter(page, per_page, data=data)


@api.route('/count_claimer_physical_expedient_by_filter')
class GetExpedientFolder(Resource):
    @audit_init
    @audit_finish
    @api.doc('get list expedients folders')
    def post(self):
        """get expedients"""
        return claimer_physical_expedient_service.get_count_claimer_physical_expedient_by_filters(dataIn=request.json)


@api.route('/claimer_physical_expedient_by_filter')
class GetExpedientFolder(Resource):
    @audit_init
    @audit_finish
    @api.expect(ClaimerPhysicalExpedientDto.filter, validate=False)
    @api.doc('get list expedients folders')
    def post(self):
        """get expedients"""
        return claimer_physical_expedient_service.get_claimer_physical_expedient_by_filters(dataIn=request.json)


@api.route('/claimer_physical_expedient/filter_count')
class ClaimerPhysicalExpedientByFilterCount(Resource):
    @api.expect(ClaimerPhysicalExpedientDto.claimer_physical_expedient, validate=False)
    @api.doc('count_claimer_physical_expedient_by_filter')
    def post(self):
        """Get subgroups by some filters """
        data = request.json

        return claimer_physical_expedient_service.count_claimer_physical_expedient_by_filter(data=data)


@api.route('/assign_physical_location')
class AssignPhysicalLocation(Resource):
    @audit_init
    @audit_finish
    @api.expect([ClaimerPhysicalExpedientDto.claimer_physical_expedient], validate=False)
    @api.doc('assign_physical_location')
    def post(self):
        """'Save documents physical location"""
        return claimer_physical_expedient_service.save_physical_location(claimer_physical_expedients=request.json)


@api.route('/generate_control_sheet/<user_fullname>/<claimers>')
@api.param('user_fullname', 'fullname user')
@api.param('claimers', 'lista elegida de expedientes')
@api.response(204, "No se pudo generar el documento")
class GenerateControlSheet(Resource):
    @api.doc('get document control sheet')
    @audit_init
    @audit_finish
    def get(self, user_fullname, claimers):
        """get document"""
        return send_file(
            io.BytesIO(generate_control_sheet(user_fullname, claimers)),
                    mimetype='application/pdf',
                    attachment_filename='list.pdf'
        )


@api.route('/generate_inventory_control')
@api.response(204, "no se pudo generar el documento")
class GenerateInventoryControl(Resource):
    @api.doc('get document inventory control')
    @audit_init
    @audit_finish
    def get(self):
        """get document"""
        return send_file(
            io.BytesIO(generate_inventory_control()),
                    mimetype='application/pdf',
                    attachment_filename='list.pdf'
        )
from flask_restplus import Resource
from flask import request
from app.main.documental.util.dto.third_person_dto import ThirdPersonDto
from app.main.documental.service import third_person_service
from app.main.util.decorator import audit_init, audit_finish

api = ThirdPersonDto.api


@api.route('/third_person/filter/<page>/<per_page>')
class ThirdPersonFilter(Resource):
    @audit_init
    @audit_finish
    @api.doc('third person filter')
    @api.expect(ThirdPersonDto.third_person, validate=False)
    @api.marshal_with(ThirdPersonDto.third_person)
    def post(self, page, per_page):
        """ third person filter """
        data = request.json
        return third_person_service.find_by_filter(data, page, per_page, False)


@api.route('/third_person/filter_count')
class ThirdPersonFilter(Resource):
    @audit_init
    @audit_finish
    @api.doc('third person filter counter')
    @api.expect(ThirdPersonDto.third_person, validate=False)
    def post(self):
        """ third person filter counter"""
        data = request.json
        return third_person_service.find_by_filter(data, None, None, True)


@api.route('/third_person/')
class ThirdPersonSave(Resource):
    @audit_init
    @audit_finish
    @api.expect(ThirdPersonDto.third_person, validate=False)
    @api.doc('save ThirdPerson')
    def post(self):
        """ save ThirdPerson """
        data = request.json
        return third_person_service.save(data)

    @audit_init
    @audit_finish
    @api.expect(ThirdPersonDto.third_person, validate=False)
    @api.doc('update ThirdPerson')
    def put(self):
        """ update ThirdPerson """
        data = request.json
        return third_person_service.update(data)


@api.route('/third_person/<third_person_id>')
class ThirdPersonGet(Resource):
    @audit_init
    @audit_finish
    @api.doc('get ThirdPerson by id')
    @api.marshal_with(ThirdPersonDto.third_person)
    def get(self, third_person_id):
        """ get ThirdPerson by id"""
        return third_person_service.get_by_id(third_person_id)

    @audit_init
    @audit_finish
    @api.doc('delete ThirdPerson')
    def delete(self, third_person_id):
        """ delete ThirdPerson """
        return third_person_service.delete_third_person(third_person_id)

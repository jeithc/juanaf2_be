from flask import request
from flask_restplus import Resource
from app.main.documental.service import request_service
from app.main.documental.util.dto.request_dto import RequestDto
from app.main.util.decorator import audit_init, audit_finish
from flask_restplus import  fields

api = RequestDto.api


@api.route('/expedient_requisition/')
class Request(Resource):
    @audit_init
    @audit_finish 
    @api.doc('post Request')
    def post(self):
        """ post Request """
        data = request.json
        return request_service.save_expedient_requisition(data)


@api.route('/expedient_requisition/<claimer_id>')
class RequestByClaimer(Resource):
    @audit_init
    @audit_finish
    @api.doc('get Requests by claimer_id')
    @api.marshal_with(RequestDto.request)
    def get(self, claimer_id):
        """ get ExpedientRequisition by claimer"""
        return request_service.get_expedient_requisition_by_claimer_id(claimer_id)


@api.route('/expedient_response/')
class AnswerRequest(Resource):
    @audit_init
    @audit_finish 
    @api.doc('post AnswerRequest')
    def post(self):
        """ post AnswerRequest """
        data = request.json
        return request_service.save_expedient_response(data)


@api.route('/answer_to_request/<claimer_id>/<document_type>/<radicate_number>')
class AnswerRequestQuery(Resource):
    @audit_init
    @audit_finish
    @api.doc('get AnswerRequest by claimer and document_type')
    def get(self, claimer_id, document_type, radicate_number):
        """ get AnswerRequest by claimer and document_type"""
        return request_service.get_response_by_claimer_type(claimer_id, document_type, radicate_number)

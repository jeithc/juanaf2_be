from flask_restplus import Resource
from app.main.documental.service import document_box_service
from app.main.documental.util.dto.document_box_dto import DocumentBoxDto

api = DocumentBoxDto.api


@api.route('/document_box')
class DocumentBoxList(Resource):

    @api.doc('get list document_box')
    def get(self):
        """ get list document_boxes """
        return document_box_service.get_all_document_box()



@api.route('/document_box_mail')
class DocumentBoxList(Resource):
    @api.doc('send mail  document_box')
    def get(self):
        """send mail  document_boxes """
        return document_box_service.document_box_mail()

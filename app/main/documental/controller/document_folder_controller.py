from flask_restplus import Resource
from app.main.documental.service import document_folder_service
from app.main.documental.util.dto.document_folder_dto import DocumentFolderDto

api = DocumentFolderDto.api


@api.route('/document_folder')
class DocumentFolderList(Resource):
    @api.marshal_with(DocumentFolderDto.document_folder)
    @api.doc('get list document_folder')
    def get(self):
        """ get list document_folders """
        return document_folder_service.get_all_document_folder()


@api.route('/document_folder_by_document_box_id/<document_box_id>')
class DocumentFolderListByDocumentBoxId(Resource):
    @api.marshal_with(DocumentFolderDto.document_folder)
    @api.doc('get list document_folder by document_box_id')
    def get(self, document_box_id):
        """ get list document_folders by document_box_id"""
        return document_folder_service.get_document_folder_by_document_box_id(document_box_id=document_box_id)


@api.route('/document_folder_by_id/<document_folder_id>')
class DocumentFolderById(Resource):
    @api.marshal_with(DocumentFolderDto.document_folder)
    @api.doc('get document_folder by document_folder_id')
    def get(self, document_folder_id):
        """ get list document_folders by document_box_id"""
        return document_folder_service.get_document_folder_by_id(document_folder_id=document_folder_id)
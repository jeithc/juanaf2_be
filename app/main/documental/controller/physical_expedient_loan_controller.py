from flask_restplus import Resource
from flask import request
from app.main.documental.util.dto.physical_expedient_loan_dto import PhysicalExpedientLoanDto
from app.main.documental.service import physical_expedient_loan_service
from app.main.util.decorator import audit_init, audit_finish

api = PhysicalExpedientLoanDto.api


@api.route('/filter/<page>/<per_page>')
class PhysicalExpedientLoanFilter(Resource):
    @audit_init
    @audit_finish
    @api.doc('physical_expedient_loan filter')
    @api.expect(PhysicalExpedientLoanDto.physical_expedient_loan, validate=False)
    @api.marshal_with(PhysicalExpedientLoanDto.physical_expedient_loan)
    def post(self, page, per_page):
        """ physical_expedient_loan filter """
        data = request.json
        return physical_expedient_loan_service.find_by_filter(data, page, per_page, False)


@api.route('/filter_count')
class PhysicalExpedientLoanCount(Resource):
    @audit_init
    @audit_finish
    @api.doc('third person filter counter')
    @api.expect(PhysicalExpedientLoanDto.physical_expedient_loan, validate=False)
    def post(self):
        """ third person filter counter"""
        data = request.json
        return physical_expedient_loan_service.find_by_filter(data, None, None, True)


@api.route('/')
class PhysicalExpedientLoanSave(Resource):
    @audit_init
    @audit_finish
    @api.expect(PhysicalExpedientLoanDto.physical_expedient_loan, validate=False)
    @api.doc('save PhysicalExpedientLoan')
    def post(self):
        """ save PhysicalExpedientLoan """
        data = request.json
        return physical_expedient_loan_service.save(data)

    @audit_init
    @audit_finish
    @api.expect(PhysicalExpedientLoanDto.physical_expedient_loan, validate=False)
    @api.doc('update PhysicalExpedientLoan')
    def put(self):
        """ update PhysicalExpedientLoan """
        data = request.json
        return physical_expedient_loan_service.update(data)


@api.route('/<loan_expedient_id>')
class PhysicalExpedientLoanGet(Resource):
    @audit_init
    @audit_finish
    @api.doc('get PhysicalExpedientLoan by id')
    @api.marshal_with(PhysicalExpedientLoanDto.physical_expedient_loan)
    def get(self, loan_expedient_id):
        """ get PhysicalExpedientLoan by id"""
        return physical_expedient_loan_service.get_by_id(loan_expedient_id)


@api.route('/expedient_loan_by_claimer_id/<claimer_id>')
class PhysicalExpedientLoanByClaimerId(Resource):
    @audit_init
    @audit_finish
    @api.doc('physical_expedient_loan by claimer_id')
    def get(self, claimer_id):
        """ physical_expedient_loan by claimer_id"""
        return physical_expedient_loan_service.get_by_claimer_id(claimer_id)


@api.route('/generate_loan_extension_date/<start_date>')
class PhysicalExpedientLoanByClaimerId(Resource):
    @audit_init
    @audit_finish
    @api.doc('generate_loan_extension_date')
    def get(self, start_date):
        """ next business days """
        return physical_expedient_loan_service.get_next_business_days(start_date)


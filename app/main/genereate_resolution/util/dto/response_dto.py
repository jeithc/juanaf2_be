from flask_restplus import Namespace, fields


class ResponseDTO:
    """class to dto response"""
    api = Namespace('generate_resolution',
                    description='generar resolución de actos')

    ListExpedients = api.model('List_expedients', {
        'claimerId': fields.String(attribute='claimer_id'),
        'documentType': fields.String(attribute='document_type'),
        'expedientId': fields.String(attribute='person_expedient_id'),
        'documentNumber': fields.String(attribute='document_number'),
        'fullName': fields.String(attribute='full_name'),
        'resourceType': fields.String(attribute='tipo_recurso'),
        'state': fields.String(attribute='state'),
        'aaId': fields.String(attribute='aa_id')
    })

    ListToFilter = api.model('List_to_filter', {
        'user_id': fields.Integer(description="id del usuario que hace la peticion", required=True),
        'expedientId': fields.Integer(description=" id expediente reclamante"),
        'documentType': fields.String(description="tipo de documento reclamante"),
        'documentNumber': fields.String(description="documento reclamante"),
        'fullName': fields.String(attribute='Nombre completo reclamante'),
        'resourceType': fields.String(attribute='Tipo de recurso'),
        'state': fields.String(description='estado de recuurso'),
        'page': fields.Integer(description="pagina lista", defaultValue=1),
        'perPage': fields.Integer(description="listado por pagina", defaultValue=10),
        'orderCriteria': fields.String(description="orden filtro"),
        'sortingOrder': fields.String(description="orden tipo")
    })

    ListResource = api.model('ListResource', {
        'resourceType': fields.String(attribute='resource_type'),
        'radicateNumber': fields.String(attribute='radicate_number'),
        'resourceRadicationDate': fields.Date(attribute='resource_radication_date'),
        'urlDocument': fields.String(attribute='url_document')
    })

    listObservation = api.model('listObservation', {
        'id': fields.String(attribute='claimer_expedient_annotation_id'),
        'date': fields.String(attribute='annotation_date'),
        'rol': fields.String(attribute='user_id'),
        'observation': fields.String(attribute='annotation')
    })

    listLinks = api.model('listLinks', {
        'link': fields.String(attribute='link'),
        'fecha': fields.String(attribute='fecha')
    })

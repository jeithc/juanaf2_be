"""imports"""
import datetime
from sqlalchemy import text
from app.main import db
from app.main.service.logging_service import loggingBDError
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.core.service.workflow_service import make_claimer_transition
from app.main.substantiation.service.task_assignament_service import populate_table
from app.main.core.model.role import Role
from app.main.core.model.user_role import UserRole
from app.main.core.model.users import Users
from app.main.core.util.dto.user_role_dto import UserRoleDto
from flask_restplus import marshal
from app.main.citation.model.aa import Aa
from app.main.core.electronic_signature.model.aa_sending_pckg import Aa_sending_pckg
from app.main.genereate_resolution.model.claimer_expedient_annotation import ClaimerExpedientAnnotation as cea
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.genereate_resolution.model.resource_per_administrative_act import ResourcePerAdministrativeAct as Rpea
from app.main.core.service.general_service import manage_db_exception
from app.main.core.electronic_signature.service.signature_service import Signature
from app.main.config import data_defensoria
from app.main.model.user import User
from app.main.util.mail import send_mail_general
# import asyncio

LIST_ROLES = {
    'name': [
        'dp_secretario_vicedp',
        'dp_secretario_dnraj',
        'dp_director_dnraj',
        'dp_vicedefensor_dp'
    ],
    'approve': [79, 76, 68, 68],
    'next_state': [77, 75, 76, 79],
    'state_assign': [119, 118, 76, 79]
}

LIST_DOCUMENT = {
    'id_juridica': [61,64,65,66,67,68,69],
    'id_jasper':[66,68,69,70,71,72,73]
}


def number_list_to_signature(data):
    """se busca el tipo de usuario y se saca el listado de inbox"""
    total = 0
    if data:
        try:
            user_id = data['userId']
        except Exception:
            user_id = data['user_id']
        indice = type_user(user_id)

        if indice != 'error':
            data_c = {
                'documentType': '',
                'expedientId': '',
                'documentNumber': '',
                'fullName': '',
                'resourceType': '',
                'state': '',
                'aaId': '',
                'subjectiveCondition':'',
                'resourceDecision':'',
                'decisionDetail':'',
                'subgroup':'',
            }

            copyOfData = dict(data)
            for (key, value) in copyOfData.items():
                data_c[key] = value

            query_filter = " "

            if data_c['documentType'] != '':
                query_filter += " AND upper(document_type) = upper('{}') ".format(
                    data_c['documentType'])

            if data_c['documentNumber'] != '':
                query_filter += " AND document_number = '{}' ".format(
                    data_c['documentNumber'])

            if data_c['fullName'] != '':
                query_filter += " AND upper(full_name) like upper('%{}%') ".format(
                    data_c['fullName'])

            if data_c['expedientId'] != '':
                query_filter += " AND person_expedient_id = {} ".format(
                    data_c['expedientId'])

            if data_c['resourceType'] != '':
                query_filter += " AND tipo_recurso like upper('%{}%') ".format(
                    data_c['resourceType'])

            if data_c['state'] != '':
                query_filter += " AND upper(resource_state) = upper('{}') ".format(
                    data_c['state'])

            if data_c['resourceDecision'] != '':
                query_filter += " AND resource_decision = '{}'".format(
                    data_c['resourceDecision'])

            if data_c['decisionDetail'] != '':
                query_filter += " AND resource_state = '{}'".format(
                    data_c['decisionDetail'])
                
            if data_c['subjectiveCondition'] != '':
                query_filter += " AND subjective_condition = '{}_{}'".format(
                    data_c['decisionDetail'],data_c['subgroup'])

            if data_c['aaId'] != '':
                query_filter += " AND upper(aa_id) = upper('{}') ".format(
                    data_c['aaId'])

            user_assign=''
            if indice < 2:
                # si son los secretarios haga proceso de inbox
                inbox_secretary(user_id, indice)            
                # si son los secretarios reviso de quien es el expediente
                user_assign = 'and user_id = {}'.format(user_id)

            state = LIST_ROLES['next_state'][indice]
            query = text("""select count(*)
            from (
            select 
                person_expedient_id,
                full_name,
                string_agg(tipo_recurso,'-') tipo_recurso,
                case 
	                when expedient_substantiation_decision='DENEGACION' then 'DENEGADO'
	                when expedient_substantiation_decision='CONCESION_PARCIAL' then 'CONCEDIDO'
	                when expedient_substantiation_decision='CONCESION_TOTAL' then 'CONCEDIDO'
	                when expedient_substantiation_decision is null then resource_decision
                end resource_decision,
                case 
	                when expedient_substantiation_decision='DENEGACION' then 'DENEGADO'
	                when expedient_substantiation_decision='CONCESION_PARCIAL' then 'PARCIAL'
	                when expedient_substantiation_decision='CONCESION_TOTAL' then 'TOTAL'
	                when expedient_substantiation_decision is null then resource_state
				end resource_state,
                final_decision,
                expedient_substantiation_decision,
                aa_id,
                document_type ,
                document_number,
                claimer_id,
                notification_id ,
                claimer_notified
                from 
                (
                    select fdc.claimer_id,
                    cud.expedient_id,
                    cud.person_expedient_id,
                    cud.document_type ,
                    cud.document_number,
                    case 
                        when ces.expedient_round = 'PRIMERA' then result_dj2
                        when ces.expedient_round = 'SEGUNDA' then result_dj2_appeal
                    end subjective_condition,
                    case 
                        when ces.expedient_round = 'PRIMERA' then final_decision
                        when ces.expedient_round = 'SEGUNDA' then final_decision_appeal
                    end decision_detail,
                    crs.resource_id,
                    crs.resource_state rs ,
                    d.aggregated_group_text resource_decision,
                    d.visual_user_text resource_state,
                    crd.resource_type tipo_recurso,
                    ces.state estado_expediente,
                    fdc.final_decision,  
                    fdc.expedient_substantiation_decision,
                    aa.aa_id,
                    cud.full_name,
                    cn.notification_id ,
                    cn.claimer_notified 
                    from 
                    ( --TABLA QUE TRAE TODOS LOS EXPEDIENTES QUE SE ENCUENTRAN EN EL ESTADO QUE INGRESA COMO PARAMETRO
                        select claimer_id 
                        from phase_2.claimer_workflow 
                        where current_state = :state -- OJO!!! ESTE ES EL ESTADO EN EL QUE ESTÁ SITUADO EL USUARIO ---
                        and   
                        end_date is null {}
                    ) cw 
                    -- TABLA DE DATOS DE SOLICITANTE
                    inner join phase_2.claimer_user_data cud 
                    on cud.claimer_id = cw.claimer_id
                    --TABLA DE DECISIONES POR EXPEDIENTE
                    inner join final_act.final_decision_claimer fdc  
                    on cw.claimer_id = fdc.claimer_id
                    inner join 
                    ( --TABLA QUE VERIFICA EN QUE RONDA VA EL EXPEDIENTE PARA PODER MOSTRAR LA INFORMACION
                        select claimer_id, expedient_round , state
                        from phase_2.claimer_expedient_state ces 
                        where end_date is null
                    ) ces 
                    on cw.claimer_id = ces.claimer_id 
                    -- TABLA DE ACTO ADMINISTRATIVO
                    inner join phase_2.administrative_act aa
                    on cw.claimer_id = aa.claimer_id and aa.expedient_round = ces.expedient_round
                    --TABLA DE RECURSOS POR ACTO ADMINISTRATIVO
                    inner join phase_2.resource_per_administrative_act rpaa
                    on rpaa.aa_id = aa.aa_id
                    -- TABLA DE ESTADOS DE RECURSO
                    inner join (select * from  phase_2.claimer_resource_state where end_date is null) crs
                    on rpaa.resource_id = crs.resource_id
                    inner join core."domain" d 
                    on d.code = crs.resource_state and d.name='RESOURCE STATE'
                    -- TABLA DE RECURSOS
                   inner join phase_2.claimer_resource_document crd
                    on crd.resource_id = rpaa.resource_id
                  left outer join phase_2.claimer_notification cn 
                    on cn.claimer_id = cw.claimer_id and cn.aa_id = aa.aa_id and cn.claimer_notified is true 
                 ) f
                where person_expedient_id is not null  and notification_id is null 
                group by  person_expedient_id,full_name,
                resource_decision,resource_state,final_decision,expedient_substantiation_decision,
                aa_id,document_type ,document_number,claimer_id ,  notification_id ,           claimer_notified  
                ) F 
                where claimer_id is not null {} """.format(user_assign,query_filter))
            try:
                total = db.engine.execute(query, state=state).first()
                db.session.close()
                if total:
                    return total[0]
                else:
                    return 0
            except Exception as error:
                db.session.close()
                print(error)
                return 0
    return 0


def list_to_signature(data):
    """se busca el tipo de usuario y se saca el listado de inbox"""
    if data:
        try:
            user_id = data['userId']
        except Exception:
            user_id = data['user_id']

        indice = type_user(user_id)
        list_for_signature = ""

        if indice != 'error':
            data_c = {
                'documentType': '',
                'expedientId': '',
                'documentNumber': '',
                'fullName': '',
                'resourceType': '',
                'state': '',
                'aaId': '',
                'subjectiveCondition':'',
                'resourceDecision':'',
                'decisionDetail':'',
                'subgroup':'',
                'page': 1,
                'perPage': 10,
                'orderCriteria': 'TYPE',
                'sortingOrder': 'DESC',
            }

            copyOfData = dict(data)
            for (key, value) in copyOfData.items():
                data_c[key] = value

            query_filter = " "

            if data_c['orderCriteria'] == 'NAME':
                data_c['orderCriteria'] = 'full_name'
            elif data_c['orderCriteria'] == 'TYPE':
                data_c['orderCriteria'] = 'document_type'
            else:
                data_c['orderCriteria'] = 'document_number'

            if data_c['documentType'] != '':
                query_filter += " AND upper(document_type) = upper('{}') ".format(
                    data_c['documentType'])

            if data_c['documentNumber'] != '':
                query_filter += " AND document_number = '{}' ".format(
                    data_c['documentNumber'])

            if data_c['fullName'] != '':
                query_filter += " AND upper(full_name) like upper('%{}%') ".format(
                    data_c['fullName'])

            if data_c['expedientId'] != '':
                query_filter += " AND person_expedient_id = {} ".format(
                    data_c['expedientId'])

            if data_c['resourceType'] != '':
                query_filter += " AND tipo_recurso like upper('%{}%') ".format(
                    data_c['resourceType'])

            if data_c['state'] != '':
                query_filter += " AND upper(resource_state) = upper('{}') ".format(
                    data_c['state'])

            if data_c['resourceDecision'] != '':
                query_filter += " AND resource_decision = '{}'".format(
                    data_c['resourceDecision'])

            if data_c['decisionDetail'] != '':
                query_filter += " AND resource_state = '{}'".format(
                    data_c['decisionDetail'])
                
            if data_c['subjectiveCondition'] != '':
                query_filter += " AND subjective_condition = '{}_{}'".format(
                    data_c['decisionDetail'],data_c['subgroup'])

            if data_c['aaId'] != '':
                query_filter += " AND upper(aa_id) = upper('{}') ".format(
                    data_c['aaId'])

            
            user_assign=''
            if indice < 2:
                # si son los secretarios reviso de quien es el expediente
                user_assign = 'and user_id = {}'.format(user_id)
            
            state = LIST_ROLES['next_state'][indice]
            query = """select *
            from (
            select 
                person_expedient_id,
                full_name,
                string_agg(tipo_recurso,'-') tipo_recurso,
                case 
	                when expedient_substantiation_decision='DENEGACION' then 'DENEGADO'
	                when expedient_substantiation_decision='CONCESION_PARCIAL' then 'CONCEDIDO'
	                when expedient_substantiation_decision='CONCESION_TOTAL' then 'CONCEDIDO'
	                when expedient_substantiation_decision is null then resource_decision
                end resource_decision,
                case 
	                when expedient_substantiation_decision='DENEGACION' then 'DENEGADO'
	                when expedient_substantiation_decision='CONCESION_PARCIAL' then 'PARCIAL'
	                when expedient_substantiation_decision='CONCESION_TOTAL' then 'TOTAL'
	                when expedient_substantiation_decision is null then resource_state
				end resource_state,
                final_decision,
                expedient_substantiation_decision,
                aa_id,
                document_type ,
                document_number,
                claimer_id,
                notification_id ,
                claimer_notified
                from 
                (
                    select fdc.claimer_id,
                    cud.expedient_id,
                    cud.person_expedient_id,
                    cud.document_type ,
                    cud.document_number,
                    case 
                        when ces.expedient_round = 'PRIMERA' then result_dj2
                        when ces.expedient_round = 'SEGUNDA' then result_dj2_appeal
                    end subjective_condition,
                    case 
                        when ces.expedient_round = 'PRIMERA' then final_decision
                        when ces.expedient_round = 'SEGUNDA' then final_decision_appeal
                    end decision_detail,
                    crs.resource_id,
                    crs.resource_state rs ,
                    d.aggregated_group_text resource_decision,
                    d.visual_user_text resource_state,
                    crd.resource_type tipo_recurso,
                    ces.state estado_expediente,
                    fdc.final_decision,  
                    fdc.expedient_substantiation_decision,
                    aa.aa_id,
                    cud.full_name,
                    cn.notification_id ,
                    cn.claimer_notified 
                    from 
                    ( --TABLA QUE TRAE TODOS LOS EXPEDIENTES QUE SE ENCUENTRAN EN EL ESTADO QUE INGRESA COMO PARAMETRO
                        select claimer_id 
                        from phase_2.claimer_workflow 
                        where current_state = :state -- OJO!!! ESTE ES EL ESTADO EN EL QUE ESTÁ SITUADO EL USUARIO ---
                        and   
                        end_date is null {}
                    ) cw 
                    -- TABLA DE DATOS DE SOLICITANTE
                    inner join phase_2.claimer_user_data cud 
                    on cud.claimer_id = cw.claimer_id
                    --TABLA DE DECISIONES POR EXPEDIENTE
                    inner join final_act.final_decision_claimer fdc  
                    on cw.claimer_id = fdc.claimer_id
                    inner join 
                    ( --TABLA QUE VERIFICA EN QUE RONDA VA EL EXPEDIENTE PARA PODER MOSTRAR LA INFORMACION
                        select claimer_id, expedient_round , state
                        from phase_2.claimer_expedient_state ces 
                        where end_date is null
                    ) ces 
                    on cw.claimer_id = ces.claimer_id
                    -- TABLA DE ACTO ADMINISTRATIVO
                    inner join phase_2.administrative_act aa
                    on cw.claimer_id = aa.claimer_id and aa.expedient_round = ces.expedient_round
                    --TABLA DE RECURSOS POR ACTO ADMINISTRATIVO
                    inner join phase_2.resource_per_administrative_act rpaa
                    on rpaa.aa_id = aa.aa_id
                    -- TABLA DE ESTADOS DE RECURSO
                    inner join (select * from  phase_2.claimer_resource_state where end_date is null) crs
                    on rpaa.resource_id = crs.resource_id
                    inner join core."domain" d 
                    on d.code = crs.resource_state and d.name='RESOURCE STATE'
                    -- TABLA DE RECURSOS
                   inner join phase_2.claimer_resource_document crd
                    on crd.resource_id = rpaa.resource_id
                  left outer join phase_2.claimer_notification cn 
                    on cn.claimer_id = cw.claimer_id and cn.aa_id = aa.aa_id and cn.claimer_notified is true 
                 ) f
                where person_expedient_id is not null  and notification_id is null 
                group by  person_expedient_id,full_name,
                resource_decision,resource_state,final_decision,expedient_substantiation_decision,
                aa_id,document_type ,document_number,claimer_id ,  notification_id ,           claimer_notified  
                ) F 
                where claimer_id is not null {} 
                order by {} {}
                LIMIT :perPage OFFSET :page""".format(user_assign,query_filter, data_c['orderCriteria'], data_c['sortingOrder'])
            try:
                list_for_signature = db.engine.execute(
                    text(query), state=state, perPage=data_c['perPage'], page=(data_c['page'] - 1) * data_c['perPage']).fetchall()
                db.session.close()

                # if indice < 2:
                #     for expedient in list_for_signature:
                #         query = text("select cw.claimer_id, aa.aa_id from phase_2.claimer_workflow cw \
                #         inner join phase_2.administrative_act aa on aa.claimer_id = cw.claimer_id \
                #         where cw.current_state = :state and cw.claimer_id = :claimer and cw.end_date is null group by cw.claimer_id, aa.aa_id")
                #         list_to_document = db.engine.execute(
                #             query, state=LIST_ROLES['next_state'][indice], claimer=expedient.claimer_id).fetchall()

                #         for act_list in list_to_document:
                #             create_document(
                #                 act_list.claimer_id, act_list.aa_id)

            except Exception as error:
                print(error)
                return manage_db_exception("Error en #list_to_signature",
                                           "Error listando en response_service", error)
            if list_for_signature:
                return list_for_signature
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'no found',
                }
            return response_object
        else:
            response_object = {
                'status': 'fail',
                'message': 'no user to find',
            }
    else:
        response_object = {
            'status': 'fail',
            'message': 'no data to find',
        }
    return response_object, 204


def list_expedients_aa(aa_id):
    """se lista los recursos seleccionados para el expediente"""
    query = text("select crd.resource_type,  cd.radicate_number, crd.resource_radication_date, \
    cd.url_document,crs.resource_state from phase_2.claimer_resource_document crd \
    inner join phase_2.claimer_documents cd on cd.document_id = crd.resource_document_id\
    inner join phase_2.resource_per_administrative_act rpaa on rpaa.resource_id = crd.resource_id\
    inner join phase_2.claimer_resource_state crs on crs.resource_id = rpaa.resource_id\
    where rpaa.aa_id = :aa_id and crs.end_date is null")
    list_for_signature = db.engine.execute(query, aa_id=aa_id).fetchall()
    return list_for_signature


def approve_to_signature(data):
    """aprobar documento para firmar"""
    indice = type_user(data['userId'])
    if indice != 'error':
        next_state = LIST_ROLES['approve'][indice]
        if 'step' in data:
            if data['step'] == 1:
                
                # cambio de estado el expediente para la firma
                cont = 0
                array = []
                for expedient in data['expedients']:
                    transition = {
                        'claimerId': expedient['claimerId'],
                        'nextState': next_state,
                        'nextUser': 10000,
                        'logUser': data['userId']
                    }
                    make_claimer_transition(transition)
                return "Se están procesando los documentos para firmar, cuando finalice recibirá un correo con los links de firma"
            if data['step'] == 2:
                # cambio de estado el expediente para la firma
                cont = 0
                array = []
                for expedient in data['expedients']:
                    act = Aa.query.filter(Aa.aa_id == expedient['aaId']).first()
                    if act:
                        if indice < 2:
                            act.approve_administrative_act_user_id = data['userId']
                            act.approve_administrative_act_date = datetime.datetime.now()
                        else:
                            act.sign_administrative_act_user_id = data['userId']
                            act.sign_administrative_act_date = datetime.datetime.now()
                            if act.aa_document_format_id:
                                try:
                                    ind = LIST_DOCUMENT['id_juridica'].index(act.aa_document_format_id)
                                    type_document = LIST_DOCUMENT['id_jasper'][ind]
                                except Exception as e:
                                    type_document = 66
                            else:
                                type_document = 66
                            document = create_document_jasper(expedient, type_document)
                            act.aa_document_id = document['document_id']
                            if document != 'error':
                                array.append({
                                    'route': document['route'],
                                    'document_id': document['document_id'], 
                                    'aa_id': expedient['aaId']
                                })
                                cont += 1
                            if cont >= 50:
                                signature = Signature(array, data['userId'])
                                signature.upload_file()
                                cont = 0
                                array = []
                        save_changes(act)
                if cont > 0:
                    signature = Signature(array, data['userId'])
                    signature.upload_file()
                signature.send_mail_to_sign()
        else:
            #proceso para secretarios 
            for expedient in data['expedients']:
                transition = {
                    'claimerId': expedient['claimerId'],
                    'nextState': next_state,
                    'nextUser': 10000,
                    'logUser': data['userId']
                }
                make_claimer_transition(transition)
                act = Aa.query.filter(Aa.aa_id == expedient['aaId']).first()
                if act:
                    act.approve_administrative_act_user_id = data['userId']
                    act.approve_administrative_act_date = datetime.datetime.now()
                    save_changes(act)
            return 'proceso finalizado'
    return 'proceso finalizado'


def create_document(claimer_id, aa):

    resources = Rpea.query.filter(Rpea.aa_id == aa).all()
    array_resource = [int(resource.resource_id) for resource in resources]
    query = text(
        "select final_act.calc_denied_text(:claimer_id, :array, :aa, '')")
    texto = db.engine.execute(query.execution_options(autocommit=True), claimer_id=claimer_id, aa=aa,
                              array=array_resource).fetchall()
    print(texto)

    return 'ok'


def create_document_jasper(expedient,id_document):
    """proceso para firmar"""
    if id_document:
        document_created = ReportJasperMasivo.create_response(id_document, expedient)
    else: 
        return 'error'
    print(document_created)
    return document_created


def list_observation(expedient_id):
    observations = cea.query.filter(cea.claimer_id == expedient_id).all()
    return observations


def save_observation(data):
    try:
        observations = cea()
        observations.claimer_id = data['claimerId']
        observations.annotation_date = datetime.datetime.now()
        observations.annotation = data['observation']
        observations.user_id = data['userId']
        save_changes(observations)
        response_object = {
            'status': 'ok',
            'message': 'observación guardada',
        }
    except Exception as e:
        return manage_db_exception("Error en #save_observation",
                                   "Error guardando en claimer_expedient_annotation", e)
    return response_object, 200


def update_observation(observation_id, data):
    try:
        observations = cea.query.filter(
            cea.claimer_expedient_annotation_id == observation_id).one()
        if not observations:
            response_object = {
                'status': 'success',
                'message': 'La observación no existe.'
            }
        else:
            observations.annotation_date = datetime.datetime.now()
            observations.annotation = data['observation']
            save_changes(observations)
            response_object = {
                'status': 'ok',
                'message': 'observación editada',
            }
    except Exception as e:
        return manage_db_exception("Error en #update_observation",
                                   "Error obteniendo claimer_expedient_annotation", e)

    return response_object, 200


def delete_observation(observation_id):
    try:
        observations = cea.query.filter(
            cea.claimer_expedient_annotation_id == observation_id).first()
        if not observations:
            response_object = {
                'status': 'success',
                'message': 'La observación no existe.'
            }
        else:
            db.session.delete(observations)
            db.session.commit()
            response_object = {
                'status': 'ok',
                'message': 'observación eliminada',
            }
    except Exception as e:
        return manage_db_exception("Error en #delete_observation",
                                   "Error obteniendo claimer_expedient_annotation", e)
    return response_object, 200


def inbox_secretary(user_id, indice):
    """se asigna el inbox a los secretarios"""
    # funcion que hace reparto a los estados iniciales
    queryOne = text("select * from phase_2.f_set_dp_inbox_roles()")
    db.engine.execute(queryOne.execution_options(autocommit=True)).fetchall()
    #proceso nuevo para asignación a todos los secretarios con el mismo rol
    query = text("select * FROM phase_2.f_set_dp_inbox_secretary(:rol,:next,:id)")
    db.engine.execute(query.execution_options(autocommit=True), 
    rol=LIST_ROLES['name'][indice], 
    next=LIST_ROLES['next_state'][indice],
    id=LIST_ROLES['state_assign'][indice]).fetchall()   

    # if list_for_assign:
    #     # se asignan los expedientes
    #     for expedient in list_for_assign:
    #         data_transition = {
    #             'claimerId': expedient.claimer,
    #             'nextState': LIST_ROLES['next_state'][indice],
    #             'nextUser': user_id,
    #             'logUser': 10000
    #         }
    #         result = make_claimer_transition(data_transition)

            # query = text("select cw.claimer_id, aa.aa_id from phase_2.claimer_workflow cw \
            # inner join phase_2.administrative_act aa on aa.claimer_id = cw.claimer_id \
            # where cw.current_state = :state and cw.claimer_id = :claimer and cw.end_date is null group by cw.claimer_id, aa.aa_id")
            # list_to_document = db.engine.execute(
            #     query, state=LIST_ROLES['next_state'][indice], claimer=expedient.claimer).fetchall()

            # for act_list in list_to_document:
            #     create_document(act_list.claimer_id, act_list.aa_id)

            # try:
            #     if result[0]['status'] == 'success':
            #         new_assigned = new_assigned + 1
            # except Exception as error:
            #     print('error al hacer transicion de estado: {}'.format(error))
            # if new_assigned >= 10:
            #     break
    return 'ok'



def type_user(user_id):
    """se saca el rol de cada usuario y se devuelve el indice de LIST_ROLES"""
    # role = devuelve los roles a asignar tareas
    role = Users.query\
        .join(UserRole)\
        .join(Role, UserRole.role_id == Role.role_id).filter(Role.short_description.in_(LIST_ROLES['name']), Users.id == user_id).first()
    if role:
        for role in marshal(role.roles, UserRoleDto.user_role):
            return LIST_ROLES['name'].index(role['shortDescription'])
    else:
        return 'error'


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as error:
        print(error)
        newLogger = loggingBDError(loggerName='audit_service')
        db.session.rollback()
        newLogger.error(error)

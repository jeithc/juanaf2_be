from flask_restplus import Resource
from flask import request
from app.main.util.decorator import audit_init, audit_finish
from app.main.genereate_resolution.services.response_services import *
from app.main.genereate_resolution.util.dto.response_dto import ResponseDTO


api = ResponseDTO.api


@api.route('/list_expedients_signature')
class toSignature(Resource):
    @audit_init
    @audit_finish
    @api.expect(ResponseDTO.ListToFilter, validate=False)
    @api.doc('lista de expedientes para firmar')
    @api.marshal_with(ResponseDTO.ListExpedients)
    def post(self):
        """generar lista de expedientes para firmar"""
        data = request.json
        return list_to_signature(data)


@api.route('/number_list_expedients_signature')
class toSignature(Resource):
    @audit_init
    @audit_finish
    @api.expect(ResponseDTO.ListToFilter, validate=False)
    @api.doc('total lista de expedientes para firmar')
    def post(self):
        """generar total lista de expedientes para firmar"""
        data = request.json
        return number_list_to_signature(data)


@api.route('/number_list_expedients_signature/<user_id>')
@api.param('user_id', 'the user to filter list')
class toSignature(Resource):
    @audit_init
    @audit_finish
    @api.doc('total lista de expedientes para firmar')
    def get(self, user_id):
        """generar lista de expedientes para firmar"""
        return number_list_to_signature(user_id)


@api.route('/list_resource_expedients/<aa_id>')
@api.param('aa_id', 'the act number to expedients')
class toListExpedient(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(ResponseDTO.ListResource)
    @api.doc('lista de recursos del acto')
    def get(self, aa_id):
        """generar lista de expedientes"""
        return list_expedients_aa(aa_id)


@api.route('/approve')
class toASignature(Resource):
    @audit_init
    @audit_finish
    @api.doc('aprobar firma')
    def post(self):
        """enviar documento para firma final"""
        data = request.json
        return approve_to_signature(data)


@api.route('/sign_document')
class signDocument(Resource):
    @audit_init
    @audit_finish
    @api.doc('firmar documento')
    def post(self):
        """firmar documento"""
        data = request.json
        return approve_to_signature(data)


@api.route('/observation/<expedient_id>')
@api.param('expedient_id', 'id de expediente para buscar')
class observationList(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(ResponseDTO.listObservation)
    @api.doc('listar observaciones')
    def get(self, expedient_id):
        """list observaciones"""
        return list_observation(expedient_id)


@api.route('/observation/<observation_id>')
@api.param('observation_id', 'id de observación para buscar')
class observationList(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(ResponseDTO.listObservation)
    def put(self, observation_id):
        """list observaciones"""
        data = request.json
        return update_observation(observation_id, data)

    def delete(self, observation_id):
        """list observaciones"""
        return delete_observation(observation_id)


@api.route('/observation')
class observationSave(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(ResponseDTO.listObservation)
    @api.doc('guardar observaciones')
    def post(self):
        """save observaciones  """
        data = request.json
        return save_observation(data)

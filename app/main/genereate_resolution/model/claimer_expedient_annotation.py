from app.main import db
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, Integer, ForeignKey

class ClaimerExpedientAnnotation(db.Model):
    """claimer_expedient_annotation model"""
    __tablename__ = 'claimer_expedient_annotation'
    __table_args__ = {'schema': 'phase_2'}

    claimer_expedient_annotation_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    annotation_date = db.Column(db.DateTime)
    annotation = db.Column(db.String)
    user_id = db.Column(db.Integer)


    def __repr__(self):
        return "<cea {}, aa {}>".format(self.claimer_expedient_annotation_id, self.annotation_date)
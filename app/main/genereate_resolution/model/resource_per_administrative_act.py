from app.main import db
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, Integer, ForeignKey


class ResourcePerAdministrativeAct(db.Model):
    """resource_per_administrative_act model"""
    __tablename__ = 'resource_per_administrative_act'
    __table_args__ = {'schema': 'phase_2'}

    rpaa_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aa_id = db.Column(db.Integer)
    resource_id = db.Column(db.Integer)


    def __repr__(self):
        return "<resource {}, aa {}>".format(self.aa_id, self.resource_id)
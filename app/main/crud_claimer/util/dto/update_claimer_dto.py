from flask_restplus import Namespace, fields
from app.main.citation.util.dto.subgroup_citation_dto import SubgroupCitationDto
from app.main.citation.util.dto.aa_dto import AaDto


class UpdateClaimerDto:
    api = api = Namespace(
        'updateclaimer',
        description='Operaciones relacionadas con la actualizacion de datos de usuario')

    updateclaimer = api.model('citation', {
        'citationId': fields.Integer(attribute='citation_id', description='id de la descripción'),
        'claimerId': fields.Integer(attribute='claimer_id', description='id del reclamante'),
        'aaId': fields.Integer(attribute='aa_id', description='id del acto administrativo'),
        'subgroupId': fields.Integer(attribute = 'subgroup_id'),
        'subgroupCitation': fields.Nested(SubgroupCitationDto.subgroup_citation, description=" id subgroup"),
        'aa': fields.Nested(AaDto.aa, description=" id subgroup")

    
    })

    claimer = api.model('user1', {
        'claimerId': fields.Integer( attribute= 'claimer_id', description='claimer_id'),
        'expedientId': fields.Integer( attribute='expedient_id'),
        'firstName': fields.String( attribute='first_name'),
        'middleName': fields.String( attribute='middle_name'),
        'lastName': fields.String( attribute='last_name'),
        'maidenName': fields.String( attribute='maiden_name'),
        'documentType': fields.String( attribute='document_type'),
        'documentNumber': fields.String( attribute='document_number'),
        'email': fields.String( attribute='email'),
        'idNatgeo': fields.Integer( attribute='id_natgeo'),
        'idBogLocNeig': fields.Integer( attribute='id_bog_loc_neig'),
        'urbanRuralLocation': fields.String( attribute='urban_rural_location'),
        'address': fields.String( attribute='address'),
        'phone1': fields.String( attribute='phone1'),
        'phone2': fields.String( attribute='phone2'),
        'username': fields.String( description='username'),
        'changeDescription': fields.String( attribute='changeDescription'),
        'authorizeElectronicNotificati': fields.Boolean( attribute='authorize_electronic_notificati'),
        #'claimerState': fields.String( attribute='claimer_state'),
        'expeditionDateAvailable': fields.Boolean( attribute='expedition_date_available'),
        'expeditionDocumentDate': fields.Date( attribute='expedition_document_date'),
        'birthDate': fields.Date( attribute='birth_date'),
        'personExpedientId': fields.Integer( attribute='person_expedient_id'),
        'approvedQuestion': fields.Boolean( attribute='approved_questions'),
        #'causeNonRegistration': fields.Boolean( attribute='cause_non_registration'),
        'registeredClaimer': fields.Boolean( attribute='registered_claimer'),
        'currentState': fields.Integer(attribute='current_state', description='Estado actual en el que se encuentra el aspirante'),
        'alternateEmail': fields.String( attribute='alternate_email')

    })




from app.main import db
from sqlalchemy.orm import relationship
from app.main.citation.model import subgroup_citation
from sqlalchemy import Table, Column, Integer, ForeignKey


class Dj1Dj2(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "cambio_datos_plataformas_dj1_dj2"
    __table_args__ = {'schema':'core'}
    change_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    expedient_id = db.Column(db.Integer)
    taiga_issue = db.Column(db.Integer)
    date_of_attention = db.Column(db.Date)
    change_description = db.Column(db.String(255))
    user_id = db.Column(db.Integer)

from flask import request
from flask_restplus import Resource
from app.main.crud_claimer.service import update_claimer_service
from app.main.crud_claimer.util.dto.update_claimer_dto import UpdateClaimerDto
from app.main.util.decorator import audit_init, audit_finish, login_app


api = UpdateClaimerDto.api


@api.route('/updateclaimer')
class SaveClamer(Resource):
    @api.doc('get updateclaimer')
    @audit_init
    @audit_finish
    def put(self):
        
        """Update  User """
        data = request.json
        user_email = request.headers.environ['HTTP_USER_ID']
        return update_claimer_service.update(data, user_email)   


@api.route('/updateclaimer/<claimer_id>')
@api.param('claimer_id', 'The User identifier')
class User(Resource):
    @api.doc('get user data')
    @api.marshal_with(UpdateClaimerDto.claimer, skip_none=False)
    #@login_app
    @audit_init
    @audit_finish
    def get(self, claimer_id):
        user = update_claimer_service.get_a_user(claimer_id)
        return user


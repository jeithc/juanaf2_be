from app.main import db
import datetime

from app.main.model.user import User, Expedients
from app.main.model.questions import Answers
from app.main.service.logging_service import loggingBDError
from app.main.crud_claimer.model.dj1_dj2 import Dj1Dj2
from app.main.core.model.users import Users


def update(data , user_email):
    user = User.query.filter_by(claimer_id=data['claimerId']).first()
    user_system = Users.query.filter_by(email=user_email).first()
    
    if user:
        if 'firstName' in data:
            user.first_name = data['firstName']

        if 'middleName' in data:
            user.middle_name = data['middleName']

        if 'lastName' in data:
            user.last_name = data['lastName']

        if 'maidenName' in data:
            user.maiden_name = data['maidenName']

        if 'documentType' in data:
            user.document_type = data['documentType']

        if 'documentNumber' in data:
            user.document_number = data['documentNumber']

        if 'email' in data:
            user.email = data['email']

        if 'idNatgeo' in data:
            if data['idNatgeo'] != 0:
                user.id_natgeo = data['idNatgeo']

        if 'idBogLocNeig' in data:
            if data['idBogLocNeig'] != 0:
                user.id_bog_loc_neig = data['idBogLocNeig']

        if 'address' in data:
            user.address = data['address']

        if 'phone1' in data:
            user.phone1 = data['phone1']

        if 'phone2' in data:
            user.phone2 = data['phone2']
        else:
            user.phone2 = ''

        if 'expedition_date_available' in data:
            user.expedition_date_available = data['expedition_date_available']

        if 'expeditionDocumentDate' in data:
            user.expedition_document_date = data['expeditionDocumentDate']

        if 'birthDate' in data:
            user.birth_date = data['birthDate']

        if 'urbanRuralLocation' in data:
            user.urban_rural_location = data['urbanRuralLocation']

        if 'authorizedNewsDataTreatment' in data:
            user.authorized_news_data_treatment = data['authorizedNewsDataTreatment']

        if 'alternateEmail' in data:
            user.alternate_email = data['alternateEmail']
        else:
            user.alternate_email = ''
        user.full_name = generate_full_name(user)
        user.username = user.document_type + user.document_number

        expedientsList = Expedients.query.filter_by(expedient_id=user.expedient_id).all()
        for expedients in expedientsList:

            if expedients:
                if 'firstName' in data:
                    expedients.first_name = data['firstName']

                if 'middleName' in data:
                    expedients.middle_name = data['middleName']

                if 'lastName' in data:
                    expedients.last_name = data['lastName']

                if 'maidenName' in data:
                    expedients.maiden_name = data['maidenName']

                if 'documentType' in data:
                    expedients.doc_type = data['documentType']

                if 'documentNumber' in data:
                    expedients.doc_number = data['documentNumber']

                expedients.full_name = generate_full_name(expedients)

                save_changes(expedients)

        expedition_date = Answers.query.filter_by(person_expedient_id=user.person_expedient_id,
                                                  registration_question_id=1).first()
        if expedition_date:

            save_answers(expedition_date, data, False, 1)
        else:
            expedition_date = Answers()

            save_answers(expedition_date, data, True, 1)

        birth_date = Answers.query.filter_by(person_expedient_id=user.person_expedient_id,
                                             registration_question_id=3).first()
        if birth_date:

            save_answers(birth_date, data, False, 3)
        else:
            birth_date = Answers()

            save_answers(birth_date, data, True, 3)

        dj1dj2 = Dj1Dj2()

        if 'claimerId' in data:
            dj1dj2.claimer_id = data['claimerId']

        if 'changeDescription' in data:
            dj1dj2.change_description = data['changeDescription']

        dj1dj2.date_of_attention = datetime.datetime.now()
        dj1dj2.expedient_id = user.expedient_id
        
        if user_system:
            dj1dj2.user_id = user_system.id

        save_changes(dj1dj2)

        try:
            save_changes(user)
            response_object = {
                'status': 'success',
                'message': 'Usuario actualizado con éxito',
            }
        except Exception as e:
            response_object = {
                'status': 'Fail',
                'message': 'Usuario NO actualizado con éxito',
            }

        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'User no exist in BD.',
        }
        return response_object, 204


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='audit_service')
        db.session.rollback()
        newLogger.error(e)


def get_a_user(claimer_id):
    claimer = User.query.filter_by(claimer_id=claimer_id).first()
    if claimer:
        return claimer
    else:
        response_object = {
            'status': 'fail',
            'message': 'user no found',
        }
        return response_object, 204


def generate_full_name(obj):
    filterobj = [obj.first_name, obj.middle_name, obj.last_name, obj.maiden_name]
    obj = list(filter(None, filterobj))
    return ' '.join(obj)
    
    
    



def save_answers(answer, data, new, date_type):
    if 'lastName' in data:
        answer.last_name = data['lastName']

    if 'maidenName' in data:
        answer.maiden_name = data['maidenName']
    if date_type == 1:
        if 'expeditionDocumentDate' in data:
            answer.answer = data['expeditionDocumentDate']
    else:
        if 'birthDate' in data:
            answer.answer = data['birthDate']

    if new:
        answer.person_expedient_id = data['personExpedientId']
        answer.act_type = 1
        answer.registration_question_id = date_type

    save_changes(answer)

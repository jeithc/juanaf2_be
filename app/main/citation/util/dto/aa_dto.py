from flask_restplus import Namespace, fields


class AaDto:
    api = Namespace('aa', description='operaciones relacionadas con la tabla administrative_act')

    aa = api.model('aa', {
        'aaId': fields.Integer(attribute='aa_id', description="id del acto"),
        'description': fields.String(attribute='description', description="Descripcion"),
        'aaNumber': fields.String(attribute='aa_number', description="numero de acto"),
        'expeditionDate': fields.String(attribute='expedition_date', description="Fecha registro acto"),
        'digitalSignatureDate': fields.String(attribute='digital_signature_date', description="Fecha firma digital acto")
    })

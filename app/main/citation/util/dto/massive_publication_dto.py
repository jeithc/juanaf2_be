from flask_restplus import Namespace, fields
from app.main.util.dto import DocumentDTO


class MassivePublicationDto:
    api = Namespace('massive_publication', description='operaciones relacionadas con la tabla massive_publication')

    massive_publication = api.model('massive_publication', {
        'publicationId': fields.Integer(attribute='publication_id', description="id del subgrupo"),
        'aaId': fields.Integer(attribute='aa_id', description="id del sitio"),
        'publicationBeginDate': fields.Date(attribute='publication_begin_date',
                                            description="fecha inicio de la citacion"),
        'publicationEndDate': fields.Date(attribute='publication_end_date', description="fecha inicio de la citacion"),
        'generatorDocumentId': fields.Integer(attribute='generator_document_id', description="id de la publicacion"),
        'generatorDocument': fields.Nested(DocumentDTO.documents, description="Documento asociado a la publicacion"),
        'publicationType': fields.String(attribute='publication_type', description="Direccion del sitio"),
        'publicationState': fields.String(attribute='publication_state', description="Direccion del sitio")

    })

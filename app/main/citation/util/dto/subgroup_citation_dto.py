from flask_restplus import Namespace, fields
from app.main.citation.util.dto.place_dto import PlaceDto
from app.main.citation.util.dto.massive_publication_dto import MassivePublicationDto


class SubgroupCitationDto:
    api = Namespace('subgroup', description='operaciones relacionadas con la tabla citation')

    subgroup_citation = api.model('subgroup_citation', {
        'subgroupId': fields.Integer(attribute='subgroup_id', description="id del subgrupo"),
        'placeId': fields.Integer(attribute='place_id', description="id del sitio"),
        'place': fields.Nested(PlaceDto.place, description=" id apoderado de la notificacion"),
        'aaId': fields.Integer(attribute='aa_id', description="id del acto administrativo"),
        'notificationDeadlin': fields.String(attribute='notification_deadlin'),
        'citationType': fields.String(attribute='citation_type'),
        'numberOfClaimers': fields.Integer(attribute='number_of_claimers'),
        'beginDateSubgroup': fields.Date(attribute='begin_date_subgroup', description="fecha inicio de la citacion"),
        'endDateSubgroup': fields.Date(attribute='end_date_subgroup', description="fecha fin de la citacion"),
        'massiveCitation': fields.Boolean(attribute='massive_citation'),
        'publicationId': fields.Integer(attribute='publication_id', description="id de la publicacion"),
        'publication': fields.Nested(MassivePublicationDto.massive_publication,
                                     description="publicacion asociada al subgrupo")

    })

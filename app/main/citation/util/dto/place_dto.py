from flask_restplus import Namespace, fields


class PlaceDto:
    api = Namespace('place', description='operaciones relacionadas con la tabla place')

    place = api.model('place', {
        'placeId': fields.Integer(attribute='place_id', description="id del sitio"),
        'address': fields.String(attribute='address', description="Direccion del sitio"),
        'maximumAttentionLoad': fields.Integer(attribute='maximum_attention_load'),

    })

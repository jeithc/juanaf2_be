from flask_restplus import Namespace, fields
from app.main.citation.util.dto.subgroup_citation_dto import SubgroupCitationDto
from app.main.citation.util.dto.aa_dto import AaDto


class CitationDto:
    api = api = Namespace(
        'citation',
        description='operaciones relacionadas con la tabla citation')

    citation = api.model('citation', {
        'citationId': fields.Integer(attribute='citation_id', description='id de la descripción'),
        'claimerId': fields.Integer(attribute='claimer_id', description='id del reclamante'),
        'aaId': fields.Integer(attribute='aa_id', description='id del acto administrativo'),
        'preferentialDate': fields.DateTime(attribute='preferential_date'),
        'subgroupId': fields.Integer(attribute='subgroup_id'),
        'claimerMessageId': fields.Integer(attribute='claimer_message_id'),
        'documentId': fields.Integer(attribute='document_id'),
        'subgroupCitation': fields.Nested(SubgroupCitationDto.subgroup_citation, description=" id subgroup",
                                          attribute='subgroup_citation'),
        'aa': fields.Nested(AaDto.aa, description=" id subgroup")
    })



from app.main import db
from sqlalchemy.orm import relationship
from app.main.citation.model import subgroup_citation
from sqlalchemy import Table, Column, Integer, ForeignKey

class Citation(db.Model):
    """Model for citation"""
    __tablename__ = "citation"
    __table_args__ = {'schema': 'phase_2'}

    citation_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aa_id = db.Column(db.Integer, ForeignKey('phase_2.administrative_act.aa_id'))
    aa = relationship("Aa")
    claimer_id = db.Column(db.Integer, ForeignKey('phase_2.claimer_user_data.claimer_id'))
    subgroup_id = db.Column(db.Integer, ForeignKey('phase_2.subgroup_citation.subgroup_id'))
    subgroup_citation = relationship("SubgroupCitation")
    claimer_message_id = db.Column(db.Integer)
    document_id = db.Column(db.Integer)

    def __repr__(self):
        return "<Citation '{}'>".format(self.citation_id)

    @staticmethod
    def init_from_dto(data):
        citation = Citation()
        if('citationId' in data and data['citationId']):
            citation.citation_id = data['citationId']
        if('subgroupId' in data and data['subgroupId']):
            citation.subgroup_id = data['subgroupId']
        if('aaId' in data and data['aaId']):
            citation.aa_id = data['aaId']
        if('claimerId' in data and data['claimerId']):
            citation.claimer_id = data['claimerId']
        if('claimerMessageId' in data and data['claimerMessageId']):
            citation.claimer_message_id = data['claimerMessageId']
        if('documentId' in data and data['documentId']):
            citation.document_id = data['documentId']


class CitationManagement(db.Model):
    """Model for citation management"""
    __tablename__ = "citation_management"
    __table_args__ = {'schema': 'phase_2'}

    subgroup_id = db.Column(db.Integer)
    claimer_id = db.Column(db.Integer, primary_key=True)
    valid_address = db.Column(db.Boolean)
    send_flag = db.Column(db.Boolean)
    claimer_message_id = db.Column(db.Integer)
    country =  db.Column(db.String(100))
    department =  db.Column(db.String(128))
    municipality =  db.Column(db.String(128))
    address =  db.Column(db.String(255))
    urban_rural_location =  db.Column(db.String(1))
    preferential_date = db.Column(db.Date)
    document_id = db.Column(db.Integer)
    document_name =  db.Column(db.String(128))
    expedient_id = db.Column(db.Integer)
    full_name =  db.Column(db.String(512))

    def __repr__(self):
        return "<CitationManagement '{}'>".format(self.claimer_id)
from app.main import db
from sqlalchemy.orm import relationship
from app.main.util import db_helper
from app.main.citation.model import citation


class SubgroupCitation(db.Model):
    """SubgroupCitation model"""
    __tablename__ = 'subgroup_citation'
    __table_args__ = {'schema': 'phase_2'}

    subgroup_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    place_id = db.Column(db.Integer, db.ForeignKey('phase_2.notification_physical_places.place_id'))
    place = relationship("Place")
    aa_id = db.Column(db.Integer)
    notification_deadlin = db.Column(db.String(100))
    citation_type = db.Column(db.String(100))
    number_of_claimers = db.Column(db.Integer)
    begin_date_subgroup = db.Column(db.Date)
    end_date_subgroup = db.Column(db.Date)
    massive_citation = db.Column(db.Boolean)
    publication_id = db.Column(db.Integer, db.ForeignKey('phase_2.massive_publication.publication_id'))
    publication = relationship("MassivePublication")

    def __repr__(self):
        return "<SubgroupCitation '{}'>".format(self.subgroup_id)

    @staticmethod
    def init_from_dto(data):
        subgroup = SubgroupCitation()

        if 'subgroupId' in data and data['subgroupId']:
            subgroup.subgroup_id = data['subgroupId']
        if 'placeId' in data and data['placeId']:
            subgroup.place_id = data['placeId']
        if 'aaId' in data and data['aaId']:
            subgroup.aa_id = data['aaId']
        if 'notificationDeadlin' in data and data['notificationDeadlin']:
            subgroup.notification_deadlin = data['notificationDeadlin']
        if 'citationType' in data and data['citationType']:
            subgroup.citation_type = data['citationType']
        if 'notificationDaysRange' in data and data['notificationDaysRange']:
            subgroup.notification_days_range = data['notificationDaysRange']
        if 'beginDateSubgroup' in data and data['beginDateSubgroup']:
            subgroup.begin_date_subgroup = data['beginDateSubgroup']
        if 'endDateSubgroup' in data and data['endDateSubgroup']:
            subgroup.end_date_subgroup = data['endDateSubgroup']
        if 'massiveCitation' in data and data['massiveCitation']:
            subgroup.massive_citation = data['massiveCitation']
        if 'publicationId' in data and data['publicationId']:
            subgroup.publication_id = data['publicationId']

        return subgroup

    def update(self, session):
        s = session
        mapped_values = db_helper.process_obj_fields(self)
        s.query(SubgroupCitation).filter(SubgroupCitation.document_id == self.document_id).update(mapped_values)
        s.commit()


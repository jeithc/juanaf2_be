from app.main import db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship


class Place(db.Model):
    """Place model"""
    __tablename__ = 'notification_physical_places'
    __table_args__ = {'schema': 'phase_2'}

    place_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    address = db.Column(db.String(100))
    maximum_attention_load = db.Column(db.Integer)

    def __repr__(self):
        return "<Place '{}'>".format(self.address)

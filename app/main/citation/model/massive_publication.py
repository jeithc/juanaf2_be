from app.main import db
from sqlalchemy.orm import relationship


class MassivePublication(db.Model):
    """ Almacena todos los atributos relacionados con las publicaciones masivas que desde el sistema se generan. """
    __tablename__ = "massive_publication"
    __table_args__ = {'schema': 'phase_2'}
    publication_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aa_id = db.Column(db.Integer)
    publication_begin_date = db.Column(db.Date)
    publication_end_date = db.Column(db.Date)
    generator_document_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_documents.document_id'))
    publication = relationship("Claimer_documents")
    publication_type = db.Column(db.String(100))
    publication_state = db.Column(db.Boolean)

    def __repr__(self):
        return "<MassivePublication '{}'>".format(self.publication_id)
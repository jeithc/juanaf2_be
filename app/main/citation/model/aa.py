from app.main import db


class Aa(db.Model):
    """Place model"""
    __tablename__ = 'administrative_act'
    __table_args__ = {'schema': 'phase_2'}

    aa_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aa_number = db.Column(db.String(50))
    aa_document_id = db.Column(db.Integer)
    description = db.Column(db.String(255))
    expedition_date = db.Column(db.Date)
    digital_signature_date = db.Column(db.Date)
    claimer_id = db.Column(db.Integer)
    aa_id_dp = db.Column(db.String(100))
    aa_resource_type = db.Column(db.String(100))
    aa_workflag = db.Column(db.Integer)
    aa_document_id = db.Column(db.Integer)
    approve_administrative_act_user_id = db.Column(db.Integer)
    approve_administrative_act_date = db.Column(db.DateTime)
    sign_administrative_act_user_id = db.Column(db.Integer)
    sign_administrative_act_date = db.Column(db.DateTime)
    epigraph_id = db.Column(db.Integer)
    aa_document_format_id = db.Column(db.Integer)

    def __repr__(self):
        return "<Aa '{}'>".format(self.description)

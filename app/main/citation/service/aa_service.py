from flask_restplus import marshal

from app.main import db
from app.main import logger
from app.main.citation.util.dto.aa_dto import AaDto
from app.main.citation.model.aa import Aa
from app.main.core.service.general_service import manage_db_exception


def get_all_aa():
    logger.debug('Entro a role_service#get_all_aa')
    try:
        aas = Aa.query.all()

        return marshal(aas, AaDto.aa), 200
    except Exception as e:
        return manage_db_exception("Error en role_service#get_all_aa - " +
                                    "Error al obtener los actos",
                                    'Ocurrió un error al obtener los actos.', e)
def get_aa(aa_id):
    logger.debug('Entro a role_service#get_all_aa')
    try:
        aa = Aa.query.filter(Aa.aa_id == aa_id).first()

        return marshal(aa, AaDto.aa), 200
    except Exception as e:
        return manage_db_exception("Error en role_service#get_all_aa - " +
                                    "Error al obtener los actos",
                                    'Ocurrió un error al obtener los actos.', e)
from app.main import db
from app.main import logger
from app.main.citation.util.dto.subgroup_citation_dto import SubgroupCitationDto
from app.main.citation.model.subgroup_citation import SubgroupCitation
from flask_restplus import marshal
from app.main.core.service.general_service import manage_db_exception
from app.main.util import db_helper


def save_subgroup_citation(data):
    logger.debug('come in save_subgroup_citation')

    subgroup = SubgroupCitation.init_from_dto(data)

    try:

        db.session.add(subgroup)
        db.session.commit()

        response_object = {
            'status': 'success',
            'message': 'subgroup_citation almacenado correctamente',
        }

        return response_object, 201

    except Exception as e:
        return manage_db_exception("Error en user_service#update_user - " +
                                   "Error al guardar el subgroup_citation",
                                   'Ocurrió un error al guardar el subgroup_citation.', e)


def get_subgroup_citation(aa_id):
    logger.debug('come in #get_subgroup_citation')
    try:
        subgroup = SubgroupCitation.query.filter(SubgroupCitation.aa_id == aa_id)
        return subgroup

    except Exception as e:
        return logger.error("Error al consultar subgrupo" + str(e)), 500


def update_subgroup_citation(data):
    logger.debug('come in #update_subgroup_citation')

    try:
        subgroup = SubgroupCitation.query. \
            filter(SubgroupCitation.claimer_id == data['claimerId']).first()

        if not subgroup:
            response_object = {
                'status': 'fail',
                'message': 'subgroup no existe'
            }
            return response_object, 500
        else:

            subgroup = SubgroupCitation.init_from_dto(data)

            db.session.add(subgroup)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'subgroup_citation actualizado correctamente'
            }
            return response_object, 201

    except Exception as e:
        return manage_db_exception("Error en user_service#update_subgroup_citation - " +
                                   "Error al guardar el subgroup_citation",
                                   'Ocurrió un error al actualizar el subgroup_citation.', e)


def find_subgroup_by_filter(page, per_page, data):
    logger.debug('come in #find_subgroup_by_filter')

    try:

        filter_data = SubgroupCitation.init_from_dto(data)

        mapped_filter = db_helper.process_obj_fields(filter_data)

        query = SubgroupCitation.query

        query = query.filter_by(**mapped_filter)

        if 'place' in data and data['place']:
            query = query.filter(SubgroupCitation.place.has(place_id=data['place']['placeId']))

        if 'publication' in data and data['publication']:
            if 'publicationBeginDate' in data['publication']:
                query = query.filter(
                    SubgroupCitation.publication.has(publication_begin_date=data['publication']['publicationBeginDate']))

        if 'publication' in data and data['publication']:
            if 'publicationEndDate' in data['publication']:
                query = query.filter(
                    SubgroupCitation.publication.has(publication_end_date=data['publication']['publicationEndDate']))

        subgroups = query.paginate(page, per_page).items

        return marshal(subgroups, SubgroupCitationDto.subgroup_citation), 200
    except Exception as e:
        return manage_db_exception("Error en subgroup_citation__service#find_subgroup_by_filter - " +
                                   "Error al obtener los subgrupos por filtro",
                                   'Ocurrió un error al obtener los subgrupos con filtros.', e)


def count_subgroup_by_filter(data):
    logger.debug('come in #count_subgroup_by_filter')

    try:

        filter_data = SubgroupCitation.init_from_dto(data)

        mapped_filter = db_helper.process_obj_fields(filter_data)

        query = SubgroupCitation.query

        query = query.filter_by(**mapped_filter)

        if 'place' in data and data['place']:
            query = query.filter(SubgroupCitation.place.has(place_id=data['place']['placeId']))

        if 'publication' in data and data['publication']:
            if 'publicationBeginDate' in data['publication']:
                query = query.filter(
                    SubgroupCitation.publication.has(publication_begin_date=data['publication']['publicationBeginDate']))

        if 'publication' in data and data['publication']:
            if 'publicationEndDate' in data['publication']:
                query = query.filter(
                    SubgroupCitation.publication.has(publication_end_date=data['publication']['publicationEndDate']))

        return query.count(), 200
    except Exception as e:
        return manage_db_exception("Error en subgroup_citation__service#find_subgroup_by_filter - " +
                                   "Error al obtener los subgrupos por filtro",
                                   'Ocurrió un error al obtener los subgrupos con filtros.', e)

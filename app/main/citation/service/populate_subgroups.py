import numpy as np
import datetime
import math
from app.main.citation.model.subgroup_citation import SubgroupCitation
from app.main.core.model.business_days import BusinessDays
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User
from app.main.citation.model.citation import Citation
from app.main import db
from sqlalchemy import text


def populate():
    sites = [1, 2]

    # personas a notificar segun dias del perdio (5, 10 ,30)
    notify_users = np.array([215000, 70000, 15000])

    # Tiempo por grupo dias
    sg_days = np.array([5, 10, 30])

    # Tiempo por grupo semanas (grupo base)
    sg_weeks = np.array([1, 2, 6])

    # Tiempo maximo del proceso
    max_days = np.array([45, 45, 45])

    # Categorias de atencion
    # masiva, c. electronico, urbano, rural X 5 10 30
    categories = np.array([[40000, 40000, 150000, 6000], [8000, 7000, 34000, 2000], [2000, 3000, 8000, 2000]])

    n_groups = np.divide(max_days, sg_days)

    # tamaño grupo para 1 sitio
    group_size = np.divide(categories, n_groups)

    # tamaño grupo para 1 sitio
    group_size_2 = np.divide(notify_users, n_groups)

    # Atencion minima por dia
    at_day = np.divide(group_size, sg_days)

    # tamaño grupo para 1 semana
    group_size_week = np.divide(group_size, sg_weeks)

    week_demand = group_size_week.sum()

    # Persona minimas atendidas por dia
    print(at_day.sum())

    # potencial dias habiles
    th = np.array([200, 100, 50])

    # potencial dias festivos
    tf = np.array([100, 50, 25])

    # cantidad dias atencion habiles y festivos
    yh = np.array([5, 5, 5])
    yf = np.array([2, 2, 2])

    # potencial de atencio por grupo base
    at = np.multiply(yh, th) + np.multiply(yf, tf)

    n_weeks = 12
    n_weeks_f = 6

    atm = n_weeks * np.multiply(yh, th) + n_weeks_f * np.multiply(yf, tf)

    week_offer = at.sum()

    ratio_service = week_offer / week_demand

    print("Ratio de servicio ", ratio_service)

    print(atm.sum())


def generate_subgroups():

    start = datetime.datetime.strptime("29-07-2019", "%d-%m-%Y")
    end = datetime.datetime.strptime("24-09-2019", "%d-%m-%Y")

    start_mass = datetime.datetime.strptime("22-07-2019", "%d-%m-%Y")

    # count_message_msj = User.query.join(ClaimerWorkflow, User.claimer_id == ClaimerWorkflow.claimer_id) \
    #     .filter(ClaimerWorkflow.current_state == 8, ClaimerWorkflow.end_date == None) \
    #     .count()
    #
    # count_message_email = User.query.join(ClaimerWorkflow, User.claimer_id == ClaimerWorkflow.claimer_id) \
    #     .filter(ClaimerWorkflow.current_state == 5, ClaimerWorkflow.end_date == None) \
    #     .count()
    #
    # count_message_mass = User.query.join(ClaimerWorkflow, User.claimer_id == ClaimerWorkflow.claimer_id) \
    #     .filter(ClaimerWorkflow.current_state == 9, ClaimerWorkflow.end_date == None) \
    #     .count()

    count_message_5 = 216000
    count_message_email_5 = 70
    count_message_mass_5 = 43215
    count_message_10 = 1727
    count_message_email_10 = 1
    count_message_30 = 17

    generate_subgroup(count_message_5, 5, start, end, 30000, 'CITACION MENSAJERIA')
    generate_subgroup(count_message_email_5, 5, start, end, 30000, 'CITACION CORREO ELECTRONICO')
    generate_subgroup(count_message_mass_5, 5, start_mass, end, 30000, 'CITACION MASIVA')

    generate_subgroup(count_message_10, 10, start, end, 30000, 'CITACION MENSAJERIA')
    generate_subgroup(count_message_email_10, 10, start, end, 30000, 'CITACION CORREO ELECTRONICO')

    generate_subgroup(count_message_30, 30, start, end, 30000, 'CITACION MENSAJERIA')


def generate_subgroup(number, size, start_date, end_date, capacity, citation_type):
    n_subgroups = number / capacity

    days = create_calendar_array(start_date, end_date)
    demand = number

    n_days = len(days)
    max_intervals = n_days / size
    subgroups = []
    k = 0

    base_date = days[k]

    for sg in range(0, math.ceil(n_subgroups)):
        k = min(k+size-1, n_days-1)
        subgroup = SubgroupCitation()
        subgroup.begin_date_subgroup = base_date
        subgroup.end_date_subgroup = days[k]
        subgroup.place_id = 1
        subgroup.aa_id = 1
        subgroup.notification_deadlin = size
        subgroup.number_of_claimers = min(capacity, demand)
        subgroup.citation_type = citation_type

        demand -= capacity

        print(demand)
        if n_days-1 != k:
            base_date = days[k + 1]
            k += 1
        elif capacity > 0:
            k = 0
            base_date = days[k]
        subgroups.append(subgroup)
        if demand <= 0:
            break

    for sg in subgroups:
        db.session.add(sg)
        db.session.commit()
        db.session.refresh(sg)
        generate_citations(sg.subgroup_id)
        if 'CITACION MASIVA' != citation_type:
            add_preferential_date(sg)
        print('{}::{}'.format(sg.begin_date_subgroup.strftime("%d-%m-%Y"), sg.end_date_subgroup.strftime("%d-%m-%Y")))


def add_preferential_date(subgroup):

    days_range = create_calendar_array(subgroup.begin_date_subgroup, subgroup.end_date_subgroup)
    days_range = days_range[::-1]
    limit = subgroup.number_of_claimers/int(subgroup.notification_deadlin)

    for day in days_range:
        update_query = ' UPDATE phase_2.citation SET preferential_date=\'' + day.strftime("%d-%m-%Y") + '\''\
                       ' FROM (SELECT citation_id ' \
             ' FROM phase_2.citation WHERE subgroup_id = ' + str(subgroup.subgroup_id) + \
             ' AND preferential_date is null LIMIT ' + str(math.ceil(limit)) + ') sg  ' \
             ' WHERE phase_2.citation.citation_id = sg.citation_id'

        db.engine.execute(text(update_query).execution_options(autocommit=True))


def generate_citations(subgroup_id):
    update_query = ' SELECT phase_2.f_automatic_citation_filler(' + str(subgroup_id) + ') '
    db.engine.execute(text(update_query).execution_options(autocommit=True))


def create_calendar_array(start, end):
    # date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

    date_generated = []
    business_days = get_business_days(start, end)

    for date in business_days:
        if date.business_day_type == 'DIA_HABIL':
            date_generated.append(date.business_day_date)

    return date_generated


def get_business_days(start_date, end_date):
    business_days = BusinessDays.query \
        .filter(BusinessDays.business_day_date.between(start_date, end_date)) \
        .order_by(BusinessDays.business_day_date.asc()).all()

    return business_days


if __name__ == "__main__":
    # populate()
    start = datetime.datetime.strptime("17-07-2019", "%d-%m-%Y")
    end = datetime.datetime.strptime("19-10-2019", "%d-%m-%Y")
    generate_subgroups(150000, 5, start, end, 6000)

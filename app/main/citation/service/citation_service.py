from app.main import db
from flask_restplus import marshal
from app.main.citation.model.citation import Citation
from app.main.citation.util.dto.citation_dto import CitationDto
from app.main.core.service.general_service import manage_db_exception
from app.main.service.logging_service import loggingBDError


def get_citation(aa_id, claimer_id):
    try:
        citation = Citation.query\
            .filter(Citation.claimer_id == claimer_id)\
            .filter(Citation.aa_id == aa_id)\
            .first()

        if not citation:
            response_object = {
                'status': 'success',
                'message': 'La citacion no existe.'
            }
            return response_object, 200
        return citation, 200

    except Exception as e:
        return manage_db_exception("Error en workflow_service#get_citation - " +
                                   "Error al consultar citacion",
                                   'Ocurrió un error al consultar la citatcion.', e)


def delete_citation(aa_id, claimer_id):
    try:
        citation = Citation.query\
            .filter(Citation.claimer_id == claimer_id)\
            .filter(Citation.aa_id == aa_id)\
            .first()
        if not citation:
            response_object = {
                'status': 'success',
                'message': 'La citacion no existe.'
            }
        else:
            db.delete(citation)
            db.commit()
    except Exception as e:
        return manage_db_exception("Error en #get_claimer_resource",
                                   "Error obteniendo claimer_resource", e)


def citation_update(data):

    citation = Citation.query.filter_by(claimer_id=data['claimerId'], subgroup_id=data['subgroup_id']).first()

    if citation:

        if 'claimerId' in data:
            citation.claimer_id = data['claimerId']
        else:
            citation.claimer_id = ''
        if 'aaId' in data:
            citation.aa_id = data['aaId']

        if 'preferentialDate' in data:
            citation.preferential_date = data['preferentialDate']

        if 'subgroupId' in data:
            citation.subgroup_id = data['subgroupId']

        if 'claimerMessageId' in data:
            citation.claimer_message_id = data['claimerMessageId']

        if 'documentId' in data:
            citation.document_id = data['documentId']

        save_changes(citation)

        response_object = {
            'status': 'success',
            'message': 'Citacion actualizada con éxito',
        }
        return response_object, 201
        # return generate_token(new_user)
    else:
        response_object = {
            'status': 'fail',
            'message': 'Citation no exist in BD.',
        }
        return response_object, 204


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        print(e)
        newLogger = loggingBDError(loggerName='audit_service')
        db.session.rollback()
        newLogger.error(e)

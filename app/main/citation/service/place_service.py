from flask_restplus import marshal

from app.main import db
from app.main import logger
from app.main.citation.util.dto.place_dto import PlaceDto
from app.main.citation.model.place import Place
from app.main.core.service.general_service import manage_db_exception


def get_all_places():
    logger.debug('Entro a role_service#get_all_roles')
    try:
        places = Place.query.all()

        return marshal(places, PlaceDto.place), 200
    except Exception as e:
        return manage_db_exception("Error en role_service#get_all_places - " +
                                   "Error al obtener los roles",
                                   'Ocurrió un error al obtener los sitios.', e)
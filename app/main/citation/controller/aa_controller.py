from flask import request
from flask_restplus import Resource
from app.main.citation.util.dto.aa_dto import AaDto
from app.main.citation.service import aa_service

api = AaDto.api


@api.route('/administrative_act')
class AaList(Resource):

    @api.doc('get list places')
    def get(self):
        """ get list places """
        return aa_service.get_all_aa()

@api.route('/administrative_act/<aa_id>')
class Aa(Resource):
    @api.doc('get list places')
    def get(self, aa_id):
        """ get list places """
        return aa_service.get_aa(aa_id=aa_id)

from flask import request
from flask import json
import atexit
from flask_restplus import Resource
from app.main.messaging.service import claimer_message_service
from app.main.messaging.util.dto.claimer_message_dto import ClaimerMessageDto
from app.main.service import user_service
from app.main.citation.service import citation_service
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.messaging.service import message_address_service
from app.main.util.decorator import audit_init, audit_finish

api = ClaimerMessageDto.api

@api.route('/citation/fisical_citation/<subgroup_id>')
class ClaimerMessagFisicalUsers(Resource):
	@audit_init
	@audit_finish
	def get(self, subgroup_id):
		users = user_service.get_users_for_fisical_citation(subgroup_id)
		lista = json.loads(users)
		for user in lista:
			
				dataMa = {
					'address' : user['address'],
					'idNatgeo' : user['id_natgeo'],
					'urbanRuralLocation' : user['urban_rural_location'],
					'idBogLocNeig' : user['id_bog_loc_neig'],
					'country' : 'COLOMBIA'
				}
				
				ma = message_address_service.save_new_message_address(dataMa)
				
				dataCm = {
					'personExpedientId' : user['person_expedient_id'],
					'claimerId' : user['claimer_id'],
					'aaId' : user['aa_id'],
					'claimerMessageMode' : 'MENSAJERIA',
					'claimerMessageType' : 'NOTIFICACION',
					'addressId' : ma[0].address_id,
				}
				message = claimer_message_service.save_new_message(dataCm)
				
				dataC = {
					'claimerId' : user['claimer_id'],
					'claimerMessageId' : message.claimer_message_id,
					'subgroup_id': subgroup_id
				}
				citation_service.citation_update(dataC)

				document = ReportJasperMasivo.create_report(8, user['claimer_id'], None, subgroup_id )
				#citation = user_service.get_users_for_post_in_citation(user['claimer_id'])

				dataC = {
					'claimerId' : user['claimer_id'],
					'subgroup_id': subgroup_id,
					'documentId': document['document_id']
				}
				citation_service.citation_update(dataC)
				
			# elif (user['notification_deadlin']) =='10': 
			# 	dataMa = {
			# 		'address' : user['address'],
			# 		'idNatgeo' : user['id_natgeo'],
			# 		'urbanRuralLocation' : user['urban_rural_location'],
			# 		'idBogLocNeig' : user['id_bog_loc_neig'],
			# 		'country' : 'COLOMBIA',
			# 	}

			# 	ma = message_address_service.save_new_message_address(dataMa)
				
			# 	dataCm = {
			# 		'personExpedientId' : user['person_expedient_id'],
			# 		'claimerId' : user['claimer_id'],
			# 		'aaId' : '1',
			# 		'claimerMessageMode' : 'FISICO',
			# 		'claimerMessageType' : 'CITACION',
			# 		'addressId' : ma[0].address_id,
			# 	}
			# 	claimer_message_service.save_new_message(dataCm)
			# 	ReportJasperMasivo.create_report(24, user['claimer_id'])
			# elif (user['notification_deadlin']) =='30': 
			# 	dataMa = {
			# 		'address' : user['address'],
			# 		'idNatgeo' : user['id_natgeo'],
			# 		'urbanRuralLocation' : user['urban_rural_location'],
			# 		'idBogLocNeig' : user['id_bog_loc_neig'],
			# 		'country' : 'COLOMBIA',
			# 	}

			# 	ma = message_address_service.save_new_message_address(dataMa)
				
			# 	dataCm = {
			# 		'personExpedientId' : user['person_expedient_id'],
			# 		'claimerId' : user['claimer_id'],
			# 		'aaId' : '1',
			# 		'claimerMessageMode' : 'FISICO',
			# 		'claimerMessageType' : 'CITACION',
			# 		'addressId' : ma[0].address_id,
			# 	}
			# 	claimer_message_service.save_new_message(dataCm)
			# 	ReportJasperMasivo.create_report(28, user['claimer_id'])
		return json.loads(users)
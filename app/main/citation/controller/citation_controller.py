from flask import request
from flask_restplus import Resource
from app.main.citation.util.dto.citation_dto import CitationDto
from app.main.citation.service import citation_service
from app.main.util.decorator import audit_init, audit_finish


api = CitationDto.api


@api.route('/citation/<claimer_id>/<aa_id>')
class CitationList(Resource):
    @audit_init
    @audit_finish 
    @api.marshal_with(CitationDto.citation)
    @api.doc('get citation')
    def get(self, claimer_id, aa_id):
        """ get citation"""
        return citation_service.get_citation(claimer_id=claimer_id, aa_id=aa_id)


@api.route('/citation/<claimer_id>/<aa_id>')
class CitationDelete(Resource):
    @audit_init
    @audit_finish 
    @api.marshal_with(CitationDto.citation)
    @api.doc('get citation')
    def delete(self, claimer_id, aa_id):
        """ get citation"""
        return citation_service.delete_citation(claimer_id=claimer_id, aa_id=aa_id)

from flask import request
from flask_restplus import Resource
from app.main.citation.util.dto.subgroup_citation_dto import SubgroupCitationDto
from app.main.citation.util.dto.massive_publication_dto import MassivePublicationDto
from app.main.citation.service import subgroup_citation_service
from app.main.citation.service import populate_subgroups
from app.main.util.decorator import audit_init, audit_finish
import datetime

api = SubgroupCitationDto.api
api_publication = MassivePublicationDto.api


@api.route('/subgroup_citation')
class SubgroupList(Resource):
    @audit_init
    @audit_finish 
    @api.doc('create new SubgroupCitation')
    @api.expect(SubgroupCitationDto.subgroup_citation, validate=False)
    def post(self):
        """ Create new SubgroupCitation """
        data = request.json
        return subgroup_citation_service.save_new_attorney(data=data)

    @api.doc('get list subgrupos')
    @api.marshal_with(SubgroupCitationDto.subgroup_citation, 200)
    def get(self):
        """ get list attorneys """
        return subgroup_citation_service.get_all_attorneys()


@api.route('/subgroup_citation/filter')
@api.param('page', 'Number of page', type='integer')
@api.param('perPage', 'Results per page', type='integer')
class SubgroupByFilter(Resource):
    @audit_init
    @audit_finish 
    @api.expect(SubgroupCitationDto.subgroup_citation, validate=False)
    @api.doc('find_subgroup_by_filter')
    def post(self):
        """Get subgroups by some filters """
        page = int(request.args.get('currentPage'))
        per_page = int(request.args.get('perPage'))
        data = request.json

        return subgroup_citation_service.find_subgroup_by_filter(page, per_page, data=data)


@api.route('/subgroup_citation/filter_count')
class SubgroupByFilterCount(Resource):
    @api.expect(SubgroupCitationDto.subgroup_citation, validate=False)
    @api.doc('count_subgroup_by_filter')
    def post(self):
        """Get subgroups by some filters """
        data = request.json

        return subgroup_citation_service.count_subgroup_by_filter(data=data)


@api.route('/subgroup_citation/generate')
class SubgroupGenerator(Resource):
    @api.doc('generate')
    def post(self):
        """Get subgroups by some filters """

        return populate_subgroups.generate_subgroups()


from flask import request
from flask_restplus import Resource
from app.main.citation.util.dto.place_dto import PlaceDto
from app.main.citation.service import place_service

api = PlaceDto.api


@api.route('/place')
class PlaceList(Resource):

    @api.doc('get list places')
    def get(self):
        """ get list places """
        return place_service.get_all_places()

from flask_restplus import Namespace, fields


class ResourceDTO:
    api = Namespace('radication', description='Operaciones relacionadas con radicación de reclamantes')
    resource = api.model('radication', {
        'resourceId': fields.Integer(attribute='resource_id', description='id del recurso'),
        'claimerId': fields.Integer(attribute='claimer_id', description='id del reclamante'),
        'aaId': fields.Integer(attribute='aa_id', description='id del acto administrativo'),
        'hasAttorney': fields.Boolean(attribute='has_attorney'),
        'attorneyId': fields.Integer(attribute='attorney_id', description='id del apoderado'),
        'resourceType': fields.String(attribute='resource_type'),
        'resourceRadicationDate': fields.Date(attribute='resource_radication_date'),
        'resourceDocumentId': fields.Integer(attribute='resource_document_id'),
        'numberOfDocuments': fields.Integer(attribute='number_of_documents'),
        'numberOfPages': fields.Integer(attribute='number_of_pages'),
        'resourcePlacedOutOfTime': fields.Integer(attribute='resource_placed_out_of_time')
    })

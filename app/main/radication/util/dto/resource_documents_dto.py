from flask_restplus import Namespace, fields
from app.main.core.util.dto.document_dto import DocumentDTO


class ResourceDTO:
    api = Namespace('radication', description='Operaciones relacionadas con radicación de reclamantes')
    radication = api.model('radication', {
        'resource_type': fields.String(),
        'review_document': fields.String(),
        'url_document': fields.String(),
        'radicate_number': fields.String(),
        'radication_date': fields.DateTime(),
        'attached_document_id': fields.Integer(),
        'number_of_pages':fields.Integer()
    })

    attachment = api.model('attachment', {
        'attachedDocumentId': fields.Integer(attribute='attached_document_id', description='id del attachment'),
        'documentId': fields.Integer(attribute='document_id', description='id del reclamante'),
        'resourceId': fields.Integer(attribute='resource_id', description='id del acto administrativo'),
        'attachedDocumentType': fields.String(attribute='attached_document_type'),
        'documentRadicationDate': fields.Date(attribute='attached_document_radication_da'),
        'document': fields.Nested(DocumentDTO.document),
    })

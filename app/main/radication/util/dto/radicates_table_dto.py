from flask_restplus import Namespace, fields
from app.main.radication.util.dto.resource_documents_dto import ResourceDTO
from app.main.core.util.dto.document_dto import DocumentDTO
from app.main.citation.util.dto.aa_dto import AaDto

class RadicatesDTO:
    api = Namespace('radication', description='Operaciones relacionadas con radicación de reclamantes')
    radication = api.model('radication', {
        'resource_type': fields.String(),
        'radicate_number': fields.Integer(),
        'resource_radication_date': fields.DateTime(),
        'description': fields.String(),
    })

    resource = api.model('resource', {
        'resourceId': fields.Integer(attribute='resource_id', description='id del recurso'),
        'claimerId': fields.Integer(attribute='claimer_id', description='id del reclamante'),
        'aaId': fields.Integer(attribute='aa_id', description='id del acto administrativo'),
        'hasAttorney': fields.Boolean(attribute='has_attorney'),
        'attorneyId': fields.Integer(attribute='attorney_id', description='id del apoderado'),
        'resourceType': fields.String(attribute='resource_type'),
        'resourceRadicationDate': fields.Date(attribute='resource_radication_date'),
        'resourceDocumentId': fields.Integer(attribute='resource_document_id'),
        'numberOfDocuments': fields.Integer(attribute='number_of_documents'),
        'numberOfPages': fields.Integer(attribute='number_of_pages'),
        'resourcePlacedOutOfTime': fields.Boolean(attribute='resource_placed_out_of_time'),
        'formerRadicateNumber': fields.String(attribute='former_radicate_number'),
        'attachments': fields.List(fields.Nested(ResourceDTO.attachment)),
        'document': fields.Nested(DocumentDTO.document),
        'aa': fields.Nested(AaDto.aa),
        'answerInitialQuestion': fields.Boolean(attribute='answer_initial_question'),
        'analysisStage': fields.String(attribute='analysis_stage'),
        'analysisStageState': fields.String(attribute='analysis_stage_state'),
        'currentState': fields.String(attribute='current_state'),
        'prevState': fields.String(attribute='prev_state')

    })

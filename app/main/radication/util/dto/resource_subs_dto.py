from flask_restplus import Namespace, fields
from app.main.radication.util.dto.resource_documents_dto import ResourceDTO
from app.main.core.util.dto.document_dto import DocumentDTO


class ResourceSubsDTO:
    api = Namespace('resource', description='Operaciones relacionadas con recursos de sustanciacion')


    resource_subs = api.model('resource', {
        'claimerId': fields.Integer(attribute='claimer_id', description='id del reclamante'),
        'resourceId': fields.Integer(attribute='resource_id', description='id del reclamante'),
        'resourceDocumentId': fields.Integer(attribute='resource_document_id'),
        'radicateNumber': fields.Integer(attribute='radicate_number'),
        'resourceRadicationDate': fields.Date(attribute='resource_radication_date'),
        'resourceType': fields.String(attribute='resource_type'),
        'numberofDocuments': fields.Integer(attribute='number_of_documents'),
        'resourceState': fields.String(attribute='resource_state'),
        'subCondition': fields.String(attribute='sub_condition'),
        'subGroup': fields.String(attribute='subgroup'),
        'beneficiary': fields.String(),
        'answerInitialQuestion': fields.Boolean(attribute='answer_initial_question'),
        'hasAttorney': fields.Boolean(attribute='attorney'),
        'analysisStage': fields.String(attribute='stage'),                                        
        'analysisStageState': fields.String(attribute='stage_state'),
        'requiresProof': fields.Boolean(attribute='requires_proof'),
        'requiresProofAppeal': fields.Boolean(attribute='requires_proof_appeal')
    })
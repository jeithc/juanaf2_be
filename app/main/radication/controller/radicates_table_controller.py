from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
import io
from app.main.util.dto import UserDto
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.radication.service.radicates_table_service import *
from app.main.radication.service import radicates_table_service
from app.main.radication.util.dto.radicates_table_dto import RadicatesDTO
from app.main.radication.util.dto.resource_subs_dto import ResourceSubsDTO
from app.main.radication.util.dto.resource_subs_dto import ResourceDTO
from flask import send_file, request


api = RadicatesDTO.api


@api.route('/resource/new-claimer')
class NewClaimer(Resource):
    @api.expect(UserDto.user, validate=False)
    #@login_app
    @audit_init
    @audit_finish
    @api.doc('new user')
    def post(self):
        """validate user with filters"""
        data = request.json
        return_value = radicates_table_service.save_new_claimer(data)
        return return_value

@api.route('/resource')
class ResourceCrud(Resource):
    @api.response(500, 'Ocurrió un error al guardar el recurso.')
    @api.doc('create a new claimer_resource_document')
    @api.expect(RadicatesDTO.resource, validate=False)
    # @auth.login_required
    @audit_init
    @audit_finish
    def post(self):
        """Creates a new claimer_resource_document """
        data = request.json
        user_email = request.headers.environ['HTTP_USER_ID']
        return radicates_table_service.save_new_resource(data=data, user_email=user_email)

    @api.response(500, 'Ocurrió un error al actualizar el recurso.')
    @api.doc('udapte claimer_resource_document')
    @api.expect(RadicatesDTO.resource, validate=False)
    # @auth.login_required
    @audit_init
    @audit_finish
    def put(self):
        """Update claimer_resource_document """
        data = request.json
        return radicates_table_service.update_resource(data=data)


@api.route('/claimer_resource/<claimer_id>')
@api.param('claimer_id', 'ID claimer')
@api.response(204, "no se encontraron radicados para el reclamante")
class Resource(Resource):
    @api.doc('view radicates for claimer')
    # @login_app
    @audit_init
    @audit_finish
    @api.marshal_with(RadicatesDTO.resource)
    def get(self, claimer_id):
        """view radicates claimer"""

        return radicates_table_service.get_claimer_resource(self, claimer_id)


@api.route('/find_radicate_number/<radicate_number>')
@api.param('claimer_id', 'ID claimer')
@api.response(204, "no se encontraron radicados para el reclamante")
class FindRadicate(Resource):
    @api.doc('Find radicate_number')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, radicate_number):
        """view radicates claimer"""

        return radicates_table_service.find_radicate_number(radicate_number)


@api.route('/get_radicate_document/<resource_id>')
@api.param('resource_id', 'ID del recurso')
@api.response(204, "no se pudo generar el documento de sticker")
class FindSticker(Resource):
    @api.doc('get sticker document ')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, resource_id):
        """get sticker document"""

        return send_file(
                    io.BytesIO(radicates_table_service.
                               get_radicate_document(resource_id)),
                    mimetype='application/pdf',
                    attachment_filename='sticker.pdf'
                    #as_attachment = False
        )


@api.route('/generate_sticker_document/<radicate_number>/<path:radicate_date>/<number_pages>/<number_of_attachments>/<claimer_id>')
@api.response(204, "no se pudo generar el documento de sticker")
class GenerateSticker(Resource):
    @api.doc('get sticker document ')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, radicate_number, radicate_date, number_pages, number_of_attachments, claimer_id):
        """get sticker document"""

        return send_file(
                    io.BytesIO(radicates_table_service.
                               generate_sticker_document(radicate_number, radicate_date, number_pages,
                                                         number_of_attachments, claimer_id)),
                    mimetype='application/pdf',
                    attachment_filename='sticker.pdf'
                    #as_attachment = False
        )


@api.route('/generate_radicate_number/<claimer_id>')
@api.param('claimer_id', 'ID claimer')
@api.response(204, "no se pudo generar el numero de radicado")
class GenerateRadicate(Resource):
    @api.doc('generate radicate_number')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, claimer_id):
        """generates radicate number"""

        return radicates_table_service.generate_radicate(claimer_id)


@api.route('/claimer_resource_for_subs/<claimer_id>')
@api.param('claimer_id', 'ID claimer')
@api.response(204, "no se encontraron recursos asociados al reclamante")
class ResourceSubs(Resource):
    @api.doc('recursos por claimer')
    # @login_app
    @audit_init
    @audit_finish
    @api.marshal_with(ResourceSubsDTO.resource_subs)
    def get(self, claimer_id):
        """recursos por claimer"""

        return radicates_table_service.get_resources(claimer_id)


@api.route('/attachment_by_resource/<resource_id>')
@api.param('resource_id', 'Resource Id')
@api.response(204, "no se encontraron documentos asociados al recurso")
class ResourceDoc(Resource):
    @api.doc('documentos por recurso')
    # @login_app
    @audit_init
    @audit_finish
    @api.marshal_with(ResourceDTO.attachment)
    def get(self, resource_id):
        """documentos por recurso"""

        return radicates_table_service.get_docs_resources(resource_id)


@api.route('/generate_radicate_number/<claimer_id>/<flow_document>/')
@api.param('claimer_id', 'ID claimer')
@api.param('flow_document', 'Flow Document 1 or 2')
@api.response(204, "no se pudo generar el numero de radicado")
class GenerateRadicateFlow(Resource):
    @api.doc('generate radicate_number')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, claimer_id, flow_document):
        """generates radicate number"""
        return radicates_table_service.generate_radicate_document_flow(claimer_id, flow_document)


@api.route('/attachment_by_claimer/<claimer_id>')
@api.param('claimer_id', 'claimer_id')
@api.response(204, "no se encontraron documentos asociados al claimer")
class ClaimerDoc(Resource):
    @api.doc('documentos por claimer')
    @audit_init
    @audit_finish
    @api.marshal_with(ResourceDTO.attachment)
    def get(self, claimer_id):
        """documentos por recurso"""

        return radicates_table_service.get_docs_resources_by_claimer(claimer_id)


@api.route('/aa_for_complain/<claimer_id>')
@api.param('claimer_id', 'claimer_id')
@api.response(204, "no se encontraron actos para radicar quejas")
class ClaimerDoc(Resource):
    @api.doc('actos para radicar quejas')
    @audit_init
    @audit_finish
    def get(self, claimer_id):
        """actos para radicar quejas"""

        return radicates_table_service.get_acts_for_complain(claimer_id)

from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.radication.service import resource_documents_service
from app.main.radication.util.dto.resource_documents_dto import ResourceDTO


api = ResourceDTO.api


@api.route('/resource_attachment')
class ResourceCrud(Resource):
    @api.response(500, 'Ocurrió un error al guardar el attachment.')
    @api.doc('create a new resource_attached_documents')
    @api.expect(ResourceDTO.attachment, validate=False)
    # @auth.login_required
    def post(self):
        """Creates a new resource_attached_documents """
        data = request.json
        return resource_documents_service.save_new_attachment(data=data)

    @api.response(500, 'Ocurrió un error al actualizar el attachment.')
    @api.doc('udapte resource_attached_documents')
    @api.expect(ResourceDTO.attachment, validate=False)
    # @auth.login_required
    def put(self):
        """Update resource_attached_documents """
        data = request.json
        return resource_documents_service.update_attachment(data=data)


@api.route('/resource_documents/<resource_id>')
@api.param('resource_id', 'ID resource')
@api.response(204, "no se encontraron documentos para el recurso")
class Resource(Resource):
    @api.doc('view documents for resource')
    #@login_app
    @audit_init
    @audit_finish
    @api.marshal_with(ResourceDTO.radication)  
    def get(self, resource_id):
        """view documents for resource"""
        #verifico tiempo de publicación
        return resource_documents_service.view_documents_resource(self,resource_id)


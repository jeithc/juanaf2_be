from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.radication.service.radication_attorney_service import save_attorney_radication
from app.main.radication.util.dto.resource_documents_dto import ResourceDTO


api = ResourceDTO.api


@api.route('/radication_attorney')
class NewAttorneyRadication(Resource):
    #@login_app
    @audit_init
    @audit_finish
    @api.doc('new attorney')
    def post(self):
        data = request.json
        return_value = save_attorney_radication(data)
        return return_value
from app.main import db
from app.main import logger
import datetime
import io
import os
from app.main.notification.service import attorney_service
from flask_restplus import marshal
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.core.service.general_service import manage_db_exception


def save_attorney_radication(data):
    try:
        attorney = attorney_service.save_new_attorney(data)
        response_object = {
            'status': 'success',
            'message': 'attorney almacenado correctamente',
        }
        return attorney, 200

    except Exception as e:
        db.session.rollback()
        return logger.error('Error al guardar attorney radication ' + str(e)), 500

def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='claimer notification service')
        db.session.rollback()
        newLogger.error(e)
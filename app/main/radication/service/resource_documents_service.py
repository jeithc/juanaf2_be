from app.main.radication.model.claimer_resource_document import *
from app.main.radication.model.resource_attached_documents import Resourceattachedocuments
from app.main.core.service.general_service import manage_db_exception
from sqlalchemy.sql import text
from app.main import logger
import datetime


def view_documents_resource(self,resource_id):
    #documents = Claimeresourcedocument.query.join(Claimeresourcedocument.roles).filter(Role.short_description == role).all()
    t = text("select RE.resource_type, DT.review_document, CD.url_document ,  CD.radicate_number, CD.radication_date, CR.attached_document_id, CD.number_of_pages from phase_2.claimer_resource_document  RE \
        inner join phase_2.resource_attached_documents CR on RE.resource_id = CR.resource_id \
        inner join phase_2.claimer_documents CD on CD.document_id = CR.document_id \
        inner join core.document_type DT on DT.id = CD.document_type_id \
        where RE.resource_id = :id order by RE.resource_type ASC")
    response_object=db.engine.execute(t, id=resource_id).fetchall()
    db.session.close()
    if response_object:
        return response_object
    else:
        response_object = {
            'status': 'fail',
            'message': "no se encontraron documentos para el recurso {}".format(resource_id),
        }
        return response_object, 204


def save_new_attachment(data):
    logger.debug('Entro a resource_documents_service#save_new_attachment')

    new_resource = Resourceattachedocuments(
        document_id=data['documentId'],
        resource_id=data['resourceId'],
        attached_document_type=data['attachedDocumentType'],
        attached_document_radication_da=datetime.datetime.now()
    )

    if 'attachedStage' in data:
        new_resource.attached_stage = data['attachedStage']

    try:
        db.session.add(new_resource)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'attachment almacenado exitosamente.'
        }
        return response_object, 201
    except Exception as e:
        return manage_db_exception("Error en resource_documents_service#save_new_attachment - Error al guardar el attachment",
                                   'Ocurrió un error al guardar el attachment.', e)


def update_attachment(data):
    logger.debug('Entro a resource_documents_service#update_attachment')

    try:
        attachment = Resourceattachedocuments.query.filter_by(attached_document_id=data['attachedDocumentId']).first()
    except Exception as e:
        return manage_db_exception("Error en resource_documents_service#update_resource - " +
                                   "Error al consultar el attachment",
                                   'Ocurrió un error al consultar el attachment.', e)
    if not attachment:
        response_object = {
            'status': 'fail',
            'message': 'El recurso no existe. Por favor verifique el id.'
        }
        return response_object, 200

    attachment.document_id = data['documentId'],
    attachment.resource_id = data['resourceId'],
    attachment.attached_document_type = data['attachedDocumentType'],
    attachment.attached_document_radication_da = data['documentRadicationDate']

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'attachment actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en resource_documents_service#update_attachment - " +
                                   "Error al actualizar el attachment",
                                   'Ocurrió un error al actualizar el attachment.', e)
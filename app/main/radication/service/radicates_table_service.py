from app.main.radication.model.claimer_resource_document import *
from app.main.util.dto import UserDto
from app.main.core.service.general_service import manage_db_exception
from app.main.radication.service import resource_documents_service
from app.main.radication.util.dto.radicates_table_dto import RadicatesDTO
from app.main.service import climer_documents_service
from app.main.core.service import workflow_service
from app.main.service import user_service
from flask_restplus import marshal
from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from sqlalchemy.sql import text
from app.main.core.service import report_service
from datetime import datetime, date
from app.main.notification.model.claimer_notification import ClaimerNotification
from sqlalchemy import and_
from flask import request
from app.main.core.model.users import Users
from app.main.model.user import User
from app.main.core.model.business_days import BusinessDays
from app.main.core.service import file_service
from app.main.model.claimer_documents import Claimer_documents
from app.main.service.jasper_serv import ReportJasper
import os
from app.main.service import s3_service
from werkzeug.utils import secure_filename
from app.main.radication.model.resource_attached_documents import Resourceattachedocuments
from app.main.service.logging_service import loggingBDError
from app.main.substantiation.service.claimer_resource_state_service import get_current_state
from app.main.substantiation.service.claimer_resource_state_service import get_prev_state


def get_claimer_resource(self, claimer_id):
    try:
        claimer_resource = ClaimerResourceDocument.query.filter_by(
            claimer_id=claimer_id).all()

        for resource in claimer_resource:
            resource_state = get_current_state(resource.resource_id)
            if resource_state:
                resource.current_state = resource_state.resource_state

            resource_prev_state = get_prev_state(resource.resource_id)
            if resource_prev_state:
                resource.prev_state = resource_prev_state.resource_state

        return claimer_resource
    except Exception as e:
        return manage_db_exception(
            "Error en radicates_table_service#get_claimer_resource - Error al consultar los recursos",
            'Ocurrió un error al consultar los recursos.', e)


def save_new_claimer(data):
    logger.debug('Entro a radicate_table_service#save_new_claimer')
    new_claimer = user_service.save_new_user(data)
    if hasattr(new_claimer, 'claimer_id'):
        workflow_service.make_claimer_transition(data)
        return marshal(new_claimer, UserDto.user), 200

    return new_claimer


def save_new_resource(data, user_email):
    logger.debug('Entro a radicate_table_service#save_new_resource')

    new_resource = ClaimerResourceDocument(
        aa_id=data['aaId'],
        claimer_id=data['claimerId'],
        has_attorney=data['hasAttorney'],
        resource_type=data['resourceType'],
        resource_radication_date=data['resourceRadicationDate'],
        resource_document_id=data['resourceDocumentId'],
        number_of_documents=data['numberOfDocuments'],
        number_of_pages=data['numberOfPages']
    )
    document_type = 21
    if new_resource.resource_type == 'QUEJA':
        new_resource.resource_placed_out_of_time = define_radicate_out_of_time(
          new_resource.claimer_id, new_resource.aa_id, 5)
        document_type = 74
    else:
        new_resource.resource_placed_out_of_time = define_radicate_out_of_time(
          new_resource.claimer_id, new_resource.aa_id, 10)

    if 'formerRadicateNumber' in data:
        new_resource.former_radicate_number = data['formerRadicateNumber']

    if 'attorneyId' in data:
        new_resource.attorney_id = data['attorneyId']

    user_system = Users.query.filter_by(email=user_email).first()
    if user_system:
        new_resource.user_id = user_system.id
    else:
        new_resource.user_id = 9999

    try:
        db.session.add(new_resource)
        db.session.commit()
        db.session.refresh(new_resource)

        for doc in data['attachments']:
            doc_name = secure_filename(doc['document']['docName'])
            doc['resourceId'] = new_resource.resource_id
            doc['document']['urlDocument'] = upload_resource_document(doc['document']['uuidCode'] +
                                                                      os.path.sep + doc_name,
                                                                      doc['document']['urlDocument'] +
                                                                      doc_name)
            user_email = request.headers.environ['HTTP_USER_ID']
            new_dc = climer_documents_service.save_new_claimer(
                doc['document'], user_email)
            doc['documentId'] = new_dc[0]['documentId']
            resource_documents_service.save_new_attachment(doc)
            if doc['attachedDocumentType'] == 'RECURSO':
                new_resource.resource_document_id = doc['documentId']
                internal_radication = doc['document']['flowDocument'] == '3'

        db.session.add(new_resource)
        db.session.commit()
        db.session.refresh(new_resource)

        new_resource = ClaimerResourceDocument.query.filter_by(
            resource_id=new_resource.resource_id).first()

        if not internal_radication:
            generate_radicate_document(
                new_resource.resource_id, new_resource.claimer_id, document_type)

        return marshal(new_resource, RadicatesDTO.resource), 201
    except Exception as e:
        return manage_db_exception("Error en radicates_table_service#save_new_resource - Error al guardar el recurso",
                                   'Ocurrió un error al guardar el recurso.', e)


def update_resource(data):
    logger.debug('Entro a radicates_table_service#update_resource')

    try:
        resource = ClaimerResourceDocument.query.filter_by(
            resource_id=data['resourceId']).first()
    except Exception as e:
        return manage_db_exception("Error en radicates_table_service#update_resource - " +
                                   "Error al consultar el recurso",
                                   'Ocurrió un error al consultar el recurso.', e)
    if not resource:
        response_object = {
            'status': 'fail',
            'message': 'El recurso no existe. Por favor verifique el id.'
        }
        return response_object, 200

    if 'analysisStage' in data:
        resource.analysis_stage = data['analysisStage']
    if 'analysisStageState' in data:
        resource.analysis_stage_state = data['analysisStageState']
    if 'answerInitialQuestion' in data:
        resource.answer_initial_question = data['answerInitialQuestion']
    if 'resourceType' in data:
        resource.resource_type = data['resourceType']
    if 'radicateNumberDp' in data:
        new_date = date(data['dateOfReceiptDp']['year'], data['dateOfReceiptDp']['month'],
                        data['dateOfReceiptDp']['day'])
        resource.radicate_number_dp = data['radicateNumberDp']
        resource.modify_resource_reason = data['modifyResourceReason']
        resource.old_resource_radication_date = resource.resource_radication_date
        resource.resource_radication_date = new_date
        resource.date_of_receipt_dp = new_date
        resource.resource_placed_out_of_time = define_radicate_out_of_time_with_date(
            new_date)
    if 'requiresProof' in data:
        resource.requires_proof = data['requiresProof']
    if 'requiresProofAppeal' in data:
        resource.requires_proof_appeal = data['requiresProofAppeal']

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'Recurso actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en radicates_table_service#update_resource - " +
                                   "Error al actualizar el recurso",
                                   'Ocurrió un error al actualizar el recurso.', e)


def find_radicate_number(radicate_number):
    logger.debug('Entro a radicates_table_service#find_radicate_number')

    try:

        t = text(
            "select * from phase_2.expedient_person_search where radicate_number = :radicate_number")

        radicate = db.engine.execute(
            t, radicate_number=radicate_number).fetchone()

        if radicate:
            return True
        else:
            return False

    except Exception as e:
        return manage_db_exception("Error en radicates_table_service#find_radicate_number - " +
                                   "Error al consultar el radicado",
                                   'Ocurrió un error al consultar el radicado.', e)


def define_radicate_out_of_time(claimer_id, aa_id, number_of_days):
    logger.debug('come in #get_claimer_notification')
    try:
        claimer_notification = ClaimerNotification.query.filter(
            and_(ClaimerNotification.claimer_id == claimer_id, ClaimerNotification.aa_id == aa_id,
                 ClaimerNotification.claimer_notified)).first()

        if not claimer_notification or not claimer_notification.notification_date:
            return False
        else:
            days_to_notify = get_next_business_days(
                claimer_notification.notification_date, number_of_days)
            today = datetime.now().date()

            if today > days_to_notify[number_of_days-1].business_day_date:
                return True
            else:
                return False

    except Exception as e:
        return logger.error("Error al consultar claimer_notification" + str(e))


def define_radicate_out_of_time_with_date(new_date):
    days_to_notify = get_next_business_days(new_date, 10)
    today = datetime.now().date()

    if today > days_to_notify[9].business_day_date:
        return True
    else:
        return False


def get_next_business_days(start_date, size):
    business_days = BusinessDays.query \
        .filter(and_(BusinessDays.business_day_date > start_date, BusinessDays.business_day_type == 'DIA_HABIL')) \
        .order_by(BusinessDays.business_day_date.asc()).limit(size).all()

    return business_days


def get_radicate_document(resource_id):
    logger.debug('Entro a radicate_table_service#get_sticker_document')

    resource = ClaimerResourceDocument.query.filter_by(
        resource_id=resource_id).first()

    # Constancia de radicacion web
    document_type = 21
    document = Claimer_documents.query.filter_by(document_id=resource.resource_document_radicate_proof).order_by(
        Claimer_documents.document_type_id.desc()).first()

    try:
        return ReportJasper.view_local_report(document.url_document)

    except Exception as e:
        return manage_db_exception(
            "Error en radicates_table_service#get_sticker_document - Error al generar sticker del recurso",
            'Ocurrió un error al guardar el recurso.', e)

    return report['report']


def generate_radicate_document(resource_id, claimer_id, document_type):
    logger.debug('Entro a radicate_table_service#get_sticker_document')

    document = Claimer_documents(
        claimer_id=claimer_id,
        document_type_id=document_type,
        expedient_entry_date=datetime.now(),
        flow_document=1,
        origin_document='SISTEMA',
        document_state=True,
        user_id=10000
    )

    save_changes(document)
    db.session.refresh(document)

    resource = ClaimerResourceDocument.query.filter_by(
        resource_id=resource_id).first()
    resource.resource_document_radicate_proof = document.document_id
    save_changes(resource)

    radicate_number = document.radicate_number
    # generate report
    params = {'param_claimer_id': claimer_id,
              'param_radicate_number': radicate_number,
              'param_resource_id': resource_id}

    try:
        report = report_service.create_report(report_service.get_report_route(
            document_type), params, 'pdf')

        if report is not None:
            file_service.save_report_file(report, document.document_id)

    except Exception as e:
        return manage_db_exception(
            "Error en radicates_table_service#get_sticker_document - Error al generar sticker del recurso",
            'Ocurrió un error al guardar el recurso.', e)


def generate_sticker_document(radicate_number, radicate_date, number_pages, number_of_attachments, claimer_id):
    logger.debug('Entro a radicate_table_service#generate_sticker_document')

    document_type = 22
    # generate report
    params = {'param_claimer_id': claimer_id,
              'param_radicate_number': radicate_number,
              'param_attached': number_of_attachments,
              'param_folios': number_pages,
              'param_timestamp': radicate_date
              }

    try:
        report = report_service.create_report(report_service.get_report_route(
            document_type), params, 'pdf')

    except Exception as e:
        return manage_db_exception(
            "Error en radicates_table_service#generate_sticker_document - Error al generar sticker del recurso",
            'Ocurrió un error al guardar el recurso.', e)

    return report['report']


def generate_radicate(claimer_id):
    update_query = 'select * from phase_2.f_return_radicate_number(' + str(
        claimer_id) + ') '
    result_db = db.engine.execute(
        text(update_query).execution_options(autocommit=True))
    for row in result_db:
        result = row['radicate_number']
        radication_date = row['generation_date']
    return {'number': process_radicate_number(str(result), True),
            'date': radication_date.strftime("%d/%m/%Y %H:%M:%S")}


def generate_radicate_document_flow(claimer_id, flow_document):
    result = None
    radication_date = None
    update_query = 'select * from phase_2.f_return_radicate_number(' + str(
        claimer_id) + ') '
    result_db = db.engine.execute(
        text(update_query).execution_options(autocommit=True))
    for row in result_db:
        result = row['radicate_number']
        radication_date = row['generation_date']
    if str(flow_document) == "1":
        return {'number': process_radicate_number(str(result), False),
                'date': radication_date.strftime("%d/%m/%Y %H:%M:%S")}
    elif str(flow_document) == "2":
        return {'number': process_radicate_number(str(result), True),
                'date': radication_date.strftime("%d/%m/%Y %H:%M:%S")}


def process_radicate_number(base_number, in_radicate):
    prefix = '2020003030'

    if in_radicate:
        sufix = '52'
    else:
        sufix = '51'

    while len(base_number) < 7:
        base_number = '0' + base_number

    return prefix + base_number + sufix


def upload_resource_document(path_from, path_to):
    """recupera el documento del buckect temporal y lo grada en la ubicacion definitiva"""

    return s3_service.copy_object_bucket(path_from, path_to)


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='claimer notification service')
        db.session.rollback()
        newLogger.error(e)


def get_resources(claimer_id):
    try:
        query = 'SELECT * from phase_2.f_resource_per_claimer(' + str(
            claimer_id) + ') where attached_document_type =\'RECURSO\' '
        result = db.engine.execute(
            text(query).execution_options(autocommit=True))
        resource_list = [row for row in result]
        print(result)
        return resource_list
    except Exception as e:
        print(e)
        newLogger = loggingBDError(
            loggerName='error al consultar los recursos')
        db.session.rollback()
        newLogger.error(e)


def get_docs_resources(resource_id):
    try:
        docs = Resourceattachedocuments.query.filter_by(
            resource_id=resource_id).all()
        return docs
    except Exception as e:
        newLogger = loggingBDError(
            loggerName='error al consultar los documentos del recurso')
        db.session.rollback()
        newLogger.error(e)


def get_docs_resources_by_claimer(claimer_id):
    try:
        docs = Resourceattachedocuments.query.join(
            ClaimerResourceDocument, ClaimerResourceDocument.resource_id == Resourceattachedocuments.resource_id).filter(
            ClaimerResourceDocument.claimer_id == claimer_id).all()
        return docs
    except Exception as e:
        newLogger = loggingBDError(
            loggerName='error al consultar los documentos del claimer')
        db.session.rollback()
        newLogger.error(e)


def get_acts_for_complain(claimer_id):
    try:
        query = ("select aa.aa_id, aa.aa_number, "
                    "aa.aa_number || ' (' ||final_act.add_list_txt(crd.resource_type||' con NR: '||cd.radicate_number) ||')' as resources "
                    "from phase_2.administrative_act aa "
                    "inner join phase_2.resource_per_administrative_act rpa on aa.aa_id = rpa.aa_id "
                    "inner join phase_2.claimer_resource_document crd on crd.resource_id = rpa.resource_id "
                    "inner join phase_2.claimer_resource_state crs on crs.resource_id = crd.resource_id "
                    "inner join phase_2.claimer_documents cd on cd.document_id = crd.resource_document_id "
                    "inner join phase_2.claimer_notification cn on cn.aa_id = aa.aa_id "
                    "where "
                    "aa.claimer_id = " + claimer_id +
                    " and crd.resource_type in ('APELACION','SUBSIDIO APELACION') "
                    "and crs.end_date is null "
                    "and crs.resource_state in ( "
                    "'RECHAZADO INTERES JURIDICO', "
                    "'RECHAZADO TEMPORALIDAD', "
                    "'DENEGADO POR LEGITIMACION', "
                    "'DENEGADO POR MOTIVACION' "
                    ")"
                    "and cn.claimer_notified = true "
                    "group by aa.aa_id, aa.aa_number "
                )
        try:
            data = db.engine.execute(query).fetchall()
            db.session.close()
        except Exception as e:
            return manage_db_exception('Error filtro', e)

        data = [dict(row) for (row) in data]
        return data

    except Exception as e:
        new_logger = loggingBDError(
          loggerName='error al consultar los actos del claimer')
        db.session.rollback()
        new_logger.error(e)

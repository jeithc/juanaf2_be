from app.main import db
from sqlalchemy.orm import relationship


class Resourceattachedocuments(db.Model):
    """ ResourceAttacheDocuments Model for storing Claimeresourcedocument related details """
    __tablename__ = "resource_attached_documents"
    __table_args__ = {'schema': 'phase_2'}

    attached_document_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_id = db.Column(db.Integer,  db.ForeignKey('phase_2.claimer_documents.document_id'))
    resource_id = db.Column(db.Integer, db.ForeignKey('phase_2.claimer_resource_document.resource_id'))
    attached_document_type = db.Column(db.String(100))
    attached_document_radication_da = db.Column(db.DateTime)
    document = relationship('Claimer_documents')
    attached_stage = db.Column(db.String(128))

    def __repr__(self):
        return "<ResourceAttacheDocuments '{}'>".format(self.attached_document_id)

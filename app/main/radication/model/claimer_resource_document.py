from app.main import db
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey
from app.main.radication.model import resource_attached_documents


class ClaimerResourceDocument(db.Model):
    """ ClaimerResourceDocument Model for storing ClaimerResourceDocument related details """
    __tablename__ = "claimer_resource_document"
    __table_args__ = {'schema': 'phase_2'}

    resource_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aa_id = db.Column(db.Integer, ForeignKey('phase_2.administrative_act.aa_id'))
    claimer_id = db.Column(db.Integer)
    has_attorney = db.Column(db.Integer)
    attorney_id = db.Column(db.Integer)
    resource_type = db.Column(db.String(100))
    resource_radication_date = db.Column(db.DateTime)
    resource_document_id = db.Column(db.Integer, ForeignKey('phase_2.claimer_documents.document_id'))
    number_of_documents = db.Column(db.Integer)
    number_of_pages = db.Column(db.Integer)
    resource_placed_out_of_time = db.Column(db.Boolean)
    former_radicate_number = db.Column(db.String(50))
    attachments = relationship('Resourceattachedocuments')
    document = relationship('Claimer_documents')
    aa = relationship('Aa')
    resource_document_radicate_proof = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    answer_initial_question = db.Column(db.Boolean)
    modify_resource_reason = db.Column(db.String(100))
    radicate_number_dp = db.Column(db.String(50))
    date_of_receipt_dp = db.Column(db.DateTime)
    old_resource_radication_date = db.Column(db.DateTime) 
    analysis_stage = db.Column(db.String)
    analysis_stage_state = db.Column(db.String)
    requires_proof = db.Column(db.Boolean)
    requires_proof_appeal = db.Column(db.Boolean)

    def __repr__(self):
        return "<ClaimerResourceDocument '{}'>".format(self.resource_id)

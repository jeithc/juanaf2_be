from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.juana1.util.dto.juana1_dto import Juana1DTO
from app.main.util.decorator import audit_init, audit_finish
from app.main.juana1.service.juana1_service import *


api = Juana1DTO.api

@api.route('/list_radicates/<claimer_id>')
@api.param('claimer_id', 'claimer_id')
class ListRadicates(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(Juana1DTO.ListExpedients)
    @api.doc('list radicates for user juana1')
    def get(self, claimer_id):
        """ list radicates juana1 """
        return list_radicates(claimer_id)


@api.route('/search_radicate/<radicate>')
@api.param('radicate', 'radicate')
class SearchRadicates(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(Juana1DTO.ListExpedients)
    @api.doc('search radicate juana1')
    def get(self, radicate):
        """ search radicate user juana 1"""
        return search_radicates(radicate)


@api.route('/questions/<claimer_id>')
@api.param('claimer_id', 'claimer_id')
class Questions(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(Juana1DTO.ListQuestions)
    @api.doc('search questions juana1')
    def get(self, claimer_id):
        """ search questions user juana 1 """
        return questions(claimer_id)


@api.route('/import/<document_id>/<claimer_id>/<resource_id>/<user_id>')
@api.param('document_id', 'document_id')
@api.param('claimer_id', 'claimer_id')
@api.param('resource_id', 'resource_id')
@api.param('user_id', 'user_id')
class ImportRadicates(Resource):
    @audit_init
    @audit_finish
    @api.doc('import radicate juana1')
    def get(self,document_id, claimer_id, resource_id, user_id):
        """ import radicate user juana 1"""
        return import_juana1(document_id,claimer_id, resource_id ,user_id)


@api.route('/zone/<claimer_id>')
@api.param('claimer_id', 'claimer_id')
class Zone(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(Juana1DTO.DataZone)
    @api.doc('search data juana1')
    def get(self, claimer_id):
        """ search data user juana 1 """
        return zone(claimer_id)
import json
import datetime
from app.main import db
from app.main.service.domain_service import get_list_domain
from app.main.juana1.model.juana1_radicate import RadicateJ1
from app.main.juana1.model.juana1_document import DocumentJ1
from app.main.juana1.model.juana1_expedient_document import ExpedientDocumentJ1
from app.main.juana1.model.juana1_expedient import ExpedientJ1
from app.main.model.claimer_documents import Claimer_documents_view as cdv
from app.main.model.claimer_documents import Claimer_documents
from app.main.notification.service.claimer_notification_service import get_filename_parts
from app.main.core.service.file_service import complete_document_data
from app.main.service.logging_service import loggingBDError
from sqlalchemy import text



def list_radicates(claimer_id):
    
    # list_expedients = db.session.query(RadicateJ1.box, RadicateJ1.number, DocumentJ1).join(DocumentJ1, RadicateJ1.id == DocumentJ1.radicate_id)\
    #     .join(ExpedientDocumentJ1, ExpedientDocumentJ1.document_id==DocumentJ1.id)\
    #     .join(ExpedientJ1, ExpedientJ1.id==ExpedientDocumentJ1.expedient_id)\
    #     .filter(ExpedientJ1.claimer_id==claimer_id).all()

    query= text("select r.number, e.claimer_id,  d.id, d.serie, d.subserie, d.initial_page, concat(box,'/',number,'.pdf') as document from juana_1.radicate r \
        inner join juana_1.document d on d.radicate_id = r.id \
        inner join juana_1.expedient_document ed on ed.document_id = d.id \
        inner join juana_1.expedient e on e.id = ed.expedient_id \
        where e.claimer_id = :id")
    list_expedients = db.engine.execute(query.execution_options(autocommit=True), id=claimer_id).fetchall()
    db.session.close()

    return list_expedients


def search_radicates(radicate):
    query= text("select r.number, e.claimer_id, d.id, d.serie, d.subserie, d.initial_page, concat(box,'/',number,'.pdf') as document from juana_1.radicate r \
        inner join juana_1.document d on d.radicate_id = r.id \
        inner join juana_1.expedient_document ed on ed.document_id = d.id \
        inner join juana_1.expedient e on e.id = ed.expedient_id \
        where r.number = :id")
    list_expedients = db.engine.execute(query.execution_options(autocommit=True), id=radicate).fetchall()
    db.session.close()
    return list_expedients


def questions(claimer_id):
    query= text("select q.title, da.answer, da.document_id \
        from juana_1.questions_per_subserie_j1 q \
        inner join juana_1.document_answer_j1 da on da.question_id = q.question_id \
        inner join juana_1.expedient e on e.id = da.expedient_id \
        where e.claimer_id = :id order by da.document_id")
    list_questions = db.engine.execute(query.execution_options(autocommit=True), id=claimer_id).fetchall()
    db.session.close()
    return list_questions


def zone(claimer_id):
    query= text("select cud.claimer_id, sub_group zona_afectacion,sub_condition as cond_subjetiva \
        from phase_2.final_act_table fat \
        inner join phase_2.claimer_user_data cud on cud.expedient_id= fat.expedient_id \
        where cud.claimer_id=:id")
    zone = db.engine.execute(query.execution_options(autocommit=True), id=claimer_id).fetchall()
    db.session.close()
    return zone


def import_juana1(id, claimer_id , resource_id, user_id):  
    total = 0 

    if(id=='0' or id==0):
        # verificamos que el usuario no tenga documentos ya migrados
        total=cdv.query.filter_by(claimer_id=claimer_id, document_type_id = 61).count()
        if total > 0:
            message= {
                'status': 'fail',
                'message': 'Ya existen documentos o subseries de Doña Juana 1 asociadas a Doña Juana le responde,\
                por favor verifique, en caso de requerir adicionar todos los documentos debe realizarlo uno a uno.',
            }
        else:
            query = text('select phase_2.f_migrate_documents_to_j1_djlr(:claimer_id, :resource_id, :user_id)')
            data=db.engine.execute(query.execution_options(autocommit=True), claimer_id=claimer_id, resource_id=resource_id ,user_id=user_id).fetchone()
    else:
        query = text('select phase_2.f_migrate_documents_to_j1_djlr(:claimer_id, :resource_id, :user_id, :id)')
        data=db.engine.execute(query.execution_options(autocommit=True), claimer_id=claimer_id, resource_id=resource_id, id=id ,user_id=user_id).fetchone()
    db.session.close()   

    list_claimer_document(claimer_id)

    if total > 0:
        result=message
    elif data[0]!=0:
        result= {
            'status': 200,
            'message': 'documentos migrados al recurso',
        } 
    else:
        result = {
            'status': 'fail',
            'message': 'Ya existe el documento de Doña Juana 1 asociado a Doña Juana le responde, por favor verifique',
        }  
    return result


def list_claimer_document(claimer_id):
    documents = Claimer_documents.query.filter(Claimer_documents.claimer_id==claimer_id, 
        Claimer_documents.number_of_pages == None, 
        Claimer_documents.origin_document == 'MIGRADO J1').all()

    for document in documents:
        path, filename = get_filename_parts(document)
        data_document=complete_document_data(path, filename)
        if data_document != 'error':
            document.document_size = data_document['size']
            document.document_checksum = data_document['checksum']
            document.number_of_pages = data_document['pages']
            save_changes(document)


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        print(e)
        newLogger = loggingBDError(loggerName='juana1_service')
        db.session.rollback()
        newLogger.error(e)
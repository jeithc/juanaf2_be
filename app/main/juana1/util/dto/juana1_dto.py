from flask_restplus import Namespace, fields


class Juana1DTO:

    api = Namespace('juana1', description='operaciones con datos de juana1')

    ListExpedients = api.model('List_expedients', {
        'number': fields.String(attribute='number'),
        'claimer_id': fields.String(attribute='claimer_id'),
        'id': fields.Integer(attribute='id'),
        'serie': fields.String(attribute='serie'),
        'subserie': fields.String(attribute='subserie'),
        'document': fields.String(attribute='document'),
        'initial_page': fields.String(attribute='initial_page')
    })


    ListQuestions = api.model('List_questions', {
        'title': fields.String(attribute='title'),
        'answer': fields.String(attribute='answer'),
        'document_id': fields.Integer(attribute='document_id')
    })


    DataZone = api.model('data_zone', {
        'zona_afectacion': fields.String(attribute='zona_afectacion'),
        'cond_subjetiva': fields.String(attribute='cond_subjetiva')
    })
from app.main import db


class AnswerJ1(db.Model):
    """ document_answer_j1 model juana1 """
    __tablename__ = 'document_answer_j1'
    __table_args__ = {'schema': 'juana_1'}

    answer_id = db.Column(db.Integer, primary_key=True)
    document_id = db.Column(db.Integer, db.ForeignKey('juana_1.document.id'))
    expedient_id = db.Column(db.Integer, db.ForeignKey('juana_1.expedient.id'))
    questionary_id = db.Column(db.Integer)
    answer = db.Column(db.Boolean)
    question_id = db.Column(db.Integer)

    def __repr__(self):
        return "<document_answer_j1: {}>".format(self.answer_id)
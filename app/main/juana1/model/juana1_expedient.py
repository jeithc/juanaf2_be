from app.main import db


class ExpedientJ1(db.Model):
    """ expedient model juana1 """
    __tablename__ = 'expedient'
    __table_args__ = {'schema': 'juana_1'}

    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.Integer)
    petitioner_id = db.Column(db.Integer)
    responsible_id = db.Column(db.Integer)
    exp_number = db.Column(db.String(600))
    exp_jur_ana = db.Column(db.Integer)
    expedient_merge_id = db.Column(db.Integer)
    analysis_date = db.Column(db.DateTime)
    substate = db.Column(db.Integer)
    generator_id = db.Column(db.Integer)
    integration_err = db.Column(db.Integer)
    integration_err_detail = db.Column(db.String(256))
    integration_err_date = db.Column(db.DateTime)
    returned_error = db.Column(db.Integer)
    claimer_id = db.Column(db.Integer)

    def __repr__(self):
        return "<expedient '{}-{}'>".format(self.id, self.exp_number)
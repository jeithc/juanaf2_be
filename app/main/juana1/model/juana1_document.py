from app.main import db


class DocumentJ1(db.Model):
    """ document model juana1 """
    __tablename__ = 'document'
    __table_args__ = {'schema': 'juana_1'}

    id = db.Column(db.Integer, primary_key=True)
    serie = db.Column(db.String(10))
    subserie = db.Column(db.String(12))
    initial_page = db.Column(db.Integer)
    radicate_id = db.Column(db.Integer, db.ForeignKey('juana_1.radicate.id'))
    exp_date = db.Column(db.Date)
    begin_date = db.Column(db.Date)
    birth_date = db.Column(db.Date)
    contract_term = db.Column(db.Integer)
    poll_number = db.Column(db.Integer)
    family_number = db.Column(db.Integer)
    profession_card = db.Column(db.String(20))
    state = db.Column(db.Integer)
    digitator_comment = db.Column(db.String(8012))
    illegible_minute_id = db.Column(db.Integer)
    subserie_ille = db.Column(db.String(12))
    lapse = db.Column(db.String(10))
    illegible_minute_id = db.Column(db.Integer)
    validated_serie = db.Column(db.Boolean)


    def __repr__(self):
        return "<document '{}'>".format(self.id)

 

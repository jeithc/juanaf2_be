from app.main import db


class RadicateJ1(db.Model):
    """ radicate model juana1 """
    __tablename__ = 'radicate'
    __table_args__ = {'schema': 'juana_1'}

    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.Integer)
    box = db.Column(db.Integer)
    folder = db.Column(db.Integer)
    folio = db.Column(db.Integer)
    number = db.Column(db.String(20))
    identification = db.Column(db.String(128))
    person_name = db.Column(db.String(500))
    radicate_date = db.Column(db.Date)
    responsible_id = db.Column(db.Integer)
    blind_id = db.Column(db.Integer)

    def __repr__(self):
        return "<radicate '{}'>".format(self.id)

 

from app.main import db


class QuestionsJ1(db.Model):
    """ questions_per_subserie_j1 model juana1 """
    __tablename__ = 'questions_per_subserie_j1'
    __table_args__ = {'schema': 'juana_1'}

    question_id = db.Column(db.Integer, primary_key=True)
    questionary_id = db.Column(db.Integer)
    serie = db.Column(db.String(10))
    subserie = db.Column(db.String(12))
    first = db.Column(db.Integer)
    code = db.Column(db.String(256))
    title = db.Column(db.String(256))
    active = db.Column(db.Boolean)

    def __repr__(self):
        return "<questions_per_subserie_j1: {}>".format(self.question_id)

 

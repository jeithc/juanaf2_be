from app.main import db


class ExpedientDocumentJ1(db.Model):
    """ expedient document model juana1 """
    __tablename__ = 'expedient_document'
    __table_args__ = {'schema': 'juana_1'}

    expedient_id = db.Column(db.Integer,  primary_key=True)
    document_id = db.Column(db.Integer,   db.ForeignKey('juana_1.document.id'))
    lapse = db.Column(db.String(30))


    def __repr__(self):
        return "<expedient: {}, document: '{}'>".format(self.expedient_id, self.document_id)

 

from flask import request, Flask, send_file
from flask_restplus import Resource
import io
from app.main.util.decorator import audit_init, audit_finish, login_app
from ..util.dto import UserDto, FilterUser, PagedFilter, ReportRequestDTO, DocumentDTO
from ..service.user_service import get_claimer_for_atril, get_all_users, get_a_user, get_users_by_filter, user_update, \
    get_total_by_filter, get_user_radicates, get_user_documents, getTotalEmail, get_by_filter_integral_search, \
    get_total_integral_search, get_all_user_documents
from app.main.model.user import User
from ..service.auth_service import basic_authentication, get_fail_authentication


api = UserDto.api
_user = UserDto.user
_filter_in = FilterUser.filter_detail_in
_filter_out = FilterUser.filter_detail_out
_filter_out_integral = FilterUser.filter_integral_out
_total = PagedFilter.filter_detail_total
_report_request = ReportRequestDTO.report_request


@api.route('/v1.0.0/user/validate')
class ValidateUser(Resource):
    @api.expect(_filter_in, validate=False)
    # @login_app
    @audit_init
    @audit_finish
    @api.marshal_with(_filter_out)
    @api.doc('validates an user')
    def post(self):
        """validate user with filters """
        data = request.json
        return_value = get_users_by_filter(data)
        return return_value


@api.route('/v1.0.0/user/validate_filter')
class ValidateUser(Resource):
    @api.expect(PagedFilter.filter_detail, validate=False)
    @audit_init
    @audit_finish
    # @api.marshal_list_with(_total, envelope='data')
    @api.doc('validate total user with filter')
    def post(self):
        """validate total rows in validate user """
        data = request.json
        return get_total_by_filter(data)


@api.route('/v1.0.0/user/list')
class UserList(Resource):
    @api.doc('list of registered users')
    @audit_init
    @audit_finish
    # @admin_token_required
    @api.marshal_list_with(_user, envelope='data')
    def get(self):
        """List all registered users"""
        # if (basic_authentication(request)):
        return get_all_users()
        # else:
        # return get_fail_authentication()


@api.route('/v1.0.0/user/update')
class UserUpdate(Resource):
    @api.expect(UserDto.user_update, validate=False)
    @audit_init
    @audit_finish
    @api.response(201, 'User successfully updated.')
    @api.doc('Update User')
    def put(self):
        # if (basic_authentication(request)):
        """Update  User """
        data = request.json
        # print(data)
        return user_update(data=data)
        # else:
        # return get_fail_authentication()


@api.route('/v1.0.0/user/radicates/<claimer_id>')
@api.param('claimer_id', 'The User identifier')
class Radicates(Resource):
    @api.doc('get radicates for user')
    @audit_init
    @audit_finish
    # @api.marshal_with(UserDto.radicates)
    def get(self, claimer_id):
        """get a radicates for user"""
        radicates = get_user_radicates(claimer_id)
        return radicates


@api.route('/v1.0.0/user/documents/<claimer_id>')
@api.param('claimer_id', 'User Documents')
class Documents(Resource):
    @api.doc('get documents for user')
    @audit_init
    @audit_finish
    @api.marshal_with(DocumentDTO.documents)
    def get(self, claimer_id):
        """get documents for user"""
        radicates = get_user_documents(claimer_id)
        return radicates


@api.route('/v1.0.0/user/all_documents/<claimer_id>')
@api.param('claimer_id', 'All User Documents')
class AllDocuments(Resource):
    @api.doc('get all documents for user')
    @audit_init
    @audit_finish
    @api.marshal_with(DocumentDTO.documents_view)
    def get(self, claimer_id):
        """get documents for user"""
        documents = get_all_user_documents(claimer_id)
        return documents


@api.route('/v1.0.0/user/<claimer_id>')
@api.param('claimer_id', 'The User identifier')
class User(Resource):
    @api.doc('get user data')
    @api.marshal_with(_user, skip_none=False)
    @audit_init
    @audit_finish
    # @login_app
    def get(self, claimer_id):
        """get a user given its identifier  default:  1199902 """
        user = get_a_user(claimer_id)
        return user


@api.route('/v1.0.0/user/totalEmail/<email>')
@api.param('email', 'Email to search')
class UserEmail(Resource):
    @api.doc('get total email')
    def get(self, email):
        """ get total email """
        return getTotalEmail(email)


@api.route('/v1.0.0/claimer_for_atril/<doc_number>')
class User(Resource):
    @api.doc('get user for atril')
    # @audit_init
    # @audit_finish
    @login_app
    def get(self, doc_number):
        """Obtener datos para atril"""
        return get_claimer_for_atril(doc_number)

@api.route('/v1.0.0/user/filter_integral_total')
class FilterIntegralSearch(Resource):

    @api.expect(PagedFilter.filter_detail, validate=False)
    # @login_app
    @api.doc('validates an user')
    def post(self):
        """validate user with filters """
        data = request.json
        return_value = get_total_integral_search(data)
        return return_value


@api.route('/v1.0.0/user/filter_integral')
class FilterIntegralSearch(Resource):
    @api.expect(_filter_in, validate=False)
    @api.marshal_with(_filter_out_integral)
    # @api.marshal_list_with(_total, envelope='data')
    @api.doc('validate total user with filter')
    def post(self):
        """validate total rows in validate user """
        data = request.json
        return get_by_filter_integral_search(data)

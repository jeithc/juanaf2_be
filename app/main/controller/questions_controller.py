from flask import request, Flask, send_file
from flask_restplus import Resource
import io
from app.main.util.decorator import audit_init, audit_finish
from ..util.dto import QuestionsDto
from ..service.questions_service import get_list_questions, get_list_options, verify_answer
from app.main.model.questions import Questions
from app.main.util.decorator import audit_init, audit_finish, login_app

api = QuestionsDto.api

@api.route('/v1.0.0/questions/')
@api.response(404, 'questions no found')
class Domain(Resource):
    @api.doc('get questions list')
    @api.marshal_with(QuestionsDto.filter_questions)
    #@login_app
    @audit_init
    @audit_finish    
    def get(self):
        """get a questions list """  
        list_questions = get_list_questions()
        if list_questions:
            return list_questions
        else:
            response_object = {
            'status': 'fail',
            'message': 'no data',
            }
            return response_object


@api.route('/v1.0.0/questions/verify_answer')
@api.expect(QuestionsDto.answer, validate=False)
class answer(Resource):
    #@login_app
    @audit_init
    @audit_finish    
    @api.doc('get answer for the question user')
    def post(self):
        """verify answer questions  """
        data = request.json  
        result = verify_answer(data)
        return result


            

@api.route('/v1.0.0/questions/options/<claimer_id>/<question_id>')
@api.param('claimer_id', 'ID user')
@api.param('question_id', 'ID question')
#@api.response(404, 'options for questions not found.')
class QuestionApi(Resource):
    @api.doc('options for questions user authentication ')
    @api.marshal_with(QuestionsDto.option_list)
    def get(self, claimer_id, question_id):

        """
        Options for question authentication
        """
        options = get_list_options(claimer_id, question_id)
        return options 


# @api.route('/v1.0.0/questions/answers')
# @api.param('name', 'The domain name')
# @api.response(404, 'domain not found.')
# class Domain(Resource):
#     @api.doc('get domain list')
#     @api.marshal_with(DomainDto.domain)
#     def get(self, name):
#         """get a domain list """
                       
#         list_domain = get_list_domain(name)
#         if not list_domain:
#             response_object = {
#             'status': 'fail',
#             'message': 'no data to name domain',
#             }
#         else:
#             return list_domain                           
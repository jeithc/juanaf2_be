from flask_restplus import Resource
from ..util.dto import NewsDTO
from ..service.news_service import get_list
from app.main.model.news import News
from app.main.util.decorator import audit_init, audit_finish, login_app

api = NewsDTO.api

@api.route('/v1.0.0/news/<newsType>/<location>')
@api.response(404, 'Data no found with user.')
class Domain(Resource):
    @api.doc('news list')
    @api.marshal_with(NewsDTO.news_filter)
    #@login_app
    @audit_init
    @audit_finish 
    def get(self, newsType, location):
        """get a news list """    
        list_news = get_list(newsType,location)
        return list_news   


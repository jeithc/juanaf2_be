import io
from flask import Flask, send_file, request
from ..util.dto import AuthDto, ReportRequestDTO, ReportDownloadDTO, ConclusiveDocumentDTO
from flask_restplus import Resource, fields
from ..service.jasper_serv import ReportJasper
from ..service.jasper_migrate import ReportJasperMigrate
from ..service.report_migrate_service import ReportJasperUserMigrate
from ..service.conclusive_conduct_service import ConclusiveConduct
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.service import s3_service
from app.main.core.service import file_service
import requests

report_request = ReportRequestDTO.report_request
report_download = ReportDownloadDTO.report_download
conclusive_documents = ConclusiveDocumentDTO.conclusive_documents
api = ReportRequestDTO.api


@api.route('/v1.0.0/report/getReport/<path:pathReport>')
class GetReportAPI(Resource):
    """Serves the logo image."""
    @api.doc('get report content')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, pathReport):
        data = pathReport
        nameReport = data.split("/")
        return send_file(
            io.BytesIO(ReportJasper.view_local_report(data)),
            mimetype='application/pdf',
            attachment_filename=nameReport[-1]
        )


@api.route('/v1.0.0/report/getReportWord/<path:pathReport>')
class GetReportWordAPI(Resource):
    """Serves the logo image."""
    @api.doc('get report content word')
    # @login_app
    @audit_init
    @audit_finish
    def get(self, pathReport):
        data = pathReport
        nameReport = data.split("/")
        return send_file(
            io.BytesIO(ReportJasper.view_local_report(data)),
            mimetype='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            attachment_filename=nameReport[-1]
        )


@api.route('/v1.0.0/report/generate')
class GenerateReportAPI(Resource):
    """Serves the logo image."""
    @api.doc('generate report')
    @api.expect(report_request)
    # @login_app
    @audit_init
    @audit_finish
    def post(self):
        data = request.json
        return ReportJasper.create_report(data)


@api.route('/v1.0.0/report/generateautomatic')
class GenerateReportAPIMigrate(Resource):
    """Serves the logo image."""
    @api.doc('generate report migrate')
    # @login_app
    @audit_init
    @audit_finish
    def post(self):
        return ReportJasperMigrate.run_migrate_report()


@api.route('/v1.0.0/report/recreate_s3_objects_migrate')
class RecreateS3Objects(Resource):
    """Serves the logo image."""
    @api.doc('Recreate s3 Objets')
    def post(self):
        return ReportJasperMigrate.run_migrate_report_s3()


@api.route('/v1.0.0/report/internal_generate')
class GenerateReportAPIInternal(Resource):
    """Serves the logo image."""
    @audit_init
    @audit_finish
    @api.doc('generate report')
    @api.expect(report_request)
    def post(self):
        data = request.json
        return ReportJasper.create_report_internal(data)


@api.route('/v1.0.0/report/internal_generateautomatic')
class GenerateReportAPIInternalMigrate(Resource):
    """Serves the logo image."""
    @audit_init
    @audit_finish
    @api.doc('generate report migrate internal')
    @api.expect(report_request)
    def post(self):
        data = request.json
        return ReportJasperUserMigrate.create_report_internal(data)


@api.route('/v1.0.0/report/internal_generateautomaticrecycle')
class GenerateReportAPIInternalMigrateRecycle(Resource):
    """Serves the logo image."""
    @audit_init
    @audit_finish
    @api.doc('generate report migrate internal recycle number report')
    @api.expect(report_request)
    def post(self):
        data = request.json
        return ReportJasperMigrate.create_report_internal_recycle()


@api.route('/v1.0.0/report/update_state_send_mail_migrate')
class GenerateReportAPIInternalMigrateStateMail(Resource):
    """Serves the logo image."""
    @audit_init
    @audit_finish
    @api.doc('update state send mail migrate')
    @api.expect(report_request)
    def post(self):
        data = request.json
        return ReportJasperMigrate.update_state_mail(data)


@api.route('/v1.0.0/report/send_mail_migrate_automatic')
class GenerateReportAPIInternalMigrateSendMail(Resource):
    """Serves the logo image."""
    @api.doc('send mail migrate automatic')
    def post(self):
        return ReportJasperMigrate.send_mail_migrate()


@api.route('/v1.0.0/report/list_s3_objects')
class ListS3Objects(Resource):
    """Serves the logo image."""
    enviroment_fields = api.model('Enviroment', {
        'enviroment': fields.String(required=True, description='Enviroment s3 bucket')
    })
    @api.expect(enviroment_fields)
    @api.doc('List s3 Objets')
    def post(self):
        data = request.json
        return ReportJasperMigrate.list_objects_s3(data)


@api.route('/v1.0.0/report/clone_object_s3')
class CloneS3(Resource):
    """Serves the logo image."""
    clone_s3_request = api.model('request_details', {
        'typeDocument': fields.Integer(required=True, description='Type document')
    })
    @api.doc('Clone s3')
    @api.expect(clone_s3_request)
    def post(self):
        data = request.json
        return ReportJasper.copy_objects_bucket_protopre(data)


@api.route('/v1.0.0/report/copy_object_s3/<document>/<subgroup>/<folder>')
@api.param('document', 'id document to copy')
@api.param('subgroup', 'subgroup to copy')
@api.param('folder', 'folder to create documents')
class ListS3Objects(Resource):
    """Serves the logo image."""
    @api.doc('Copy s3 Objets')
    def get(self, document, subgroup, folder):
        return ReportJasper.copy_objects_bucket(document, subgroup, folder)


@api.route('/v1.0.0/report/conclusive_report')
class GenerateReportConclusiveConduct(Resource):
    """Serves the logo image."""
    @audit_init
    @audit_finish
    @api.doc('generate report conclusive conduct')
    @api.expect(conclusive_documents)
    def post(self):
        data = request.json
        return ConclusiveConduct.create_report(data)


@api.route('/v1.0.0/report/search_conclusive_conduct')
class SearchConclusiveConduct(Resource):
    """Serves the logo image."""
    @audit_init
    @audit_finish
    @api.doc('generate report conclusive conduct')
    @api.expect(conclusive_documents)
    def post(self):
        data = request.json
        return ConclusiveConduct.search_conclusive_conduct(data)


@api.route('/v1.0.0/report/delete_object_bucket/<path:pathDocument>')
class DeleteObjectBucket(Resource):
    """delete object of bucket"""
    @api.doc('delete object of bucket')
    def delete(self, pathDocument):
        print(pathDocument)
        return s3_service.delete_object_bucket(pathDocument)


@api.route('/v1.0.0/report/save_file_temp')
class File(Resource):
    @audit_init
    @audit_finish
    @api.response(201, 'Archivo almacenado exitosamente.')
    @api.response(500, 'Ocurrió un error al guardar el archivo.')
    @api.doc('create a new file')
    # @auth.login_required
    def post(self):
        """Creates a new file """
        input_file = request.files['inputFile']
        return file_service.save_file_temp(input_file)


@api.route('/v1.0.0/report/save_upload_file_f1')
class File(Resource):
    request_f1_upload = api.model('request_details', {
        'path': fields.String(required=True, description='Path')
    })
    @audit_init
    @audit_finish
    @api.doc('Upload file f1 to bucket')
    @api.expect(request_f1_upload)
    def post(self):
        data = request.json
        return ReportJasperMigrate.run_upload_f1_bucket(data)


@api.route('/v1.0.0/report/move_file_s3')
class File(Resource):
    request_f1_upload = api.model('request_details', {
        'path': fields.String(required=True, description='Path')
    })
    @audit_init
    @audit_finish
    @api.doc('Upload file server to bucket')
    @api.expect(request_f1_upload)
    def post(self):
        data = request.json
        return ReportJasperMigrate.run_upload_bucket(data)


@api.route('/v1.0.0/report/upload_temp_files/<path>')
@api.param('path', 'ruta de carpeta')
class File(Resource):
    @api.doc('Upload file temp to bucket')
    def get(self, path):
        return ReportJasperMigrate.upload_files_bucket(self, path)


@api.route('/v1.0.0/report/upload_juana1_files/<path>')
@api.param('path', 'ruta de carpeta sin primer separador')
class File(Resource):
    @api.doc('Upload file temp to bucket')
    def get(self, path):
        return ReportJasperMigrate.upload_files_juana1(self, path)

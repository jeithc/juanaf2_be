from flask import request, Flask, send_file
from flask_restplus import Resource
import io
from app.main.util.decorator import audit_init, audit_finish, login_app, admin_token_required
from ..util.dto import DomainDto
from ..service.domain_service import get_list_domain
from app.main.model.domain import Domain
from ..service.auth_service import basic_authentication, get_fail_authentication


api = DomainDto.api

@api.route('/v1.0.0/domain/<name>')
@api.param('name', 'The domain name')
@api.response(404, 'domain not found.')
class Domain(Resource):
    @api.doc('get domain list')
    @api.marshal_with(DomainDto.domain, skip_none=True)
    #@login_app
    #@audit_init
    #@audit_finish 
    def get(self, name):
        """get a domain list """
        list_domain = get_list_domain(name)
        if not list_domain:
            response_object = {
            'status': 'fail',
            'message': 'no data to name domain',
            }
            return response_object, 404
        else:
            return list_domain                            
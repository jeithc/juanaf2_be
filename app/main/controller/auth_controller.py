from flask import request, make_response
from flask_restplus import Resource
from app.main.service.auth_helper import Auth, update_password_new
from ..util.dto import AuthDto
from ..util.dto import RecoverPasswordDto, QuestionAnswersDTO
from ..service.auth_service import basic_authentication, get_fail_authentication
from app.main.util.decorator import audit_init, audit_finish, login_app
api = AuthDto.api
user_auth = AuthDto.user_auth
recover_data = RecoverPasswordDto.recover_data


@api.route('/v1.0.0/authentication/login')
class UserLogin(Resource):
    @api.doc('user login')
    @api.expect(user_auth, validate=False)
    #@login_app
    @audit_init
    @audit_finish
    def post(self):
        #print(request.headers.environ)
        """User Login Resource"""
        # get the post data
        post_data = request.json       
        return Auth.login_user(post_data) 


@api.route('/v1.0.0/authentication/hash/<password>')
@api.param('password', 'password')
class UserLogin(Resource):
    @api.doc('hash password')
    def get(self, password):
        #print(request.headers.environ)
        """create password hash"""
        # get the post data     
        return Auth.password_hash_new(password)                                                


@api.route('/v1.0.0/authentication/logout/<claimer_id>')
@api.param('claimer_id', 'ID user')
class LogoutAPI(Resource): 
    @api.doc('logout a user')
    #@login_app
    @audit_init
    @audit_finish 
    def get(self, claimer_id):
        
        """
        Logout Resource
        """
        # get auth token
        auth_header = request.headers.get('Authorization')
        #if (basic_authentication(request)):        
        return Auth.logout_user(data=claimer_id)
        #else:
        #return get_fail_authentication()            
        

@api.route('/v1.0.0/authentication/createpassword/')
class Pass_init(Resource): 
    @api.doc('inicial password user')
    #@api.marshal_with(Password.password)
    #@login_app
    @audit_init
    @audit_finish   
    def get(self):
        """
        init password Resource
        """
        return Auth.init_password()


@api.route('/v1.0.0/authentication/recover/<username>/<email>')
@api.param('username', 'the user username')
@api.param('email', 'the user email')
@api.response(404, 'Data no found with user.')
class RecoverAPI(Resource):
    @api.doc('recover password')
    #@login_app
    @audit_init
    @audit_finish
    def get(self, username, email): 
        """
        Recover user password
        """
        # get the post data
        #if (basic_authentication(request)):
        return Auth.change_password(username, email)
        #else:
           #return get_fail_authentication()


@api.route('/v1.0.0/authentication/changepassword')
class ChangePassAPI(Resource):
    @api.doc('change password')
    @api.expect(RecoverPasswordDto.change_password)
    #@login_app
    @audit_init
    @audit_finish
    def put(self): 
        """
        Change user password
        """
        # get the post data
        data = request.json      
        return Auth.update_password(data)



@api.route('/v1.0.0/authentication/update_password/<username>/<email>')
@api.param('username', 'the user username')
@api.param('email', 'the user email')
class UpdatePass(Resource):
    @api.doc('update password')
    #@login_app
    @audit_init
    @audit_finish
    def get(self, username, email):
        """
        Recover user password
        """
        return update_password_new(username, email)


# @api.route('/v1.0.0/authentication/recover')
# class RecoverAPI(Resource):
#     """
#     Recover Resource
#     """
#     @api.doc('logout a user')
#     def post(self):
#         # get auth token
#         print("test")
#         auth_header = request.headers.get('Authorization')        
#         return Auth.logout_user(data=auth_header)
from flask_restplus import Resource

from app.main.service import notorious_facts_places_service
from ..util.dto import NotoriousFactsPlacesDto

api = NotoriousFactsPlacesDto.api

@api.route('/v1.0.0/notorious_facts_places/')
class Domain(Resource):
    @api.doc('get NotoriousFactsPlaces list')
    @api.marshal_with(NotoriousFactsPlacesDto.notorious_facts_places)
    def get(self):
        """get a NotoriousFactsPlaces list """
        list_nfp = notorious_facts_places_service.get_list()
        if not list_nfp:
            response_object = {
                'status': 'fail',
                'message': 'no data to notorious facts',
            }
        else:
            return list_nfp
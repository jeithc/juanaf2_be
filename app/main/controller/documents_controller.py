import io
from flask import Flask, send_file, request
from ..util.dto import AuthDto, ReportRequestDTO, ReportDownloadDTO
from flask_restplus import Resource
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.service.climer_documents_service import get_radicate_documents, claimer_search
from app.main.util.dto import RadicateFilerRequestDTO
from app.main.util.dto import RadicateDocumentDTO
from app.main.util.dto import DocumentDTO


report_download = ReportDownloadDTO.report_download
api = RadicateDocumentDTO.api


@api.route('/v1.0.0/document/radication')
class UserLogin(Resource):
    @api.doc('radications')
    @api.expect(RadicateFilerRequestDTO.radicate_filter, validate=False)
    # @login_app
    @audit_init
    @audit_finish
    @api.marshal_with(RadicateDocumentDTO.radicate_documents)
    def post(self):
        #print(request.headers.environ)
        """User Login Resource"""
        post_data = request.json
        return get_radicate_documents(post_data)


@api.route('/v1.0.0/document/get_documents_by_type/<claimer_id>/<document_type_id>')
class GetDocuments(Resource):
    @api.doc('documents by type')
    # @login_app
    @audit_init
    @audit_finish
    @api.marshal_with(DocumentDTO.documents)
    def get(self, claimer_id, document_type_id):
        """Buscar documentos por tipo"""
        return claimer_search(claimer_id, document_type_id)

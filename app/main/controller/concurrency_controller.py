import io
from flask import Flask, send_file, request
from flask_restplus import Resource
from ..service.concurrency_service import DocumentNonConcurrency
from app.main.util.decorator import audit_init, audit_finish, login_app
from ..util.dto import DocumentNonConcurrencyDTO
import requests

api = DocumentNonConcurrencyDTO.api

@api.route('/v1.0.0/document/generatenonconcurrency')
class GenerateDocumentNonConcurrencyAPI(Resource):
    """Serves the logo image."""
    @api.doc('generate non concurrency document')
    #@login_app
    @audit_init
    @audit_finish    
    def post(self):
        return  DocumentNonConcurrency.create_report()
from flask_restplus import Resource
from ..util.dto import Nat_geoDto
from ..service.nat_geo_service import get_list, get_list_locality
from app.main.model.domain import Domain
from ..service.auth_service import basic_authentication, get_fail_authentication
from app.main.util.decorator import audit_init, audit_finish, login_app

api = Nat_geoDto.api

@api.route('/v1.0.0/nat_geo/')
#@api.param('name', 'The domain name')
#@api.response(404, 'domain not found.')
class Domain(Resource):
    @api.doc('get Nat geo list')
    @api.marshal_with(Nat_geoDto.nat_geo)
    #@login_app
    #@audit_init
    #@audit_finish    
    def get(self):
        """get a domain list """    
        list_ng = get_list()
        if not list_ng:
            response_object = {
            'status': 'fail',
            'message': 'no data to name domain',
            }
        else:
            return list_ng   

@api.route('/v1.0.0/locality/')
#@api.param('name', 'The domain name')
#@api.response(404, 'domain not found.')
class Domain(Resource):
    @api.doc('get locality list')
    @api.marshal_with(Nat_geoDto.locality)
    #@login_app
    #@audit_init
    #@audit_finish    
    def get(self):
        """get a locality list """    
        list_ng = get_list_locality()
        if not list_ng:
            response_object = {
            'status': 'fail',
            'message': 'no data to locality',
            }
        else:
            return list_ng                            
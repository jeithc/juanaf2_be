from app.main import db

class Document_typology(db.Model) :
    """ Model of Document_typology """
    __tablename__ = "document_typology"
    __table_args__ = {'schema': 'core'}

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_typology = db.Column(db.String(64))
    generate_radication = db.Column(db.Boolean)
    active = db.Column(db.Boolean)

def __repr__(self):
    return "<document_typology '{}'>".format(self.id)

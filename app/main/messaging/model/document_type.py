from sqlalchemy.orm import relationship

from app.main import db

class Document_type(db.Model) :
    """ Model of Document_type """
    __tablename__ = "document_type"
    __table_args__ = {'schema': 'core'}

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_type = db.Column(db.String(128))
    document_typology_id = db.Column(db.Integer, db.ForeignKey('core.document_typology.id'))
    document_typology = relationship("Document_typology")
    review_document = db.Column(db.Text)
    active = db.Column(db.Boolean)
    claimer_visualized = db.Column(db.Boolean)
    physical_format = db.Column(db.Boolean)
    electronic_format = db.Column(db.Boolean)
    digital_format = db.Column(db.Boolean)
    document_route = db.Column(db.Text)

def __repr__(self):
    return "<document_type '{}'>".format(self.document_id)

from app.main import db

class ClaimerMessage(db.Model) :
    """ Model of messaging """
    __tablename__ = "claimer_message"
    __table_args__ = {'schema': 'phase_2'}

    claimer_message_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    aa_id = db.Column(db.Integer)
    person_expedient_id = db.Column(db.Integer)
    address_id = db.Column(db.Integer)
    message_provider_id = db.Column(db.String(50))
    track_id = db.Column(db.String(50))
    claimer_message_mode = db.Column(db.String(100))
    claimer_message_type = db.Column(db.String(100))
    sending_date = db.Column(db.Date)
    delivery_date = db.Column(db.Date)
    delivery_result = db.Column(db.String(100))
    email = db.Column(db.String(100))
    receiver_document_type = db.Column(db.String(100))
    receiver_document_number = db.Column(db.String(20))
    receiver_full_name = db.Column(db.String(128))
    success_notification_document_id = db.Column(db.Integer)
    document_tracking_id = db.Column(db.Integer)

    def __repr__(self):
        return "<claimer_message_id '{}'>".format(self.claimer_message_id)

#TODO: Guarda relacion message_documentsa
# class Message_document(db.Model):
#     """ Almacena todos los mensajes enviados a los reclamantes. """
#     __tablename__ = "message_document"
#     __table_args__ = {'schema':'phase_2'}
#     document_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     claimer_message_id  = db.Column(db.Integer)
       
#     def __repr__(self):
#         return "<Message_document '{}'>".format(self.document_id)
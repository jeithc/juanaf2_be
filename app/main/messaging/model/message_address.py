from app.main import db

class MessageAddress(db.Model) :
    """ Model of message address """
    __tablename__ = "message_address"
    __table_args__ = {'schema': 'phase_2'}

    address_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    country = db.Column(db.Integer)
    id_natgeo = db.Column(db.Integer)
    id_bog_location = db.Column(db.Integer)
    address = db.Column(db.String(255))
    urban_rural_location = db.Column(db.String(1))
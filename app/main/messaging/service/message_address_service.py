from app.main import Flask
from app.main import db
from app.main import logger
from app.main.messaging.model.message_address import MessageAddress
from app.main.core.service.general_service import manage_db_exception

def save_new_message_address(data):
    message_address = MessageAddress()

    if 'country' in data and data['country']:
        message_address.country = data['country']
    if 'idNatgeo' in data and data['idNatgeo']:
        message_address.id_natgeo = data['idNatgeo']
    if 'idBogLocation' in data and data['idBogLocation']:
        message_address.id_bog_location = data['idBogLocation']
    if 'address' in data and data['address']:
        message_address.address = data['address']
    if 'urbanRuralLocation' in data and data['urbanRuralLocation']:
        message_address.urban_rural_location = data['urbanRuralLocation']

    try:
        db.session.add(message_address)
        db.session.commit()
        print(message_address)
        return message_address , 200

    except Exception as e:
        db.session.rollback()
        return manage_db_exception('Error en #save_new_message_address - ',
                                    'Error al guardar message_address', e) 

def get_all_messageAddress():
    logger.debug('come in #get_message_address')
    messageAddresses = MessageAddress.query.all()
    return messageAddresses  

def updated_message_address(data):
    logger.debug('come in #updated_message_address')

    try:
        messageAddress = MessageAddress.query. \
            filter(MessageAddress.address_id == data['addressId']).first()
                   
        if not messageAddress:
            response_object = {
                'status': 'fail',
                'message': 'address no existe'
            }
            return response_object, 500
        else:
            messageAddress.country = data['country']
            messageAddress.address = data['address']
            messageAddress.urban_rural_location = data['urbanRuralLocation']
            

            db.session.add(messageAddress)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'address actualizada correctamente'
            }
            return response_object, 201

    except Exception as e:
        db.session.rollback()
        return logger.error('Error al actualizar message_address' + str(e)), 500

from flask_restplus import marshal

from app.main import logger
from app.main.messaging.util.dto.document_type_dto import DocumentTypeDto
from app.main.messaging.model.document_type import Document_type
from app.main.core.service.general_service import manage_db_exception


def get_all_types():
    logger.debug('Entro a document_type_service#get_all_types')
    try:
        users = Document_type.query.all()

        return marshal(users, DocumentTypeDto.document_type), 200
    except Exception as e:
        return manage_db_exception("Error en document_type_service#get_all_types - " +
                                   "Error al obtener los tipos de documentos",
                                   'Ocurrió un error al obtener los tipos de documentos.', e)

def get_types_by_topology(id):
    logger.debug('Entro a document_type_service#get_types_by_topology')
    try:
        documentos = Document_type.query.filter_by(document_typology_id=int(id)).all();

        return marshal(documentos, DocumentTypeDto.document_type), 200
    except Exception as e:
        return manage_db_exception("Error en document_type_service#get_all_types - " +
                                   "Error al obtener los tipos de documentos",
                                   'Ocurrió un error al obtener los tipos de documentos.', e)

def get_types_by_topology_request():
    logger.debug('Entro a document_type_service#get_types_by_topology_request')
    try:
        documentos = Document_type.query.filter(Document_type.document_typology_id.in_(
            [10,16,17,18,19])).all();

        return marshal(documentos, DocumentTypeDto.document_type), 200
    except Exception as e:
        return manage_db_exception("Error en document_type_service#get_all_types - " +
                                   "Error al obtener los tipos de documentos",
                                   'Ocurrió un error al obtener los tipos de documentos.', e)

def get_types_by_topology_request_documents():
    logger.debug('Entro a document_type_service#get_types_by_topology_request_documents')
    try:
        documentos = Document_type.query.filter(Document_type.id.in_(
            [37, 43, 44, 45, 50, 51, 58])).all()

        return marshal(documentos, DocumentTypeDto.document_type), 200
    except Exception as e:
        return manage_db_exception("Error en document_type_service#get_all_types - " +
                                   "Error al obtener los tipos de documentos",
                                   'Ocurrió un error al obtener los tipos de documentos.', e)

def get_types_by_topology_response_documents():
    logger.debug('Entro a document_type_service#get_types_by_topology_response_documents')
    try:

        documentos = Document_type.query.filter(Document_type.id.in_(
          [46, 48, 52])).all()

        return marshal(documentos, DocumentTypeDto.document_type), 200
    except Exception as e:
        return manage_db_exception("Error en document_type_service#get_all_types - " +
                                   "Error al obtener los tipos de documentos",
                                   'Ocurrió un error al obtener los tipos de documentos.', e)

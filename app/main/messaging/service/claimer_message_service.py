from app.main import Flask
from app.main import db
from app.main.messaging.model.claimer_message import ClaimerMessage
from app.main.core.service.general_service import manage_db_exception

def save_new_message(data):
    message = ClaimerMessage()

    if 'claimerId' in data and data['claimerId']:
        message.claimer_id = data['claimerId']
    if 'aaId' in data and data['aaId']:
        message.aa_id = data['aaId']
    if 'personExpedientId' in data and data['personExpedientId']:
        message.person_expedient_id = data['personExpedientId']
    if 'claimerMessageMode' in data and data['claimerMessageMode']:
        message.claimer_message_mode = data['claimerMessageMode']
    if 'claimerMessageType' in data and data['claimerMessageType']:
        message.claimer_message_type = data['claimerMessageType']
    if 'sendingDate' in data and data['sendingDate']:
        message.sending_date = data['sendingDate']
    if 'deliveryDate' in data and data['deliveryDate']:
        message.delivery_date = data['deliveryDate']
    if 'email' in data and data['email']:
        message.email = data['email']
    if 'addressId' in data and data['addressId']:
        message.address_id = data['addressId']
    if 'trackId' in data and data['trackId']:
        message.track_id = data['trackId']
    if 'messageProviderId' in data and data['messageProviderId']:
        message.message_provider_id = data['messageProviderId']
    if 'deliveryResult' in data and data['deliveryResult']:
        message.delivery_result = data['deliveryResult']
    if 'receiverDocumentType' in data and data['receiverDocumentType']:
        message.receiver_document_type = data['receiverDocumentType']
    if 'receiverDocumentNumber' in data and data['receiverDocumentNumber']:
        message.receiver_document_number = data['receiverDocumentNumber']
    if 'receiverFullName' in data and data['receiverFullName']:
        message.receiver_full_name = data['receiverFullName']
    if 'successNotificationDocumentI' in data and data['successNotificationDocumentI']:
        message.success_notification_document_id= data['successNotificationDocumentI']  
    if 'documentTrackingId' in data and data['documentTrackingId']:
        message.document_tracking_id= data['documentTrackingId'] 

    try:
        db.session.add(message)
        db.session.commit()
        return message
    except Exception as e:
        db.session.rollback()
        return manage_db_exception('Error en #save_new_message - ',
                                   'Error al guardar claimer_message' , e)


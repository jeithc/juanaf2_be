
from flask_restplus import marshal

from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from app.main.messaging.model.document_typology import Document_typology
from app.main.messaging.util.dto.document_typology_dto import DocumentTypologyDto


def get_all_typologies():
    logger.debug('Entro a document_typology_service#get_all_typologies')
    try:
        typologies = Document_typology.query.all()

        return marshal(typologies, DocumentTypologyDto.document_typology), 200
    except Exception as e:
        return manage_db_exception("Error en document_typology_service#get_all_typologies - " +
                                   "Error al obtener las tipologias de documentos",
                                   'Ocurrió un error al obtener las tipologias de documentos.', e)
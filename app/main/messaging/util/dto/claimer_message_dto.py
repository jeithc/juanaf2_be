from flask_restplus import Namespace, fields
from app.main.core.util.dto.role_dto import RoleDto

class ClaimerMessageDto:
    api = Namespace('claimer_message', description='operaciones tabla claimer_message')
    claimer_message = api.model('claimerMessage', {
        'claimerId': fields.Integer(description='Id del usuario', attribute='claimer_id'),
        'aaId': fields.Integer(description='id del acto administrativo', attribute='aa_id'),
        'personExpedientId': fields.Integer(description='id del RAI', attribute='person_expedient_id'),
        'claimerMessageMode': fields.String(description='Medio de envio del mensaje', attribute='claimer_message_mode'),
        'claimerMessageType': fields.String(description='Tipo de envio del mensaje', attribute='claimer_message_type'),
        'sendingDate': fields.Date(description='Fecha de envio del mensaje', attribute='sending_date'),
        'deliveryDate': fields.Date(description='Fecha de engtrega del mensaje', attribute='delivery_date'),
        'email': fields.String(description='Correo electronico'),
        'addressId': fields.Integer(description='Id de la direccion', attribute='address_id'),
        'trackId': fields.Integer(description='Id de la guia', attribute='track_id'),
        'messageProviderId': fields.Integer(description='Id de la empresa de mesajeria', attribute='message_provider_id'),
        'deliveryResult': fields.String(description='Resultado de la entrega', attribute='delivery_result'),
        'receiverDocumentType': fields.String(description='Documento de quien recive el mensaje', attribute='receiver_document_type'),
        'receiverDocumentNumber': fields.String(description='Numero de documento de quien recibe el mensaje', attribute='receiver_document_number'),
        'receiverFullName': fields.String(description='Nombre de quien recibe el mensaje', attribute='receiver_full_name'),
        'successNotificationDocumentI': fields.Integer(description='Referencia al doc de claimer document', attribute='success_notification_document_id')
    })


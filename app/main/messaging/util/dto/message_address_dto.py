from flask_restplus import Namespace, fields
from app.main.core.util.dto.role_dto import RoleDto

class MessageAddressDto:
    api = Namespace('message_address', description='Operaciones sobre la tabla message_address')
    message_address = api.model('messageAddress', {
        'country': fields.String(description='Pais al que se dirije el mensaje'),
        'idNatgeo':fields.Integer(description='Depto o Municipio de direccion', attribute='id_natgeo'),
        'idBogLocation':fields.Integer(description='Localidad y barrio de direccion', attribute='id_bog_location'),
        'address':fields.String(description='Direccion registrada por reclamante en el registro'),
        'urbanRuralLocation':fields.String(description='Atributo que clasifica entre rural y urbana', attribute='urban_rural_location')
    })
from flask_restplus import Namespace, fields

from app.main.messaging.util.dto.document_typology_dto import DocumentTypologyDto


class DocumentTypeDto:
    api = Namespace('document_type', description='Operaciones relacionadas con la tabla de tipo de documento')

    document_type = api.model('document_type', {
        'id': fields.Integer(description='Id del tipo'),
        'documentType': fields.String(attribute='document_type'),
        'documentTypologyId': fields.Integer(attribute='document_typology_id'),
        'documentTypology': fields.Nested(DocumentTypologyDto.document_typology, attribute='document_typology'),
        'reviewDocument': fields.String(attribute='review_document'),
        'active': fields.Boolean(attribute='active'),
        'claimerVisualized': fields.Boolean(attribute='claimer_visualized'),
        'physicalFormat': fields.Boolean(attribute='physical_format'),
        'electronicFormat': fields.Boolean(attribute='electronic_format'),
        'digitalFormat': fields.Boolean(attribute='digital_format'),
        'documentRoute': fields.String(attribute='document_route')
    })

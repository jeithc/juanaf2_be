from flask_restplus import Namespace, fields


class DocumentTypologyDto:
    api = Namespace('document_typology', description='Operaciones relacionadas con la tabla de tipología de documento')

    document_typology = api.model('document_typology', {
        'id': fields.Integer(description='Id del tipo'),
        'documentTypology': fields.String(attribute='document_typology'),
        'generateRadication': fields.Boolean(attribute='generate_radication'),
        'active': fields.Boolean(attribute='active')
    })

from flask_restplus import Resource

from app.main.messaging.service import document_typology_service
from app.main.messaging.util.dto.document_typology_dto import DocumentTypologyDto

api = DocumentTypologyDto.api


@api.route('/document_typology')
class DocumentTypologyList(Resource):
    @api.doc('list_of_registered_typology')
    # @auth.login_required
    def get(self):
        """List all registered typologies"""

        return document_typology_service.get_all_typologies()

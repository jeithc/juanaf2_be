from flask import request
from flask_restplus import Resource

from app.main.messaging.util.dto.document_type_dto import DocumentTypeDto
from app.main.messaging.service import document_type_service


api = DocumentTypeDto.api


@api.route('/document_type')
class DocumentTypeList(Resource):
    @api.doc('list_of_registered_users')
    # @auth.login_required
    def get(self):
        """List all registered users"""

        return document_type_service.get_all_types()

@api.route('/<id>')
class DocumentTypeList(Resource):
    @api.doc('list_of_registered_document_type')
    # @auth.login_required
    def get(self, id):
        """List all DocumentType"""
        return document_type_service.get_types_by_topology(id)      

@api.route('/request')
class DocumentTypeList(Resource):
    @api.doc('list_of_registered_document_type_request')
    # @auth.login_required
    def get(self):
        """List all DocumentType"""
        return document_type_service.get_types_by_topology_request()             


@api.route('/request_documents')
class DocumentTypeList(Resource):
    @api.doc('list_of_request_documents')
    # @auth.login_required
    def get(self):
        """List all DocumentType"""
        return document_type_service.get_types_by_topology_request_documents() 

@api.route('/response_documents')
class DocumentTypeList(Resource):
    @api.doc('list_of_answer_documents')
    # @auth.login_required
    def get(self):
        """List all DocumentType"""
        return document_type_service.get_types_by_topology_response_documents()
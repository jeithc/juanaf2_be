from flask import request
from flask import json
from flask_restplus import Resource
from app.main.messaging.util.dto.claimer_message_dto import ClaimerMessageDto
from app.main.messaging.service import claimer_message_service
from app.main.service import user_service
from app.main.service import climer_documents_service
from app.main.core.service import report_notification_massive_service
from app.main.messaging.service import message_address_service

api = ClaimerMessageDto.api

@api.route('/claimer_message/')
class ClaimerMessageList(Resource):
    @api.doc('create new claimer_message')
    @api.expect(ClaimerMessageDto.claimer_message , validate=False)
    @api.marshal_with(ClaimerMessageDto.claimer_message, skip_none=False)

    def post(self):
        data = request.json
        return claimer_message_service.save_new_message(data)



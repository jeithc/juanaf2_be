from flask import request
from flask_restplus import Resource
from app.main.messaging.util.dto.message_address_dto import MessageAddressDto
from app.main.messaging.service import message_address_service
from app.main.service import user_service
from app.main.util.decorator import audit_init, audit_finish

api = MessageAddressDto.api

@api.route('/message_address')
class MessageAddressList(Resource):
    @api.doc('Create a new message_address')
    @api.expect(MessageAddressDto.message_address , validate=False)
    @api.marshal_with(MessageAddressDto.message_address, skip_none=False)
    @audit_init
    @audit_finish 

    def post(self):
        data = request.json
        return message_address_service.save_new_message_address(data)

    @api.doc('get list message_address')
    @api.marshal_with(MessageAddressDto.message_address, 200)
    def get(self):
        """ get list of message_address """
        return message_address_service.get_all_messageAddress()

    @api.doc('update message_address')
    @api.expect(MessageAddressDto.message_address , validate=False)
    def put(self):
        """ update message_address"""
        data = request.json
        return message_address_service.updated_message_address(data = data)
import requests
import json
from app.main.config import mail_credentials_dev, mail_credentials_prod
import base64
import os
import datetime
import io
from sqlalchemy import text
from app.main import db
from datetime import date, datetime as dtime
from app.main.messaging.service.claimer_message_service import save_new_message
from app.main.model.user import User
from app.main.notification.model.claimer_notification import ClaimerNotification
from app.main.core.service.massive_publication_helper import add_mail_message_document
from app.main.messaging.model.claimer_message import ClaimerMessage
from app.main.model.claimer_message_tracking import ClaimerMessageTracking
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.model.claimer_documents import Claimer_documents
from app.main.core.service.file_service import save_file_from_url
from app.main.mail.model.email_message_metadata import EmailMetadata
from app.main.service import climer_documents_service
from app.main.core.util.verify_mail_status import verify_mail_status,verify_mail_status2
from app.main.notification.model.claimer_notification import ClaimerNotification
from app.main.util.mail import send_mail_general
import csv
from app.main.mail.service.message_layout import html_notification_auto 


SEND_MAIL = 'notificaciones_juana@defensoria.gov.co'


def send_mail_auto():
    print(os.getcwd())
    list_email = []
    with open(os.getcwd()+'\src\documents\datos.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        send = 0
        line1=0
        for row in csv_reader:
            if line1 == 0:
                line1 = 1
            else:
                list_email.append(row[0])
                line_count += 1
                send += 1
            if line_count >= 49:
                # print(list_email)
                subject = 'comunicación sobre la expedición de autos de prueba.'
                html_body = html_notification_auto
                send_result = send_mail_general(list_email, "Doña Juana le responde - {}".format(subject), html_body)
                list_email = []
                line_count = 0
                send+=1
                print(send)

        if line_count > 0:
            # print(list_email)
            subject = 'comunicación sobre la expedición de autos de prueba.'
            html_body = html_notification_auto
            send_result = send_mail_general(list_email, "Doña Juana le responde - {}".format(subject), html_body)
            send+=1
            print(send)
 

def send_email_sign(data):

    encoded_string = ''
    table = ''
    document_name = False

    footer = '<p></p><p></p><p>Por favor, <b>no responda a este mensaje</b>, ya que este buzón de correo electrónico no se encuentra habilitado para recibir mensajes, \
                pues solamente tiene funciones de distribución. Si requiere más información, acuda a los canales de atención de la Defensoría del Pueblo.</p>\
                <p>La Defensoría del pueblo <b>nunca le solicitará</b> sus datos personales o sus credenciales de acceso a la plataforma <b>DOÑA JUANA LE RESPONDE</b> \
                mediante vínculos de correo electrónico. En caso de recibir alguno, repórtelo de inmediato a través de los canales de atención de la Defensoría del Pueblo.</p>\
                <p><b>Aviso de confidencialidad:</b> Este mensaje está dirigido para ser usado por su destinatario(a) exclusivamente y puede contener información confidencial \
                y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribución o reproducción de este, o de cualquiera de sus \
                anexos, está estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notifíquenos inmediatamente y elimine su texto original, incluidos \
                los anexos, o destruya cualquier reproducción de este.</p>'


    mail_credentials = mail_credentials_prod
 

    payload = {
        "fromName": "Doña Juana le responde",
        "fromAddress": SEND_MAIL,
        "certification": data['certification'],
        "recipients":
        [
                    data['claimer'].email
        ],
        "subject": "Doña Juana le responde - {}".format(data['subject']),
        "bodyContent": "<p>{} {} {}</p>".format(data['body'], footer, table),
        "attachments": [
            {
                "name": document_name,
                "data": encoded_string
            }
        ]
    }

    print(payload)
    result = requests.post(
        mail_credentials['url'], headers=mail_credentials['headers'], data=json.dumps(payload))
    # # print(result.headers)
    response = result.json()
    print(result.text)
    print(response['recipients'][0]['messageId'])
    if result.status_code == 200:
        return "mensaje enviado"
    else:
        return "error"


def send_email_user(data, document_path=False, document_name=False, aa = 1):
    try:

        if data['attachments'] != False and document_path:
            pathDocument = document_path+document_name
            with open(pathDocument, "rb"):
                encoded_string = str(base64.b64encode(
                    open(pathDocument, "rb").read()).decode("ascii"))
                table = "${ATTACHMENTS_TABLE}"
        else:
            encoded_string = ''
            table = ''

        footer = '<p></p><p></p><p>Por favor, <b>no responda a este mensaje</b>, ya que este buzón de correo electrónico no se encuentra habilitado para recibir mensajes, \
                pues solamente tiene funciones de distribución. Si requiere más información, acuda a los canales de atención de la Defensoría del Pueblo.</p>\
                <p>La Defensoría del pueblo <b>nunca le solicitará</b> sus datos personales o sus credenciales de acceso a la plataforma <b>DOÑA JUANA LE RESPONDE</b> \
                mediante vínculos de correo electrónico. En caso de recibir alguno, repórtelo de inmediato a través de los canales de atención de la Defensoría del Pueblo.</p>\
                <p><b>Aviso de confidencialidad:</b> Este mensaje está dirigido para ser usado por su destinatario(a) exclusivamente y puede contener información confidencial \
                y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribución o reproducción de este, o de cualquiera de sus \
                anexos, está estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notifíquenos inmediatamente y elimine su texto original, incluidos \
                los anexos, o destruya cualquier reproducción de este.</p>'

        # if data['certification'] == True:
        #         mail_credentials = mail_credentials_prod
        # else:
        #     mail_credentials = mail_credentials_dev   

        mail_credentials = mail_credentials_prod
        payload = {
            "fromName": "Doña Juana le responde",
            "fromAddress": SEND_MAIL,
            "certification": data['certification'],
            "recipients": [data['claimer'].email],
            "subject": "Doña Juana le responde - {}".format(data['subject']),
            "bodyContent": "<p>{} {} {}</p>".format(data['body'], footer, table),
            "attachments": [
                {
                    "name": document_name,
                    "data": encoded_string
                }
            ]
        }
        # print(payload)

        if verify_message_send(data['claimer'].claimer_id, data['claimer_message_type'], aa ):
            result = requests.post(
                mail_credentials['url'], headers=mail_credentials['headers'], data=json.dumps(payload))
            # # print(result.headers)
            response = result.json()
            print(result.text)
            print(response['recipients'][0]['messageId'])
            if result.status_code == 200:
                # creo el documento que mas tarde voy a llenar en la verificación
                # document = Claimer_documents.query.filter(
                #     Claimer_documents.claimer_id == data['claimer'].claimer_id, Claimer_documents.document_type_id == 41).first()
                # if not document:
                document = Claimer_documents(
                    claimer_id=data['claimer'].claimer_id,
                    document_type_id=41,
                    flow_document="1",
                    origin_document='TERCEROS',
                    document_state=False
                )
                save_changes(document)
                # creo el mensaje en claimer_message
                message = {
                    'claimerId': data['claimer'].claimer_id,
                    'personExpedientId': data['claimer'].person_expedient_id,
                    'claimerMessageMode': 'ELECTRONICO',
                    'claimerMessageType': data['claimer_message_type'],
                    'sendingDate': datetime.datetime.now(),
                    'email': data['claimer'].email,
                    'messageProviderId': response['recipients'][0]['messageId'],
                    'documentTrackingId': document.document_id,
                    'aaId': aa
                }
                message_saved = save_new_message(message)

                if data['claimer_message_type'] == 'NOTIFICATION':
                    notification = ClaimerNotification()
                    notification.claimer_id = data['claimer'].claimer_id
                    # notification.notification_document_id = document.document_id
                    notification.aa_id = aa
                    notification.claimer_notified = False
                    notification.notification_type = 'ELECTRONICAMENTE'
                    notification.claimer_message_id = message_saved.claimer_message_id
                    user_notify = 1
                    save_changes(notification)

                if data['attachments'] != False:
                    add_mail_message_document(
                        data['document_id'], message_saved.claimer_message_id)
            return result.status_code
        else:
            return 'usuario ya se le envió notificación'
    except Exception as e:
        print(e)
        print('error al enviar mensaje')
        return 'error al enviar mensaje'


def send_email_user_multidocument(data, documents, update=False):
    try:
        print('entro a enviar el mensaje linea 188 mail service')
        table = "${ATTACHMENTS_TABLE}"
        footer = '<p></p><p></p><p>Por favor, <b>no responda a este mensaje</b>, ya que este buzón de correo electrónico no se encuentra habilitado para recibir mensajes, \
            pues solamente tiene funciones de distribución. Si requiere más información, acuda a los canales de atención de la Defensoría del Pueblo.</p>\
            <p>La Defensoría del pueblo <b>nunca le solicitará</b> sus datos personales o sus credenciales de acceso a la plataforma <b>DOÑA JUANA LE RESPONDE</b> \
            mediante vínculos de correo electrónico. En caso de recibir alguno, repórtelo de inmediato a través de los canales de atención de la Defensoría del Pueblo.</p>\
            <p><b>Aviso de confidencialidad:</b> Este mensaje está dirigido para ser usado por su destinatario(a) exclusivamente y puede contener información confidencial \
            y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribución o reproducción de este, o de cualquiera de sus \
            anexos, está estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notifíquenos inmediatamente y elimine su texto original, incluidos \
            los anexos, o destruya cualquier reproducción de este.</p>'

        # if data['certification'] == True:
        #         mail_credentials = mail_credentials_prod
        # else:
        #     mail_credentials = mail_credentials_dev

        mail_credentials = mail_credentials_prod

        payload = {
            "fromName": "Doña Juana le responde",
            "fromAddress": SEND_MAIL,
            "certification": data['certification'],
            "recipients":
                [
                    data['claimer'].email
            ],
            "subject": "Doña Juana le responde - {}".format(data['subject']),
            "bodyContent": "<p>{} {} {}</p>".format(data['body'], footer, table),
            "attachments": documents['documents_mail']

        }
        print('va a verificar mensaje enviado anteriormente mail service')

        if verify_message_send(data['claimer'].claimer_id, data['claimer_message_type']):
            print('siguió proceso de envió , no se le ha enviado mensaje')
            result = requests.post(
                mail_credentials['url'], headers=mail_credentials['headers'], data=json.dumps(payload))
            # # print(result.headers)
            response = result.json()
            # print(result.text)
            # print(response['recipients'][0]['messageId'])
            if result.status_code == 200:
                # creo el documento que mas tarde voy a llenar en la verificación
                # document = Claimer_documents.query.filter(
                #     Claimer_documents.claimer_id == data['claimer'].claimer_id, Claimer_documents.document_type_id == 41).first()
                # if not document:
                document = Claimer_documents(
                    claimer_id=data['claimer'].claimer_id,
                    document_type_id=41,
                    flow_document="1",
                    origin_document='TERCEROS',
                    document_state=False
                )
                save_changes(document)
                # creo el mensaje en claimer_message
                if update == True:
                    message = ClaimerMessage.query.filter(ClaimerMessage.claimer_id == data['claimer'].claimer_id,
                                                          ClaimerMessage.claimer_message_mode == 'ELECTRONICO',
                                                          ClaimerMessage.claimer_message_type == 'NOTIFICATION'
                                                          ).first()
                    if message:
                        message.sending_date = datetime.datetime.now()
                        message.message_provider_id = response['recipients'][0]['messageId']
                        message.document_tracking_id = document.document_id
                        message.delivery_result = None
                        message.success_notification_document_id = None
                        message.delivery_date = None
                        save_changes(message)
                else:
                    message = {
                        'claimerId': data['claimer'].claimer_id,
                        'personExpedientId': data['claimer'].person_expedient_id,
                        'claimerMessageMode': 'ELECTRONICO',
                        'claimerMessageType': data['claimer_message_type'],
                        'sendingDate': datetime.datetime.now(),
                        'email': data['claimer'].email,
                        'aaId': '1',
                        'messageProviderId': response['recipients'][0]['messageId'],
                        'documentTrackingId': document.document_id
                    }
                    message_saved = save_new_message(message)
                # print(documents['id_documents'])
                    for list_id in documents['id_documents']:
                        add_mail_message_document(
                            list_id, message_saved.claimer_message_id)
                result_message = 'ok'
            else:
                newLoggererror = configLoggingError(
                    loggerName='send_email_multidocument')
                newLoggererror.error(result.content)
                result_message = result.content
            return {'code': result.status_code, 'message': result_message}
        else:
            print('salio de  proceso de envió , ya se le ha enviado mensaje')
            return 'usuario ya se le envió notificación'
    except Exception as e:
        newLogger = configLoggingError(
            loggerName='mail service-send_email_user_multidocument')
        newLogger.error(e)
        print('error en linea 277 mail_service Exception')
        print(e)
        return 'error al enviar a usuario'


def verify_message_send(claimer_id, message_type, aa= 1):
    message = ClaimerMessage.query.filter(ClaimerMessage.claimer_id == claimer_id,
                                          ClaimerMessage.aa_id == aa, ClaimerMessage.claimer_message_mode == 'ELECTRONICO',
                                          ClaimerMessage.claimer_message_type == message_type).all()
    if message:
        return False
    else:
        return True


def verify_all_mail():
    messages = ClaimerMessage.query\
        .filter(ClaimerMessage.delivery_result.is_(None))\
        .filter(ClaimerMessage.claimer_message_mode == 'ELECTRONICO')\
        .filter(ClaimerMessage.message_provider_id.isnot(None)).all()   
    if messages:
        for row in messages:
            if not row.document_tracking_id:
                document = Claimer_documents(
                    claimer_id=row.claimer_id,
                    document_type_id=41,
                    flow_document="1",
                    origin_document='TERCEROS',
                    document_state=False
                )
                save_changes(document)
                row.document_tracking_id = document.document_id

            mail_credentials = mail_credentials_prod

            payload = {'uuid': row.message_provider_id}
            result = requests.get(
                mail_credentials['url_verify'], headers=mail_credentials['headers'], params=payload)
            response = result.json()
            try:
                status = response['reports'][0]['emailStatus']
                if status != 'SENDING':
                    if status == 'DELIVERED':
                        if response['reports'][0]['user']['openingsCount'] > 0:
                            status = 'OPEN'
                        date_status = dtime.fromtimestamp(
                            response['reports'][0]['delivery']['reception'])
                        server_response = response['reports'][0]['delivery']['response']
                        row.delivery_date = date_status
                    elif status == 'BOUNCE':
                        date_status = dtime.fromtimestamp(
                            response['reports'][0]['bounce']['reception'])
                        server_response = 'Mail Bounce {}'.format(
                            response['reports'][0]['emailAddress'])
                        row.delivery_date = date_status
                    row.delivery_result = status
                    save_changes(row)
                    # verifico si ya se guardo estado actual
                    tracking = ClaimerMessageTracking.query\
                        .filter(ClaimerMessageTracking.claimer_message_id == row.claimer_message_id)\
                        .filter(ClaimerMessageTracking.current_message_state == status).first()
                    if not tracking:
                        # Guardo el documento en la tabla claimer document y en s3
                        save_file_from_url(
                            response['reports'][0]['reportUrl'], row.document_tracking_id, row.document_tracking_id)
                        tracking_m = ClaimerMessageTracking(
                            claimer_message_id=row.claimer_message_id,
                            registration_date=datetime.datetime.now(),
                            current_message_state=status,
                            tracking_date=date_status,
                            tracking_detail=server_response,
                            verify=False
                        )
                        save_changes(tracking_m)
                        verify_mail_status(tracking_m, row)
            except:
                print('Error al buscar id: {}'.format(row.message_provider_id))
        return 200
    else:
        return 'no se encontraron mensajes por verificar'


def verify_mail():
    query = """select te.claimer_id ,te.message_id ,cd.url_document from phase_2.claimer_documents cd 
            inner join tempwork.taiga2145_ex te on te.claimer_id = cd.claimer_id 
            where document_type_id = 41 
            and url_document is null"""
    messages = db.engine.execute(
        text(query).execution_options(autocommit=True)).fetchall()
    if messages:
        for row2 in messages:
            row = ClaimerMessage.query.filter(ClaimerMessage.claimer_message_id==row2['message_id']).first()
            if not row.document_tracking_id:
                document = Claimer_documents(
                    claimer_id=row.claimer_id,
                    document_type_id=41,
                    flow_document="1",
                    origin_document='TERCEROS',
                    document_state=False
                )
                save_changes(document)
                row.document_tracking_id = document.document_id

            mail_credentials = mail_credentials_prod

            payload = {'uuid': row.message_provider_id}
            result = requests.get(
                mail_credentials['url_verify'], headers=mail_credentials['headers'], params=payload)
            response = result.json()
            try:
                status = response['reports'][0]['emailStatus']
                if status != 'SENDING':
                    if status == 'DELIVERED':
                        if response['reports'][0]['user']['openingsCount'] > 0:
                            status = 'OPEN'
                        date_status = dtime.fromtimestamp(
                            response['reports'][0]['delivery']['reception'])
                        server_response = response['reports'][0]['delivery']['response']
                        row.delivery_date = date_status
                    elif status == 'BOUNCE':
                        date_status = dtime.fromtimestamp(
                            response['reports'][0]['bounce']['reception'])
                        server_response = 'Mail Bounce {}'.format(
                            response['reports'][0]['emailAddress'])
                        row.delivery_date = date_status
                    row.delivery_result = status
                    save_changes(row)
                    # verifico si ya se guardo estado actual
                    tracking = ClaimerMessageTracking.query\
                        .filter(ClaimerMessageTracking.claimer_message_id == row.claimer_message_id)\
                        .filter(ClaimerMessageTracking.current_message_state == status).first()
                    if tracking:
                        save_delete(tracking)

                    # Guardo el documento en la tabla claimer document y en s3
                    save_file_from_url(
                        response['reports'][0]['reportUrl'], row.document_tracking_id, row.document_tracking_id)
                    tracking_m = ClaimerMessageTracking(
                        claimer_message_id=row.claimer_message_id,
                        registration_date=datetime.datetime.now(),
                        current_message_state=status,
                        tracking_date=date_status,
                        tracking_detail=server_response,
                        verify=False
                    )
                    save_changes(tracking_m)
                    verify_mail_status(tracking_m, row)
            except:
                print('Error al buscar id: {}'.format(row.message_provider_id))
            
        return 200
    else:
        return 'no se encontraron mensajes por verificar'




def verify_mail_process():
    query = """select cm.* from tempwork.taiga2145_ex ex
        inner join phase_2.claimer_message cm on cm.claimer_id = ex.claimer_id 
        where cm.aa_id = 1 and claimer_message_type = 'NOTIFICATION' and claimer_message_mode = 'ELECTRONICO'"""
    messages = db.engine.execute(
        text(query).execution_options(autocommit=True)).fetchall()
    for row in messages:
        verify_mail_status2(row)





def verify_mail_file():
    mail_credentials = mail_credentials_prod

    query = "select distinct(cm.*), cn.notification_document_id from phase_2.claimer_notification cn \
    inner join phase_2.claimer_documents cd on cd.document_id=cn.notification_document_id \
    inner join phase_2.claimer_message cm on cm.claimer_id = cn.claimer_id and cm.claimer_message_type = 'NOTIFICATION' and cm.claimer_message_mode = 'ELECTRONICO' \
    where cn.notification_type='ELECTRONICAMENTE' and cn.claimer_notified is true \
    and (cd.url_document is null or cd.document_size is null) and cn.claimer_id > 0"
    documents = db.engine.execute(
        text(query).execution_options(autocommit=True)).fetchall()

    if documents:
        for row in documents:
            payload = {'uuid': row.message_provider_id}
            result = requests.get(
                mail_credentials['url_verify'], headers=mail_credentials['headers'], params=payload)
            response = result.json()
            file_save = save_file_from_url(
                response['reports'][0]['reportUrl'], row.notification_document_id, row.claimer_message_id)
            try:
                os.remove(file_save[0]['local'])
            except:
                print('error al remover')
        return 200
    else:
        return 'no se encontraron documentos por verificar'


def get_attribute(obj, attrib):
    if attrib in obj:
        print("found " + str(attrib))
        return obj[attrib]
    else:
        print("NOT found " + str(attrib))


def update_tracking_provider(data):
    try:
        current_state = get_attribute(data, 'current_state')
        claimer_message_id = get_attribute(data, 'claimer_message_id')
        tracking_date = get_attribute(data, 'tracking_date')
        tracking_detail = get_attribute(data, 'tracking_detail')
        document_tracking = get_attribute(data, 'document_tracking')
        document_certificate = get_attribute(data, 'document_certificate')
        document_number = get_attribute(data, 'document_number')
        name = get_attribute(data, 'name')
        tracking = get_attribute(data, 'tracking')
        print('current_state ' + str(data['current_state']))
        messages = ClaimerMessage.query.filter(
            ClaimerMessage.claimer_message_id == claimer_message_id).all()
        # .filter(ClaimerMessage.document_tracking_id.isnot(None)).filter(ClaimerMessage.message_provider_id.isnot(None))

        if messages:
            for row in messages:
                change = False
                print(str(row))
                # payload={'uuid': row.message_provider_id}
                # result = requests.get(mail_credentials['url_verify'], headers=mail_credentials['headers'], params=payload)
                # response=result.json()
                # Creo el documento para agregar el resultado del envio

                id_document = row.success_notification_document_id
                if not row.success_notification_document_id:
                    document = Claimer_documents(
                        claimer_id=row.claimer_id,
                        document_type_id=42,
                        flow_document="1",
                        origin_document='TERCEROS',
                        document_state=True
                    )
                    save_changes(document)
                    row.success_notification_document_id = document.document_id
                    change = True
                    id_document = document.document_id
                    
                if document_certificate.find('://') > 0:
                    save_file_from_url(document_certificate, id_document, claimer_message_id, 'physical_certificate')
                    change = True

                id_document_2 = row.document_tracking_id
                create = 0
                if id_document_2 == id_document:
                    create = 1
                if not row.document_tracking_id or create == 1:
                    document = Claimer_documents(
                        claimer_id=row.claimer_id,
                        document_type_id=57,
                        flow_document="1",
                        origin_document='TERCEROS',
                        document_state=True
                    )
                    save_changes(document)
                    row.document_tracking_id = document.document_id
                    id_document_2 = document.document_id
                    change = True

                if document_tracking.find('://') > 0:

                    save_file_from_url(document_tracking, id_document_2, claimer_message_id, 'physical_tracking')
                    change = True

                if change:
                    status = current_state
                    date_status = tracking_date
                    server_response = tracking_detail
                    if date_status != "":
                        row.delivery_date = date_status
                    row.delivery_result = status
                    row.track_id = tracking
                    row.receiver_document_number = document_number
                    row.receiver_full_name = name
                    save_changes(row)

                    # verifico si ya se guardo estado actual
                    tracking = ClaimerMessageTracking.query\
                        .filter(ClaimerMessageTracking.claimer_message_id == row.claimer_message_id)\
                        .filter(ClaimerMessageTracking.current_message_state == status).first()
                    if not tracking:
                        tracking = ClaimerMessageTracking()
                        if date_status != "":
                            tracking.tracking_date = date_status
                        tracking.claimer_message_id = row.claimer_message_id
                        tracking.registration_date = datetime.datetime.now()
                        tracking.current_message_state = status
                        tracking.tracking_detail = server_response
                        tracking.verify = False
                        save_changes(tracking)
                    verify_mail_status(tracking, row)
            return 200
    except Exception as e:
        newLoggererror = configLoggingError(
            loggerName='update_tracking_provider')
        newLoggererror.error('error al guaradar '+e)
        return 'error al guardar tracking'


def email_metadata(email_id):
    email_data = EmailMetadata.query.filter_by(email_id=email_id).first()
    if email_data:
        data = {
            'certification': email_data.certified_email,
            'subject': email_data.subject,
            'body': email_data.whole,
            'claimer_message_type': email_data.email_message_type,
            'attachments': email_data.attachments
        }
        return data


def build_message_user(pathDocument, document, user, document_id, email_id):
    result = {
        'send': 0,
        'no_send': 0
    }
    data_send_mail = email_metadata(email_id)
    data_add = {
        'claimer': user,
        'document_id': document_id
    }
    data_send_mail.update(data_add)
    data_send_mail['body'] = build_body_mail(
        data_send_mail['body'], email_id, user)
    send_result = send_email_user(data_send_mail, pathDocument, document)
    if send_result == 200:
        result['send'] = result['send'] + 1
    else:
        result['no_send'] = result['no_send'] + 1
    return result


def build_body_mail(body_mail, email_id, claimer):
    if email_id == 3:
        # se editan los espacios en el body para agregar los datos reales
        body_mail = body_mail.format(
            claimer.full_name,
            'lugar',
            'dirección',
            'horario',
            'no acto',
            'nombre acto'
        )
    return body_mail


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='mail_service')
        db.session.rollback()
        newLogger.error(e)


def save_delete(data):
    db.session.delete(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='mail_service')
        db.session.rollback()
        newLogger.error(e)

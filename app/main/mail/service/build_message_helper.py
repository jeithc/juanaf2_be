from app.main import db
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User
from app.main.citation.model.aa import Aa
from app.main.service.logging_service import configLoggingError
from app.main.mail.service.mail_service import *
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.mail.model.email_message_metadata import EmailMetadata
from app.main.messaging.model.claimer_message import ClaimerMessage
import os
from sqlalchemy import text
from app.main.core.service.file_service import verify_document_mail


def build_message(workflow_state, email_id, id_document=False):
    # verifico que el claimer este en el estado necesario

    query = text('select  cud.* from (	select cw.claimer_id	from phase_2.claimer_workflow  cw	\
        where cw.current_state = 5 and cw.end_date is null ) cw left outer join (select claimer_id 	 \
        from phase_2.claimer_message cm  where cm.claimer_message_mode = :mode and CM.claimer_message_type = :type) cm on  \
        cw.claimer_id = cm.claimer_id inner join phase_2.claimer_user_data cud on cud.claimer_id = cw.claimer_id where cm.claimer_id is null')
    claimers = db.engine.execute(query.execution_options(
        autocommit=True), mode='ELECTRONICO', type='CITACION').fetchall()
    db.session.close()
    # claimers = User.query.join(ClaimerWorkflow, ClaimerWorkflow.claimer_id==User.claimer_id)\
    #     .filter(ClaimerWorkflow.current_state==workflow_state,ClaimerWorkflow.end_date == None).all()
    result = {
        'send': 0,
        'no_send': 0
    }
    if claimers:
        # saco los datos de metadata para el mail necesario
        email_data = email_metadata(email_id)
        if email_data:
            # recorro el proceso para llamar a cada claimer
            for claimer in claimers:
                if verify_user_for_mail(email_id, claimer.claimer_id):
                    if id_document:
                        document_created = ReportJasperMasivo.create_report(
                            id_document, claimer.claimer_id)
                        if document_created['route'] != 'Ya registró un documento anteriormente':
                            print(document_created)
                            print(claimer.claimer_id)
                            data_send_mail = email_data
                            data_add = {
                                'claimer': claimer,
                                'document_id': document_created['document_id']
                            }
                    else:
                        data_add = {
                            'claimer': claimer,
                        }
                    data_send_mail.update(data_add)
                    # se construye el body cambiando los datos necesarios
                    data_send_mail['body'] = build_body_mail(
                        data_send_mail['body'], email_id, claimer)
                    # SE ENVIÁ EL MAIL CON LOS DATOS DE data_send_mail
                    send_result = send_email_user(
                        data_send_mail, document_created['document'], document_created['name'])
                    if send_result == 200:
                        result['send'] = result['send'] + 1
                    else:
                        result['no_send'] = result['no_send'] + 1
                else:
                    print('no autorizo o no esta registrado user: {}'.format(
                        claimer.claimer_id))
                    result['no_send'] = result['no_send'] + 1
            return result
        else:
            return "error al consultar datos de mail o adjuntar documentos"
    else:
        return 'no se encontraron usuarios para envió'


def build_message_response(claimer_id, workflow_state, email_id, id_document, aa= 1):
    # verifico que el claimer este en el estado necesario

    query = text('select cud.* from phase_2.claimer_user_data cud \
    inner join phase_2.claimer_workflow cw on cw.claimer_id = cud.claimer_id\
    where cud.claimer_id = :claimer_id and cw.current_state = :state and cw.end_date is null')
    claimers = db.engine.execute(query.execution_options(autocommit=True),
                                 state=workflow_state, claimer_id=claimer_id).fetchall()
    db.session.close()
    if claimers:
        # saco los datos de metadata para el mail necesario
        email_data = email_metadata(email_id)
        if email_data:
            # recorro el proceso para llamar a cada claimer
            for claimer in claimers:
                if id_document:
                    data_send_mail = email_data
                    data_add = {
                        'claimer': claimer,
                        'document_id': id_document
                    }
                else:
                    data_add = {
                        'claimer': claimer,
                    }
                data_send_mail.update(data_add)
                # se construye el body cambiando los datos necesarios
                data_send_mail['body'] = build_body_mail(
                    data_send_mail['body'], email_id, claimer)

                route, name = verify_document_mail(id_document)
                if route != 'error':
                    # SE ENVIÁ EL MAIL CON LOS DATOS DE data_send_mail
                    send_result = send_email_user(
                        data_send_mail, route, name, aa)
                    if send_result == 200:
                        result_send = 1
                    else:
                        result_send = 0
                else:
                    result_send = 0

            return result_send
        else:
            return "error al consultar datos de mail o adjuntar documentos"
    else:
        return 'no se encontraron usuarios para envió'


def verify_user_for_mail(email_id, claimer_id, notify=False):
    if email_id == 1 or email_id == 7:
        # verifico que este registrado y que no haya aceptado la autorización de notificación personal electrónica.
        # verify_user_status = User.query\
        #     .filter(User.claimer_id == claimer_id, User.registered_claimer.is_(True), User.email.like('%@%'))\
        #     .filter((User.authorize_electronic_notificati.is_(notify)) | (User.authorize_electronic_notificati == None)).first()
        verify_user_status = User.query.filter(User.claimer_id == claimer_id, User.email.like('%@%')).first()
        return verify_user_status
    return True


def build_body_mail(body_mail, email_id, claimer):
    if email_id == 1:
        act = Aa.query.filter(Aa.aa_id == 1).first()
        query = text('select NPP.PLACE_NAME, NPP.ADDRESS AS PLACE_ADDRESS, NPR.SCHEDULE_ATTENTION \
        from phase_2.NOTIFICATION_PLACE_RULES NPR \
        INNER JOIN PHASE_2.NOTIFICATION_PHYSICAL_PLACES NPP ON NPR.place_id = NPP.PLACE_ID \
        inner join PHASE_2.SUBGROUP_CITATION SC ON SC.PLACE_ID = NPP.PLACE_ID \
        INNER JOIN PHASE_2.CITATION C ON C.subgroup_id = sc.subgroup_id \
        where c.claimer_id = :claimer ')
        citation = db.engine.execute(query.execution_options(
            autocommit=True), claimer=claimer.claimer_id).fetchone()
        db.session.close()
        # se editan los espacios en el body para agregar los datos reales
        body_mail = body_mail.format(
            claimer.full_name,
            citation.place_name,
            citation.place_address,
            citation.schedule_attention,
            act.aa_number,
            act.description,
        )
    if email_id == 2:
        act = Aa.query.filter(Aa.aa_id == 1).first()
        body_mail = body_mail.format(
            claimer.full_name,
            act.aa_number,
            act.description,
            claimer.person_expedient_id,
            claimer.email
        )
    if email_id == 4:
        act = Aa.query.filter(Aa.aa_id == 1).first()
        body_mail = body_mail.format(
            claimer.full_name,
            act.aa_number,
            act.description,
            claimer.person_expedient_id,
            claimer.email
        )
    if email_id == 5:
        act = Aa.query.filter(Aa.aa_id == 1).first()
        body_mail = body_mail.format(
            claimer.full_name,
            act.aa_number,
            act.description
        )
    if email_id == 7:
        act = Aa.query.filter(Aa.claimer_id == claimer.claimer_id).first()
        body_mail = body_mail.format(
            claimer.full_name,
            act.aa_number,
            claimer.email
        )
    if email_id == 9:
        body_mail = body_mail.format(
            claimer.full_name
        )
    return body_mail



def send_notification_users():

    # query = text("select  cud.* from ( \
    #     select cw.claimer_id	from phase_2.claimer_workflow  cw \
    #     inner join phase_2.claimer_notification cn on cn.claimer_id = cw.claimer_id \
    #     where cw.current_state = 3 and cw.end_date is null and cn.claimer_notified is true and cn.notification_type = 'ELECTRONICAMENTE' ) cw \
    #     left outer join (select claimer_id \
    #     from phase_2.claimer_message cm  where cm.claimer_message_mode = :mode and CM.claimer_message_type = :type) cm on \
    #     cw.claimer_id = cm.claimer_id inner join phase_2.claimer_user_data cud on cud.claimer_id = cw.claimer_id where cm.claimer_id is null")
    # Claimers = db.engine.execute(query.execution_options(
    #     autocommit=True), mode='ELECTRONICO', type='ACTA').fetchall()
    query = text("""select distinct(cud.*), cn.aa_id from phase_2.claimer_notification cn 
    inner join phase_2.claimer_user_data cud on cud.claimer_id = cn.claimer_id   
    where cn.notification_type = :mode and cn.aa_id >2 and notification_document_id is null
    and cn.claimer_notified is true""")
    Claimers = db.engine.execute(query.execution_options(
        autocommit=True), mode='ELECTRONICAMENTE').fetchall()
    db.session.close()

    if Claimers:
        for row in Claimers:
            # try:        
            email_data = email_metadata(9)
            if email_data:
                try:
                    document_created = ReportJasperMasivo.create_report(
                        64, row.claimer_id, row.email, None, row.aa_id)
                except Exception as e:
                    newLogger = loggingBDError(
                        loggerName='build_message_helper send notification user  crear en jasper')
                    newLogger.error(e)
                    print(e)
                print(document_created)
                if document_created['route'] != 'Ya registró un documento anteriormente':
                    print(row.claimer_id)
                    data_send_mail = email_data
                    data_add = {
                        'claimer': row,
                        'document_id': document_created['document_id']
                    }
                    data_send_mail.update(data_add)
                    data_send_mail['body'] = build_body_mail(
                        data_send_mail['body'], 9, row)
                    send_result = send_email_user(
                        data_send_mail, document_created['document'], document_created['name'], row.aa_id)
                    if send_result == 200:
                        try:
                            os.remove(
                                '{}/{}'.format(document_created['document'], document_created['name']))
                        except:
                            print('error al eliminar documento {}/{}'.format(
                                document_created['document'], document_created['name']))
            else:
                return "error al consultar datos de mail o adjuntar documentos"
            # except Exception as e:
            #     newLogger = configLoggingError(loggerName='notification service- message_notification')
            #     newLogger.error(e)
            #     print(e)
    else:
        return "no hay usuarios para notificar"


def resend_notification_users(total):

    if total == 1:
        claimers = User.query.join(ClaimerWorkflow, ClaimerWorkflow.claimer_id == User.claimer_id)\
            .join(ClaimerMessage, ClaimerMessage.claimer_id == User.claimer_id)\
            .filter(ClaimerWorkflow.current_state == 3,
                    ClaimerWorkflow.end_date == None)\
            .filter(ClaimerMessage.success_notification_document_id.is_(None))\
            .filter((ClaimerMessage.delivery_result == 'DELIVERED') | (ClaimerMessage.delivery_result == 'OPEN')).limit(1).all()
    else:
        claimers = User.query.join(ClaimerWorkflow, ClaimerWorkflow.claimer_id == User.claimer_id)\
            .join(ClaimerMessage, ClaimerMessage.claimer_id == User.claimer_id)\
            .filter(ClaimerWorkflow.current_state == 3,
                    ClaimerWorkflow.end_date == None)\
            .filter(ClaimerMessage.success_notification_document_id.is_(None))\
            .filter((ClaimerMessage.delivery_result == 'DELIVERED') | (ClaimerMessage.delivery_result == 'OPEN')).all()

    if claimers:
        for row in claimers:
            # try:
            email_data = email_metadata(5)
            if email_data:
                document_created = ReportJasperMasivo.update_report(
                    13, row, row.email)
                print(document_created)
                if document_created['route'] != 'error jasper':
                    print(document_created)
                    print(row.claimer_id)
                    data_send_mail = email_data
                    data_add = {
                        'claimer': row,
                        'document_id': document_created['document_id']
                    }
                    data_send_mail.update(data_add)
                    data_send_mail['body'] = build_body_mail(
                        data_send_mail['body'], 5, row)
                    send_result = send_email_user(
                        data_send_mail, document_created['document'], document_created['name'])
                    if send_result == 200:
                        row.success_notification_document_id = document_created['document_id']
                        save_changes(row)
            else:
                return "error al consultar datos de mail o adjuntar documentos"
            # except Exception as e:
            #     newLogger = configLoggingError(loggerName='notification service- message_notification')
            #     newLogger.error(e)
            #     print(e)
    else:
        return "no hay usuarios para notificar"


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='build_message_helper')
        db.session.rollback()
        newLogger.error(e)

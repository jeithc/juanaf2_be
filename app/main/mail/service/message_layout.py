
html_notification_auto = """<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
        <title>
        
        </title>
        <!--[if !mso]><!-- -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
        #outlook a { padding:0; }
        .ReadMsgBody { width:100%; }
        .ExternalClass { width:100%; }
        .ExternalClass * { line-height:100%; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
        </style>
        <!--[if !mso]><!-->
        <style type="text/css">
        @media only screen and (max-width:480px) {
            @-ms-viewport { width:320px; }
            @viewport { width:320px; }
        }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <!--[if lte mso 11]>
        <style type="text/css">
        .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
        
    <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet" type="text/css">
        <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
@import url(https://fonts.googleapis.com/css?family=Cabin:400,700);
        </style>
    <!--<![endif]-->

    
        
    <style type="text/css">
    @media only screen and (min-width:480px) {
        .mj-column-per-100 { width:100% !important; max-width: 100%; }
    }
    </style>
    

        <style type="text/css">
        
        
        </style>
        <style type="text/css">.hide_on_mobile { display: none !important;} 
        @media only screen and (min-width: 480px) { .hide_on_mobile { display: block !important;} }
        .hide_section_on_mobile { display: none !important;} 
        @media only screen and (min-width: 480px) { .hide_section_on_mobile { display: table !important;} }
        .hide_on_desktop { display: block !important;} 
        @media only screen and (min-width: 480px) { .hide_on_desktop { display: none !important;} }
        .hide_section_on_desktop { display: table !important;} 
        @media only screen and (min-width: 480px) { .hide_section_on_desktop { display: none !important;} }
        [owa] .mj-column-per-100 {
            width: 100%!important;
        }
        [owa] .mj-column-per-50 {
            width: 50%!important;
        }
        [owa] .mj-column-per-33 {
            width: 33.333333333333336%!important;
        }
        p {
            margin: 0px;
        }
        @media only print and (min-width:480px) {
            .mj-column-per-100 { width:100%!important; }
            .mj-column-per-40 { width:40%!important; }
            .mj-column-per-60 { width:60%!important; }
            .mj-column-per-50 { width: 50%!important; }
            mj-column-per-33 { width: 33.333333333333336%!important; }
            }</style>
        
    </head>
    <body style="background-color:#FFFFFF;">
        
        
    <div style="background-color:#FFFFFF;">
        
    
    <!--[if mso | IE]>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    
    
    <div style="Margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
            <tr>
            <td style="direction:ltr;font-size:0px;padding:9px 0px 9px 0px;text-align:center;vertical-align:top;">
                <!--[if mso | IE]>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
    
            <td
            class="" style="vertical-align:top;width:600px;"
            >
        <![endif]-->
            
    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tbody><tr>
            <td align="justify" style="font-size:0px;padding:3px 3px 3px 3px;word-break:break-word;">
                
    <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:justify;color:#000000;">
        <div style="text-align: justify;"><span style="font-size: 16px;"><span style="font-family: helvetica, arial, sans-serif;">Cordial saludo,</span></span></div>
<div style="text-align: justify;">&#xA0;</div>
<div style="text-align: justify;"><span style="font-size: 16px; font-family: helvetica, arial, sans-serif;">La Defensor&#xED;a del Pueblo se permite <strong>COMUNICARLE</strong>, en su calidad de recurrente de la Resoluci&#xF3;n N.&#xB0; 20190030300000016 de 2019, que ha iniciado la expedici&#xF3;n de los autos que resuelven la proposici&#xF3;n y solicitud de medios de prueba radicados con cada uno de los recursos interpuestos en contra del se&#xF1;alado acto administrativo.</span><br><span style="font-size: 16px; font-family: helvetica, arial, sans-serif;">Sin embargo, es importante aclarar que <strong>NO EN TODOS LOS CASOS</strong> se expedir&#xE1;n los autos de prueba anteriormente indicados; pues estos solamente proceden cuando se aportaron o solicitaron medios de prueba junto con el/los recurso/s.</span><br><span style="font-size: 16px; font-family: helvetica, arial, sans-serif;">Para consultar el contenido &#xED;ntegro de esas determinaciones usted deber&#xE1; ingresar a su sesi&#xF3;n personal en la plataforma &quot;Do&#xF1;a Juana le Responde&quot; <a href="https://donajuanaleresponde.defensoria.gov.co">https://donajuanaleresponde.defensoria.gov.co</a>, autentic&#xE1;ndose con su usuario y contrase&#xF1;a.</span></div>
<div style="text-align: justify;"><span style="font-size: 16px; font-family: helvetica, arial, sans-serif;">En este contexto, teniendo en cuenta que la expedici&#xF3;n de los mencionados autos de prueba est&#xE1; realiz&#xE1;ndose de forma gradual, es importante que los solicitantes recurrentes accedan de manera peri&#xF3;dica a la aplicaci&#xF3;n ya que, actualmente, es el &#xFA;nico canal para las comunicaciones y notificaciones relativas a la resoluci&#xF3;n de los recursos del caso Do&#xF1;a Juana.</span><br><span style="font-size: 16px;"><span style="font-family: helvetica, arial, sans-serif;">Lo anterior, de conformidad con las reglas que para la comunicaci&#xF3;n de actuaciones administrativas previeron el Decreto Legislativo 491 de 2020 y la Resoluci&#xF3;n N.&#xB0; 20200030300000026 de 2020 de la Defensor&#xED;a del Pueblo, expedida espec&#xED;ficamente para la conclusi&#xF3;n del procedimiento atinente al caso del Relleno Sanitario &quot;Do&#xF1;a Juana&quot;.</span><span style="font-family: helvetica, arial, sans-serif;">Finalmente, vale la pena se&#xF1;alar que las determinaciones contenidas en los mencionados autos de tr&#xE1;mite versan &#xFA;nicamente sobre los medios de prueba propuestos y/o solicitados y <strong><span style="text-decoration: underline;">NO CONSTITUYEN la decisi&#xF3;n de fondo de los recursos interpuestos.</span></strong></span></span></div>
<div style="text-align: justify;"><br><br><span style="font-size: 16px; font-family: helvetica, arial, sans-serif;">Cordialmente,</span><br><span style="font-size: 16px;"><strong><span style="font-family: helvetica, arial, sans-serif;">DO&#xD1;A JUANA LE RESPONDE.</span></strong></span></div>
<div style="text-align: justify;">&#xA0;</div>
<div style="text-align: justify;"><span style="font-size: 16px;"><span style="font-family: helvetica, arial, sans-serif;">Por favor,&#xA0;</span></span><span style="font-size: 16px;"><span style="font-family: helvetica, arial, sans-serif;"><strong>no responda a este mensaje</strong>, ya que este buz&#xF3;n de correo electr&#xF3;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&#xF3;n. Si requiere m&#xE1;s informaci&#xF3;n, acuda a los canales de atenci&#xF3;n de la Defensor&#xED;a del Pueblo.</span></span></div>
<div style="text-align: justify;"><span style="font-size: 16px;"><span style="font-family: helvetica, arial, sans-serif;">La Defensor&#xED;a del pueblo <strong>nunca le solicitar&#xE1;</strong> sus datos personales o sus credenciales de acceso a la plataforma DO&#xD1;A JUANA LE RESPONDE mediante v&#xED;nculos de correo electr&#xF3;nico. En caso de recibir alguno, rep&#xF3;rtelo de inmediato a trav&#xE9;s&#xA0; de los canales de atenci&#xF3;n de la Defensor&#xED;a del Pueblo.</span></span></div>
<div style="text-align: justify;"><span style="font-size: 16px;"><span style="font-family: helvetica, arial, sans-serif;"><strong>Aviso de confidencialidad:</strong> Este mensaje est&#xE1; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&#xF3;n confidencial &#xA0;y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&#xF3;n o reproducci&#xF3;n de este, o de cualquiera de sus anexos, est&#xE1; estrictamente prohibida. Si usted ha recibido&#xA0;&#xA0;este mensaje por error, por favor notif&#xED;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&#xF3;n de este.</span></span></div>
    </div>
    
            </td>
            </tr>
        
    </tbody></table>
    
    </div>
    
        <!--[if mso | IE]>
            </td>
        
        </tr>
    
                </table>
                <![endif]-->
            </td>
            </tr>
        </tbody>
        </table>
        
    </div>
    
    
    <!--[if mso | IE]>
        </td>
        </tr>
    </table>
    <![endif]-->
    
    
    </div>
    
    
    
</body></html>

"""
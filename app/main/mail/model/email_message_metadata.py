from app.main import db


class EmailMetadata(db.Model):
    """ Role Model for storing mail send """
    __tablename__ = "email_message_metadata"
    __table_args__ = {'schema': 'phase_2'}
    email_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email_message_type = db.Column(db.String(100))
    subject = db.Column(db.String(512))
    whole = db.Column(db.String)
    limit_days_before_notification = db.Column(db.Integer)
    number_of_resends = db.Column(db.Integer)
    certified_email = db.Column(db.Boolean)
    attachments = db.Column(db.Boolean)

    def __repr__(self):
        return "<email_message_metadata '{}'>".format(self.email_id)

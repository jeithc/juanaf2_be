from flask_restplus import Resource
from flask_restplus import Namespace
from flask import request
from app.main.mail.service.mail_service import *
from app.main.mail.service.build_message_helper import send_notification_users, resend_notification_users
from app.main.util.decorator import login_provider
from app.main.core.util.dto.mail_provider_dto import TestMailDTO, Namespace

api = TestMailDTO.api
mail_test = TestMailDTO.test_mail


@api.route('/send_mail_info_auto')
class MailTest(Resource):
    @api.doc('mail for info auto')
    def get(self):
        """send email Resource"""
        # get the post data
        return send_mail_auto()


@api.route('/verify_message_send')
class SendResult(Resource):
    @api.doc('verify result send mail')
    def get(self):
        """Verify email send Resource"""
        return verify_mail()


@api.route('/verify_message_send_error')
class SendResult(Resource):
    @api.doc('verify result send mail error')
    def get(self):
        """Verify email send Resource error"""
        return verify_mail_process()


@api.route('/mail_file_result')
class SendResult(Resource):
    @api.doc('verify file mail result')
    def get(self):
        """verify file mail result"""
        return verify_mail_file()


@api.route('/send_result_notification')
class SendResult(Resource):
    @api.doc('send_result_notification')
    def get(self):
        """send_result_notification"""
        return send_notification_users()


@api.route('/v1.0.0/mail/resend_result_notification/<total>')
@api.param('total', 'total send' )
class SendResult(Resource):
    @api.doc('resend_result_notification')
    def get(self, total):
        """resend_result_notification"""
        return resend_notification_users(total)
    

@api.route('/tracking_message_provider')
class SendResultProvider(Resource):
    @login_provider
    @api.doc('provider update tracking')
    @api.expect(mail_test, validate=False)
    def post(self):
        """api to provider update tracking"""
        print("INVOKED")
        data=request.data
        return update_tracking_provider(request.json)
from .. import db

class NotoriousFactsPlaces(db.Model):
    """ NotoriousFactsPlaces Model for app """
    __tablename__ = "notorious_facts_places"
    __table_args__ = {'schema':'core'}
    notorious_facts_id = db.Column(db.Integer, primary_key=True)
    notorious_facts_name = db.Column(db.String(512))
    notorious_facts_address = db.Column(db.String(256))

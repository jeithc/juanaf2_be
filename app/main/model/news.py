from .. import db

class News(db.Model):
    """ News Model for app """
    __tablename__ = "news_management"
    __table_args__ = {'schema':'core'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    news_order = db.Column(db.Integer)
    active = db.Column(db.Boolean)
    news_type = db.Column(db.String(100))
    news_pub_date = db.Column(db.Date)
    news_location = db.Column(db.String(100))
    news_text = db.Column(db.Text)

    
    def __repr__(self):
        return "<News '{}'>".format(self.id)
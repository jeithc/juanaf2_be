from sqlalchemy.orm import relationship

from .. import db, flask_bcrypt
import datetime
from ..config import key
import jwt
from app.main.util import db_helper


class Claimer_documents(db.Model):
    """ claimer_documents Model for storing user related details """
    __tablename__ = "claimer_documents"
    __table_args__ = {'schema': 'phase_2'}
    document_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    radicate_number = db.Column(db.String(20))
    document_type_id = db.Column(db.Integer, db.ForeignKey('core.document_type.id'))
    document_type = relationship("Document_type")
    url_document = db.Column(db.String)
    radication_date = db.Column(db.Date)
    expedient_entry_date = db.Column(db.Date)
    flow_document = db.Column(db.String(1))
    number_of_pages = db.Column(db.Integer)
    origin_document = db.Column(db.String(10))
    document_checksum = db.Column(db.String(512))
    metadata_server_document = db.Column(db.String)
    xml_document = db.Column(db.String)
    document_size = db.Column(db.Integer)
    document_state = db.Column(db.Boolean)
    document_format = db.Column(db.Integer)
    document_format_initials = db.Column(db.String(10))
    user_id = db.Column(db.Integer)

    def __repr__(self):
        return "<Documents '{}'>".format(self.document_id)

    def update(self, session):
        s = session
        mapped_values = db_helper.process_obj_fields(self)
        s.query(Claimer_documents).filter(Claimer_documents.document_id == self.document_id).update(mapped_values)
        s.commit()

    @staticmethod
    def init_from_dto(data):
        claimerDocument = Claimer_documents()
        if ('radicateNumber' in data and data['radicateNumber']):
            claimerDocument.radicate_number = data['radicateNumber']
        if ('documentTypeId' in data and data['documentTypeId']):
            claimerDocument.document_type_id = data['documentTypeId']
        if ('documentId' in data and data['documentId']):
            claimerDocument.document_id = data['documentId']
        if ('radicationDate' in data and data['radicationDate']):
            claimerDocument.radication_date = data['radicationDate']

        return claimerDocument


class Publication_claimer(db.Model):
    """ Amacena todos los reclamantes que están relacionados con una publicación masiva. """
    __tablename__ = "publication_claimer"
    __table_args__ = {'schema':'phase_2'}
    publication_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id  = db.Column(db.Integer)

    def __repr__(self):
        return "<Publication_claimer '{}'>".format(self.publication_id)



class Claimer_documents_view(db.Model):
    """ claimer_documents view with documents publications """
    __tablename__ = "vw_documents_per_claimer"
    __table_args__ = {'schema': 'phase_2'}
    document_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    radicate_number = db.Column(db.String(20))
    document_type_id = db.Column(db.Integer)
    url_document = db.Column(db.String)
    radication_date = db.Column(db.Date)
    expedient_entry_date = db.Column(db.Date)
    flow_document = db.Column(db.String(1))
    number_of_pages = db.Column(db.Integer)
    origin_document = db.Column(db.String(10))
    document_checksum = db.Column(db.String(512))
    metadata_server_document = db.Column(db.String)
    xml_document = db.Column(db.String)
    document_size = db.Column(db.Integer)
    document_state = db.Column(db.Boolean)
    document_format = db.Column(db.Integer)

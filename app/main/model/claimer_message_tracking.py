from app.main import db

class ClaimerMessageTracking(db.Model):
    __tablename__ = 'claimer_message_tracking'
    __table_args__ = {'schema': 'phase_2'}

    claimer_message_tracking_id = db.Column(db.Integer, primary_key=True)
    claimer_message_id = db.Column(db.Integer)
    registration_date = db.Column(db.DateTime)
    current_message_state = db.Column(db.String(100))
    document_tracking_id = db.Column(db.Integer)
    tracking_date = db.Column(db.DateTime)
    tracking_detail = db.Column(db.Text)
    verify = db.Column(db.Boolean)

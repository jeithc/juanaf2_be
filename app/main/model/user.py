
from .. import db, flask_bcrypt


class User(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "claimer_user_data"
    __table_args__ = {'schema': 'phase_2'}
    claimer_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    expedient_id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(128))
    middle_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128))
    maiden_name = db.Column(db.String(128))
    document_type = db.Column(db.String(10))
    document_number = db.Column(db.String(20))
    email = db.Column(db.String(255))
    id_natgeo = db.Column(db.Integer)
    id_bog_loc_neig = db.Column(db.Integer)
    urban_rural_location = db.Column(db.String)
    address = db.Column(db.String(255))
    phone1 = db.Column(db.String(64))
    phone2 = db.Column(db.String(64))
    username = db.Column(db.String(250))
    password_hash = db.Column(db.String(100))
    updated_at = db.Column(db.Date)
    last_updated_by = db.Column(db.String(100))
    authorize_electronic_notificati = db.Column(db.Boolean)
    electronic_authorization_date = db.Column(db.Date)
    claimer_state = db.Column(db.String(1))
    initial_origin_data = db.Column(db.String(1))
    expedition_date_available = db.Column(db.Boolean)
    expedition_document_date = db.Column(db.Date)
    birth_date = db.Column(db.Date)
    cause_non_registration = db.Column(db.String(100))
    full_name = db.Column(db.String(512))
    person_expedient_id = db.Column(db.Integer)
    approved_questions = db.Column(db.Boolean)
    email_dp = db.Column(db.String(255))
    registered_claimer = db.Column(db.Boolean)
    authorized_news_data_treatment = db.Column(db.Boolean)
    alternate_email = db.Column(db.String(255))
    expedients = db.relationship("Expedients")
    country = db.Column(db.String(128))
    city = db.Column(db.String(128))
    non_former_claimer = db.Column(db.Boolean)
    valid_email = db.Column(db.Boolean)
    current_payment_state = db.Column(db.String(128))
    allow_radication = db.Column(db.Boolean)

    @staticmethod
    def init_from_dto(data):
        user = User()

        if 'email' in data and data['email']:
            user.email = data['email']
        if 'email2' in data and data['email2']:
            user.email2 = data['email2']
        if 'username' in data and data['username']:
            user.username = data['username']
        if 'password' in data and data['password']:
            user.password = data['password']
        if 'firstName' in data and data['firstName']:
            user.first_name = data['firstName']
        if 'middleName' in data and data['middleName']:
            user.middle_name = data['middleName']
        if 'lastName' in data and data['lastName']:
            user.last_name = data['lastName']
        if 'maidenName' in data and data['maidenName']:
            user.maiden_name = data['maidenName']
        if 'documentType' in data and data['documentType']:
            user.document_type = data['documentType']
        if 'documentNumber' in data and data['documentNumber']:
            user.document_number = data['documentNumber']
        if 'expeditionDocumentDate' in data and data['expeditionDocumentDate']:
            user.expedition_document_date = data['expeditionDocumentDate']
        if 'idNatgeo' in data and data['idNatgeo']:
            user.id_natgeo = data['idNatgeo']
        if 'idBogLocNeig' in data and data['idBogLocNeig']:
            user.id_bog_loc_neig = data['idBogLocNeig']
        if 'urbanRuralLocation' in data and data['urbanRuralLocation']:
            user.urban_rural_location = data['urbanRuralLocation']
        if 'birthDate' in data and data['birthDate']:
            user.birth_date = data['birthDate']
        if 'address' in data and data['address']:
            user.address = data['address']
        if 'phone1' in data and data['phone1']:
            user.phone1 = data['phone1']
        if 'phone2' in data and data['phone2']:
            user.phone2 = data['phone2']
        if 'country' in data and data['country']:
            user.country = data['country']
        if 'city' in data and data['city']:
            user.city = data['city']
        if 'expedientId' in data and data['expedientId']:
            user.expedient_id = data['expedientId']
        if 'nonFormerClaimer' in data and data['nonFormerClaimer']:
            user.non_former_claimer = data['nonFormerClaimer']
        if 'currentPaymentState' in data and data['currentPaymentState']:
            user.current_payment_state = data['currentPaymentState']
        if 'allowRadication' in data and data['allowRadication']:
            user.allow_radication = data['allowRadication']
        user.full_name = User.generate_full_name(user)
        return user

    @staticmethod
    def generate_full_name(obj):
        filterobj = [obj.first_name, obj.middle_name, obj.last_name, obj.maiden_name]
        obj = list(filter(None, filterobj))
        return ' '.join(obj)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def password_encrypt(self, password):
        return flask_bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User '{}'>".format(self.username)


class Expedients(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "expedient_person_search"
    __table_args__ = {'schema': 'phase_2'}
    expedient_id = db.Column(db.Integer, primary_key=True)
    radicate_number = db.Column(db.String(40), primary_key=True)
    doc_type = db.Column(db.String(10))
    doc_number = db.Column(db.String(20), db.ForeignKey(
        'phase_2.claimer_user_data.document_number'))
    first_name = db.Column(db.String(128))
    middle_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128))
    maiden_name = db.Column(db.String(128))
    person_expedient_id = db.Column(db.Integer)
    full_name = db.Column(db.String(128))


class InitialPassword(db.Model):
    __tablename__ = "claimer_initial_pass"
    __table_args__ = {'schema': 'phase_2'}
    claimer_id = db.Column(db.Integer, primary_key=True)
    initial_password = db.Column(db.String(128))
    used_password = db.Column(db.Boolean)

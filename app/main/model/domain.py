from .. import db

class Domain(db.Model):
    """ Domain Model for app """
    __tablename__ = "domain"
    __table_args__ = {'schema':'core'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(100))
    value = db.Column(db.String(2000))
    active = db.Column(db.Boolean)
    name = db.Column(db.String(100))
    parent_code = db.Column(db.Integer)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    last_updated_by = db.Column(db.String(20))

    
    def __repr__(self):
        return "<Domain '{}'>".format(self.id)


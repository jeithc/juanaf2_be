from .. import db

class Nat_geo(db.Model):
    """ nat_geo Model for app """
    __tablename__ = "nat_geo_division"
    __table_args__ = {'schema':'core'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    department = db.Column(db.String(128))
    municipality = db.Column(db.String(128))
    dane_code = db.Column(db.String(5))

    
    def __repr__(self):
        return "<nat_geo '{}'>".format(self.id)


class Locality(db.Model):
    """ locality_neighborhood Model for app """
    __tablename__ = "locality_neighborhood"
    __table_args__ = {'schema':'core'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    locality = db.Column(db.String(128))
    neighborhood = db.Column(db.String(128))

    
    def __repr__(self):
        return "<locality '{}'>".format(self.id)


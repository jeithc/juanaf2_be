from .. import db
from sqlalchemy.sql import func

class UserAudit(db.Model):
    """ user_audit Model for app """
    __tablename__ = "user_audit"
    __table_args__ = {'schema':'core'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ip = db.Column(db.String(20))
    user = db.Column(db.String(250))
    user_agent = db.Column(db.String(250))
    method = db.Column(db.String(20))
    service = db.Column(db.String(200))
    data = db.Column(db.String(1000))
    type = db.Column(db.String(10))
    last_updated_by = db.Column(db.String(20))
    created_at = db.Column(db.DateTime, server_default=func.now())

    
    def __repr__(self):
        return "<User_audit '{}'>".format(self.id)
from .. import db

class Questions(db.Model):
    """ Question model for user authentication """
    __tablename__ = "registration_question"
    __table_args__ = {'schema':'phase_2'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    question = db.Column(db.String(512))
    state = db.Column(db.String(1))
    multiple_answer = db.Column(db.Boolean(128))
    exclude_question = db.Column(db.String(128))
    visualization_order = db.Column(db.Integer)

    def __repr__(self):
        return "<Question '{}'>".format(self.id)
    

class Answers(db.Model):
    """ Answer model for user authentication """
    __tablename__ = "qa_decision_table"
    __table_args__ = {'schema':'phase_2'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    person_expedient_id = db.Column(db.Integer)
    last_name = db.Column(db.String(128))
    maiden_name = db.Column(db.String(128))
    act_type = db.Column(db.String(2))
    registration_question_id = db.Column(db.Integer)
    question= db.Column(db.String(512))
    answer= db.Column(db.String(512))


    def __repr__(self):
        return "<Answer '{}'>".format(self.id)

class Answers_registration(db.Model):
    """ Answer model for answers authentication """
    __tablename__ = "answer_registration"
    __table_args__ = {'schema':'phase_2'}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    answer_event_id = db.Column(db.String(64))
    answer_event_date = db.Column(db.Date)
    person_notification_id = db.Column(db.Integer)
    registration_question_id = db.Column(db.Integer)
    answer = db.Column(db.String(512))
    question = db.Column(db.String(512))
    valid_answer = db.Column(db.Boolean)


    def __repr__(self):
        return "<Answers_registration '{}'>".format(self.id)

from app.main import db

class DocumentosFase1(db.Model):
    """ Envio Masivo Model """
    __tablename__ = "radicate_juana1"
    __table_args__ = {'schema':'tempwork'}
    id = db.Column(db.Integer, primary_key=True)
    box = db.Column(db.Integer)
    number = db.Column(db.String(20)) 
    state = db.Column(db.String(20))
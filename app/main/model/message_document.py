from app.main import db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

class MessageDocument(db.Model):
    "claimerNotification model"
    __tablename__ = "message_document"
    __table_args__ = {'schema' : 'phase_2'}
    claimer_message_id = Column("claimer_message_id", Integer, ForeignKey("phase_2.claimer_message.claimer_message_id"), primary_key=True)
    document_id = Column("document_id", Integer, ForeignKey("phase_2.claimer_documents.document_id"), primary_key=True)

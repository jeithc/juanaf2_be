from app.main import db

class EnvioMasivo(db.Model):
    """ Envio Masivo Model """
    __tablename__ = "envio_masivo_migracion_defensoria"
    __table_args__ = {'schema':'tempwork'}
    registro = db.Column(db.Integer, primary_key=True)
    claimer_id = db.Column(db.Integer)
    estado = db.Column(db.String(20))
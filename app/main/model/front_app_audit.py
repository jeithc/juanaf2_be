from .. import db

class FrontAudit(db.Model):
    """ front_app_audit Model for app """
    __tablename__ = "front_app_audit"
    __table_args__ = {'schema':'core'}
    row_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_date = db.Column(db.DateTime)
    event_desc = db.Column(db.Text)

    
    def __repr__(self):
        return "<front_app_audit '{}'>".format(self.row_id)
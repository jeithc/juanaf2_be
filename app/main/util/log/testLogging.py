import logging
import os
import json
import logging.config

print("Hello")

print("__name__ value: ", __name__)

def setup_logging(
    default_path='logging.json',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

def main():
    print("python main function begin")

    # logging.basicConfig(filename='example.log',level=logging.DEBUG)
    # logging.debug('This message should go to the log file')
    # logging.info('So should this')
    # logging.warning('And this, too')

    setup_logging()
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info('test3')    


    # logging.getLogger(__name__).addHandler(logging.NullHandler())    
    
    # logger = logging.getLogger()
    # logger.setLevel(logging.INFO)
    # logger.info('test2')
    print("python main function end")


if __name__ == '__main__':
    main()
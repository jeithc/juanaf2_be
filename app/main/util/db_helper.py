

def process_obj_fields(db_object):
    mapped_values = {}
    for attr, value in db_object.__dict__.items():

        if '_sa_instance_state' != attr:
            mapped_values[attr] = value

    return mapped_values

#from random import SystemRandom
from .. import flask_bcrypt
import random
import string

def _create_password(num):

    # length = 8
    # value = "123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ"
    # cryptogen = SystemRandom()
    # p = ""
    # while length > 0:
    #     p = p + cryptogen.choice(value)
    #     length = length - 1
    # return p

    character= "$%&/#"
    length = num-3
    pwd = []
    pwd.append(random.choice(string.ascii_uppercase))
    pwd.append(str(random.randint(1,9)))
    pwd.append(random.choice(character))
    for i in range(0, length):
        pwd.append(random.choice(string.ascii_lowercase))
    total = random.shuffle(pwd)
    return ''.join(pwd)

def password_encrypt(password):
        return  flask_bcrypt.generate_password_hash(password).decode('utf-8')
        
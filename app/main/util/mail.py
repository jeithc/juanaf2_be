from flask import Flask
from flask_mail import Mail, Message
from threading import Thread
from time import sleep
from app.main.service.logging_service import configLoggingError
import datetime

app = Flask(__name__)

# app.config['MAIL_SERVER']='smtp.gmail.com'
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USERNAME'] = 'juanafase2@gmail.com'
# app.config['MAIL_PASSWORD'] = 'Master01*'
# app.config['MAIL_USE_TLS'] = False
# app.config['MAIL_USE_SSL'] = True
# app.config['MAIL_ASCII_ATTACHMENTS'] = False

app.config['MAIL_SERVER'] = 'email-smtp.us-east-1.amazonaws.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = 'AKIAZPQ5GGGD4TQH54MK'
app.config['MAIL_PASSWORD'] = 'BHCucLTH1eQP+LIHA+iOrrdr5k/YjVdwJghi9YIqcV5b'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_ASCII_ATTACHMENTS'] = False

# app.config['MAIL_SERVER'] = 'smtp.office365.com'
# app.config['MAIL_PORT'] = 587
# app.config['MAIL_USERNAME'] = 'notificaciones_juana@defensoria.gov.co'
# app.config['MAIL_PASSWORD'] = 'DonaJuana.2019'
# app.config['MAIL_USE_TLS'] = True
# app.config['MAIL_USE_SSL'] = False
# app.config['MAIL_ASCII_ATTACHMENTS'] = False


mail = Mail(app)
mail.init_app(app)


def send_mail(datamail, password):
    msg = Message(u"Doña Juana le responde - cambio de clave", sender='notificaciones_juana@defensoria.gov.co',
                  recipients=[datamail])
    msg.html = "<p>Su clave ha sido modificada en el sistema Do&ntilde;a Juana le responde </p> <p>Su nueva contrase&ntilde;a es: </p>  <p> CONTRASE&Ntilde;A: {} </p>".format(
        password)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr


def send_mail_report(subject, message, datamail, email, archive):
    msg = Message(subject, sender='notificaciones_juana@defensoria.gov.co', recipients=[email])
    msg.html = message.encode('utf-8').strip()
    with app.open_resource(datamail) as fp:
        msg.attach(archive, "application/pdf", fp.read())
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr


def send_async_email(app, msg):
    with app.app_context():
        newLogger = configLoggingError(loggerName='Mail_send_error')
        try:
            mail.send(msg)
        except Exception as e:
            print('error')
            print(e)
            newLogger.error(
                "hour {}  []  error: {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), str(e)))


def send_mail_general(datamail, subject, html_body):
    footer = '<p></p><p></p><p>Por favor, <b>no responda a este mensaje</b>, ya que este buzón de correo electrónico \
        no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribución. Si requiere \
        más información, acuda a los canales de atención de la Defensoría del Pueblo.</p><p>La Defensoría del pueblo \
        <b>nunca le solicitará</b> sus datos personales o sus credenciales de acceso a la plataforma <b>DOÑA JUANA LE \
        RESPONDE</b>mediante vínculos de correo electrónico. En caso de recibir alguno, repórtelo de inmediato a través\
        de los canales de atención de la Defensoría del Pueblo.</p><p><b>Aviso de confidencialidad:</b> Este mensaje \
        está dirigido para ser usado por su destinatario(a) exclusivamente y puede contener información confidencial \
        y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribución\
        o reproducción de este, o de cualquiera de sus anexos, está estrictamente prohibida. Si usted ha recibido \
        este mensaje por error, por favor notifíquenos inmediatamente y elimine su texto original, incluidos \
        los anexos, o destruya cualquier reproducción de este.</p>'
    msg = Message(subject, sender='notificaciones_juana@defensoria.gov.co',
                    recipients=[datamail])
    msg.html = format_html(html_body+footer)
    # msg = Message(subject, sender='notificaciones_juana@defensoria.gov.co',
    #                 recipients=['notificaciones_juana@defensoria.gov.co'], bcc=datamail )
    thr = Thread(target=send_async_email, args=[app, msg])
    sleep(4)
    thr.start()
    return thr


def format_html(html):
    html = html.replace('ñ', '&ntilde;')
    html = html.replace('Ñ', '&Ntilde;')
    html = html.replace('á', '&aacute;')
    html = html.replace('Á', '&Aacute;')
    html = html.replace('é', '&eacute;')
    html = html.replace('É', '&Eacute;')
    html = html.replace('í', '&iacute;')
    html = html.replace('Í', '&Iacute;')
    html = html.replace('ó', '&oacute;')
    html = html.replace('Ó', '&Oacute;')
    html = html.replace('ú', '&uacute;')
    html = html.replace('Ú', '&Uacute;')
    html = html.replace('°', '&deg;')

    return html

def get_email_footer():

    footer = '<b/> <b/> <b/>Por favor, no responda a este mensaje, ya que este buzón de correo electrónico no se encuentra ' \
             'habilitado para recibir mensajes, pues solamente tiene funciones de distribución. Si requiere más ' \
             'información, acuda a los canales de atención de la Defensoría del Pueblo. <b/> <b/>' \
             'La Defensoría del pueblo nunca le solicitará sus datos personales o sus credenciales de acceso a la ' \
             'plataforma DOÑA JUANA LE RESPONDE mediante vínculos de correo electrónico. En caso de recibir alguno, ' \
             'repórtelo de inmediato a través de los canales de atención de la Defensoría del Pueblo. <b/> <b/>' \
             'Aviso de confidencialidad: Este mensaje está dirigido para ser usado por su destinatario(a) exclusivamente' \
             ' y puede contener información confidencial y/o reservada o protegida legalmente. Si usted no es el ' \
             'destinatario, le comunicamos que cualquier distribución o reproducción de este, o de cualquiera de sus ' \
             'anexos, está estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notifíquenos ' \
             'inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducción de este.'

    return format_html(footer)


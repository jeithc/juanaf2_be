from functools import wraps
from app.main.service.audit_service import *
from app.main.service.auth_service import basic_authentication, get_fail_authentication, provider_authentication
from flask import request
from app.main.service.auth_helper import Auth


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')

        if not token:
            return data, status

        return f(*args, **kwargs)

    return decorated


def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')

        if not token:
            return data, status

        admin = token.get('admin')
        if not admin:
            response_object = {
                'status': 'fail',
                'message': 'admin token required'
            }
            return response_object

        return f(*args, **kwargs)

    return decorated

def login_app(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        authentication=basic_authentication(request)
        if not authentication:
            return get_fail_authentication()
        return f(*args, **kwargs)
    return decorated

def login_provider(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        authentication=provider_authentication(request)
        if not authentication:
            return get_fail_authentication()
        return f(*args, **kwargs)
    return decorated


def audit_front(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        new_front_audit(request)
        return f(*args, **kwargs)
    return decorated


def audit_init(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'User-Id' in request.headers:
            if '@' in request.headers['User-Id'] :
                new_user_audit_init(request)
            else:
                save_new_audit_ini(request)
        else:
            save_new_audit_ini(request)
        return f(*args, **kwargs)
    return decorated


def audit_finish(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        return_value = f(*args, **kwargs)
        if 'User-Id' in request.headers:
            if '@' in request.headers['User-Id'] :
                new_user_audit_finish(return_value, request)
            else:
                save_new_audit_out(return_value, request)
        else:
            save_new_audit_ini(request)
        return return_value
    return decorated

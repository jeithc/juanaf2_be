from flask_restplus import Namespace, fields

ns = Namespace('juanaf2', description='User related operations')
#ns2 = Namespace('S3', description='User related operations')

class UserDto:
    api = ns
    user = api.model('user1', {
        'claimerId': fields.Integer( attribute= 'claimer_id', description='claimer_id'),
        'expedientId': fields.Integer( attribute='expedient_id'),
        'firstName': fields.String( attribute='first_name'),
        'middleName': fields.String( attribute='middle_name'),
        'lastName': fields.String( attribute='last_name'),
        'maidenName': fields.String( attribute='maiden_name'),
        'documentType': fields.String( attribute='document_type'),
        'documentNumber': fields.String( attribute='document_number'),
        'email': fields.String( attribute='email'),
        'idNatgeo': fields.Integer( attribute='id_natgeo'),
        'idBogLocNeig': fields.Integer( attribute='id_bog_loc_neig'),
        'urbanRuralLocation': fields.String( attribute='urban_rural_location'),
        'address': fields.String( attribute='address'),
        'phone1': fields.String( attribute='phone1'),
        'phone2': fields.String( attribute='phone2'),
        'username': fields.String( description='username'),
        'authorizeElectronicNotificati': fields.Boolean( attribute='authorize_electronic_notificati'),
        'electronicAuthorizationDate': fields.Date(attribute='electronic_authorization_date'),
        #'claimerState': fields.String( attribute='claimer_state'),
        'expeditionDateAvailable': fields.Boolean( attribute='expedition_date_available'),
        'personExpedientId': fields.Integer( attribute='person_expedient_id'),
        'approvedQuestion': fields.Boolean( attribute='approved_questions'),
        #'causeNonRegistration': fields.Boolean( attribute='cause_non_registration'),
        'registeredClaimer': fields.Boolean( attribute='registered_claimer'),
        'currentState': fields.Integer(attribute='current_state', description='Estado actual en el que se encuentra el aspirante'),
        'alternateEmail': fields.String( attribute='alternate_email'),
        'country': fields.String( attribute='country'),
        'city': fields.String( attribute='city'),
        'fullName': fields.String(attribute='full_name'),
        'nonFormerClaimer': fields.String(attribute='non_former_claimer'),
        'currentPaymentState': fields.String(attribute='current_payment_state'),
        'allowRadication': fields.Boolean(attribute='allow_radication')
    })

    user_update = api.model('user', {

        'claimerId': fields.Integer( description='claimer_id'),
        'expedientId': fields.Integer( description='expedient_id'),
        'firstName': fields.String( description='first_name'),
        'middleName': fields.String( description='middle_name'),
        'lastName': fields.String( description='last_name'),
        'maidenName': fields.String( description='maiden_name'),
        'documentType': fields.String( description='document_type'),
        'documentNumber': fields.String( description='document_number'),
        'email': fields.String(  description='user email address'),
        'idNatgeo': fields.Integer( description='id_natgeo'),
        'idBogLocNeig': fields.Integer( description='id_bog_loc_neig'),
        'urbanRuralLocation': fields.String( description='urban_rural_location'),
        'address': fields.String( description='address'),
        'phone1': fields.String( description='phone1'),
        'phone2': fields.String( description='phone2'),
        'authorizedNewsDataTreatment': fields.Boolean( attribute='authorized_news_data_treatment'),
        'alternateEmail': fields.String( attribute='alternate_email'),
        'city': fields.String( attribute='city'),
        'dataUpdateRequired': fields.Boolean( attribute='data_update_required'),
        'fullName': fields.String( attribute='full_name')
        #'authorizeElectronicNotificati': fields.Boolean( description='authorize_electronic_notificati'),
        #'claimerState': fields.String( description='address')
    })

    claimer_for_atril = api.model('claimer', {

        'tipoDocumento': fields.String(attribute='tipo_documento'),
        'numeroDocumento': fields.String(attribute='numero_documento'),
        'nombre': fields.String(attribute='nombre'),
        'notificado': fields.Boolean(attribute='notificado'),
        'tipoNotificacion': fields.String(attribute='tipo_notificacion'),
        'fechaNotificacion': fields.String(attribute='fecha_notificacion'),
        'citado': fields.Boolean(attribute='citado'),
        'fechaCitacion': fields.String(attribute='fecha_citacion')
    })


class AuthDto:
    api = ns
    user_auth = api.model('auth_details', {
        'username': fields.String(required=True, description='The username'),
        'password': fields.String(required=True, description='The user password '),
    })

    logout = api.model('logout', {
        'userId': fields.String(required=True, description='The user id')
    })


class RecoverPasswordDto:
    api = ns
    recover_data = api.model('recover_details', {
        'username': fields.String(required=True, description='The username'),
        'email': fields.String(required=True, description='The user email')
    })

    change_password = api.model('change_details', {
        'claimer_id': fields.String(required=True, description='id username'),
        'new_password': fields.String(required=True, description='The new password')
    })


class PagedFilter:
    api = ns
    filter_detail = api.model('filter_detail', {
        'doc_type':fields.String(required=False, description='Document Type ', default=''),
        'doc_number': fields.String(required=False, description='Document Number' , default=''),
        'first_name': fields.String(required=False, description='First Name' , default=' '),
        'middle_name': fields.String(required=False, description='Middle Name' , default=''),
        'last_name': fields.String(required=False, description='Surname' , default=''),
        'maiden_name': fields.String(required=False, description='Second Surname' , default=''),
        'radicate_number': fields.String(required=False, description='radicate_number' , default='0000'),
        'radicate_citation': fields.String(required=False, description='radicate_citation' , default='0000'),
        'current_state' : fields.String(required=False , description='current state' , default=''),
        'person_expedient_id': fields.String(required=False, description='expedient Number' , default=''),
        'area_affected': fields.String(required=False, description='expedient Number' , default=''),
        'subjectivity_condition': fields.String(required=False, description='expedient Number' , default=''),
        'beneficiary': fields.String(required=False, description='expedient Number' , default=''),
        'aa': fields.String(required=False, description='expedient Number' , default='')
    })

    filter_detail_total = api.model('filter_detail_total', {
        'total':fields.Integer(description='total', default='')
    })


class FilterUser:
    api = ns

    filter_detail_in = api.model('filter_details', {
        'doc_type':fields.String(required=False, description='Document Type ', default=''),
        'doc_number': fields.String(required=False, description='Document Number' , default=''),
        'first_name': fields.String(required=False, description='First Name' , default=' '),
        'middle_name': fields.String(required=False, description='Middle Name' , default=''),
        'last_name': fields.String(required=False, description='Surname' , default=''),
        'maiden_name': fields.String(required=False, description='Second Surname' , default=''),
        'radicate_number': fields.String(required=False, description='radicate_number' , default='0000'),
        'person_expedient_id': fields.String(required=False, description='expedient Number' , default=''),
        'area_affected': fields.String(attribute='sub_group'),
        'subjectivity_condition': fields.String(attribute='sub_condition'),
        'beneficiary': fields.String(attribute='beneficiary'),
        'aa': fields.String(attribute='aa'),
        'page': fields.Integer(required=False, description='Page Number' , default=''),
        'perPage': fields.Integer(required=False, description='Number of records per page' , default='10'),
        'orderCriteria': fields.String(required=False, description='Order Criteria' , default=' NAME, DOCUMENT'),
        'sortingOrder': fields.String(required=False, description='Sorting Order' , default=' DESC, ASC')
    })

    filter_detail_out = api.model('filter_details_out', {
        'documentType':fields.String(attribute= 'document_type'),
        'documentNumber': fields.String(attribute= 'document_number'),
        'firstName': fields.String(attribute= 'first_name'),
        'middleName': fields.String (attribute= 'middle_name'),
        'lastName': fields.String(attribute= 'last_name'),
        'maidenName': fields.String(attribute= 'maiden_name'),
        'fullName': fields.String(attribute= 'full_name'),
        'personExpedientId': fields.String(attribute= 'person_expedient_id'),
        'claimerId': fields.String(attribute= 'claimer_id'),
        'approvedQuestion' : fields.Boolean(attribute= 'approved_questions'),
        'causeNonRegistration' : fields.String(attribute= 'cause_non_registration'),
        'registeredClaimer' : fields.String(attribute= 'registered_claimer'),
        'current_state': fields.String(attribute='current_state'),
        'nonFormerClaimer': fields.Boolean(attribute='non_former_claimer'),
        'authorizeElectronicNotificati': fields.Boolean( attribute='authorize_electronic_notificati'),
        'currentPaymentState': fields.String(attribute='current_payment_state'),
        'allowRadication': fields.Boolean(attribute='allow_radication')

    })

    filter_integral_out = api.model('filter_details_out', {
        'documentType':fields.String(attribute='document_type'),
        'documentNumber': fields.String(attribute='document_number'),
        'firstName': fields.String(attribute='first_name'),
        'middleName': fields.String (attribute='middle_name'),
        'lastName': fields.String(attribute='last_name'),
        'maidenName': fields.String(attribute='maiden_name'),
        'fullName': fields.String(attribute='full_name'),
        'personExpedientId': fields.String(attribute='person_expedient_id'),
        'claimerId': fields.String(attribute='claimer_id'),
        'area_affected': fields.String(attribute='sub_group'),
        'subjectivity_condition': fields.String(attribute='sub_condition'),
        'beneficiary': fields.String(attribute='beneficiary'),
        'aa': fields.String(attribute='aa')
    })


class QuestionRequestDTO:
    api = ns
    filter_questions = api.model('question_details', {
        'typeId':fields.String(required=False, description='Question Identifier'),
        'statement':fields.String(required=False, description='Statement')
    })


class QuestionStatementDTO:
    api = ns
    filter_questions = api.model('question_details', {
        'typeId':fields.String(required=False, description='Question Identifier'),
        'statement':fields.String(required=False, description='Statement')
    })


class QuestionAnswersDTO:
    api = ns
    filter_questions = api.model('question_details', {
        'question_id':fields.String(required=False, description='Question Identifier', attribute='question_id'),
        'quest': fields.String(required=False, description='Option 1'),
        'ans': fields.String(required=False, description='Option 2'),
        'validity': fields.String(required=False, description='Option 3'),
        'vis_order': fields.String(required=False, description='Option 4'),
        'exclude_quest': fields.String(required=False, description='Option 4'),
        'multiple_ans': fields.String(required=False, description='Option 4')
    })


class CorrectAnswerDTO:
    api = ns
    filter_questions = api.model('question_details', {
       'typeId':fields.String(required=False, description='Question Identifier'),
        'correctAnswer':fields.String(required=False, description='Correct Question Answer')
    })


class ReportRequestDTO:
    api = ns
    report_request = api.model('request_details', {
        'claimerId':fields.String(required=False, description='Claimer'),
        'reportType': fields.String(required=False, description='Report type')
    })


class DomainDto:
    api = ns
    domain = api.model('domain', {
        'code':fields.String(required=False, description='code domain'),
        'value': fields.String(required=False, description='domain value')
    })


class Nat_geoDto:
    api = ns
    nat_geo = api.model('nat_geo', {
        'id':fields.Integer(description='code domain'),
        'department': fields.String(description='department'),
        'municipality': fields.String(description='municipality'),
        'dane_code': fields.String(description='dane_code')
    })

    locality = api.model('locality', {
        'id':fields.Integer(description='id locality'),
        'locality': fields.String(description='locality'),
        'neighborhood': fields.String(description='municipality')
    })


class NotoriousFactsPlacesDto:
    api = ns
    notorious_facts_places = api.model('notorious_facts_places', {
        'notorious_facts_id':fields.Integer(description='id notorious facts'),
        'notorious_facts_name': fields.String(description='name'),
        'notorious_facts_address': fields.String(description='address')
    })


class QuestionsDto:
    api = ns
    filter_questions = api.model('questions', {
        'id':fields.Integer(required=False, description='Question Identifier'),
        'question': fields.String(required=False, description='question'),
        'state': fields.String(required=False, description='state'),
        'multiple_answer': fields.Boolean(required=False, description='multiple_answer'),
        'exclude_question': fields.Boolean(required=False, description='exclude_question'),
        'visualization_order': fields.Integer(required=False, description='visualization_order')
    })

    answer = api.model('answer', {
        'idQuestion':fields.Integer(required=True, description='Question Identifier'),
        'question':fields.String(required=True, description='Question text'),
        'claimerId':fields.Integer(required=True, description='Claimer Identifier'),
        'personExpedientId':fields.Integer(required=True, description='Person expedient id'),
        'answer':fields.String(required=True, description='answer user')
    })

    option_list = api.model('option_list', {
        'ans': fields.String(required=False, description='question'),
    })


class ReportDownloadDTO:
    api = ns
    report_download = api.model('download_details', {
        'url':fields.String(required=False, description='URL Report')
    })


class DocumentDTO:
    api = ns
    documents = api.model('documents', {
        'radicateNumber':fields.String(attribute='radicate_number'),
        'documentTypeId':fields.Integer(attribute='document_type_id'),
        'urlDocument':fields.String(attribute='url_document'),
        'radicationDate':fields.Date(attribute='radication_date'),
        'documentId': fields.Integer(attribute='document_id')
    })

    documents_view = api.model('documents_view', {
        'radicateNumber': fields.String(attribute='radicate_number'),
        'documentTypeId': fields.Integer(attribute='document_type_id'),
        'urlDocument': fields.String(attribute='url_document'),
        'radicationDate': fields.Date(attribute='radication_date'),
        'documentId': fields.Integer(attribute='document_id'),
        'documentType': fields.String(attribute='document_type'),
        'expedientEntryDate': fields.Date(attribute='expedient_entry_date'),
        'originDocument': fields.String(attribute='origin_document'),
        'documentFormat': fields.Integer(attribute='document_format'),
        'documentFormatInitials': fields.String(attribute='document_format_initials'),
        'documentOrder': fields.Integer(attribute='document_order'),
        'initPage': fields.Integer(attribute='init_page'),
        'endPage': fields.Integer(attribute='end_page')
    })


class NewsDTO:
    api = ns
    news_filter = api.model('news_filter', {
        'id':fields.Integer(attribute='id'),
        'newsOrder':fields.Integer(attribute='news_order'),
        'newsType':fields.String(attribute='news_type'),
        'newsPubDate':fields.Date(attribute='news_pub_date'),
        'newsLocation':fields.String(attribute='news_location'),
        'newsText':fields.String(attribute='news_text')
    })

class DocumentNonConcurrencyDTO:
    api = ns

class RadicateFilerRequestDTO:
    api = ns
    radicate_filter = api.model('radicate_filter', {
        'claimerId':fields.String(attribute='claimer_id'),
        'documentType':fields.String(attribute='document_type'),
        'documentNumber':fields.String(attribute='radicate_number'),
        'firstName':fields.Date(attribute='first_name'),
        'middleName':fields.String(attribute='middle_name'),
        'lastName': fields.String(attribute='last_name'),
        'maidenName': fields.String(attribute='maiden_name'),
        'aaNumber': fields.String(attribute='aa_number'),
        'perPage': fields.String(attribute='per_page'),
        'page': fields.String(attribute='page')
    })

class RadicateDocumentDTO:
    api = ns
    radicate_documents = api.model('radicate_documents', {
        'documentId':fields.String(attribute='document_id'),
        'resourceType':fields.String(attribute='resource_type'),
        'radicateNumber':fields.String(attribute='radicate_number'),
        'radicationDate':fields.Date(attribute='radication_date'),
        'administrativeAct':fields.String(attribute='description'),
        'documentQuantity': fields.Integer(attribute='document_quantity')
    })

class ConclusiveDocumentDTO:
    api = ns
    conclusive_documents = api.model('conclusive_details', {
        'claimerid':fields.String(required=True, description='claimer_id'),
        'waiverOfTerms':fields.Boolean(required=False, description='waiverOfTerms')
    })

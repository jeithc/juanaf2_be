from datetime import datetime

from app.main import logger, db
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.claimer_expedient_state import ClaimerExpedientState
from sqlalchemy import text


def make_expedient_transition(claimer_id, next_state, annotation):
    logger.debug('Entro a workflow_service#make_claimer_transition')

    try:
        current_state = get_current_state(claimer_id)

        if not current_state:
            response_object = {
                'status': 'fail',
                'message': 'El expediente no está registrado en ningun estado.'
            }
            return response_object, 200

        if current_state.state == next_state:
            response_object = {
                'status': 'success',
                'message': 'El expediente ya esta en ese estado.'
            }
            return response_object, 200

        return advance_state(claimer_id, next_state, current_state, annotation)

    except Exception as e:
        return manage_db_exception("Error en workflow_service#advance_process - " +
                                   "Error al consultar el flujo de trabajo del solicitante",
                                   'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def get_current_state(claimer_id):
    logger.debug('Entro a expedient_state_service#get_current_state')

    try:
        expedient_state = ClaimerExpedientState.query \
            .filter(ClaimerExpedientState.claimer_id == claimer_id)\
            .filter(ClaimerExpedientState.end_date == None)\
            .first()

        if not expedient_state:
            return {}

        return expedient_state

    except Exception as e:
        return manage_db_exception("Error en expedient_state_service#get_current_state - " +
                                   "Error al consultar el flujo de trabajo del expediente",
                                   'Ocurrió un error al consultar el flujo de trabajo del expediente.', e)


def advance_state(claimer_id, next_state, current_state, annotation):
    logger.debug('Entro a expedient_state_service#advance_state')

    try:
        current_state.end_date = datetime.now()
    except Exception as e:
        return manage_db_exception("Error en expedient_state_service#advance_state - " +
                                   "Error al consultar el flujo de trabajo del claimer",
                                   'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)

    try:
        new_expedient_state = ClaimerExpedientState(
            claimer_id=claimer_id,
            state=next_state,
            start_date=datetime.now(),
            annotation=annotation,
            expedient_instance=current_state.expedient_instance,
            expedient_round=current_state.expedient_round
        )

        db.session.add(new_expedient_state)
        db.session.commit()

        response_object = {
            'status': 'success',
            'message': 'El expediente se avanzó exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en expedient_state_service#advance_state - " +
                                   "Error al crear el nuevo flujo de trabajo del solicitante",
                                   'Ocurrió un error al crear el nuevo flujo de trabajo del solicitante.', e)


def setExpedientInstance(claimer_id):
    try:
        query = text(
            'SELECT phase_2.f_set_expedient_instance('+str(claimer_id)+')')
        db.engine.execute(query.execution_options(autocommit=True)).fetchone()
    except Exception as e:
        print(e)


def sendGeomatics(claimer_id):
    try:
        query = text(
            'SELECT public.send_to_geomatic_process('+str(claimer_id)+')')
        result = db.engine.execute(
            query.execution_options(autocommit=True)).fetchone()

        return result[0], 200
    except Exception as e:
        print(e)

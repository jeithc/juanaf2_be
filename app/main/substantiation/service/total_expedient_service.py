import json
import datetime
from flask_restplus import marshal
from app.main import db
from datetime import date, datetime as dtime
from app.main.core.model.role import Role
from app.main.core.model.user_role import UserRole
from app.main.core.model.users import Users
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.core.model.state_procs import StateProcs
from app.main.core.util.dto.user_role_dto import UserRoleDto
from app.main.substantiation.service.expedient_state_service import make_expedient_transition
from sqlalchemy.sql import text
from app.main.core.service.workflow_service import make_claimer_transition, get_suspend_state, get_activate_state
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.service.domain_service import get_list_domain
from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from app.main.core.service import report_service



#PARAMETROS iniciales para filtrar

LIST_ROLES = {
    'name': ['aj_supervisor_produccion','aj_analista','aj_abogado_base'],
    'id': [8, 9, 12]
    }



def total_list(user_id, data):
    """verificar según usuario que rol tiene"""
    if data:
        #indice del rol de usuario para poder filtrar
        indice = type_user(user_id)
        array = [80, 81, 84, 95, 34, 35]
        if indice != 'error':
            users = data['users']
            if indice == 2:
                #si el rol a buscar es todos se dejan todos los estados
                array = [80, 81, 84, 95, 34, 35, 98, 110, 94, 108, 56, 58, 99, 109, 65, 61, 102, 103]
                if data['role'] == 9:
                    array = [80, 81, 84, 95, 34, 35]
                if data['role'] == 18:
                    array = [98, 110, 94, 108, 56, 58, 99]
                if data['role'] == 19:
                    array = [109, 65, 61, 102, 103]
            elif indice==1:
                users = [user_id]

            query = text("select names,string_agg(long_description,'-') as rol,total\
                from (\
                    select concat(u.names, ' ', u.surnames) as names , r.long_description, count(*) as total\
                    from (\
                    select a.*\
                    from (select p.*,\
                        row_number() over (partition by claimer_id, current_state order by end_date desc) as seq\
                        from(select * from phase_2.claimer_workflow cw\
                                where end_date is not null and start_date::date between :start_date and :end_date\
                                and current_state in :array) p\
                        ) a \
                    where seq = 1\
                    ) b\
                    inner join core.users u on u.id = b.user_id\
                    inner join core.user_role ur on ur.user_id = b.user_id \
                    inner join core.role r on r.role_id = ur.role_id\
                    where b.user_id in :users\
                    group by b.user_id, u.names, r.long_description,  u.surnames  \
                )f \
                group by names,total LIMIT :perPage OFFSET :page")
            filter_list = db.engine.execute(query, array = tuple(array) , users = tuple(users), start_date=data['startDate'], 
            end_date=data['endDate'], perPage=data['perPage'], page=(data['page'] - 1) * data['perPage'] ).fetchall()
            db.session.close()
            return filter_list
        else:
            return 'no data'

    else:
        return 'no data'


def count_total_list(user_id, data):
    """verificar según usuario que rol tiene"""
    if data:
        #indice del rol de usuario para poder filtrar
        indice = type_user(user_id)
        array = [80, 81, 84, 95, 34, 35]
        if indice != 'error':
            users = data['users']
            if indice == 2:
                #si el rol a buscar es todos se dejan todos los estados
                array = [80, 81, 84, 95, 34, 35, 98, 110, 94, 108, 56, 58, 99, 109, 65, 61, 102, 103]
                if data['role'] == 9:
                    array = [80, 81, 84, 95, 34, 35]
                if data['role'] == 18:
                    array = [98, 110, 94, 108, 56, 58, 99]
                if data['role'] == 19:
                    array = [109, 65, 61, 102, 103]
            elif indice==1:
                users = [user_id]


            query = text("select count(*) from (select names,string_agg(long_description,'-') as rol,total\
                from (\
                    select concat(u.names, ' ', u.surnames) as names , r.long_description, count(*) as total\
                    from (\
                    select a.*\
                    from (select p.*,\
                        row_number() over (partition by claimer_id, current_state order by end_date desc) as seq\
                        from(select * from phase_2.claimer_workflow cw\
                                where end_date is not null and start_date::date between :start_date and :end_date\
                                and current_state in :array) p\
                        ) a \
                    where seq = 1\
                    ) b\
                    inner join core.users u on u.id = b.user_id\
                    inner join core.user_role ur on ur.user_id = b.user_id \
                    inner join core.role r on r.role_id = ur.role_id\
                    where b.user_id in :users\
                    group by b.user_id, u.names, r.long_description,  u.surnames  \
                )f \
                group by names,total ) c")
            total = db.engine.execute(query, array = tuple(array) , users = tuple(users), start_date=data['startDate'], 
            end_date=data['endDate'], perPage=data['perPage'], page=(data['page'] - 1) * data['perPage'] ).fetchall()
            db.session.close()
            return total[0][0]
        else:
            return 'no data'

    else:
        return 'no data'


def type_user(user_id):
    """se saca el rol de cada usuario y se devuelve el indice de LIST_ROLES"""
    # role = devuelve los roles a asignar tareas
    role = Users.query\
        .join(UserRole)\
        .join(Role, UserRole.role_id == Role.role_id).filter(Role.short_description.in_(LIST_ROLES['name']), Users.id == user_id).first()
    if role:
        for role in marshal(role.roles, UserRoleDto.user_role):
            return LIST_ROLES['name'].index(role['shortDescription'])
    else:
        return 'error'


def generate_document(user_id, document_type, users, role, startDate, endDate):
    logger.debug('Entro a total_expedient_service#generate_document')
    document_id = 67
    #indice del rol de usuario para poder filtrar
    indice = type_user(user_id)
    string_state = "80, 81, 84, 95, 34, 35"
    if indice != 'error':
        users = users
        if indice == 2:
            #si el rol a buscar es todos se dejan todos los estados
            string_state = "80, 81, 84, 95, 34, 35, 98, 110, 94, 108, 56, 58, 99, 109, 65, 61, 102, 103"
            if role == '9':
                string_state = "80, 81, 84, 95, 34, 35"
            if role == '18':
                string_state = "98, 110, 94, 108, 56, 58, 99"
            if role == '19':
                string_state = "109, 65, 61, 102, 103"
        elif indice==1:
            users = user_id

        # generate report
        print(LIST_ROLES)
        params = {
            'param_start_date': startDate,
            'param_end_date': endDate,
            'param_users': users,
            'param_states': string_state,
            'param_rol': LIST_ROLES['name'][indice]
        }

        try:
            report = report_service.create_report(report_service.get_report_route(
                document_id), params, document_type)

        except Exception as e:
            return manage_db_exception(
                "Error en total_expedient_service#generate_document - Error al generar documento",
                'Ocurrió un error al guardar el recurso.', e)

        return report['report']
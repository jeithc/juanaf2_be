import json
import sys
from sqlalchemy import asc
from app.main import db
from app.main.substantiation.model.answer_to_questionnaire import AnswerToQuestionnaire as Answer
from app.main.substantiation.model.resource_proof_request import ResourceProofRequest
from app.main.substantiation.model.substantiation_questionaire import SubstantiationQuestionaire as Question
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.util.dto.questionnaire_dto import QuestionnaireDto
from flask_restplus import marshal
from app.main import logger


def get_questions(scope, workflow_id, section):

    try:
        questions = Question.query.filter(
            Question.scope == scope, Question.workflow_id == workflow_id,
            Question.substantiation_section == section)\
            .order_by(asc(Question.question_level)).all()

        if not questions:
            response_object = {
                'status': 'success',
                'message': 'No existen preguntas para estos criterios',
            }
            return response_object, 200

        return questions

    except Exception as e:
        return manage_db_exception("Error en questionaire_service #get_question - " +
                                   "Error al consultar las preguntas del cuestionario",
                                   'Ocurrió un error al consultar las preguntas del cuestionario.', e)


def save_answers(answers):

    try:
        save=[]
        list1 = None
        list2 = None
        error = True
        i_simple = [18, 17, 19]
        i_cualificada = [20, 21, 22, 738]
        for answer in answers:
            new_answer = Answer.init_from_dto(answer)
            if new_answer.res_proof_request_id:
                if next((x for x in save if x.s_questionnaire_id == new_answer.s_questionnaire_id), None) is None:
                    if verify_res_proof_request_id(new_answer):
                        save.append(new_answer)
            else:
                error = False
                db.session.add(new_answer)

        for answer2 in save:
            error = False
            answer_r = answer2.s_questionnaire_id
            if answer_r in i_simple:
                if list1:
                    try:
                        index = i_simple.index(answer_r)
                        if i_simple[index-1] == list1:
                            list1 = answer_r
                        else:
                            error = True
                    except:
                        error = True    
                else:
                    list1 = answer_r
            else:
                if list2:
                    try:
                        index = i_cualificada.index(answer_r)
                        if i_cualificada[index-1] == list2:
                            list2 = answer_r
                        else:
                            error = True
                    except:
                        error = True    
                else:
                    list2 = answer_r
            if error:
                break
            db.session.add(answer2)
            if answer2.answer == False:
                break
        if error:
            db.session.rollback()
            response_object = {
                'status': 'fail',
                'message': 'Error al guardar respuestas, favor vuelva a intentarlo',
            }
        else:
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'Respuestas almacenadas correctamente',
            }

        return response_object, 201

    except Exception as e:
        db.session.rollback()
        return manage_db_exception("Error en questionnaire_service #save_answers - " +
                                   "Error al guardar las respuestas del cuestionario",
                                   'Ocurrió un error al guardar las respuestas del cuestionario.', e)


def filter_questionnaire(filter, str):

    class_name = getattr(sys.modules[__name__], str)
    query = class_name.query

    try:

        for key, value in filter.items():
            query = query.filter(getattr(class_name, key) == value)

        return query

    except Exception as e:
        return manage_db_exception("Error en questionnaire_service #filter_questionnaire - " +
                                   "Error al guardar las respuestas del cuestionario",
                                   'Ocurrió un error al guardar las respuestas del cuestionario.', e)


def get_questionnaire(scope, workflow_id, section, params):

    data = {
        'scope': scope,
        'workflow_id': workflow_id
    }
    if 'ss_id' in params:
        data['ss_id'] = params['ss_id']
        del params['ss_id']

    if 'decision_attribute_1' in params:
        data['decision_attribute_1'] = params['decision_attribute_1']
        del params['decision_attribute_1']

    if section:
        data['substantiation_section'] = section

    questions = filter_questionnaire(data, 'Question').order_by(asc(Question.question_level)).all()
    answers = filter_questionnaire(params, 'Answer').all()

    try:
        if answers:
            for question in questions:
                x = [answer for answer in answers if answer.s_questionnaire_id == question.s_questionnaire_id]
                if x:
                    question.answer = x[0]

        return marshal(questions, QuestionnaireDto.questionnaire), 200

    except Exception as e:
        return manage_db_exception("Error en questionnaire_service #save_answers - " +
                                   "Error al guardar las respuestas del cuestionario",
                                   'Ocurrió un error al guardar las respuestas del cuestionario.', e)


def delete_answers(scope, workflow_id, section, params):

    data = {
        'scope': scope,
        'workflow_id': workflow_id,
    }
    if section:
        data['substantiation_section'] = section

    if 'ss_id' in params and params['ss_id']:
        data['ss_id'] = params['ss_id']
        del params['ss_id']

    try:
        answers = filter_questionnaire(params, 'Answer').all()
        questions = filter_questionnaire(data, 'Question').all()
        count = 0
        for question in questions:
            for answer in answers:
                if answer.s_questionnaire_id == question.s_questionnaire_id:
                    db.session.delete(answer)
                    count = count + 1
        db.session.commit()

        response_object = {
            'status': 'success',
            'message': str(count) + ' respuesta(s) eliminada(s) correctamente',
        }

        return response_object, 201

    except Exception as e:
        return manage_db_exception("Exrror en questionnaire_service #save_answers - " +
                                   "Error al guardar las respuestas del cuestionario",
                                   'Ocurrió un error al guardar las respuestas del cuestionario.', e)


def verify_resource_qnr(resource_id, user_id, auditor_id):

    if auditor_id == 'undefined':
        auditor_id = '-11111'
    query = 'select * from (' \
        ' select rd.res_doc_ss_id, rd.serie, rd.subserie, aq.user_id, count(aq.s_questionnaire_id),' \
        ' case when aq.user_id = ' + auditor_id + ' then true end audit, ' \
        ' case when aq.user_id = ' + user_id + ' then true end analist' \
        ' from phase_2.res_document_serie_subserie rd ' \
        ' inner  join phase_2.resource_attached_documents rad on rad.attached_document_id = rd.attached_document_id  ' \
        ' left join phase_2.answer_to_questionnaire aq on aq.res_doc_ss_id = rd.res_doc_ss_id ' \
        ' where rad.resource_id = '+resource_id + \
        ' group by rd.res_doc_ss_id, aq.user_id' \
        ' ) ans '
        #'where ans.user_id is null or ans.user_id = ' + user_id

    try:
        data = db.engine.execute(query).fetchall()
        db.session.close()
        if data:
            data = [dict(row) for (row) in data]
            return data, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'no found',
            }
            return response_object

    except Exception as e:
        return manage_db_exception("Error en questionnaire_service #verify_resource_qnr - " +
                                   "Error al consultar cuestionario",
                                   'Error al consultar cuestionario.', e)


def get_subserie_answer_by_resource_id(resource_id):
    try:

        query = ' select rdss.res_doc_ss_id, rdss.subserie, count(aq.answer_to_questionnaire_id)' \
                ' from phase_2.answer_to_questionnaire aq ' \
                ' right join phase_2.res_document_serie_subserie rdss on rdss.res_doc_ss_id = aq.res_doc_ss_id ' \
                ' left join phase_2.resource_attached_documents rad on rad.attached_document_id ' \
                '  = rdss.attached_document_id where rad.resource_id = ' + resource_id + ' group by rdss.res_doc_ss_id'

        try:
            data = db.engine.execute(query).fetchall()
            db.session.close()
        except Exception as e:
            return manage_db_exception('Error filtro', e)

        if data:
            data = [dict(row) for (row) in data]
            return data
        else:
            response_object = {
                'status': 'fail',
                'message': 'no found',
            }
            return response_object

    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_subserie_answer_by_resource_id - " +
                                   "Error al obtener los registros de subserie_answer_by_resource",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def get_first_round_qnr_user(document_id, second_round_user):
    logger.debug('Entro en get_first_round_qnr_user')

    try:
        query = 'select  user_id from phase_2.answer_to_questionnaire atq where res_doc_ss_id =' + second_round_user + \
                ' and user_id <> ' + document_id + ' order by answer_to_questionnaire_id desc limit 1'

        try:
            data = db.engine.execute(query).fetchall()
            db.session.close()
        except Exception as e:
            return manage_db_exception('Error filtro', e)

        if data:
            data = [dict(row) for (row) in data]
            return data[0]['user_id']
        else:
            response_object = {
              'status': 'fail',
              'message': 'no found',
            }
        return response_object

    except Exception as e:
        return manage_db_exception("Error en questinary_service#get_first_round_qnr_user - " +
                               "Error al obtener el usuario de la primer instancia.",
                               'Ocurrió un error al obtener el usuario de la primer instancia.', e)


def verify_res_proof_request_id(answer):
    try:
        # answer_first = Question.query.filter(
        #     Question.s_questionnaire_id==answer.s_questionnaire_id).first()
        resource_proof_request = ResourceProofRequest.query.filter(
            ResourceProofRequest.res_proof_request_id == answer.res_proof_request_id
        ).first()

        # answer_list = Answer.query.join(Question, Answer.s_questionnaire_id == Question.s_questionnaire_id).filter(
        #     Answer.res_proof_request_id == answer.res_proof_request_id,
        #     Question.substantiation_section == answer_first.substantiation_section
        #     ).all()
        # if len(answer_list) > 0:
        #     return False
        if answer.resource_id != resource_proof_request.resource_id:
            return False
        else:
            return True
    except Exception as e:
        return manage_db_exception("Error en questinary_service#verify_res_proof_request_id - " +
                               "Error al comprobar el res_proof_request_id.",
                               'Ocurrió un error al comprobar el res_proof_request_id..', e)
        

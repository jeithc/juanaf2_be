from flask_restplus import marshal
from sqlalchemy import text, false
from sqlalchemy.orm import Session

from app.main import logger, db
from app.main.core.service.general_service import manage_db_exception
from app.main.radication.model.resource_attached_documents import Resourceattachedocuments
from app.main.radication.service import radicates_table_service
from app.main.substantiation.model.res_document_serie_subserie import ResDocumentSerieSubserie
from app.main.substantiation.model.subserie_correction_request import SubserieCorrectionRequest
from app.main.substantiation.service import res_answer_per_document_service, subserie_correction_request_service
from app.main.substantiation.util.dto.res_document_serie_subserie_dto import ResDocumentSerieSubserieDto


def get_all_res_document_serie_subserie():
    logger.debug('Ingresa en get_all_res_document_serie_subserie')

    try:
        res_document_serie_subserie_group = ResDocumentSerieSubserie.query.all()

        return marshal(res_document_serie_subserie_group, ResDocumentSerieSubserieDto.res_document_serie_subserie), 200
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#get_all_res_document_serie_subserie - " +
                                   "Error al obtener los registros de res document serie subserie",
                                   'Ocurrió un error al obtener las res document serie subserie.', e)


def get_res_document_serie_subserie_by_resource_id(resourceId):
    logger.debug('Ingresa en get_res_document_serie_subserie_by_resource_id')

    try:
        res_document_serie_subserie_group = ResDocumentSerieSubserie.query.join(Resourceattachedocuments).filter(Resourceattachedocuments.resource_id == resourceId).all()

        return marshal(res_document_serie_subserie_group, ResDocumentSerieSubserieDto.res_document_serie_subserie), 200
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#get_res_document_serie_subserie_by_resource_id - " +
                                   "Error al obtener los registros de res document serie subserie by resourceId",
                                   'Ocurrió un error al obtener las res document serie subserie.', e)


def get_res_document_serie_subserie_by_attached_document_id(attachedDocumentId):
    logger.debug('Ingresa en get_res_document_serie_subserie_by_attached_document_id')

    try:
        res_document_serie_subserie_group = ResDocumentSerieSubserie.query.filter_by(attached_document_id=attachedDocumentId).all()

        return marshal(res_document_serie_subserie_group, ResDocumentSerieSubserieDto.res_document_serie_subserie), 200
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#get_res_document_serie_subserie_by_attached_document_id - " +
                                   "Error al obtener los registros de res document serie subserie by attachedDocumentId",
                                   'Ocurrió un error al obtener las res document serie subserie.', e)


def validate_number_of_pages_by_resource_id(resourceId):
    logger.debug('Ingresa en validate_number_of_pages_by_resource_id con resourceId: {}'.format(resourceId))

    t = text("SELECT crs.resource_state, ad.attached_document_id, cd.number_of_pages \
        FROM phase_2.claimer_resource_document rd \
        INNER JOIN phase_2.resource_attached_documents ad ON rd.resource_id = ad.resource_id \
        INNER JOIN phase_2.claimer_documents cd ON cd.document_id = ad.document_id \
        INNER JOIN phase_2.claimer_resource_state crs ON crs.resource_id = rd.resource_id \
        WHERE cd.juana_1_document_id IS NULL AND crs.end_date IS NULL AND rd.resource_id = :id")
    response_object = db.engine.execute(t, id=resourceId).fetchall()

    listAttachedDocuments = marshal(response_object, ResDocumentSerieSubserieDto.total_pages)
    validate = {
            'validate': "true"
        }

    for attachedDocument in listAttachedDocuments:
        str = attachedDocument['resource_state']
        denegado = str.find('DENEGADO')
        rechazado = str.find('RECHAZADO')

        if denegado == -1 and rechazado == -1:
            totalPages = attachedDocument['number_of_pages']
            listSubseries = list_subseries_by_attached_document_id(attachedDocument['attached_document_id'])
            pagesValidates = []

            for subserie in listSubseries:
                initPage = int(subserie['init_page'])
                lastPage = int(subserie['last_page']) + 1
                for page in range(initPage, lastPage):
                    if page not in pagesValidates:
                        pagesValidates.append(page)

            if len(pagesValidates) < totalPages:
                validate = {
                    'validate': "false"
                }
                break;

    db.session.close()

    return validate


def validate_number_of_pages_by_claimer_id(claimerId):
    logger.debug('Ingresa en validate_number_of_pages_by_claimer_id con claimerId: {}'.format(claimerId))

    validate = 'true'

    resources = radicates_table_service.get_resources(claimerId);

    for resource in resources:
        str = resource['resource_state']
        denegado = str.find('DENEGADO')
        rechazado = str.find('RECHAZADO')

        if denegado == -1 and rechazado == -1:
            validate = validate_number_of_pages_by_resource_id(resource['resource_id'])['validate'] == "true"
            if not validate:
                break

    response = {
        'validate': validate
    }

    return response


def list_subseries_by_attached_document_id(attachedDocumentId):
    logger.debug('Ingresa en list_subseries_by_attached_document_id con attachedDocumentId: {}'.format(attachedDocumentId))

    t = text("SELECT dss.res_doc_ss_id, dss.subserie, ip.answer1 AS init_page, lp.answer1 AS last_page \
        FROM phase_2.res_document_serie_subserie dss \
        INNER JOIN ( \
            SELECT apd.res_doc_ss_id, apd.answer1 \
             FROM phase_2.res_answer_per_document apd \
             INNER JOIN core.serie_subserie_metadata ssm ON ssm.ssm_id = apd.ssm_id \
             INNER JOIN core.serie_subserie_attributes ssa ON ssa.ss_attribute_id = ssm.ss_attribute_id \
             WHERE ssa.ss_attribute_id = 1) ip ON ip.res_doc_ss_id = dss.res_doc_ss_id \
        INNER JOIN ( \
            SELECT apd.res_doc_ss_id, apd.answer1 \
             FROM phase_2.res_answer_per_document apd \
             INNER JOIN core.serie_subserie_metadata ssm ON ssm.ssm_id = apd.ssm_id \
             INNER JOIN core.serie_subserie_attributes ssa ON ssa.ss_attribute_id = ssm.ss_attribute_id \
             WHERE ssa.ss_attribute_id = 2) lp ON lp.res_doc_ss_id = dss.res_doc_ss_id \
        WHERE dss.attached_document_id = :id \
        ORDER BY init_page")
    response_object = db.engine.execute(t, id=attachedDocumentId).fetchall()

    listSubseries = marshal(response_object, ResDocumentSerieSubserieDto.subserie)

    return listSubseries


def save_res_document_serie_subserie(data):
    print(data)
    logger.debug('Entro a res_document_serie_subserie_service#save_res_document_serie_subserie')
    res_document_serie_subserie = ResDocumentSerieSubserie(
        attached_document_id=data['attachedDocumentId'],
        serie=data['serie'],
        subserie=data['subserie'],
        user_id=data['userId']
    )

    try:
        db.session.add(res_document_serie_subserie)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'res_document_serie_subserie almacenado correctamente ',
            'resDocSsId': res_document_serie_subserie.res_doc_ss_id
        }
        return response_object, 201
    except Exception as e:
        logger.error('Error al guardar res_document_serie_subserie' + str(e))
        db.session.rollback()


def update_res_document_serie_subserie(data):
    logger.debug('Entro a res_document_serie_subserie_service#update_res_document_serie_subserie')

    try:
        res_document_serie_subserie = ResDocumentSerieSubserie.query.filter_by(res_doc_ss_id=data['resDocSsId']).first()
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#update_res_document_serie_subserie - " +
                                   "Error al consultar el res_document_serie_subserie",
                                   'Ocurrió un error al consultar el res_document_serie_subserie.', e)
    if not res_document_serie_subserie:
        response_object = {
            'status': 'fail',
            'message': 'El res_document_serie_subserie no existe. Por favor verifique el res_doc_ss_id.'
        }
        return response_object, 200

    if 'attachedDocumentId' in data and data['attachedDocumentId']:
        res_document_serie_subserie.attached_document_id = data['attachedDocumentId']
    if ('serie' in data and data['serie'] and data['serie'] != res_document_serie_subserie.serie) or ('subserie' in data and data['subserie'] and data['subserie'] != res_document_serie_subserie.subserie):
        res_answer_per_document_service.delete_res_answer_per_document(res_document_serie_subserie.res_doc_ss_id)
        res_document_serie_subserie.serie = data['serie']
        res_document_serie_subserie.subserie = data['subserie']

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'ResDocumentSerieSubserie actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#update_res_document_serie_subserie - " +
                                   "Error al actualizar el res_document_serie_subserie",
                                   'Ocurrió un error al actualizar el res_document_serie_subserie.', e)


def delete_res_document_serie_subserie(resDocSsId):
    logger.debug('Entro a res_document_serie_subserie_service#delete_res_document_serie_subserie')

    try:
        res_document_serie_subserie = ResDocumentSerieSubserie.query.filter(ResDocumentSerieSubserie.res_doc_ss_id == resDocSsId).first()
        if res_document_serie_subserie:
            resDocumentSerieSubserie = marshal(res_document_serie_subserie, ResDocumentSerieSubserieDto.res_document_serie_subserie)
            response_answers = res_answer_per_document_service.delete_res_answer_per_document(res_document_serie_subserie.res_doc_ss_id)
            response_correction = subserie_correction_request_service.delete_subserie_correction_request(res_document_serie_subserie.res_doc_ss_id)
            db.session.delete(res_document_serie_subserie)
            db.session.commit()
            response_object = [{'status': 'success', 'message': 'res_document_serie_subserie eliminado exitosamente.',
                                'resDocSsId': resDocumentSerieSubserie['resDocSsId'], 'attachedDocumentId': resDocumentSerieSubserie['attachedDocumentId'],
                                'serie': resDocumentSerieSubserie['serie'], 'subserie': resDocumentSerieSubserie['subserie']},
                                response_answers]
            return response_object, 200
        else:
            response_object = {
                'status': 'success',
                'message': 'El res_document_serie_subserie no existe, por favor verifique el id.'
            }
            return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#delete_res_document_serie_subserie - " +
                                   "Error al eliminar el res_document_serie_subserie",
                                   'Ocurrió un error al eliminar el res_document_serie_subserie.', e)


def get_res_document_serie_subserie_by_resource_id_with_correction_req(resourceId):
    logger.debug('Ingresa en get_res_document_serie_subserie_by_resource_id_with_correction_req')

    try:
        response_object = ResDocumentSerieSubserie.query\
            .join(Resourceattachedocuments) \
            .join(SubserieCorrectionRequest, ResDocumentSerieSubserie.res_doc_ss_id == SubserieCorrectionRequest.res_doc_ss_id) \
            .filter(Resourceattachedocuments.resource_id == resourceId).all()

        listSubseries = marshal(response_object, ResDocumentSerieSubserieDto.res_document_serie_subserie)

        return listSubseries, 200
    except Exception as e:
        return manage_db_exception("Error en res_document_serie_subserie_service#get_res_document_serie_subserie_by_resource_id_with_correction_req - " +
                                   "Error al obtener los registros de res document serie subserie by resourceId",
                                   'Ocurrió un error al obtener las res document serie subserie.', e)

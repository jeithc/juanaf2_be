from flask_restplus import marshal

from app.main import logger
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.document_section import DocumentSection
from app.main.substantiation.util.dto.document_section_dto import DocumentSectionDto


def get_document_section_by_claimer_acto(claimerId, actoId):
    logger.debug('Ingresa en get_document_section_by_claimer_acto')

    try:
        document_section_group = DocumentSection.query.filter(
            DocumentSection.claimer_id == claimerId, DocumentSection.acto_id == actoId).order_by(DocumentSection.id).all()

        return marshal(document_section_group, DocumentSectionDto.document_section), 200
    except Exception as e:
        return manage_db_exception("Error en document_section_service#get_document_section_by_claimer_acto - " +
                                   "Error al obtener los registros de document_section",
                                   'Ocurrió un error al obtener las document_section del claimer: ' + claimerId, e)


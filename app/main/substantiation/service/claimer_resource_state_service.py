from flask_restplus import marshal

from app.main import logger, db
from datetime import datetime
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.claimer_resource_state import ClaimerResourceState


def make_expedient_transition(data):
  logger.debug('Entro a claimer_resource_state_service#make_claimer_transition')

  resource_id = data['resourceId']
  next_state = data['nextState']

  try:
    current_state = get_current_state(resource_id)

    if not current_state:
      response_object = {
        'status': 'fail',
        'message': 'El expediente no está registrado en ningun estado.'
      }
      return response_object, 200

    if current_state.resource_state == next_state:
      response_object = {
        'status': 'success',
        'message': 'El expediente ya esta en ese estado.'
      }
      return response_object, 200

    return advance_state(resource_id, next_state, current_state.resource_state)

  except Exception as e:
    return manage_db_exception("Error en workflow_service#advance_process - " +
                               "Error al consultar el flujo de trabajo del solicitante",
                               'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)


def get_current_state(resource_id):
  logger.debug('Entro a claimer_resource_state_service#get_current_state')

  try:
    expedient_state = ClaimerResourceState.query \
      .filter(ClaimerResourceState.resource_id == resource_id).filter(ClaimerResourceState.end_date==None).first()

    if not expedient_state:
      return {}

    return expedient_state

  except Exception as e:
    return manage_db_exception("Error en expedient_state_service#get_current_state - " +
                               "Error al consultar el flujo de trabajo del expediente",
                               'Ocurrió un error al consultar el flujo de trabajo del expediente.', e)


def get_prev_state(resource_id):
    logger.debug('Entro a claimer_resource_state_service#get_current_state')

    try:
        expedient_state = ClaimerResourceState.query \
          .filter(ClaimerResourceState.resource_id == resource_id).filter(ClaimerResourceState.end_date != None).order_by(
                            ClaimerResourceState.resource_state_id.desc()).first()

        if not expedient_state:
            return {}

        return expedient_state

    except Exception as e:
        return manage_db_exception("Error en expedient_state_service#get_current_state - " +
                                 "Error al consultar el flujo de trabajo del expediente",
                                 'Ocurrió un error al consultar el flujo de trabajo del expediente.', e)


def advance_state(resource_id, next_state, current_state):
  logger.debug('Entro a claimer_resource_state_service#advance_state')

  try:
    expedient_state = ClaimerResourceState.query.filter(
      ClaimerResourceState.resource_id == resource_id).filter(ClaimerResourceState.resource_state == current_state).filter(
        ClaimerResourceState.end_date.is_(None)).first()

    expedient_state.end_date = datetime.now()
  except Exception as e:
    return manage_db_exception("Error en expedient_state_service#advance_state - " +
                               "Error al consultar el flujo de trabajo del claimer",
                               'Ocurrió un error al consultar el flujo de trabajo del solicitante.', e)

  try:
    new_expedient_state = ClaimerResourceState(
      resource_id=resource_id,
      resource_state=next_state,
      start_date=datetime.now()
    )

    db.session.add(new_expedient_state)
    db.session.commit()

    response_object = {
      'status': 'success',
      'message': 'El expediente se avanzó exitosamente.'
    }
    return response_object, 200
  except Exception as e:
    return manage_db_exception("Error en expedient_state_service#advance_state - " +
                               "Error al crear el nuevo flujo de trabajo del solicitante",
                               'Ocurrió un error al crear el nuevo flujo de trabajo del solicitante.', e)

import json
import datetime

from flask_restplus import marshal

from app.main import db
from datetime import date, datetime as dtime
from app.main.core.model.role import Role
from app.main.core.model.user_role import UserRole
from app.main.core.model.users import Users
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.core.model.state_procs import StateProcs
from app.main.core.util.dto.user_role_dto import UserRoleDto
from app.main.substantiation.service.expedient_state_service import make_expedient_transition
from sqlalchemy.sql import text
from app.main.core.service.workflow_service import make_claimer_transition, get_suspend_state, get_activate_state
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.service.domain_service import get_list_domain



#PARAMETROS iniciales para las bandejas
list_roles = {
    'name':['gd_gestor','gd_supervisor','aj_analista', 'aj_auditor','aj_jefe_auditor', 'aj_abogado_base','aj_supervisor_produccion'],
    'next_state':[27,42,30,45,64,39,52],
    'state_assign':[26,40,29,85,63,86,38],
    'find_state':[[27,28,31],[41,42,43], [30,34,35,80,81,84], [45,56,107,111,61], [65,109,64,117,103,71,114], [39,48,59,69], [52,53]] #asignados q se le cuenta en los totales
}

def user_task_assignament(user_id):
    #se busca el tipo de usuario
    users= type_user(user_id)
    if users == 'no role':
        return 'usuario no tiene rol necesario'
    else:
        # result=[]
        # for user in users:
        #     for role in marshal(user.roles, UserRoleDto.user_role):
        #         try:
        #             indice = list_roles['name'].index(role['shortDescription'])
        #             result_u=assing_expedients_user(user_id, indice)
        #             result.append(result_u)
        #         except Exception as e:
        #             print(e)
        populate_table(user_id)
        # return result
    return 'ok'





def type_user(user_id):
    # role = devuelve los roles a asignar tareas
    role = Users.query\
        .join(UserRole)\
        .join(Role, UserRole.role_id == Role.role_id).filter(Role.short_description.in_(list_roles['name']), Users.id == user_id ).all()
    if role:
        return role
    else:
        return 'no role'


def assing_expedients_user(user_id, indice):
    new_assigned=0
    #buscamos cuantos expedientes tiene asignados
    assigned= ClaimerWorkflow.query.filter(ClaimerWorkflow.user_id==user_id,ClaimerWorkflow.end_date == None)\
        .filter(ClaimerWorkflow.current_state.in_(list_roles['find_state'][indice])).count()
    assigned+=1
    status=verify_total(user_id, assigned, indice)
    if status['result'] =='ok':
        #se verifica si se puede asignar  los anteriores
        assigned, new_assigned= reasing_expedients(user_id, indice, assigned, status['total'])
        #buscamos lista de todos los expedientes para asignar
        # list_for_assign = db.session.query(func.phase_2.f_substantiation_expedient_distribution_1(data['state_assign'])).fetchall()
        t = text("select * FROM phase_2.f_substantiation_expedient_distribution_1(:workflow)")
        list_for_assign=db.engine.execute(t, workflow=list_roles['state_assign'][indice]).fetchall()
        db.session.close()
        if(list_for_assign):
            if assigned < (status['total']):
                #se asignan los expedientes
                for expedient in list_for_assign:
                    if (indice == 4 or indice == 3):
                        stateN = expedient.next_step
                    else:
                        stateN = list_roles['next_state'][indice]
                    data_transition = {
                        'claimerId' :expedient.claimer,
                        'nextState' :stateN,
                        'nextUser' :user_id,
                        'logUser' :1
                    }
                    result = make_claimer_transition(data_transition)
                    
                    try:
                        if result[0]['status'] == 'success':
                            assigned += 1
                            new_assigned += 1
                    except Exception as e:
                        print('error al hacer transicion de estado')
                    
                    if (assigned >= status['total']):
                        break
                    
            if new_assigned == 1:
                textassing='ha'
            else:
                textassing= 'han'
            return {'status':'ok', 'role':list_roles['name'][indice], 'message':'se {} asignado {} expediente(s) para su correspondiente sustanciación'.format(textassing,new_assigned)}
        return {'status':'ok', 'role':list_roles['name'][indice], 'message':'No hay expedientes para asignar'}
    else:
        return {'status':'ok', 'role':list_roles['name'][indice], 'message':status['result'] }


def verify_total(user_id,total,indice):
    t = text("select * FROM phase_2.f_substantiation_suspend_expedient_gesdoc_1(:user_id)")
    datalist=db.engine.execute(t, user_id=user_id).fetchall()
    db.session.close()
    for data in datalist:
        if data.rol == list_roles['name'][indice]:
            #si es el rol buscado hacemos las verificaciones
            if total < data.max_exp_gesdoc:
                if data.suspendrequest_gesdoc < data.max_exp_suspendrequest_gesdoc:
                    if data.temporalsuspend_gesdoc < data.max_exp_temporalsuspend_gesdoc:
                        if data.systemfailsuspend < data.max_exp_systemfailsuspend:
                            return {'total':data.max_exp_gesdoc,'result':'ok'}
                        else:
                            return {'total':data.max_exp_systemfailsuspend,'result':'La cantidad de expedientes suspendidos por problemas con la plataforma se encuentra al límite, no pueden ser asignados nuevos expedientes, por favor consulte con su supervisor.'}
                    else:
                        return {'total':data.max_exp_temporalsuspend_gesdoc,'result':'La cantidad de expedientes suspendidos temporalmente se encuentra al límite, no pueden ser asignados nuevos expedientes, debe finalizarlos para una nueva asignación'   }
                else:
                    return {'total':data.max_exp_suspendrequest_gesdoc,'result':'La cantidad de expedientes suspendidos por consulta se encuentra al límite, no pueden ser asignados nuevos expedientes, debe finalizarlos para una nueva asignación'}
            else:
                return {'total':data.max_exp_gesdoc,'result':'La cantidad de expedientes a ser asignados esta completa, por favor finalice los mismos'}

        print(datalist)
    return True


def automatic_relase_expedients():
    #para correr diario y quitar los usuarios que quedaron con reportes asignados sin gestionar
    list_expedients= ClaimerWorkflow.query.filter(ClaimerWorkflow.current_state.in_(list_roles['next_state']))\
        .filter(ClaimerWorkflow.claimer_id != None,   ClaimerWorkflow.end_date == None).all()
    total=0
    for expedient in list_expedients:
        data_transition = {
            'claimerId' :expedient.claimer_id,
            'nextState' :expedient.current_state,
            'nextUser' :0,
            'logUser' :1
            }
        make_claimer_transition(data_transition)
        total+=1
    return total



def reasing_expedients(user_id, indice, assigned, maxassign):
    new=0
    total= 0
    list_for_assign= ClaimerWorkflow.query.filter(ClaimerWorkflow.current_state==list_roles['next_state'][indice])\
    .filter(ClaimerWorkflow.claimer_id == 0,   ClaimerWorkflow.end_date == None).all()
    for expedient in list_for_assign:
        if assigned < maxassign:
            data_transition = {
                'claimerId' :expedient.claimer_id,
                'nextState' :list_roles['next_state'][indice],
                'nextUser' :user_id,
                'logUser' :1
            }
            make_claimer_transition(data_transition)
            assigned+=1
            new+=1
        else:
            break
    return assigned, new


def populate_table(user_id):
    update_query = text(' SELECT * FROM phase_2.f_populate_user_inbox(:user_id) ')
    db.engine.execute(update_query.execution_options(autocommit=True), user_id=user_id)
    return 'ok'


def reactivate_expedient(claimer_id, data):
    user_id = data['userId']   
    next_state = get_activate_state(claimer_id)
    #se busca el tipo de usuario
    users= type_user(user_id)
    result=[]
    user= users[0]
    response_object = {
        'status': 'fail',
        'message': 'Error en suspender expediente.'
    }
    for role in marshal(user.roles, UserRoleDto.user_role):
        try:
            indice = list_roles['name'].index(role['shortDescription'])
            #buscamos cuantos expedientes tiene asignados
            assigned= ClaimerWorkflow.query.filter(ClaimerWorkflow.user_id==user_id,ClaimerWorkflow.end_date == None)\
                .filter(ClaimerWorkflow.current_state.in_(list_roles['find_state'][indice])).count()
            assigned+=1
            #se verifica bandeja
            t = text("select * FROM phase_2.f_substantiation_suspend_expedient_gesdoc_1(:user_id)")
            datalist=db.engine.execute(t, user_id=user_id).fetchall()
            db.session.close()
            status = False
            for data in datalist:
                if data.rol == list_roles['name'][indice]:
                    #si es el rol buscado hacemos las verificaciones
                    if assigned <= data.max_exp_gesdoc:
                        status= True
                    else:
                        status= False
            if status:               
                data_transition = {
                    'claimerId': claimer_id,
                    'nextState': next_state,
                    'nextUser': user_id,
                    'logUser': user_id
                }

                try:
                    next_expedient_state = 'EN_PROCESO'
                    make_expedient_transition(claimer_id, next_expedient_state, None)
                    return make_claimer_transition(data_transition)
                except Exception as e:
                    response_object = {
                        'status': 'fail',
                        'message': 'Error en suspender expediente.'
                    }
                    return response_object
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Debe finalizar los expedientes que se encuentran en proceso para poder trabajar el expediente seleccionado'
                }
        except Exception as e:
            response_object = {
                'status': 'fail',
                'message': 'Error en suspender expediente.'
            }
    return response_object


def verify_suspended(claimer_id, dataquery):
    status= ClaimerWorkflow.query.filter(ClaimerWorkflow.claimer_id==claimer_id,ClaimerWorkflow.end_date == None).one()
    role=StateProcs.query.filter(StateProcs.node_id == status.current_state).one()
    print(role.role_id)
    if role.role_id:
        t = text("select * FROM phase_2.f_substantiation_suspend_expedient_gesdoc_1(:user_id)")
        datalist=db.engine.execute(t, user_id=dataquery['userId']).fetchall()
        db.session.close()
        for data in datalist:
            if dataquery['suspendType'] == 'SUSPENDIDO CONSULTA':
                if data.rol_id == role.role_id:
                    if data.suspendrequest_gesdoc < data.max_exp_suspendrequest_gesdoc:
                        return {'status':'success'}
                    else:
                        return {'status':'fail', 'message': 'El usuario tiene lleno el buzón de suspendidos por consulta.'}
            if dataquery['suspendType'] == 'SUSPENDIDO TEMPORAL':
                if data.rol_id == role.role_id:
                    if data.temporalsuspend_gesdoc < data.max_exp_temporalsuspend_gesdoc:
                        return {'status':'success'}
                    else:
                        return {'status':'fail', 'message': 'El usuario tiene lleno el buzón de suspendido temporal.'}
            if dataquery['suspendType'] == 'SUSPENDIDO FALLA':
                if data.rol_id == role.role_id:
                    if data.systemfailsuspend < data.max_exp_systemfailsuspend:
                        return {'status':'success'}
                    else:
                        return {'status':'fail', 'message': 'El usuario tiene lleno el buzón de suspendido por falla.'}
    else:
        return {'status':'fail', 'message': 'Estado no tiene rol asignado'}


def suspend_expedient(claimer_id, data):
    status = {'status':'',
        'message': ''
    }
    status['status']=''
    #validacion para suspender
    status=verify_suspended(claimer_id, data)
    if status['status']!='fail':
        user_id = data['userId']
        suspend_type = data['suspendType']
        suspend_obs = data['suspendObs']
        next_state = get_suspend_state(claimer_id, suspend_type)

        data_transition = {
            'claimerId': claimer_id,
            'nextState': next_state,
            'nextUser': user_id,
            'logUser': user_id
        }

        try:
            domains = get_list_domain('CLAIMER_EXPEDIENT_STATE')
            for d in domains:
                idx = d.value.find('SUSPENDIDO')
                if idx == -1:
                    continue
                suspend_type_d = d.value[idx:]

                if suspend_type_d == suspend_type:
                    next_expedient_state = d.code
                    break
            make_expedient_transition(claimer_id, next_expedient_state, suspend_obs)
            return make_claimer_transition(data_transition)
        except Exception as e:
            response_object = {
                'status': 'fail',
                'message': 'Error en suspender expediente.'
            }
    else:
        response_object = status
    return response_object


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='task_assignament_service')
        db.session.rollback()
        newLogger.error(e)

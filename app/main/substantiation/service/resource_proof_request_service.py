from flask_restplus import marshal
from sqlalchemy import text
from datetime import datetime

from app.main import logger, db
from app.main.core.service.general_service import manage_db_exception
from app.main.radication.model.claimer_resource_document import ClaimerResourceDocument
from app.main.substantiation.model.resolutive_numeral import ResolutiveNumeral
from app.main.substantiation.model.resource_proof_request import ResourceProofRequest
from app.main.substantiation.service import questionaire_service, expedient_state_service
from app.main.substantiation.util.dto.resource_proof_request_dto import ResourceProofRequestDto


def get_all_proof_aprobate_generate():
    logger.debug('Ingresa en get_all_resource_proof_request')

    try:
        resource_proof_request_group = ResourceProofRequest.query.all()

        return marshal(resource_proof_request_group, ResourceProofRequestDto.resource_proof_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_all_resource_proof_request - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def save_resource_proof_request(data):
    print(data)
    logger.debug(
        'Entro a res_answer_per_document_service#save_res_answer_per_document')
    resource_proof_request = ResourceProofRequest.init_from_dto(data)
    resource_proof_request.request_date = datetime.now()

    try:
        db.session.add(resource_proof_request)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'resource_proof_request almacenado correctamente ',
            'resProofRequestId': resource_proof_request.res_proof_request_id
        }
        return response_object, 201
    except Exception as e:
        logger.error('Error al guardar resource_proof_request' + str(e))
        db.session.rollback()


def update_resource_proof_request(data):
    logger.debug(
        'Entro a res_answer_per_document_service#resource_proof_request')

    try:
        resource_proof_request = ResourceProofRequest.query.filter_by(
            res_proof_request_id=data['resProofRequestId']).first()
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#update_resource_proof_request - " +
                                   "Error al consultar el resource_proof_request",
                                   'Ocurrió un error al consultar el resource_proof_request.', e)
    if not resource_proof_request:
        response_object = {
            'status': 'fail',
            'message': 'El resource_proof_request no existe. Por favor verifique el res_proof_request_id.'
        }
        return response_object, 200

    if 'requestProof' in data and data['requestProof']:
        resource_proof_request.request_proof = data['requestProof']
    if 'sourceOfProof' in data and data['sourceOfProof']:
        resource_proof_request.source_of_proof = data['sourceOfProof']
    if 'sourceOfProofDescription' in data and data['sourceOfProofDescription']:
        resource_proof_request.source_of_proof_description = data['sourceOfProofDescription']
    if 'autoProofDecision' in data and data['autoProofDecision']:
        resource_proof_request.auto_proof_decision = data['autoProofDecision']

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'ResourceProofRequest actualizado exitosamente.',
            'resProofRequestId': resource_proof_request.res_proof_request_id
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#update_resource_proof_request - " +
                                   "Error al actualizar el update_resource_proof_request",
                                   'Ocurrió un error al actualizar el update_resource_proof_request.', e)


def get_resource_proof_request_by_resource_id(resourceId):
    logger.debug('Ingresa en get_resource_proof_request_by_resource_id')

    try:
        resource_proof_request_group = ResourceProofRequest.query.filter(
            ResourceProofRequest.resource_id == resourceId).all()

        return marshal(resource_proof_request_group, ResourceProofRequestDto.resource_proof_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_resource_proof_request_by_resource_id - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def get_resource_proof_request_by_res_proof_request_id(resProofRequestId):
    logger.debug(
        'Ingresa en get_resource_proof_request_by_res_proof_request_id')

    try:
        resource_proof_request_group = ResourceProofRequest.query.filter(
            ResourceProofRequest.res_proof_request_id == resProofRequestId).first()

        return marshal(resource_proof_request_group, ResourceProofRequestDto.resource_proof_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_resource_proof_request_by_res_proof_request_id - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def delete_resource_proof_request_service(resProofRequestId):
    logger.debug(
        'Entro a resource_proof_request_service#delete_resource_proof_request_service')

    try:
        resource_proof_request = ResourceProofRequest.query.filter(
            ResourceProofRequest.res_proof_request_id == resProofRequestId).first()
        if resource_proof_request:
            resolutiveNumeralList = ResolutiveNumeral.query.filter(ResolutiveNumeral.res_proof_request_id == resProofRequestId).all();
            if resolutiveNumeralList:
                for resolutiveNumeral in resolutiveNumeralList:
                    db.session.delete(resolutiveNumeral)
            resourceProofRequest = marshal(
                resource_proof_request, ResourceProofRequestDto.resource_proof_request)
            params = {
                'resource_id': resourceProofRequest['resourceId'],
                'res_proof_request_id': resourceProofRequest['resProofRequestId']
            }
            db.session.delete(resource_proof_request)
            db.session.commit()
            response_object = [{'status': 'success', 'message': 'resource_proof_request eliminado exitosamente.',
                                'resProofRequest': resourceProofRequest}]
            return response_object, 200
        else:
            response_object = {
                'status': 'success',
                'message': 'El resource_proof_request no existe, por favor verifique el id.'
            }
            return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#delete_resource_proof_request_service - " +
                                   "Error al eliminar el resource_proof_request",
                                   'Ocurrió un error al eliminar el resource_proof_request.', e)


def get_resource_proof_request(resourceId, params):

    params['resource_id'] = resourceId

    query = ResourceProofRequest.query

    try:
        for key, value in params.items():
            query = query.filter(getattr(ResourceProofRequest, key) == value)

        resource_proof_request = query.order_by(ResourceProofRequest.request_proof).all()
        return marshal(resource_proof_request, ResourceProofRequestDto.resource_proof_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_resource_proof_request - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def validate_resource_proof_request(resProofRequestId):
    validate = 'false'
    resource_proof_request = ResourceProofRequest.query.filter(
        ResourceProofRequest.res_proof_request_id == resProofRequestId).first()

    try:
        if resource_proof_request:
            params = {
                'res_proof_request_id': resProofRequestId
            }
            answers = questionaire_service.get_questionnaire(
                'RECURSO', 84, 'IDONEIDAD SIMPLE', params)
            if answers:
                for answer in answers[0]:
                    if answer['answer'] and answer['answer']['answer'] is False:
                        validate = 'true'
                        break

            if validate == 'false':
                answers = questionaire_service.get_questionnaire(
                    'RECURSO', 84, 'IDONEIDAD CUALIFICADA', params)
                if answers:
                    for answer in answers[0]:
                        if answer['answer'] and answer['answer']['answer'] is False:
                            validate = 'true'
                            break

        response = {
            'validate': validate
        }

        return response
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#validate_resource_proof_request - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def validate_resource_proof_request_by_resource_id(resourceId):
    validate = 'false'

    try:
        resource = ClaimerResourceDocument.query.filter_by(
            resource_id=resourceId).first()

        expedient_state = expedient_state_service.get_current_state(resource.claimer_id)
        if expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'PRIMERA':
            round = 1
        elif expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'SEGUNDA':
            round = 2
        elif expedient_state.expedient_instance == 'UNICA':
            round = 1

        params = {'round_analysis': round}
        if round == 1 and resource.requires_proof:
            if resource.requires_proof == True:
                resource_proof_request_list = get_resource_proof_request(
                    resourceId, params)

                for resource_proof_request in resource_proof_request_list[0]:
                    validate = validate_resource_proof_request(
                        resource_proof_request['resProofRequestId'])['validate']
                    if not validate or validate == 'false':
                        break
            else:
                validate = 'true'
        elif round == 2 and resource.requires_proof_appeal:
            if resource.requires_proof_appeal == True:
                resource_proof_request_list = get_resource_proof_request(
                    resourceId, params)

                for resource_proof_request in resource_proof_request_list[0]:
                    validate = validate_resource_proof_request(
                        resource_proof_request['resProofRequestId'])['validate']
                    if not validate or validate == 'false':
                        break
            else:
                validate = 'true'


        response = {
            'validate': validate
        }

        return response
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#validate_resource_proof_request_by_resource_id - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def insert_resolutive_numeral(claimer_id):
    try:
        logger.debug(
            'Entro a resource_proof_request_service#insert_resolutive_numeral')
        update_query = 'SELECT phase_2.f_insert_resolutive_numeral(' + str(
            claimer_id) + ') '
        db.engine.execute(text(update_query).execution_options(autocommit=True))
        response_object = {
            'status': 'success',
            'message': 'Funcion de numerales ejecutada exitosamente!'
        }
        return response_object, 200
    except Exception as e:
        response_object = {
            'status': 'fail',
            'message': 'Fallo en función de numerales',
        }
        return response_object, 201


def get_proof_by_claimer(claimer_id):
    try:
        expedient_state = expedient_state_service.get_current_state(claimer_id)
        if expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'PRIMERA':
            round = 1
        elif expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'SEGUNDA':
            round = 2
        elif expedient_state.expedient_instance == 'UNICA':
            round = 1

        resources = ResourceProofRequest.query.join(
            ClaimerResourceDocument, ClaimerResourceDocument.resource_id == ResourceProofRequest.resource_id)\
            .filter(ClaimerResourceDocument.claimer_id == claimer_id and ResourceProofRequest.round_analysis == round).all()
        return marshal(resources, ResourceProofRequestDto.resource_proof_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_resource_proof_for_claimer - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)

from flask_restplus import marshal

from app.main import logger, db
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.resource_proof_request import ResourceProofRequest
from app.main.substantiation.util.dto.resource_proof_request_dto import ResourceProofRequestDto


def get_all_proof_aprobate_generate():
    logger.debug('Ingresa en get_all_resource_proof_request')

    try:
        resource_proof_request_group = ResourceProofRequest.query.all()

        return marshal(resource_proof_request_group, ResourceProofRequestDto.resource_proof_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_all_resource_proof_request - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def save_proof_aprobate_generate(data):
    print(data)
    logger.debug('Entro a res_answer_per_document_service#save_res_answer_per_document')
    resource_proof_request = ResourceProofRequest(
        resource_id=data['resourceId'],
        request_proof=data['requestProof'],
        user_id=data['userId']
    )

    try:
        db.session.add(resource_proof_request)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'resource_proof_request almacenado correctamente '
        }
        return response_object, 201
    except Exception as e:
        logger.error('Error al guardar resource_proof_request' + str(e))
        db.session.rollback()


def update_proof_aprobate_generate(data):
    logger.debug('Entro a res_answer_per_document_service#resource_proof_request')

    try:
        resource_proof_request = ResourceProofRequest.query.filter_by(res_proof_request_id=data['resProofRequestId']).first()
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#update_resource_proof_request - " +
                                   "Error al consultar el resource_proof_request",
                                   'Ocurrió un error al consultar el resource_proof_request.', e)
    if not resource_proof_request:
        response_object = {
            'status': 'fail',
            'message': 'El resource_proof_request no existe. Por favor verifique el res_proof_request_id.'
        }
        return response_object, 200

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'ResourceProofRequest actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#update_resource_proof_request - " +
                                   "Error al actualizar el update_resource_proof_request",
                                   'Ocurrió un error al actualizar el update_resource_proof_request.', e)


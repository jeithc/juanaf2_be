import datetime

from flask_restplus import marshal

from app.main import logger, db
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.res_document_serie_subserie import ResDocumentSerieSubserie
from app.main.substantiation.model.subserie_correction_request import SubserieCorrectionRequest
from app.main.substantiation.util.dto.subserie_correction_request_dto import SubserieCorrectionRequestDto
from app.main.radication.model.claimer_resource_document import ClaimerResourceDocument
from app.main.radication.model.resource_attached_documents import Resourceattachedocuments


def get_all_subserie_correction_request():
    logger.debug('Ingresa en get_all_subserie_correction_request')

    try:
        subserie_correction_request_list = SubserieCorrectionRequest.query.all()

        return marshal(subserie_correction_request_list, SubserieCorrectionRequestDto.subserie_correction_request), 200
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#get_all_subserie_correction_request - " +
                                   "Error al obtener los registros de subserie_correction_request",
                                   'Ocurrió un error al obtener las subserie_correction_request.', e)


def get_subserie_correction_request_by_res_doc_ss_id(resDocSsId):
    logger.debug('Ingresa en get_subserie_correction_request_by_res_doc_ss_id')

    try:
        subserie_correction_request_lsit = SubserieCorrectionRequest.query.join(
            ResDocumentSerieSubserie).filter(ResDocumentSerieSubserie.res_doc_ss_id == resDocSsId).all()

        return marshal(subserie_correction_request_lsit, SubserieCorrectionRequestDto.subserie_correction_request), 200
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#get_subserie_correction_request_by_res_doc_ss_id - " +
                                   "Error al obtener los registros de subserie_correction_request by resDocSsId",
                                   'Ocurrió un error al obtener las subserie_correction_request.', e)


def save_subserie_correction_request(data):
    print(data)
    logger.debug(
        'Entro a subserie_correction_request_service#save_subserie_correction_request')
    subserie_correction_request = SubserieCorrectionRequest(
        res_doc_ss_id=data['resDocSsId'],
        correction_reason=data['correctionReason'],
        correction_request_open=data['correctionRequestOpen'],
        user_placing_request=data['userPlacingRequest'],
        correction_request_date=datetime.datetime.now()
    )

    try:
        db.session.add(subserie_correction_request)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'subserie_correction_request almacenado correctamente ',
            'subserieCorrectionRequestId': subserie_correction_request.res_doc_ss_id
        }
        return response_object, 201
    except Exception as e:
        logger.error('Error al guardar subserie_correction_request' + str(e))
        db.session.rollback()


def update_subserie_correction_request(data):
    logger.debug(
        'Entro a subserie_correction_request_service#update_subserie_correction_request')

    try:
        subserie_correction_request = SubserieCorrectionRequest.query.filter_by(
            subserie_correction_request_id=data['subserieCorrectionRequestId']).first()
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#update_subserie_correction_request - " +
                                   "Error al consultar el subserie_correction_request",
                                   'Ocurrió un error al consultar el subserie_correction_request.', e)
    if not subserie_correction_request:
        response_object = {
            'status': 'fail',
            'message': 'El subserie_correction_request no existe. Por favor verifique el subserie_correction_request_id.'
        }
        return response_object, 200

    if 'correctionReason' in data and data['correctionReason']:
        subserie_correction_request.correction_reason = data['correctionReason']
    if 'correctionRequestDate' in data and data['correctionRequestDate']:
        subserie_correction_request.correction_request_date = data['correctionRequestDate']
    if 'userPlacingRequest' in data and data['userPlacingRequest']:
        subserie_correction_request.user_placing_request = data['userPlacingRequest']
    if 'correctionRequestOpen' in data and data['correctionRequestOpen']:
        subserie_correction_request.correction_request_open = data['correctionRequestOpen']

    try:
        db.session.add(subserie_correction_request)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'SubserieCorrectionRequest actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#update_subserie_correction_request - " +
                                   "Error al actualizar el subserie_correction_request",
                                   'Ocurrió un error al actualizar el subserie_correction_request.', e)


def close_subserie_correction_request(data):
    logger.debug(
        'Entro a subserie_correction_request_service#close_subserie_correction_request')

    try:
        subserie_correction_request = SubserieCorrectionRequest.query.filter_by(
            subserie_correction_request_id=data['subserieCorrectionRequestId']).first()
        subserie_correction_request.correction_close_request_date = datetime.datetime.now()
    except Exception as e:
        return manage_db_exception(
            "Error en subserie_correction_request_service#update_subserie_correction_request - " +
            "Error al consultar el subserie_correction_request",
            'Ocurrió un error al consultar el subserie_correction_request.', e)
    if not subserie_correction_request:
        response_object = {
            'status': 'fail',
            'message': 'El subserie_correction_request no existe. Por favor verifique el subserie_correction_request_id.'
        }
        return response_object, 200

    if 'correctionCloseReason' in data and data['correctionCloseReason']:
        subserie_correction_request.correction_close_reason = data['correctionCloseReason']
    if 'userClosesRequest' in data and data['userClosesRequest']:
        subserie_correction_request.user_closes_request = data['userClosesRequest']
    if 'resDocSsId' in data and data['resDocSsId']:
        subserie_correction_request.res_doc_ss_id = data['resDocSsId']
    subserie_correction_request.correction_close_request_date = datetime.datetime.now()
    subserie_correction_request.correction_request_open = False;

    try:
        db.session.add(subserie_correction_request)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'SubserieCorrectionRequest actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#update_subserie_correction_request - " +
                                   "Error al actualizar el subserie_correction_request",
                                   'Ocurrió un error al actualizar el subserie_correction_request.', e)


def delete_subserie_correction_request(resDocSsId):
    logger.debug(
        'Entro a subserie_correction_request_service#delete_subserie_correction_request')

    try:
        subserie_correction_request_list = SubserieCorrectionRequest.query.filter(
            SubserieCorrectionRequest.res_doc_ss_id == resDocSsId).all()
        if subserie_correction_request_list:
            for subserie_correction_request in subserie_correction_request_list:
                db.session.delete(subserie_correction_request)
        db.session.commit()
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#delete_subserie_correction_request - " +
                                   "Error al eliminar el subserie_correction_request",
                                   'Ocurrió un error al eliminar el subserie_correction_request.', e)


def get_subserie_correction_by_claimer_id(claimer_id):
    logger.debug('Ingresa en get_subserie_correction_by_claimer_id')

    try:
        response_object = SubserieCorrectionRequest.query.join(
            ResDocumentSerieSubserie, ResDocumentSerieSubserie.res_doc_ss_id == SubserieCorrectionRequest.res_doc_ss_id
        ).join(
            Resourceattachedocuments, Resourceattachedocuments.attached_document_id == ResDocumentSerieSubserie.attached_document_id
        ).join(
            ClaimerResourceDocument, ClaimerResourceDocument.resource_id == Resourceattachedocuments.resource_id
        ).filter(
            ClaimerResourceDocument.claimer_id == claimer_id).all()

        return marshal(response_object, SubserieCorrectionRequestDto.subserie_correction_request), 200
    except Exception as e:
        return manage_db_exception("Error en resource_proof_request_service#get_subserie_correction_by_claimer_id - " +
                                   "Error al obtener los registros de resource_proof_request",
                                   'Ocurrió un error al obtener las resource_proof_request.', e)


def denegate_all_subserie_correction_open_by_claimer_id(data):
    logger.debug('Ingresa en denegate_all_subserie_correction_open_by_claimer_id')

    try:
        claimer_id = data['claimerId']
        user_id = data['userId']
        list_correction_open = SubserieCorrectionRequest.query.join(
            ResDocumentSerieSubserie, ResDocumentSerieSubserie.res_doc_ss_id == SubserieCorrectionRequest.res_doc_ss_id
        ).join(
            Resourceattachedocuments, Resourceattachedocuments.attached_document_id == ResDocumentSerieSubserie.attached_document_id
        ).join(
            ClaimerResourceDocument, ClaimerResourceDocument.resource_id == Resourceattachedocuments.resource_id
        ).filter(
            ClaimerResourceDocument.claimer_id == claimer_id
        ).filter(
            SubserieCorrectionRequest.correction_request_open == True
        ).all()

        for correction_open in list_correction_open:
            subserie_correction_close = {
                'subserieCorrectionRequestId': correction_open.subserie_correction_request_id,
                'userClosesRequest': user_id,
                'correctionCloseReason': 'CORRECCION DENEGADA AUTOMATICO',
                'resDocSsId': correction_open.res_doc_ss_id
            }
            close_subserie_correction_request(subserie_correction_close)

        return 'Todas las solicitudes de corrección han quedado cerradas', 200
    except Exception as e:
        return manage_db_exception("Error en subserie_correction_request_service#denegate_all_subserie_correction_open_by_claimer_id - " +
                                   "Error al intentar cerrar las solicitudes de corrección de un recurso",
                                   'Ocurrió un error al intentar cerrar las solicitudes de corrección de un recurso.', e)


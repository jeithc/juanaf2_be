from flask_restplus import marshal

from app.main import logger, db
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.res_answer_per_document import ResAnswerPerDocument
from app.main.substantiation.util.dto.res_answer_per_document_dto import ResAnswerPerDocumentDto


def get_all_res_answer_per_document():
    logger.debug('Ingresa en get_all_res_answer_per_document')

    try:
        res_answer_per_document_group = ResAnswerPerDocument.query.all()

        return marshal(res_answer_per_document_group, ResAnswerPerDocumentDto.res_answer_per_document), 200
    except Exception as e:
        return manage_db_exception("Error en res_answer_per_document_service#get_all_res_answer_per_document - " +
                                   "Error al obtener los registros de res_answer_per_document",
                                   'Ocurrió un error al obtener las res_answer_per_document.', e)


def save_res_answer_per_document(data):
    print(data)
    logger.debug('Entro a res_answer_per_document_service#save_res_answer_per_document')
    res_answer_per_document = ResAnswerPerDocument(
        res_doc_ss_id=data['resDocSsId'],
        ssm_id=data['ssmId'],
        user_id=data['userId']
    )
    if 'answer1' in data and data['answer1']:
        res_answer_per_document.answer1 = data['answer1']
    if 'answer2' in data and data['answer2']:
        res_answer_per_document.answer2 = data['answer2']
    if 'answer3' in data and data['answer3']:
        res_answer_per_document.answer3 = data['answer3']
    if 'answer4' in data and data['answer4']:
        res_answer_per_document.answer4 = data['answer4']

    try:
        db.session.add(res_answer_per_document)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'res_answer_per_document almacenado correctamente '
        }
        return response_object, 201
    except Exception as e:
        logger.error('Error al guardar res_answer_per_document' + str(e))
        db.session.rollback()


def update_res_answer_per_document(data):
    logger.debug('Entro a res_answer_per_document_service#update_res_answer_per_document')

    try:
        res_answer_per_document = ResAnswerPerDocument.query.filter_by(res_doc_ss_id=data['resDocSsId'], ssm_id=data['ssmId']).first()
    except Exception as e:
        return manage_db_exception("Error en res_answer_per_document_service#update_res_answer_per_document - " +
                                   "Error al consultar el res_answer_per_document",
                                   'Ocurrió un error al consultar el res_answer_per_document.', e)
    if not res_answer_per_document:
        response_object = {
            'status': 'fail',
            'message': 'El res_answer_per_document no existe. Por favor verifique el res_doc_ss_id y el ssm_id.'
        }
        return response_object, 200

    if 'answer1' in data and data['answer1']:
        res_answer_per_document.answer1 = data['answer1']
    if 'answer2' in data and data['answer2']:
        res_answer_per_document.answer2 = data['answer2']
    if 'answer3' in data and data['answer3']:
        res_answer_per_document.answer3 = data['answer3']
    if 'answer4' in data and data['answer4']:
        res_answer_per_document.answer4 = data['answer4']

    try:
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'ResAnswerPerDocument actualizado exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en res_answer_per_document_service#update_res_answer_per_document - " +
                                   "Error al actualizar el res_answer_per_document",
                                   'Ocurrió un error al actualizar el res_answer_per_document.', e)

def get_res_answer_per_document_serie_subserie_by_res_document_serie_subserie_id(resDocSsId):
    logger.debug('Ingresa en get_res_answer_per_document_serie_subserie_by_res_document_serie_subserie_id')

    try:
        res_answer_per_document_group = ResAnswerPerDocument.query.filter_by(res_doc_ss_id=resDocSsId).all()

        return marshal(res_answer_per_document_group, ResAnswerPerDocumentDto.res_answer_per_document), 200
    except Exception as e:
        return manage_db_exception("Error en res_answer_per_document_service#get_res_answer_per_document_serie_subserie_by_res_document_serie_subserie_id - " +
                                   "Error al obtener los registros de res answer per document by resDocSsId",
                                   'Ocurrió un error al obtener las res answer per document.', e)


def delete_res_answer_per_document(resDocSsId):
    logger.debug('Entro a res_answer_per_document_service#delete_res_answer_per_document')

    try:
        res_answer_per_document_list = ResAnswerPerDocument.query.filter(ResAnswerPerDocument.res_doc_ss_id == resDocSsId).all()
        if res_answer_per_document_list:
            response = [{}]
            for res_answer_per_document in res_answer_per_document_list:
                resAnswerPerDocument = marshal(res_answer_per_document, ResAnswerPerDocumentDto.res_answer_per_document)
                db.session.delete(res_answer_per_document)
                response.append({'attributeId': resAnswerPerDocument['ssm']['ssAttribute']['ssAttributeId'],
                                 'attributeName': resAnswerPerDocument['ssm']['ssAttribute']['attributeName'],
                                 'answer1': resAnswerPerDocument['answer1'],
                                 'answer2': resAnswerPerDocument['answer2'],
                                 'answer3': resAnswerPerDocument['answer3'],
                                 'answer4': resAnswerPerDocument['answer4']})
            db.session.commit()
        else:
            response = {
                'status': 'success',
                'message': 'El res_document_serie_subserie no existe, por favor verifique el id.'
            }
        return response
    except Exception as e:
        return manage_db_exception("Error en res_answer_per_document_service#res_answer_per_document - " +
                                   "Error al eliminar el res_answer_per_document",
                                   'Ocurrió un error al eliminar el res_answer_per_document.', e)

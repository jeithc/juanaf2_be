from flask_restplus import marshal

from app.main import logger, db
from app.main.substantiation.model.claimer_auto_proof_document import ClaimerAutoProofDocument
from sqlalchemy import text
from app.main.core.service.general_service import manage_db_exception
from app.main.substantiation.model.res_auto_proof_document import ResAutoProofDocument
from app.main.substantiation.model.resolutive_numeral import ResolutiveNumeral
from app.main.radication.model.claimer_resource_document import ClaimerResourceDocument
from app.main.substantiation.service import expedient_state_service
from app.main.substantiation.util.dto.res_auto_proof_document_dto import ResAutoProofDocumentDto
from app.main.substantiation.util.dto.resolutive_numeral_dto import ResolutiveNumeralDto
from app.main.substantiation.util.dto.approve_auto_proof_dto import ApproveAutoProofDto
from app.main.substantiation.model.resource_proof_request import ResourceProofRequest
from app.main.documental.model.request import Request
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.model.claimer_documents import Claimer_documents
from datetime import datetime, date
from app.main.core.service import report_service
from app.main.core.service import file_service
from app.main.core.electronic_signature.service.signature_service import Signature
from app.main.core.service.app_params_service import get_app_params
from app.main.core.model.user_role import UserRole
from app.main.core.model.users import Users
from app.main.core.util.dto.user_role_dto import UserRoleDto
from app.main.core.model.role import Role
from threading import Thread


def type_user(user_id):
    """se saca el rol de cada usuario y se devuelve el indice de LIST_ROLES"""
    LIST_ROLES = {'name': ['dp_director_dnraj','dp_vicedefensor_dp']}
    # role = devuelve los roles a asignar tareas
    role = Users.query\
        .join(UserRole)\
        .join(Role, UserRole.role_id == Role.role_id).filter(Role.short_description.in_(LIST_ROLES['name']), Users.id == user_id).first()
    if role:
        for role in marshal(role.roles, UserRoleDto.user_role):
            return LIST_ROLES['name'].index(role['shortDescription'])
    else:
        return 'error'

def get_auto_proof_by_claimer(claimer_id, round_analysis):
    logger.debug('Ingresa en get_auto_proof_by_claimer')

    try:
        autos_by_claimer = ResAutoProofDocument.query.join(
            ClaimerResourceDocument, ClaimerResourceDocument.resource_id == ResAutoProofDocument.resource_id
        ).filter(ClaimerResourceDocument.claimer_id == claimer_id).filter(
            ResAutoProofDocument.round_analysis == round_analysis).all()

        return marshal(autos_by_claimer, ResAutoProofDocumentDto.res_auto_proof_document), 200
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document#get_auto_proof_by_claimer - " +
                                   "Error al obtener los registros de res_auto_proof_document",
                                   'Ocurrió un error al obtener las res_auto_proof_document.', e)


def save_auto_proof(data):
    logger.debug('Entro a res_auto_proof_document#save_auto_proof')
    auto_proof = ResAutoProofDocument.init_from_dto(data)

    try:
        db.session.add(auto_proof)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'res_auto_proof_document almacenado correctamente ',
            'resProofRequestId': auto_proof.res_apd_id
        }
        return response_object, 201
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document#get_auto_proof_by_claimer - " +
                                   "Error al obtener los registros de res_auto_proof_document",
                                   'Ocurrió un error al obtener las res_auto_proof_document.', e)


def save_auto_proof_list(expedient_round, claimer_id, autos_proofs):
    print(autos_proofs)

    try:
        for auto in autos_proofs:
            auto_proof = ResAutoProofDocument.init_from_dto(auto)
            document_id = auto_proof.document_id

        claimer_auto = ClaimerAutoProofDocument()
        claimer_auto.auto_proof_document_id = document_id
        claimer_auto.expedient_round = expedient_round
        claimer_auto.claimer_id = claimer_id
        db.session.add(claimer_auto)
        db.session.commit()
        db.session.refresh(claimer_auto)

        for auto in autos_proofs:
            auto_proof = ResAutoProofDocument.init_from_dto(auto)
            auto_proof.auto_proof_id = claimer_auto.auto_proof_id
            db.session.add(auto_proof)

        db.session.commit()

        response_object = {
            'status': 'success',
            'message': 'res_auto_proof_document almacenado correctamente ',
            'resProofRequestId': auto_proof.res_apd_id
        }
        return response_object, 201
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document#get_auto_proof_by_claimer - " +
                                   "Error al obtener los registros de res_auto_proof_document",
                                   'Ocurrió un error al obtener las res_auto_proof_document.', e)


def getmaxRoundResAuto(resource_id):
    try:
        max_round = db.session.query(
            db.func.max(ResAutoProofDocument.round_analysis)) \
            .filter(ResAutoProofDocument.resource_id == resource_id)\
            .first()
        response_object = {
            'maxRound': max_round[0]
        }
        return response_object
    except Exception as e:
        print(e)


def saveResolutivesNumerals(data):

    resolutive_numeral = ResolutiveNumeral.init_from_dto(data)
    try:
        db.session.add(resolutive_numeral)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'resolutive numeral almacenado correctamente ',
            'resolutiveNumeralId': resolutive_numeral.resolutive_numeral_id
        }
        return response_object
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'error al guardar numeral resolutivo ',
        }
        return response_object


def updateResolutiveNumeral(data):
    try:
        rsNumeral = ResolutiveNumeral.query.filter(
            ResolutiveNumeral.resolutive_numeral_id == data['resolutiveNumeralId']).first()
        if not rsNumeral:
            response_object = {
                'status': 'fail',
                'message': 'Numeral Resolutivo no existe'
            }
        else:
            rsNumeral.resolutive_numeral_object = data['article']
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'resolutive numeral actualizado'
            }
    except Exception as e:
        print(e)


def getResolutiveNumerals(claimerId):
    try:
        expedient_state = expedient_state_service.get_current_state(claimerId)
        if expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'PRIMERA':
            round = 1
        elif expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'SEGUNDA':
            round = 2
        elif expedient_state.expedient_instance == 'UNICA':
            round = 1
        resolutivesNumerals = ResolutiveNumeral.query.join(ResourceProofRequest).filter(
            ResourceProofRequest.round_analysis == round).filter(ResolutiveNumeral.claimer_id == claimerId)\
            .order_by(ResolutiveNumeral.resolutive_numeral_old.asc()).all()
        if len(resolutivesNumerals) == 0:
            response_object = {
                'status': 'success',
                'message': 'No existen numerales resolutivos para ese claimer'
            }
            return response_object
        return marshal(resolutivesNumerals, ResolutiveNumeralDto.resolutive_numeral), 200
    except Exception as e:
        print(e)


def deleteResolutiveNumeral(resolutive_numeral_id):
    try:
        resolutiveNumeral = ResolutiveNumeral.query.filter(
            ResolutiveNumeral.resolutive_numeral_id == resolutive_numeral_id
        ).first()
        if not resolutiveNumeral:
            response_object = {
                'status': 'success',
                'message': 'El numeral resolutivo no existe.'
            }
        else:
            db.session.delete(resolutiveNumeral)
            db.session.commit()
            response_object = {
                'status': 'success',
                'message': 'Numeral resolutivo eliminado exitosamente.'
            }
            return response_object, 200
    except Exception as e:
        print(e)


def generate_auto_proof(claimer, document_type_id, user_id):
    try:
        expedient_state = expedient_state_service.get_current_state(claimer['claimerId'])
        if expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'PRIMERA':
            round = 1
        elif expedient_state.expedient_instance == 'DOBLE' and expedient_state.expedient_round == 'SEGUNDA':
            round = 2
        elif expedient_state.expedient_instance == 'UNICA':
            round = 1

        data = {
            'claimerId': claimer['claimerId'],
            'documentTypeId': document_type_id,
        }

        document = Claimer_documents(
            claimer_id=claimer['claimerId'],
            flow_document=2,
            number_of_pages=1,
            origin_document='SISTEMA',
            document_state=False,
            document_type_id=document_type_id)

        save_changes(document)
        db.session.refresh(document)

        params = {
            'param_claimer_id': claimer['claimerId'],
            'param_round_analysis': round
        }

        report = report_service.create_report(
            report_service.get_report_route(document.document_type_id), params, 'docx')

        request = Request()
        request.claimer_id = claimer['claimerId']
        request.document_id = document.document_id
        request.request_document_name = report['file_name']
        request.user_id = user_id
        request.request_type = 'AUTO DE PRUEBAS'
        request.request_claimer_date = datetime.now()

        save_changes(request)

        response = file_service.save_report_file(report, document.document_id)
        #TODO verificar si se puede subir documento al crearlo en este punto


        response_object = {
            'document_id': document.document_id,
            'url_document': response[0]['url_document']
        }
        return response_object
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document_service#generate_auto_proof - " +
                                   "Error al generar reporte res_auto_proof",
                                   'Error al generar reporte res_auto_proof', e)



def approve_auto_proof(data):
    if type_user(data['userId']) == 1:
        resource_type= """ and (
            (crd.resource_type = 'APELACION') or
            (crd.resource_type = 'SUBSIDIO APELACION' and ces.expedient_round = 'SEGUNDA')) """
    else:
        resource_type= """ and (
            (crd.resource_type = 'REPOSICION') or
            (crd.resource_type = 'SUBSIDIO APELACION' and ces.expedient_round = 'PRIMERA') ) """

    query_filter = ''

    try:

        if 'documentNumber' in data and data['documentNumber']:
            query_filter += " AND document_number = '{}' ".format(
                data['documentNumber'])
        if 'documentType' in data and data['documentType']:
            query_filter += " AND document_type = upper('{}') ".format(
                data['documentType'])
        if 'fullName' in data and data['fullName']:
            query_filter += " AND full_name like upper('%{}%') ".format(
                data['fullName'])

        query = text("""select  crd.resource_type,cud.document_number,capd.auto_proof_id ,cud.claimer_id,cud.document_type,
        cud.full_name, capd.expedient_round ,cw.current_state 
        from phase_2.claimer_auto_proof_document capd
        inner join phase_2.res_auto_proof_document rapd  on capd.auto_proof_id = rapd.auto_proof_id
        inner join phase_2.claimer_resource_document crd on rapd.resource_id = crd.resource_id
        inner join phase_2.claimer_user_data cud on crd.claimer_id = cud.claimer_id
        inner join phase_2.claimer_expedient_state ces on crd.claimer_id = ces.claimer_id and ces.end_date is null
        inner join phase_2.claimer_workflow cw on cw.claimer_id  = capd.claimer_id and cw.end_date is null 
        where cw.current_state = 123 and capd.auto_proof_document_id_dp is null    {} {}
        group by crd.resource_type,cud.document_number,capd.auto_proof_id,cud.claimer_id,cud.document_type,cud.full_name,cw.current_state LIMIT :perPage OFFSET :page""".format(resource_type, query_filter))
        autos_proofs = db.engine.execute(
            query, perPage=data['perPage'], page=(data['page'] - 1) * data['perPage']).fetchall()
        db.session.close()
        return marshal(autos_proofs, ApproveAutoProofDto.approve_auto_proof), 200
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document_service#approve_auto_proof - " +
                                   "Error al generar reporte approve_auto_proof",
                                   'Error al generar reporte approve_auto_proof', e)


def approve_auto_proof_count(data):
    if type_user(data['userId']) == 1:
        resource_type= """ and (
            (crd.resource_type = 'APELACION') or
            (crd.resource_type = 'SUBSIDIO APELACION' and ces.expedient_round = 'SEGUNDA')) """
    else:
        resource_type= """ and (
            (crd.resource_type = 'REPOSICION') or
            (crd.resource_type = 'SUBSIDIO APELACION' and ces.expedient_round = 'PRIMERA') ) """

    query_filter = ''

    try:

        if 'documentNumber' in data and data['documentNumber']:
            query_filter += " AND cud.document_number = '{}' ".format(
                data['documentNumber'])
        if 'documentType' in data and data['documentType']:
            query_filter += " AND cud.document_type = upper('{}') ".format(
                data['documentType'])
        if 'fullName' in data and data['fullName']:
            query_filter += " AND cud.full_name like upper('%{}%') ".format(
                data['fullName'])

        query = text("""select count(*)
            from (  select  crd.resource_type,cud.document_number,capd.auto_proof_id ,cud.claimer_id,cud.document_type,
        cud.full_name, capd.expedient_round ,cw.current_state 
        from phase_2.claimer_auto_proof_document capd
        inner join phase_2.res_auto_proof_document rapd  on capd.auto_proof_id = rapd.auto_proof_id
        inner join phase_2.claimer_resource_document crd on rapd.resource_id = crd.resource_id
        inner join phase_2.claimer_user_data cud on crd.claimer_id = cud.claimer_id
        inner join phase_2.claimer_expedient_state ces on crd.claimer_id = ces.claimer_id and ces.end_date is null
        inner join phase_2.claimer_workflow cw on cw.claimer_id  = capd.claimer_id and cw.end_date is null 
        where cw.current_state = 123 and capd.auto_proof_document_id_dp is null  {} {}
        group by crd.resource_type,cud.document_number,capd.auto_proof_id,cud.claimer_id,cud.document_type,cud.full_name,cw.current_state )f""".format(resource_type, query_filter))

        autos_proofs = db.engine.execute(query).first()
        db.session.close()
        return autos_proofs[0]
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document_service#approve_auto_proof - " +
                                   "Error al generar reporte approve_auto_proof",
                                   'Error al generar reporte approve_auto_proof', e)


def firm_auto_proof(autos_proof):
    # """proceso para firmar depende del tipo de recurso"""
    # print(autos_proof)
    count_vice=0
    array_vice=[]
    user_vice=get_app_params('userVice')[0]
    count_dnraj=0
    array_dnraj=[]
    user_dnraj=get_app_params('userDNRAJ')[0]
    try:
        for auto in autos_proof:
            document= Claimer_documents.query.join(
            Request, Request.document_id==Claimer_documents.document_id).filter(
            Request.claimer_id==auto['claimerId']).order_by(Request.request_id.desc()).first()
            if document:
                if auto['resourceType']!='APELACION':
                    array_dnraj.append({
                        'route': document.url_document,
                        'document_id': document.document_id,
                        'aa_id': auto['resApdId']
                    })
                    count_dnraj+=1
                else:
                    array_vice.append({
                        'route': document.url_document,
                        'document_id': document.document_id,
                        'aa_id': auto['resApdId']
                    })
                    count_vice+=1
                if count_dnraj >= 50:
                    signature = Signature(array_dnraj, user_dnraj.value, 1)
                    thr = Thread(target=signature.upload_file())
                    thr.start()
                    count_dnraj=0
                    array_dnraj=[]
                if count_vice >= 50:
                    signature = Signature(array_vice, user_vice.value, 1)
                    thr = Thread(target=signature.upload_file())
                    thr.start()
                    count_vice=0
                    array_vice=[]
            else:
                return {
                    "status": "ok",
                    "message": " se están enviando los documentos para firmar , pronto recibirá un correo con los link para firmar"
                }

        if count_dnraj > 0:
            signature = Signature(array_dnraj, user_dnraj.value, 1)
            thr = Thread(target=signature.upload_file())
            thr.start()
            count_dnraj=0
            array_dnraj=[]
        if count_vice > 0:
            signature = Signature(array_vice, user_vice.value, 1)
            thr = Thread(target=signature.upload_file())
            thr.start()
            count_vice=0
            array_vice=[]
        thr = Thread(target=signature.send_mail_to_sign())
        # thr.start()
        return {
            "status": "ok",
            "message": " se están enviando los documentos para firmar , pronto recibirá un correo con los link para firmar"
        }
    except Exception as e:
        return manage_db_exception("Error en res_auto_proof_document_service#firm_auto_proof - " +
                                   "Error al generar documentos",
                                   'Error al generar reporte firm_auto_proof', e)



def save_changes(data):
    db.session.add(data)
    try:

        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='claimer notification service')
        db.session.rollback()
        newLogger.error(e)

from sqlalchemy.orm import relationship

from app.main import db


class SubserieCorrectionRequest(db.Model):
    """SubserieCorrectionRequest model"""
    __tablename__ = 'subserie_correction_request'
    __table_args__ = {'schema': 'phase_2'}

    subserie_correction_request_id = db.Column(db.Integer, primary_key=True);
    res_doc_ss_id = db.Column(db.Integer, db.ForeignKey('phase_2.res_document_serie_subserie.res_doc_ss_id'))
    res_doc_ss = relationship('ResDocumentSerieSubserie')
    correction_reason = db.Column(db.String(1000))
    correction_request_date = db.Column(db.Date)
    correction_request_open = db.Column(db.Boolean)
    user_placing_request = db.Column(db.Integer)
    user_closes_request = db.Column(db.Integer)
    correction_close_request_date = db.Column(db.Date)
    correction_close_reason = db.Column(db.String(1000))

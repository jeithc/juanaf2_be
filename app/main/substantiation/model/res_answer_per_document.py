from sqlalchemy.orm import relationship

from app.main import db


class ResAnswerPerDocument(db.Model):
    """ResAnswerPerDocument model"""
    __tablename__ = 'res_answer_per_document'
    __table_args__ = {'schema': 'phase_2'}

    res_doc_ss_id = db.Column(db.Integer, db.ForeignKey('phase_2.res_document_serie_subserie.res_doc_ss_id'), primary_key=True)
    res_doc_ss = relationship('ResDocumentSerieSubserie')
    ssm_id = db.Column(db.Integer, db.ForeignKey('core.serie_subserie_metadata.ssm_id'), primary_key=True)
    ssm = relationship('SerieSubserieMetadata')
    answer1 = db.Column(db.String)
    answer2 = db.Column(db.String)
    answer3 = db.Column(db.String)
    answer4 = db.Column(db.String)
    user_id = db.Column(db.Integer)

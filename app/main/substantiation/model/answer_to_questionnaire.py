from app.main import db


class AnswerToQuestionnaire(db.Model):
    """ answer_to_questionnaire model """
    __tablename__ = 'answer_to_questionnaire'
    __table_args__ = {'schema': 'phase_2'}

    answer_to_questionnaire_id = db.Column(db.Integer, primary_key=True)
    s_questionnaire_id = db.Column(db.Integer)
    claimer_id = db.Column(db.Integer)
    resource_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    answer = db.Column(db.Boolean)
    answer_date = db.Column(db.Date)
    res_doc_ss_id = db.Column(db.Integer)
    res_proof_request_id = db.Column(db.Integer)

    @staticmethod
    def init_from_dto(data):
        answer = AnswerToQuestionnaire()

        if 'sQuestionnaireId' in data and data['sQuestionnaireId']:
            answer.s_questionnaire_id = data['sQuestionnaireId']
        if 'claimerId' in data and data['claimerId']:
            answer.claimer_id = data['claimerId']
        if 'userId' in data and data['userId']:
            answer.user_id = data['userId']
        if 'resourceId' in data and data['resourceId']:
            answer.resource_id = data['resourceId']
        if 'answer' in data and data['sQuestionnaireId']:
            answer.answer = data['answer']
        if 'answerDate' in data and data['answerDate']:
            answer.answerDate = data['answerDate']
        if 'resDocSsId' in data and data['resDocSsId']:
            answer.res_doc_ss_id = data['resDocSsId']
        if 'resProofRequestId' in data and data['resProofRequestId']:
            answer.res_proof_request_id = data['resProofRequestId']
        return answer

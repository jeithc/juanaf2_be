from sqlalchemy.orm import relationship

from app.main import db


class ResourceProofRequest(db.Model):
    """ resource_proof_request model """
    __tablename__ = 'resource_proof_request'
    __table_args__ = {'schema': 'phase_2'}

    res_proof_request_id = db.Column(db.Integer, primary_key=True)
    resource_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.claimer_resource_document.resource_id'))
    resource = relationship('ClaimerResourceDocument')
    request_proof = db.Column(db.String(1024))
    request_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)
    source_of_proof = db.Column(db.String(100))
    source_of_proof_description = db.Column(db.String(256))
    round_analysis = db.Column(db.Integer)
    auto_proof_decision = db.Column(db.String(100))

    @staticmethod
    def init_from_dto(data):
        proofRequest = ResourceProofRequest()

        if 'resProofRequestId' in data and data['resProofRequestId']:
            proofRequest.res_proof_request_id = data['resProofRequestId']
        if 'resourceId' in data and data['resourceId']:
            proofRequest.resource_id = data['resourceId']
        if 'requestProof' in data and data['requestProof']:
            proofRequest.request_proof = data['requestProof']
        if 'userId' in data and data['userId']:
            proofRequest.user_id = data['userId']
        if 'sourceOfProof' in data and data['sourceOfProof']:
            proofRequest.source_of_proof = data['sourceOfProof']
        if 'sourceOfProofDescription' in data and data['sourceOfProofDescription']:
            proofRequest.source_of_proof_description = data['sourceOfProofDescription']
        if 'roundAnalysis' in data and data['roundAnalysis']:
            proofRequest.round_analysis = data['roundAnalysis']
        return proofRequest

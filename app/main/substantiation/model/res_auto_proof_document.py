from sqlalchemy.orm import relationship

from app.main import db
import datetime


class ResAutoProofDocument(db.Model):
    """ res_auto_proof_document model """
    __tablename__ = 'res_auto_proof_document'
    __table_args__ = {'schema': 'phase_2'}

    res_apd_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    resource_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.claimer_resource_document.resource_id'))
    document_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.claimer_documents.document_id'), primary_key=True)
    res_apd_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer)
    observation = db.Column(db.String(1024))
    round_analysis = db.Column(db.Integer)
    sign_auto_proof_document_user_id = db.Column(db.Integer)
    sign_auto_proof_document_date = db.Column(db.DateTime)
    rapd_id_dp = db.Column(db.String(100))
    auto_proof_id = db.Column(db.Integer)

    @staticmethod
    def init_from_dto(data):
        res_auto_proof_document = ResAutoProofDocument()
        res_auto_proof_document.res_apd_date = datetime.datetime.now()
        if 'resApdId' in data and data['resApdId']:
            res_auto_proof_document.res_apd_id = data['resApdId']
        if 'resourceId' in data and data['resourceId']:
            res_auto_proof_document.resource_id = data['resourceId']
        if 'documentId' in data and data['documentId']:
            res_auto_proof_document.document_id = data['documentId']
        if 'resApdDate' in data and data['resApdDate']:
            res_auto_proof_document.res_apd_date = data['resApdDate']
        if 'userId' in data and data['userId']:
            res_auto_proof_document.user_id = data['userId']
        if 'observation' in data and data['observation']:
            res_auto_proof_document.observation = data['observation']
        if 'roundAnalysis' in data and data['roundAnalysis']:
            res_auto_proof_document.round_analysis = data['roundAnalysis']

        return res_auto_proof_document

from app.main import db


class ClaimerExpedientState(db.Model):
    """ClaimerExpedientState model"""
    __tablename__ = 'claimer_expedient_state'
    __table_args__ = {'schema': 'phase_2'}

    ces_id = db.Column(db.Integer, primary_key=True)
    claimer_id = db.Column(db.Integer)
    state = db.Column(db.String)
    start_date = db.Column(db.Integer)
    end_date = db.Column(db.Integer)
    annotation = db.Column(db.String)
    expedient_instance = db.Column(db.String)
    expedient_round = db.Column(db.String)


def __repr__(self):
    return "<ClaimerExpedientState '{}'>".format(self.ces_id)

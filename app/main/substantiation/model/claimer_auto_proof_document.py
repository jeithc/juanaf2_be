from app.main import db

class ClaimerAutoProofDocument(db.Model):
    """ThirdPerson model"""
    __tablename__ = 'claimer_auto_proof_document'
    __table_args__ = {'schema': 'phase_2'}

    auto_proof_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    auto_proof_document_id = db.Column(db.Integer)
    auto_proof_document_id_dp = db.Column(db.String(100))
    expedient_round = db.Column(db.String(100))
    claimer_id = db.Column(db.String(100))

    def __repr__(self):
        return "<ThirdPerson '{}'>".format(self.auto_proof_id)

    @staticmethod
    def init_from_dto(data):
        third_person = ClaimerAutoProofDocument()
        return ClaimerAutoProofDocument.fill_from_data(data, third_person)

    @staticmethod
    def fill_from_data(data, claimer_auto):
        if 'autoProofId' in data and data['autoProofId']:
            claimer_auto.auto_proof_id = data['autoProofId']
        if 'autoProofDocumentId' in data and data['autoProofDocumentId']:
            claimer_auto.auto_proof_document_id = data['autoProofDocumentId']
        if 'autoProofDocumentIdDp' in data and data['autoProofDocumentIdDp']:
            claimer_auto.auto_proof_document_id_dp = data['autoProofDocumentIdDp']
        if 'expedientRound' in data and data['expedientRound']:
            claimer_auto.expedient_round = data['expedientRound']
        if 'claimerId' in data and data['claimerId']:
            claimer_auto.claimer_id = data['claimerId']

        return claimer_auto

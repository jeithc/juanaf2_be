from app.main import db
from sqlalchemy import Table, Column, Integer


class SubstantiationQuestionaire(db.Model):
    """ questions model """
    __tablename__ = 'substantiation_questionaire'
    __table_args__ = {'schema': 'phase_2'}

    s_questionnaire_id = db.Column(db.Integer, primary_key=True)
    workflow_id = db.Column(db.Integer)
    substantiation_section = db.Column(db.String(100))
    first_question = db.Column(db.Boolean)
    decision_attribute_1 = db.Column(db.String(100))
    decision_attribute_2 = db.Column(db.String(100))
    next_question_id_true = db.Column(db.Integer)
    next_question_id_false = db.Column(db.Integer)
    scope = db.Column(db.String(100))
    question = db.Column(db.String(1024))
    question_level = db.Column(db.Integer)
    ss_id = db.Column(db.Integer)


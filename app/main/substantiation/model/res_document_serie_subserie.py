from sqlalchemy.orm import relationship

from app.main import db


class ResDocumentSerieSubserie(db.Model):
    """ResDocumentSerieSubserie model"""
    __tablename__ = 'res_document_serie_subserie'
    __table_args__ = {'schema': 'phase_2'}

    res_doc_ss_id = db.Column(db.Integer, primary_key=True)
    attached_document_id = db.Column(db.Integer, db.ForeignKey('phase_2.resource_attached_documents.attached_document_id'))
    attached_document = relationship('Resourceattachedocuments')
    serie = db.Column(db.String(4))
    subserie = db.Column(db.String(15))
    user_id = db.Column(db.Integer)

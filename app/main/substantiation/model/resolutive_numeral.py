from sqlalchemy.orm import relationship

from app.main import db


class ResolutiveNumeral(db.Model):
    """ resolutiveNumeral model """
    __tablename__ = 'resolutive_numeral'
    __table_args__ = {'schema': 'phase_2'}

    resolutive_numeral_id = db.Column(
        db.Integer, primary_key=True, autoincrement=True)
    res_proof_request_id = db.Column(db.Integer, db.ForeignKey(
        'phase_2.resource_proof_request.res_proof_request_id'))
    res_proof_request = relationship("ResourceProofRequest")
    claimer_id = db.Column(db.Integer)
    resource_id = db.Column(db.Integer)
    res_auto_pd = db.Column(db.Integer)
    resolutive_numeral_object = db.Column(db.String)
    resolutive_numeral_date = db.Column(db.Date)
    resolutive_numeral_old = db.Column(db.Boolean)

    @staticmethod
    def init_from_dto(data):
        resolutive_numeral = ResolutiveNumeral()

        if 'resolutiveNumeralId' in data and data['resolutiveNumeralId']:
            resolutive_numeral.resolutive_numeral_id = data['resolutiveNumeralId']
        if 'claimerId' in data and data['claimerId']:
            resolutive_numeral.claimer_id = data['claimerId']
        if 'resourceId' in data and data['resourceId']:
            resolutive_numeral.resource_id = data['resourceId']
        if 'resAutoPd' in data and data['resAutoPd']:
            resolutive_numeral.res_auto_pd = data['resAutoPd']
        if 'resolutiveNumeralObject' in data and data['resolutiveNumeralObject']:
            resolutive_numeral.resolutive_numeral_object = data['resolutiveNumeralObject']
        if 'resProofRequest' in data and data['resProofRequest']:
            resolutive_numeral.res_proof_request = data['resProofRequest']
        if 'resProofRequestId' in data and data['resProofRequestId']:
            resolutive_numeral.res_proof_request_id = data['resProofRequestId']
        if 'resolutiveNumeralDate' in data and data['resolutiveNumeralDate']:
            resolutive_numeral.resolutive_numeral_date = data['resolutiveNumeralDate']
        return resolutive_numeral

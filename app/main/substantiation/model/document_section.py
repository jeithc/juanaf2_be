from app.main import db


class DocumentSection(db.Model):
    """ document_section model """
    __tablename__ = 'document_section'
    __table_args__ = {'schema': 'final_act'}

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    final_text = db.Column(db.String)
    claimer_id = db.Column(db.Integer)
    acto_id = db.Column(db.Integer)

    @staticmethod
    def init_from_dto(data):
        documentSection = DocumentSection()

        if 'id' in data and data['id']:
            documentSection.id = data['id']
        if 'title' in data and data['title']:
            documentSection.title = data['title']
        if 'finalText' in data and data['finalText']:
            documentSection.final_text = data['finalText']
        if 'claimerId' in data and data['claimerId']:
            documentSection.claimer_id = data['claimerId']
        if 'actoId' in data and data['actoId']:
            documentSection.acto_id = data['actoId']
        return documentSection

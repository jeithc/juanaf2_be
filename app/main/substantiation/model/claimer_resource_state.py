from sqlalchemy.orm import relationship
from app.main import db


class ClaimerResourceState(db.Model):
    """ClaimerResourceState model"""
    __tablename__ = 'claimer_resource_state'
    __table_args__ = {'schema': 'phase_2'}

    resource_state_id = db.Column(db.Integer, primary_key=True)
    resource_id = db.Column(db.Integer)
    resource_state = db.Column(db.Integer)
    start_date = db.Column(db.Integer)
    end_date = db.Column(db.Integer)
    
def __repr__(self):
        return "<ClaimerResourceState '{}'>".format(self.resource_id)
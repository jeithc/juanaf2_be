from flask_restplus import Namespace, fields
from app.main.util.dto import DocumentDTO
from app.main.radication.util.dto.radicates_table_dto import RadicatesDTO


class ApproveAutoProofDto:
    api = Namespace(
        'res_auto_proof_document', description='operaciones relacionadas con la tabla res_auto_proof_document')

    approve_auto_proof = api.model('res_auto_proof_document', {
        'roundAnalysis': fields.String(attribute='round_analysis'),
        'resApdId': fields.String(attribute='auto_proof_id'),
        'resourceType': fields.String(attribute='resource_type'),
        'documentType': fields.String(attribute='document_type'),
        'claimerId': fields.String(attribute='claimer_id'),
        'documentNumber': fields.String(attribute='document_number'),
        'fullName': fields.String(attribute='full_name'),
    })

from flask_restplus import Namespace, fields


class TotalExpedientsDTO:
    api = Namespace('substantiation', description='operaciones con sustanciación de documentos')

    total = api.model('res_auto_proof_document', {
        'names': fields.String(attribute='names'),
        'rol': fields.String(attribute='rol'),
        'total': fields.String(attribute='total')
    })

from flask_restplus import Namespace, fields

from app.main.core.util.dto.serie_subserie_metadata_dto import SerieSubserieMetadataDto
from app.main.substantiation.util.dto.res_document_serie_subserie_dto import ResDocumentSerieSubserieDto


class ResAnswerPerDocumentDto:
    api = Namespace('res_answer_per_document', description='operaciones relacionadas con la tabla res_answer_per_document')

    res_answer_per_document = api.model('res_answer_per_document', {
        'resDocSsId': fields.Integer(attribute='res_doc_ss_id'),
        'resDocSs': fields.Nested(ResDocumentSerieSubserieDto.res_document_serie_subserie, attribute='res_doc_ss'),
        'ssmId': fields.Integer(attribute='ssm_id'),
        'ssm': fields.Nested(SerieSubserieMetadataDto.serie_subserie_metadata, attribute='ssm'),
        'answer1': fields.String(attribute='answer1'),
        'answer2': fields.String(attribute='answer2'),
        'answer3': fields.String(attribute='answer3'),
        'answer4': fields.String(attribute='answer4'),
        'userId': fields.String(attribute='user_id')
    })

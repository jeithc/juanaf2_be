from flask_restplus import Namespace, fields


class ExpedientStateDto:

    api = Namespace('substantiation',
                    description='Operaciones relacionadas con expedient_state')

    expedient_state = api.model('', {
        'cesId': fields.Integer(attribute='ces_id'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'state': fields.String(),
        'annotation': fields.String(),
        'expedientInstance': fields.String(attribute='expedient_instance'),
        'expedientRound': fields.String(attribute='expedient_round'),
    })

from flask_restplus import Namespace, fields

from app.main.radication.util.dto.resource_documents_dto import ResourceDTO


class ResourceProofRequestDto:
    api = Namespace(
        'resource_proof_request', description='operaciones relacionadas con la tabla resource_proof_request')

    resource_proof_request = api.model('', {
        'resProofRequestId': fields.Integer(attribute='res_proof_request_id'),
        'resourceId': fields.Integer(attribute='resource_id'),
        'resource': fields.Nested(ResourceDTO.attachment),
        'requestProof': fields.String(attribute='request_proof'),
        'requestDate': fields.DateTime(attribute='request_date'),
        'userId': fields.Integer(attribute='user_id'),
        'sourceOfProof': fields.String(attribute='source_of_proof'),
        'autoProofDecision': fields.String(attribute='auto_proof_decision'),
        'sourceOfProofDescription': fields.String(attribute='source_of_proof_description'),
        'roundAnalysis': fields.Integer(attribute='round_analysis')
    })

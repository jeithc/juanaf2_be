from flask_restplus import Namespace, fields


class DocumentSectionDto:
    api = Namespace(
        'document_section', description='operaciones relacionadas con la tabla final_act.document_section')

    document_section = api.model('', {
        'id': fields.Integer(attribute='id'),
        'title': fields.String(attribute='title'),
        'finalText': fields.String(attribute='final_text'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'actoId': fields.Integer(attribute='acto_id')
    })

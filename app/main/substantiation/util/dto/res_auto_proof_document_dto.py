from flask_restplus import Namespace, fields
from app.main.util.dto import DocumentDTO


class ResAutoProofDocumentDto:
    api = Namespace(
        'res_auto_proof_document', description='operaciones relacionadas con la tabla res_auto_proof_document')

    res_auto_proof_document = api.model('', {
        'resApdId': fields.Integer(attribute='res_apd_id'),
        'resourceId': fields.Integer(attribute='resource_id'),
        'documentId': fields.Integer(attribute='document_id'),
        'document': fields.Nested(DocumentDTO.documents, attribute='document',  allow_null=True),
        'resApdDate': fields.String(attribute='res_apd_date'),
        'userId': fields.Integer(attribute='user_id'),
        'observation': fields.String(attribute='observation'),
        'roundAnalysis': fields.String(attribute='round_analysis')
    })


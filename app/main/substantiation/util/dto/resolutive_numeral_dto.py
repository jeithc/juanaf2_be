from flask_restplus import Namespace, fields
from app.main.substantiation.util.dto.resource_proof_request_dto import ResourceProofRequestDto


class ResolutiveNumeralDto:
    api = Namespace(
        'resolutive_numeral', description='operaciones relacionadas con la tabla resolutive_numeral')

    resolutive_numeral = api.model('resolutive_numeral', {
        'claimerId': fields.Integer(attribute='claimer_id'),
        'resourceId': fields.Integer(attribute='resource_id'),
        'resAutoPd': fields.Integer(attribute='res_auto_pd'),
        'resolutiveNumeralObject': fields.String(attribute='resolutive_numeral_object'),
        'resolutiveNumeralDate': fields.Date(attribute='resolutive_numeral_date'),
        'resProofRequest': fields.Nested(ResourceProofRequestDto.resource_proof_request, attribute='res_proof_request'),
        'resolutiveNumeralId': fields.Integer(attribute='resolutive_numeral_id'),
        'resolutiveNumeralOld': fields.Boolean(attribute='resolutive_numeral_old'),
    })

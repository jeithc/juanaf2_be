from flask_restplus import Namespace, fields


class QuestionnaireDto:
    api = Namespace(
        'questionnaire', description='operaciones preguntas del cuestionario')

    question = api.model('', {
        'sQuestionnaireId': fields.Integer(attribute='s_questionnaire_id'),
        'workflowId': fields.Integer(attribute='workflow_id'),
        'substantiationSection': fields.String(attribute='substantiation_section'),
        'firstQuestion': fields.Boolean(attribute='first_question'),
        'decisionAttribute1': fields.String(attribute='decision_attribute_1'),
        'decisionAttribute2': fields.String(attribute='decision_attribute_2'),
        'nextQuestionIdTrue': fields.Integer(attribute='next_question_id_true'),
        'nextQuestionIdFalse': fields.Integer(attribute='next_question_id_false'),
        'scope': fields.String(),
        'question': fields.String(),
        'questionLevel': fields.Integer(attribute='question_level')

    })

    answer = api.model('answer_to_questionnaire', {
        'answerToQuestionnaireId': fields.Integer(attribute='answer_to_questionnaire_id'),
        'sQuestionnaireId': fields.Integer(attribute='s_questionnaire_id'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'resourceId': fields.Integer(attribute='resource_id'),
        'answer': fields.Boolean(),
        'userId': fields.Integer(attribute='user_id'),
        'answerDate': fields.Date(attribute='answer_date'),
        'resDocSsId': fields.Integer(attribute='res_doc_ss_id'),
        'resProofRequestId': fields.Integer(attribute='res_proof_request_id')
    })

    questionnaire = api.model('substantiation_questionaire', {
        'sQuestionnaireId': fields.Integer(attribute='s_questionnaire_id'),
        'workflowId': fields.Integer(attribute='workflow_id'),
        'substantiationSection': fields.String(attribute='substantiation_section'),
        'firstQuestion': fields.Boolean(attribute='first_question'),
        'decisionAttribute1': fields.String(attribute='decision_attribute_1'),
        'decisionAttribute2': fields.String(attribute='decision_attribute_2'),
        'nextQuestionIdTrue': fields.Integer(attribute='next_question_id_true'),
        'nextQuestionIdFalse': fields.Integer(attribute='next_question_id_false'),
        'scope': fields.String(),
        'question': fields.String(),
        'questionLevel': fields.Integer(attribute='question_level'),
        'answer': fields.Nested(answer, description='respuesta a la pregunta', allow_null=True),
    })

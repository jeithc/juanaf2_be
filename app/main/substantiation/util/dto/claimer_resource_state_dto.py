from flask_restplus import Namespace, fields
from app.main.radication.util.dto.resource_documents_dto import ResourceDTO
from app.main.core.util.dto.document_dto import DocumentDTO


class ClaimerResourceStateDTO:
    api = Namespace('substantiation', description='Operaciones relacionadas con recursos de sustanciacion')


    claimer_resource_state = api.model('resource', {
        'resourceStateId': fields.Integer (attribute='resource_state_id'),
        'resourceId': fields.Integer(attribute='resource_id'),
        'resourceState': fields.String(attribute='resource_state'),
        'startDate': fields.Date(attribute='start_date'),
        'endDate': fields.Date(attribute='end_date')
    })
from flask_restplus import Namespace, fields

from app.main.substantiation.util.dto.res_document_serie_subserie_dto import ResDocumentSerieSubserieDto


class SubserieCorrectionRequestDto:
    api = Namespace('subserie_correction_request', description='operaciones relacionadas con la tabla subserie_correction_request')
    subserie_correction_request = api.model('subserie_correction_request', {
        'subserieCorrectionRequestId': fields.Integer(attribute='subserie_correction_request_id'),
        'resDocSsId': fields.Integer(attribute='res_doc_ss_id'),
        'resDocSs': fields.Nested(ResDocumentSerieSubserieDto.res_document_serie_subserie, attribute='res_doc_ss'),
        'correctionReason': fields.String(attribute='correction_reason'),
        'correctionRequestDate': fields.DateTime(attribute='correction_request_date'),
        'correctionRequestOpen': fields.Boolean(attribute='correction_request_open'),
        'userPlacingRequest': fields.Integer(attribute='user_placing_request'),
        'userClosesRequest': fields.Integer(attribute='user_closes_request'),
        'correctionCloseRequestDate': fields.DateTime(attribute='correction_close_request_date'),
        'correctionCloseReason': fields.String(attribute='correction_close_reason')
    })
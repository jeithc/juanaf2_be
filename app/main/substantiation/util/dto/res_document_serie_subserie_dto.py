from flask_restplus import Namespace, fields

from app.main.radication.util.dto.resource_documents_dto import ResourceDTO


class ResDocumentSerieSubserieDto:
    api = Namespace('res_document_serie_subserie', description='operaciones relacionadas con la tabla res_document_serie_subserie')
    res_document_serie_subserie = api.model('res_document_serie_subserie', {
        'resDocSsId': fields.Integer(attribute='res_doc_ss_id'),
        'attachedDocumentId': fields.Integer(attribute='attached_document_id'),
        'attachedDocument': fields.Nested(ResourceDTO.attachment, attribute='attached_document'),
        'serie': fields.String(attribute='serie'),
        'subserie': fields.String(attribute='subserie'),
        'userId': fields.String(attribute='user_id')
    })

    total_pages = api.model('total_pages', {
        'resource_state': fields.String(),
        'attached_document_id': fields.Integer(),
        'number_of_pages': fields.Integer()
    })

    subserie = api.model('subserie', {
        'res_doc_ss_id': fields.Integer(),
        'subserie': fields.String(),
        'init_page': fields.String(),
        'last_page': fields.String()
    })

from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import res_answer_per_document_service
from app.main.substantiation.util.dto.res_answer_per_document_dto import ResAnswerPerDocumentDto
from app.main.util.decorator import audit_init, audit_finish

api = ResAnswerPerDocumentDto.api


@api.route('/res_answer_per_document')
class ResAnswerPerDocumentList(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list res_answer_per_document')
    def get(self):
        """Get list ResAnswerPerDocument"""
        return res_answer_per_document_service.get_all_res_answer_per_document()

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al guardar el res_answer_per_document.')
    @api.doc('create a new res_answer_per_document')
    @api.expect(ResAnswerPerDocumentDto.res_answer_per_document, validate=False)
    # @auth.login_required
    def post(self):
        """Create a new ResAnswerPerDocument"""
        data = request.json
        return_value = res_answer_per_document_service.save_res_answer_per_document(data=data)
        return return_value

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al actualizar el res_answer_per_document.')
    @api.doc('update res_answer_per_document')
    @api.expect(ResAnswerPerDocumentDto.res_answer_per_document, validate=False)
    # @auth.login_required
    def put(self):
        """Update ResAnswerPerDocument"""
        data = request.json
        return res_answer_per_document_service.update_res_answer_per_document(data=data)


@api.route('/res_answer_per_document_by_res_doc_ss_id/<resDocSsId>')
@api.param('resDocSsId', 'The id of resource document serie subserie')
class ResDocumentSerieSubserieByAttachedDocumentId(Resource):
    @audit_init
    @audit_finish
    @api.doc('get list res_answer_per_document_by_res_doc_ss_id')
    def get(self, resDocSsId):
        """Get list ResAnswerPerDocument By resDocSsId"""
        return res_answer_per_document_service.get_res_answer_per_document_serie_subserie_by_res_document_serie_subserie_id(resDocSsId)
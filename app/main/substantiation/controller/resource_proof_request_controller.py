from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import resource_proof_request_service
from app.main.substantiation.util.dto.resource_proof_request_dto import ResourceProofRequestDto
from app.main.util.decorator import audit_init, audit_finish

api = ResourceProofRequestDto.api


@api.route('/resource_proof_request')
class ResourceProofRequestList(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list resource_proof_request')
    def get(self):
        """Get list ResourceProofRequest"""
        return resource_proof_request_service.get_all_resource_proof_request()

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al guardar el resource_proof_request.')
    @api.doc('create a new resource_proof_request')
    @api.expect(ResourceProofRequestDto.resource_proof_request, validate=False)
    # @auth.login_required
    def post(self):
        """Create a new ResourceProofRequest"""
        data = request.json
        return_value = resource_proof_request_service.save_resource_proof_request(
            data=data)
        return return_value

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al actualizar el resource_proof_request.')
    @api.doc('update resource_proof_request')
    @api.expect(ResourceProofRequestDto.resource_proof_request, validate=False)
    # @auth.login_required
    def put(self):
        """Update ResourceProofRequest"""
        data = request.json
        return resource_proof_request_service.update_resource_proof_request(data=data)


@api.route('/resource_proof_request_by_resource_id/<resourceId>')
@api.param('resourceId', 'The id of resource')
class ResourceProofRequestByResourceId(Resource):
    @api.doc('get resource_proof_request by resource_id')
    def get(self, resourceId):
        """Get list ResourceProofRequest by resource_id"""
        return resource_proof_request_service.get_resource_proof_request_by_resource_id(resourceId)


@api.route('/resource_proof_request_by_res_proof_request_id/<resProofRequestId>')
@api.param('resProofRequestId', 'The id of resProofRequestId')
class ResourceProofRequestByResProofRequestId(Resource):
    @api.doc('get resource_proof_request by res_proof_request_id')
    def get(self, resProofRequestId):
        """Get ResourceProofRequest by res_proof_request_id"""
        return resource_proof_request_service.get_resource_proof_request_by_res_proof_request_id(resProofRequestId)

    @audit_init
    @audit_finish
    @api.doc('delete resource_proof_request')
    def delete(self, resProofRequestId):
        """ Delete resource_proof_request"""
        return resource_proof_request_service.delete_resource_proof_request_service(resProofRequestId)


@api.route('/resource_proof_request/<resourceId>')
@api.param('resourceId', type='integer')
class ResourceProofRequest(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list resource_proof_request by any param')
    def post(self, resourceId):
        """get resource_proof_request"""

        return resource_proof_request_service.get_resource_proof_request(resourceId, params=request.json)


@api.route('/validate_resource_proof_request/<resProofRequestId>')
@api.param('resProofRequestId', type='integer')
class ResourceProofRequest(Resource):

    @audit_init
    @audit_finish
    @api.doc('validate resource_proof_request by id')
    def get(self, resProofRequestId):
        """validate resource_proof_request"""

        return resource_proof_request_service.validate_resource_proof_request(resProofRequestId)


@api.route('/validate_resource_proof_request_by_resource_id/<resourceId>')
@api.param('resourceId', type='integer')
class ResourceProofRequest(Resource):

    @audit_init
    @audit_finish
    @api.doc('validate resource_proof_request by resource id')
    def get(self, resourceId):
        """validate resource_proof_request by resource"""

        return resource_proof_request_service.validate_resource_proof_request_by_resource_id(resourceId)


@api.route('/insert_resolutive_numeral_by_claimer_id/<claimerId>')
@api.param('claimerId', type='integer')
class ResourceProofRequest(Resource):

    @audit_init
    @audit_finish
    @api.doc('insert resolutive numeral by claimer id')
    def get(self, claimerId):
        """validate resolutive numeral by claimer"""

        return resource_proof_request_service.insert_resolutive_numeral(claimerId)


@api.route('/get_proof_by_claimer/<claimerId>')
@api.param('claimerId', type='integer')
class ResProofRequestClaimer(Resource):

    @audit_init
    @audit_finish
    @api.doc('get proof for claimer')
    def get(self, claimerId):
        """get proof for claimer"""
        return resource_proof_request_service.get_proof_by_claimer(claimerId)

from flask import request
from flask_restplus import Resource
from app.main.util.decorator import audit_init, audit_finish
from app.main.substantiation.service import expedient_state_service
from app.main.substantiation.util.dto.expedient_state_dto import ExpedientStateDto
from flask_restplus import Namespace, fields


api = ExpedientStateDto.api


@api.route('/expedient_state/get_current_state/<claimer_id>')
class ListQuestions(Resource):
    @api.marshal_with(ExpedientStateDto.expedient_state)
    @api.doc('get current state')
    def get(self, claimer_id):
        """'get curren state """

        return expedient_state_service.get_current_state(claimer_id=claimer_id)


@api.route('/expedient_state/set_expedient_instance/<claimer_id>')
class ExpedientInstance(Resource):
    @api.doc('set expedient instance')
    def post(self, claimer_id):
        """ set expedient instance """
        return expedient_state_service.setExpedientInstance(claimer_id=claimer_id)


@api.route('/expedient_state/send_geomatics/<claimer_id>')
class ExpedientInstance(Resource):
    @api.doc('send geomatics ')
    def post(self, claimer_id):
        """ send geomatics"""
        return expedient_state_service.sendGeomatics(claimer_id=claimer_id)

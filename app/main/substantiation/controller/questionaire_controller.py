from flask import request
from flask_restplus import Resource
from app.main.substantiation.util.dto.questionnaire_dto import QuestionnaireDto
from app.main.substantiation.service import questionaire_service
from app.main.util.decorator import audit_init, audit_finish

api = QuestionnaireDto.api


@api.route('/get_questions/<scope>/<workflow_id>/<section>')
class ListQuestions(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list questions of questionaire')
    def get(self, scope, workflow_id, section):
        """'get list questions of questionaire """
        return questionaire_service.get_questions(scope=scope, section=section, workflow_id=workflow_id)


@api.route('/save_answers')
class SaveAnswers(Resource):

    @audit_init
    @audit_finish
    @api.expect([QuestionnaireDto.answer], validate=False)
    @api.doc('save list answers of questionnaire')
    def post(self):
        """'save list answers of questionnaire """
        return questionaire_service.save_answers(answers=request.json)


@api.route('/answers_by_resource/<resource_id>/<user_id>/<auditor_id>')
class GetAnswers(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list answers by subserie for resource')
    def get(self, resource_id, user_id, auditor_id):
        """get list answers by subserie for resource """
        return questionaire_service.verify_resource_qnr(resource_id, user_id, auditor_id)


@api.route('/questionnaire/<scope>/<workflow_id>')
@api.param('section', 'section', type='string')
class Questionnaire(Resource):

    @audit_init
    @audit_finish
    @api.expect(QuestionnaireDto.answer, validate=False)
    @api.doc('get list questions and answers of questionnaire')
    def post(self, scope, workflow_id):
        """get questionnaire"""

        section = None

        if request.args.get('section'):
            section = str(request.args.get('section'))

        return questionaire_service.get_questionnaire(scope=scope, section=section, workflow_id=workflow_id,
                                                      params=request.json)

    @audit_init
    @audit_finish
    @api.expect(QuestionnaireDto.answer, validate=False)
    @api.doc('delete list answers of questionnaire')
    def put(self, scope, workflow_id):
        """delete list answers of questionnaire"""

        section = None

        if request.args.get('section'):
            section = str(request.args.get('section'))

        return questionaire_service.delete_answers(scope=scope, section=section, workflow_id=workflow_id,
                                                   params=request.json)

    @api.route('/subserie_answer_by_resource_id/<resource_id>')
    @api.param('resource_id', 'The id of resource')
    class AnswerToQuestionnaireByResourceId(Resource):
        @api.doc('get subserie answer by resource_id')
        def get(self, resource_id):
            """Get list subserie answer by resource_id"""
            return questionaire_service.get_subserie_answer_by_resource_id(resource_id=resource_id)    @api.route('/subserie_answer_by_resource_id/<resource_id>')


@api.route('/get_first_round_qnr_user/<document_id>/<second_round_user>')
@api.param('document_id', 'The id of document serie subserie')
@api.param('second_round_user', 'The id of user of second round')
class AnswerToQuestionnaireByResourceId(Resource):
        @api.doc('get get_first_round_qnr_user')
        def get(self, document_id, second_round_user):
            """Obtiene el usuario que realizo el ultimo analisis del dcumento dado en primera ronda"""
            return questionaire_service.get_first_round_qnr_user(document_id, second_round_user)

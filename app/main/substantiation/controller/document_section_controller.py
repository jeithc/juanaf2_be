from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import document_section_service
from app.main.substantiation.util.dto.document_section_dto import DocumentSectionDto

api = DocumentSectionDto.api


@api.route('/document_section_by_claimer_acto/<claimerId>/<actoId>')
@api.param('claimerId', 'The id of claimer')
@api.param('actoId', 'The id of acto')
class DocumentSectionByClaimerActo(Resource):
    @api.doc('get document_section by claimer and acto')
    def get(self, claimerId, actoId):
        """Get list DocumentSection by claimer_id and acto_id"""
        return document_section_service.get_document_section_by_claimer_acto(claimerId, actoId)


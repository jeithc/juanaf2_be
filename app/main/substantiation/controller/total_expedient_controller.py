from flask import request, send_file
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.util.decorator import audit_init, audit_finish
from app.main.substantiation.service.total_expedient_service import total_list, count_total_list, generate_document
from app.main.substantiation.util.dto.total_expedient_dto import TotalExpedientsDTO
import io

api = TotalExpedientsDTO.api


@api.route('/total/<user_id>')
@api.param('user_id', 'id User')
class TaskAssignament(Resource):
    @audit_init
    @audit_finish
    @api.marshal_with(TotalExpedientsDTO.total)
    @api.doc('list total substantiation')
    def post(self, user_id):
        """ list total substantiation """
        data = request.json
        return total_list(user_id, data)


@api.route('/count_total/<user_id>')
@api.param('user_id', 'id User')
class TaskAssignament(Resource):
    @audit_init
    @audit_finish
    @api.doc('list count total substantiation')
    def post(self, user_id):
        """ list count total substantiation """
        data = request.json
        return count_total_list(user_id, data)


@api.route('/export/<user_id>/<document_type>/<users>/<role>/<startDate>/<endDate>')
@api.param('user_id', 'id User')
@api.param('document_type', 'tipo de documento a exportar')
@api.param('users', 'usuarios a buscar')
@api.param('role', 'role busqueda')
@api.param('startDate', 'fecha inicio')
@api.param('endDate', 'fecha fin')
@api.response(204, "no se pudo generar el documento")
class GenerateDocument(Resource):
    @api.doc('get document')
    @audit_init
    @audit_finish
    def get(self, user_id, document_type, users, role, startDate, endDate):
        """get document"""
        if document_type == 'pdf':
            mimetype = 'application/pdf'
        else:
            mimetype= 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        return send_file(
            io.BytesIO(generate_document(user_id, document_type, users, role, startDate, endDate)),
                    mimetype=mimetype,
                    attachment_filename='list.'+document_type
                    #as_attachment = False
        )

from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
import io
from app.main.substantiation.service.claimer_resource_state_service import make_expedient_transition
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.substantiation.util.dto.claimer_resource_state_dto import ClaimerResourceStateDTO
from flask import send_file, request


api = ClaimerResourceStateDTO.api


@api.route('/claimer_resource_state')
class rsClaimer(Resource):
    @audit_init
    @audit_finish
    @api.doc('estado de recursos por claimer')
    def post(self):
        """estado de recursos por claimer"""
        data = request.json
        return make_expedient_transition(data)

from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import res_auto_proof_document_service
from app.main.substantiation.util.dto.resolutive_numeral_dto import ResolutiveNumeralDto
from app.main.util.decorator import audit_init, audit_finish


api = ResolutiveNumeralDto.api


@api.route('/resolutive_numeral')
class ResolutiveNumeral(Resource):

    @audit_init
    @audit_finish
    @api.doc('save resolutive Numeral')
    @api.expect(ResolutiveNumeralDto.resolutive_numeral, validate=False)
    def post(self):
        """save resolutive Numeral"""
        return res_auto_proof_document_service.saveResolutivesNumerals(data=request.json)

    @audit_init
    @audit_finish
    @api.doc('update resolutive Numeral')
    @api.expect(ResolutiveNumeralDto.resolutive_numeral, validate=False)
    def put(self):
        """update resolutive Numeral"""
        return res_auto_proof_document_service.updateResolutiveNumeral(data=request.json)


@api.route('/resolutive_numeral/<claimerId>')
class ResolutiveNumeral (Resource):
    @audit_init
    @audit_finish
    @api.doc('get list resolutive Numeral for claimer')
    def get(self, claimerId):
        """ get lista resolutive numeral for claimer """
        return res_auto_proof_document_service.getResolutiveNumerals(claimerId=claimerId)


@api.route('/resolutive_numeral/<resolutive_numeral_id>')
class DeleteResolutiveNumeral (Resource):
    @audit_init
    @audit_finish
    @api.doc('delete esolutive Numeral')
    def delete(self, resolutive_numeral_id):
        """ delete resolutive numeral """
        return res_auto_proof_document_service.deleteResolutiveNumeral(resolutive_numeral_id=resolutive_numeral_id)

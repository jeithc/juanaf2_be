from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import res_document_serie_subserie_service
from app.main.substantiation.util.dto.res_document_serie_subserie_dto import ResDocumentSerieSubserieDto
from app.main.util.decorator import audit_finish, audit_init

api = ResDocumentSerieSubserieDto.api


@api.route('/res_document_serie_subserie')
class ResDocumentSerieSubserieList(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list res_document_serie_subserie')
    def get(self):
        """Get list ResDocumentSerieSubserie"""
        return res_document_serie_subserie_service.get_all_res_document_serie_subserie()

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al guardar el res_document_serie_subserie.')
    @api.doc('create a new res_document_serie_subserie')
    @api.expect(ResDocumentSerieSubserieDto.res_document_serie_subserie, validate=False)
    # @auth.login_required
    def post(self):
        """Create a new ResDocumentSerieSubserie"""
        data = request.json
        return_value = res_document_serie_subserie_service.save_res_document_serie_subserie(data=data)
        return return_value

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al actualizar el res_document_serie_subserie.')
    @api.doc('update res_document_serie_subserie')
    @api.expect(ResDocumentSerieSubserieDto.res_document_serie_subserie, validate=False)
    def put(self):
        """Update ResAnswerPerDocument"""
        data = request.json
        return res_document_serie_subserie_service.update_res_document_serie_subserie(data=data)


@api.route('/res_document_serie_subserie_by_res_doc_ss_id/<resDocSsId>')
@api.param('resDocSsId', 'The id of resource')
class ResDocumentSerieSubserieByResDocSsId(Resource):
    @audit_init
    @audit_finish
    @api.doc('delete res_document_serie_subserie')
    def delete(self, resDocSsId):
        """ Delete res_document_serie_subserie"""
        return res_document_serie_subserie_service.delete_res_document_serie_subserie(resDocSsId)


@api.route('/res_document_serie_subserie_by_resource_id/<resourceId>')
@api.param('resourceId', 'The id of resource')
class ResDocumentSerieSubserieByResourceId(Resource):
    @audit_init
    @audit_finish
    @api.doc('get list res_document_serie_subserie by resourceId')
    def get(self, resourceId):
        """Get list ResDocumentSerieSubserie By ResourceId"""
        return res_document_serie_subserie_service.get_res_document_serie_subserie_by_resource_id(resourceId)


@api.route('/res_document_serie_subserie_by_attached_document_id/<attachedDocumentId>')
@api.param('attachedDocumentId', 'The id of attached document')
class ResDocumentSerieSubserieByAttachedDocumentId(Resource):
    @api.doc('get list res_document_serie_subserie by attachedDocumentId')
    def get(self, attachedDocumentId):
        """Get list ResDocumentSerieSubserie By attachedDocumentId"""
        return res_document_serie_subserie_service.get_res_document_serie_subserie_by_attached_document_id(attachedDocumentId)


@api.route('/validate_pages_by_resource_id/<resourceId>')
@api.param('resourceId', 'The id of resource')
class ValidateNumberOfPagesByResourceId(Resource):
    @api.doc('validate if all documents of resource is completed')
    def get(self, resourceId):
        """Get validate pages by resourceId"""
        return res_document_serie_subserie_service.validate_number_of_pages_by_resource_id(resourceId)


@api.route('/validate_pages_by_claimer_id/<claimerId>')
@api.param('claimerId', 'The id of claimer')
class ValidateNumberOfPagesByClaimerId(Resource):
    @api.doc('validate if all documents of expedient of a claimer is completed')
    def get(self, claimerId):
        """Get validate pages by claimerId"""
        return res_document_serie_subserie_service.validate_number_of_pages_by_claimer_id(claimerId)


@api.route('/res_document_serie_subserie_by_resource_id_with_correction_req/<resourceId>')
@api.param('resourceId', 'The id of resource')
class ResDocumentSerieSubserieByResourceIdWithCorrectionReq(Resource):
    @audit_init
    @audit_finish
    @api.doc('get list res_document_serie_subserie by resourceId and the correction request')
    def get(self, resourceId):
        """Get list ResDocumentSerieSubserie By ResourceId with correction request"""
        return res_document_serie_subserie_service.get_res_document_serie_subserie_by_resource_id_with_correction_req(resourceId)
from flask import request
from flask_restplus import Resource
from flask_restplus import Namespace
from app.main.util.decorator import audit_init, audit_finish
from app.main.substantiation.service.task_assignament_service import *

api = Namespace('substantiation', description='operaciones con sustanciación de documentos')

@api.route('/new_assignament/<user_id>')
@api.param('user_id', 'id User')
class TaskAssignament(Resource):
    @audit_init
    @audit_finish
    @api.doc('new task assignament')
    def get(self, user_id):
        """ new task assignament """
        return user_task_assignament(user_id)


@api.route('/reactivate_expedient/<claimer_id>')
@api.param('claimer_id', 'id claimer')
class ReactivateExpedient(Resource):
    @audit_init
    @audit_finish
    @api.doc('Reactiva un expediente')
    def post(self, claimer_id):
        """ Reactiva un expediente """
        data = request.json

        return reactivate_expedient(claimer_id, data)


@api.route('/suspend_expedient/<claimer_id>')
@api.param('claimer_id', 'id claimer')
class SuspendExpedient(Resource):
    @audit_init
    @audit_finish
    @api.doc('Suspende un expediente')
    def post(self, claimer_id):
        """ Suspende un expediente """
        data = request.json

        return suspend_expedient(claimer_id, data)

from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import subserie_correction_request_service
from app.main.substantiation.util.dto.subserie_correction_request_dto import SubserieCorrectionRequestDto
from app.main.util.decorator import audit_finish, audit_init

api = SubserieCorrectionRequestDto.api


@api.route('/subserie_correction_request')
class SubserieCorrectionRequestList(Resource):
    @audit_init
    @audit_finish
    @api.doc('get list subserie_correction_request')
    def get(self):
        """Get list SubserieCorrectionRequest"""
        return subserie_correction_request_service.get_all_subserie_correction_request()

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al guardar el subserie_correction_request.')
    @api.doc('create a new subserie_correction_request')
    @api.expect(SubserieCorrectionRequestDto.subserie_correction_request, validate=False)
    # @auth.login_required
    def post(self):
        """Create a new SubserieCorrectionRequest"""
        data = request.json
        return_value = subserie_correction_request_service.save_subserie_correction_request(
            data=data)
        return return_value

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al actualizar el subserie_correction_request.')
    @api.doc('update subserie_correction_request')
    @api.expect(SubserieCorrectionRequestDto.subserie_correction_request, validate=False)
    # @auth.login_required
    def put(self):
        """Update SubserieCorrectionRequest"""
        data = request.json
        return subserie_correction_request_service.update_subserie_correction_request(data=data)


@api.route('/subserie_correction_request_by_res_doc_ss_id/<resDocSsId>')
@api.param('resDocSsId', 'The id of resource')
class SubserieCorrectionRequestByResDocSsId(Resource):
    @audit_init
    @audit_finish
    @api.doc('delete res_document_serie_subserie')
    def get(self, resDocSsId):
        """ Get SubserieCorrectionRequest by resDocSsId"""
        return subserie_correction_request_service.get_subserie_correction_request_by_res_doc_ss_id(resDocSsId)


@api.route('/subserie_correction_request_by_claimer_id/<claimer_id>')
@api.param('claimer_id', 'The id of claimer')
class SubserieCorrectionRequestByClaimerId(Resource):
    @api.doc('get subserie_correction_request by claimer_id')
    def get(self, claimer_id):
        """Get list subserie_correction_request by claimer_id"""
        return subserie_correction_request_service.get_subserie_correction_by_claimer_id(claimer_id)


@api.route('/close_subserie_correction_request')
class CloseSubserieCorrectionRequest(Resource):
    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al close subserie_correction_request.')
    @api.doc('close subserie_correction_request')
    @api.expect(SubserieCorrectionRequestDto.subserie_correction_request, validate=False)
    # @auth.login_required
    def put(self):
        """Update SubserieCorrectionRequest"""
        data = request.json
        return subserie_correction_request_service.close_subserie_correction_request(data=data)


@api.route('/no_approve_all_subserie_correction_request')
class SubserieCorrectionRequestList(Resource):
    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al no aprobar todas las correcciones abiertas.')
    @api.doc('create a new subserie_correction_request')
    @api.expect(SubserieCorrectionRequestDto.subserie_correction_request, validate=False)
    # @auth.login_required
    def post(self):
        """No approve all SubserieCorrectionRequest opnened"""
        data = request.json
        return subserie_correction_request_service.denegate_all_subserie_correction_open_by_claimer_id(data=data)


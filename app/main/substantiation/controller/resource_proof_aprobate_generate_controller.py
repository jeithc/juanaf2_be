from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import res_answer_per_document_service, resource_proof_aprobate_generate_service
from app.main.substantiation.util.dto.res_answer_per_document_dto import ResAnswerPerDocumentDto
from app.main.substantiation.util.dto.resource_proof_request_dto import ResourceProofRequestDto
from app.main.util.decorator import audit_init, audit_finish

api = ResourceProofRequestDto.api


@api.route('/resource_proof_aprobate_generate')
class ResAnswerPerDocumentList(Resource):

    @audit_init
    @audit_finish
    @api.doc('get list proof_aprobate_generate')
    def get(self):
        """Get list ResourceProofRequest"""
        return resource_proof_aprobate_generate_service.get_all_proof_aprobate_generate()

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al guardar el proof_aprobate_generate.')
    @api.doc('create a new proof_aprobate_generate')
    @api.expect(ResourceProofRequestDto.resource_proof_request, validate=False)
    # @auth.login_required
    def post(self):
        """Create a new ResourceProofRequest"""
        data = request.json
        return_value = resource_proof_aprobate_generate_service.save_proof_aprobate_generate(data=data)
        return return_value

    @audit_init
    @audit_finish
    @api.response(500, 'Ocurrió un error al actualizar el proof_aprobate_generate.')
    @api.doc('update proof_aprobate_generate')
    @api.expect(ResourceProofRequestDto.resource_proof_request, validate=False)
    # @auth.login_required
    def put(self):
        """Update ResourceProofRequest"""
        data = request.json
        return resource_proof_aprobate_generate_service.update_proof_aprobate_generate(data=data)


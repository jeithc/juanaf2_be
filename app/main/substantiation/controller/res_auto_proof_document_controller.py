from flask import request
from flask_restplus import Resource

from app.main.substantiation.service import res_auto_proof_document_service
from app.main.substantiation.util.dto.res_auto_proof_document_dto import ResAutoProofDocumentDto
from app.main.util.decorator import audit_init, audit_finish

api = ResAutoProofDocumentDto.api


@api.route('/res_auto_proof_document/<claimer_id>/<round_analysis>')
class ProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('get auto proof by claimerId')
    def get(self, claimer_id, round_analysis):
        """Get list res_auto_proof_document"""
        return res_auto_proof_document_service.get_auto_proof_by_claimer(claimer_id, round_analysis)


@api.route('/max_round_res_auto_proof/<resource_id>')
class MaxRoundProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('get max round proof by claimerId')
    def get(self, resource_id):
        """Get max round res_auto_proof_document"""
        return res_auto_proof_document_service.getmaxRoundResAuto(resource_id=resource_id)


@api.route('/res_auto_proof_document/<expedient_round>/<claimer_id>')
class SaveProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('save auto_proof list')
    @api.expect(ResAutoProofDocumentDto.res_auto_proof_document)
    def post(self, expedient_round, claimer_id):
        """save auto_proof list"""
        return res_auto_proof_document_service.save_auto_proof_list(expedient_round, claimer_id, autos_proofs=request.json)


@api.route('/res_auto_proof_document/generate_document/<document_type_id>/<user_id>')
class GenerateProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('generate auto_proof')
    def post(self, document_type_id, user_id):
        """generate auto_proof"""
        return res_auto_proof_document_service.generate_auto_proof(claimer=request.json,
                                                                   document_type_id=document_type_id,
                                                                   user_id=user_id)


@api.route('/res_auto_proof_document/approve_auto_proof')
class GenerateProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('approve auto proof')
    def post(self):
        """approve auto proof"""
        return res_auto_proof_document_service.approve_auto_proof(data=request.json)


@api.route('/res_auto_proof_document/approve_auto_proof_count')
class GenerateProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('approve auto proo count')
    def post(self):
        """approve auto proof count"""
        return res_auto_proof_document_service.approve_auto_proof_count(data=request.json)


@api.route('/res_auto_proof_document/firm_autos')
class GenerateProofAuto(Resource):

    @audit_init
    @audit_finish
    @api.doc('list firm autof proof')
    def post(self):
        """list firm autof proof"""
        return res_auto_proof_document_service.firm_auto_proof(autos_proof=request.json)

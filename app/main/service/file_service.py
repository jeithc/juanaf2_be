import json

configPath = '/home/carlos/0VSCodePROJECTS/juanaf2_be/config.json'

def main():
    # loadFile('/home/carlos/0VSCodePROJECTS/juanaf2_be/config.json')
    print(readCredentials())

    # "people": [
    #     {
    #         "website": "stackabuse.com", 
    #         "from": "Nebraska", 
    #         "name": "Scott"
    #     }
    # ]   

def loadFile(filepath):
    with open(filepath) as json_file:  
        data = json.load(json_file)
        for p in data['people']:
            print('Name: ' + p['name'])
            print('Website: ' + p['website'])
            print('From: ' + p['from'])
            print('') 

def readCredentials():
    with open(configPath) as json_file:  
        data = json.load(json_file)
        print(data)
        return data['authentication']

if __name__ == '__main__':
    main()                

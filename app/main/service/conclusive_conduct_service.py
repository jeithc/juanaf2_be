import datetime
import hashlib
import io
import json
import os
import webbrowser
import time

import requests

from app.main import db
from app.main.config import ruta_archivos, ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.util.mail import send_mail_report
from app.main.model.claimer_documents import Claimer_documents
from app.main.model.user import User
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.service.s3_service import upload_file, read_content_file
from app.main.notification.model.claimer_notification import ClaimerNotification
from sqlalchemy import and_
from app.main.messaging.model.document_type import Document_type

class ConclusiveConduct:
    
    def create_report(conclusive_documents):
        return None
        auth = (usuario_jasper, contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        waiver_of_terms = bool(conclusive_documents.get("waiverOfTerms"))

        if waiver_of_terms:
            data = """{
            "reportUnitUri": "",
            "async": "false",
            "freshData": "false",
            "saveDataSnapshot": "false",
            "outputFormat": "",
            "interactive": "true",
            "ignorePagination": "false",
            "parameters": {
                "reportParameter": [
                    {"name": "claimer_id","value": [""]},
                    {"name": "param_radicate_number","value": [""]}
                ]
            }
            }"""
        else:
            data = """{
            "reportUnitUri": "",
            "async": "false",
            "freshData": "false",
            "saveDataSnapshot": "false",
            "outputFormat": "",
            "interactive": "true",
            "ignorePagination": "false",
            "parameters": {
                "reportParameter": [
                    {"name": "param_claimer_id","value": [""]},
                    {"name": "param_radicate_number","value": [""]}
                ]
            }
            }"""
            
        if waiver_of_terms:
            typeReport = "17"
        else:
            typeReport = "15"

        claimer_id_request = conclusive_documents.get("claimerid")

        claimerWorkflow = ClaimerWorkflow.query.filter(and_(ClaimerWorkflow.current_state.in_(
            [2,4,5,6,7,8,9,10,11,12,15,16,17,18]),ClaimerWorkflow.end_date==None,ClaimerWorkflow.claimer_id==claimer_id_request)).first()

        user = User.query.filter_by(claimer_id=claimer_id_request).first()

        origin_document = "SISTEMA"
        tr = typeReport

        if claimerWorkflow:
            if user:
                documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

                if not documentoEx:

                    document = Claimer_documents(
                        claimer_id = user.claimer_id,
                        document_type_id = int(tr),
                        flow_document = "1",              
                        origin_document = origin_document,
                        document_state = True
                    )

                    ConclusiveConduct.save_changes(document)  

                    claimerNotification = ClaimerNotification(
                        claimer_id = user.claimer_id,
                        notification_type='CONDUCTA CONCLUYENTE',
                        has_attorney = False,
                        notification_date = datetime.datetime.now(),
                        notification_document_id = document.document_id, 
                        aa_id = 1,
                        claimer_notified = True
                    )

                    ConclusiveConduct.save_changes(claimerNotification)       
                    
                    jsonData = json.loads(data)
                    url = ruta_jasper_server+"reportExecutions"

                    document_jasper = Document_type.query.filter_by(id=typeReport).first()
                    jsonData['reportUnitUri'] = "/reports/{}".format(document_jasper.document_route)

                    jsonData['outputFormat'] = "pdf"
                    jsonData['parameters']['reportParameter'][0]['value'] = [user.claimer_id]
                    jsonData['parameters']['reportParameter'][1]['value'] = [str(document.radicate_number)]

                    headers = {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                    }

                    response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                    dataResponse = response.json()
                    sessionid = s.cookies.get('JSESSIONID')
                    request_id = ""
                    export_id = ""
                    number_of_pages = 0
                    fileName = ""

                    try:
                        request_id = dataResponse.get("requestId")
                        export_id = dataResponse.get("exports")[0].get("id")
                        number_of_pages = dataResponse.get("totalPages")
                        fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
                    except:
                        print("An exception occurred invoke report jasper")

                    now = datetime.datetime.now()

                    report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                    url_document= report_url+"?"+sessionid
                    document.metadata_server_document = json.dumps(dataResponse)
                    document.number_of_pages = number_of_pages
                    urlData={"url": url_document}

                    report= ConclusiveConduct.view_report(urlData)

                    base_dir = ruta_archivos
                    store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
                    pathDocument = base_dir+store_path

                    if not os.path.exists(pathDocument):
                        os.makedirs(pathDocument)

                    ConclusiveConduct.save_changes(document)

                    name = "{documentId}-".format(documentId=document.document_id)+fileName
                    document.url_document = store_path+name 

                    with open(pathDocument+name, "wb") as code:
                        try:
                            code.write(report)
                            document.document_size = os.path.getsize(pathDocument+name)
                            document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                            upload_file(store_path, name, pathDocument+name)
                        except:
                            print("An exception occurred write report to disk")

                    document.url_document = store_path+name

                    ConclusiveConduct.save_changes(document)
                    os.remove(pathDocument+name)

                    claimerWorkflow.end_date = datetime.datetime.now()
                    ConclusiveConduct.save_changes(claimerWorkflow)

                    claimerWorkflowC = ClaimerWorkflow(
                        claimer_id = claimerWorkflow.claimer_id,
                        user_id = 1,
                        current_state = 14,
                        start_date = datetime.datetime.now(),        
                        end_date = None,
                        triggered_event_user = 0
                    )

                    ConclusiveConduct.save_changes(claimerWorkflowC) 

                    return store_path+name
        else:
            print("User without phase workflow")
            return None

    def search_conclusive_conduct(conclusive_documents):
        claimer_id_request = conclusive_documents.get("claimerid")
        claimerWorkflow = ClaimerWorkflow.query.filter(and_(ClaimerWorkflow.current_state.in_([2,4,5,6,7,8,9,10,11,12,15,16,17,18]),ClaimerWorkflow.end_date==None,ClaimerWorkflow.claimer_id==claimer_id_request)).first()

        if claimerWorkflow:
            documentoEx = Claimer_documents.query.filter_by(claimer_id=claimer_id_request, document_type_id=int(32), document_state=True).first()

            if documentoEx:
                return documentoEx.url_document
            else:
                return None

    def view_report(report_download):
        auth = (usuario_jasper, contrasena_jasper)
        s1 = requests.Session()
        url = report_download.get('url').split("?")
        s1.cookies['JSESSIONID']=url[1]
        headersS = {"Accept": "application/json",
                "Content-Type": "application/xml"}
        ret = s1.get(url[0], headers = headersS, auth = auth)
        return ret.content
    
    def view_local_report(report_download):
        path = ruta_archivos+report_download
        print(path)
        bytesReport = open(path, 'rb').read()
        return bytesReport

    def save_changes(data):
        db.session.add(data)
        db.session.commit()

    def calculate_checksum(ret, id):
        hash = ""
        name = "p"+str(id)+".pdf"
        with open(name, "wb") as code:
            code.write(ret)
            hash = hashlib.md5(open(name, 'rb').read()).hexdigest()
        os.remove(name)
        return hash

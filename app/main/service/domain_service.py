
import uuid
import datetime
import json
from app.main import db
from app.main.model.domain import Domain
from sqlalchemy.sql import text


def get_list_domain(name):
    domain= Domain.query.order_by(Domain.value).filter_by(active=True, name = name).all()
    return domain


def save_changes(data):
    db.session.add(data)
    db.session.commit()


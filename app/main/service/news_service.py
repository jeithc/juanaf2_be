from app.main import db
from app.main.model.news import News


def get_list(newsType, location):
    print(newsType, location)
    list_notices = News.query.filter_by(news_type=newsType, news_location=location, active=True).\
        order_by(News.news_order.asc()).all()
    return list_notices


def save_changes(data):
    db.session.add(data)
    db.session.commit()

from flask import request, make_response
from flask_restplus import Resource

from app.main.service.auth_helper import Auth
from ..util.dto import AuthDto

api = AuthDto.api
user_auth = AuthDto.user_auth


# @api.route('/V1.0.0/authentication/login')
# class UserLogin(Resource):
#     """
#         User Login Resource
#     """
#     @api.doc('user login')
#     @api.expect(user_auth, validate=True)
#     def post(self):
#         # get the post data
#         post_data = request.json
#         #print("   CLASSTYPE:"+post_data.__class__.__name__)
#         if request.authorization and request.authorization.username == 'juanaf2' and request.authorization.password == 'ju4n4f2':
#             return Auth.login_user(data=post_data)
#         else:
#             return make_response('Could not verify your login!', 401, {'WWW-Authenticate': 'Basic realm="Login Required"'})

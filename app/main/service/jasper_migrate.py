import datetime
import hashlib
import io
import json
import os
import webbrowser
import requests
from app.main import db
from app.main.model.user import User,InitialPassword
from app.main.model.envio_masivo import EnvioMasivo
from app.main.model.documentsf1 import DocumentosFase1
from app.main.service.report_migrate_service import ReportJasperUserMigrate
from sqlalchemy import asc
from app.main.util.password_generator import  password_encrypt
from app.main.service.logging_service import loggingBDError
from app.main.service.s3_service import list_objects_bucket
from app.main.service.s3_service import upload_file
from os import walk, getcwd
from app.main.model.claimer_documents import Claimer_documents




class ReportJasperMigrate:
    def run_migrate_report():
        usuariosMasivo = EnvioMasivo.query.filter_by(estado='SIN PROCESAR').order_by(asc(EnvioMasivo.registro)).limit(30).all()
        for usuario in usuariosMasivo:
            user = User.query.filter_by(initial_origin_data='1', claimer_id=usuario.claimer_id).first()
            if user:
                query= InitialPassword.query.filter(InitialPassword.claimer_id==usuario.claimer_id).first()
                if query:
                    password_init = query.initial_password
                else:
                    password_init = 'raSja#s8'

                pass_hash = password_encrypt(password_init)
                user.password_hash = pass_hash
                ReportJasperMigrate.save_changes(user)
                ReportJasperUserMigrate.create_report(user.claimer_id)

            
        return "Process migrate report complete"

    def run_migrate_report_s3():
        usuariosMasivo = EnvioMasivo.query.filter_by(estado='PROCESADO').order_by(asc(EnvioMasivo.registro)).limit(10).all()
        for usuario in usuariosMasivo:
            user = User.query.filter_by(initial_origin_data='1', claimer_id=usuario.claimer_id).first()
            if user:
                ReportJasperUserMigrate.recreate_report(user.claimer_id)
           
        return "Process recreate report complete"        
    
    def run_upload_f1_bucket(ruta):
        documentosf1 = DocumentosFase1.query.filter_by(state='SIN_PROCESAR').order_by(asc(DocumentosFase1.id)).limit(10).all()
        for documentof1 in documentosf1:
            store_path = "{box}/{number}.pdf".format(box=documentof1.box, number=documentof1.number)
            pathDocument = ruta.get("path")+store_path
            try:
                print(store_path)
                upload_file("0juana1/"+store_path, "", pathDocument)
                documentof1.state = 'ALMACENADO'
                ReportJasperMigrate.save_changes(documentof1)
            except:
                documentof1.state = 'FALLO'
                ReportJasperMigrate.save_changes(documentof1)
        return "Process upload file f1 complete"


    def run_upload_bucket(ruta):
 
        ruta = ruta['path']

        listaarchivos = []
        for (dirpath, _, archivos) in walk(ruta):
            for archivo in archivos:
                data = "{}/{}".format(dirpath,archivo)
                listaarchivos.extend(data)
                store_path = "{}/{}".format(dirpath.strip(),archivo)
                # print(store_path)
                pathDocument = data
                try:
                    print(store_path)
                    upload_file("0temp_revision/"+store_path, "", pathDocument)
                except Exception as e:
                    print(e)
        return listaarchivos
           

    def list_objects_s3(report_request):
        list_objects_bucket(report_request.get("enviroment"))

    def update_state_mail(report_request):
        usuarioMasivo = EnvioMasivo.query.filter_by(claimer_id=report_request.get("claimerId")).first()
        if usuarioMasivo:
            usuarioMasivo.estado = 'EMAIL_SEND'
            ReportJasperUserMigrate.save_changes(usuarioMasivo)
            return 'claimer_temp_update_successfull'
        else:
            return 'claimer_not_found'
    
    def send_mail_migrate():
        usuariosMasivo = EnvioMasivo.query.filter(EnvioMasivo.estado!='EMAIL_SEND').order_by(asc(EnvioMasivo.registro)).limit(10).all()
        for usuario in usuariosMasivo:
            print(usuario.claimer_id)
            user = User.query.filter_by(initial_origin_data='1', claimer_id=usuario.claimer_id).first()
            if user:
                ReportJasperUserMigrate.send_mail_migrate(user.claimer_id)
    
    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(loggerName='run_migrate_report#jasper_migrate')
            db.session.rollback()
            newLogger.error(e)

    def create_report_internal_recycle():
        usuarioMasivo = EnvioMasivo.query.filter_by(estado='SIN_PROCESAR').first()
        if usuarioMasivo:
            user = User.query.filter_by(initial_origin_data='1', claimer_id=usuarioMasivo.claimer_id).first()
            ReportJasperUserMigrate.create_report_internal_recycle(user.claimer_id)
        return "Process migrate report recycle complete"

    
    def upload_files_bucket(self, ruta):
        listaarchivos = []
        ruta = "/recuperado"
        for (dirpath, _, archivos) in walk(ruta):
            for archivo in archivos:
                document_id=os.path.basename(os.path.normpath(dirpath))
                document= Claimer_documents.query.filter(Claimer_documents.document_id == document_id).first()
                path = generate_path(document)
                data = "{}/{}".format(dirpath,archivo)
                listaarchivos.extend(data)
                store_path = "{}/{}".format(dirpath.strip(),archivo)
                # print(store_path)
                pathDocument = data
                try:
                    print(store_path)
                    ruta_guardado=upload_file(path, archivo, pathDocument)
                    document.url_document = ruta_guardado
                    save_changes(document)
                    try:
                        os.remove(pathDocument) 
                    except Exception as e: 
                        print('error '+e)
                except Exception as e:
                    print(e)
        return listaarchivos


    def upload_files_juana1(self, ruta):
        listaarchivos = []
        ruta = "{}{}".format(os.sep,ruta)
        for (dirpath, _, archivos) in walk(ruta):
            for archivo in archivos:
                document_id=os.path.basename(os.path.normpath(dirpath))
                path = document_id+"/"
                data = "{}/{}".format(dirpath,archivo)
                listaarchivos.extend(data)
                store_path = "{}/{}".format(dirpath.strip(),archivo)
                # print(store_path)
                pathDocument = data
                try:
                    print(store_path)
                    ruta_guardado=upload_file(path, archivo, pathDocument, "j1")
                    try:
                        os.remove(pathDocument) 
                    except Exception as e: 
                        print('error '+e)
                except Exception as e:
                    print(e)
        return listaarchivos      


def generate_path(document):
    now = datetime.datetime.now()
    path = "{}/{}/{}/{}/".format(str(document.claimer_id), document.origin_document.strip(), 
        str(document.document_type_id),now.strftime('%Y%m%d'))
    return path


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='jasper migrate')
        db.session.rollback()
        newLogger.error(e)

from flask import request
from app.main.config import BasicCredentials, ProviderCredentials
from .logging_service import configLogging
import logging
import datetime

credentials = BasicCredentials

def basic_authentication(request):
    newLogger = configLogging(loggerName='authLogger')
    username = ''
    password = ''
    try:
        if hasattr(request, 'authorization'):    
            authorization = request.authorization
            if hasattr(authorization, 'username'):
                username = request.authorization.username                
            if hasattr(authorization, 'password'):                
                password = request.authorization.password 
        else:
            return False        
    except Exception as e:
        print(e)     
  
    # newLogger.info( "hour {}\t user: {} \t url: {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),username, request.full_path) )
    if username == credentials['username'] and password == credentials['password']:
        return True
    else:
        return False


def provider_authentication(request):
    username = ''
    password = ''
    try:
        if hasattr(request, 'authorization'):    
            authorization = request.authorization
            if hasattr(authorization, 'username'):
                username = request.authorization.username                
            if hasattr(authorization, 'password'):                
                password = request.authorization.password 
        else:
            return False        
    except Exception as e:
        print(e)     

    if username == ProviderCredentials['username'] and password == ProviderCredentials['password']:
        return True
    else:
        return False

def get_fail_authentication():
    response_object = {
            'status': 'fail',
            'message': 'Could not verify your login!'
        }
    return response_object
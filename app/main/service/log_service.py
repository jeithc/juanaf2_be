import uuid
import datetime
from app.main import db
from app.main.service.logging_service import loggingBDError


def save_new_audit(data):

    new_log = Log(
        public_id=str(uuid.uuid4()),
        email=data['email'],
        username=data['username'],
        password=data['password'],
        registered_on=datetime.datetime.utcnow()
    )
    save_changes(new_user)
    return response_object, 201
def get_all_users():
    return User.query.all()


def get_a_user(public_id):
    return User.query.filter_by(public_id=public_id).first()


def generate_token(user):
    try:
        # generate the auth token
        auth_token = User.encode_auth_token(user.id)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
            'Authorization': auth_token.decode()
        }
        return response_object, 201
    except Exception as e:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='log_service')
        db.session.rollback()
        newLogger.error(e)


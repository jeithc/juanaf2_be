import datetime
import hashlib
import io
import json
import os
import webbrowser

import requests
from app.main.service.logging_service import loggingBDError
from app.main import db
from app.main.model.claimer_documents import Claimer_documents
from app.main.model.user import User
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.messaging.model.document_type import Document_type
from app.main.util.mail import send_mail_report
from app.main.config import ruta_archivos, ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.service.s3_service import upload_file, read_content_file
from sqlalchemy import and_
from sqlalchemy.sql import text 

class DocumentNonConcurrency:
    def create_report():
        body_mail='<html><head><meta http-equiv=Content-Type content="text/html; charset=utf-8"><style></style></head><body style="tab-interval:35.4pt"><div><p align=center style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:center"><o:p>&nbsp;</o:p></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">Señor(a)</span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">$nombreSolicitante<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">$correo<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">CIUDAD</span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">&nbsp;</span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">Cordial saludo,<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">Mediante el presente mensaje y con la constancia adjunta, la Defensor&iacute;a del Pueblo mediante la Universidad Nacional de Colombia le hace saber que la oportunidad otorgada al ciudadano SOLICITANTE para que acudiera a notificarse personalmente de la Resoluci&oacute;n No. $numActo, por medio de la cual “$encabezadoacto”, venci&oacute; el d&iacute;a $diaVencimiento a las $horaVencimiento, sin que este hubiera concurrido.<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">Lo anterior, de conformidad con la citaci&oacute;n entregada v&iacute;a “$viaCitacion”, Identificada con el No. &uacute;nico de verificaci&oacute;n: $numRadicadoCita.<o:p></o:p></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">SOLICITANTE:&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;  $nombreSolicitante.<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">IDENTIFICACI&oacute;N:&nbsp; &nbsp; $tipoDocumento No. $numDocumento.<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black"><o:p>&nbsp;</o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">En ese contexto, se proceder&aacute; a realizar la correspondiente notificaci&oacute;n al SOLICITANTE a trav&eacute;s de aviso, de conformidad con lo establecido en el Art&iacute;culo 69 de la Ley 1437 de 2011.&nbsp;<o:p></o:p></span></b></p><p style="margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;text-align:justify"><b><span style="font-family:"Trebuchet MS","sans-serif";color:black">Resulta importante señalar que, el(la) SOLICITANTE podr&aacute; autorizar en cualquier momento la notificaci&oacute;n por medios electr&oacute;nicos, hacer seguimiento a todas las etapas del procedimiento administrativo y enterarse de las novedades del mismo, ingresando a la plataforma “DOÑA JUANA LE RESPONDE”, con arreglo a lo dispuesto en el art&iacute;culo 56 de la Ley 1437 de 2011.<o:p></o:p></span></b></p><p><o:p>&nbsp;</o:p></p></div></body></html>'
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_radicate_number","value": [""]}
            ]
        }
        }"""

        typeReport = "16"

        textQuery = text('select  cud.* \
        from (SELECT DISTINCT CUD.claimer_id FROM phase_2.claimer_user_data CUD \
		INNER JOIN phase_2.citation CIT ON CIT.claimer_id = CUD.claimer_ID \
		INNER JOIN phase_2.subgroup_citation SGC ON SGC.subgroup_id = CIT.subgroup_id \
		INNER JOIN phase_2.claimer_workflow cwf ON cwf.claimer_id = CUD.claimer_id \
		and cwf.end_date is null WHERE now()>SGC.end_date_subgroup AND cwf.current_state in (7,10,11,12) and cud.claimer_id > 100 ) cw \
        left outer join (select claimer_id from phase_2.claimer_documents cd  where cd.document_type_id = 16 ) cm \
        on  cw.claimer_id = cm.claimer_id inner join phase_2.claimer_user_data cud on cud.claimer_id = cw.claimer_id \
        where cm.claimer_id is null limit 100')

        claimers = db.engine.execute(textQuery)

        for user in claimers:
            # user = User.query.filter_by(claimer_id=claimer_id['claimer_id']).first()

            origin_document = "SISTEMA"
            tr = typeReport
            # documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            # if not documentoEx:
            document = Claimer_documents(
                claimer_id = user.claimer_id,
                document_type_id = int(tr),
                flow_document = "1",              
                origin_document = origin_document,
                document_state = True
            )

            DocumentNonConcurrency.save_changes(document)        
            jsonData = json.loads(data)
            url = ruta_jasper_server+"reportExecutions"

            document_jasper = Document_type.query.filter_by(id=typeReport).first()
            jsonData['reportUnitUri'] = "/reports/{}".format(document_jasper.document_route)
                                
            jsonData['outputFormat'] = "pdf"
            jsonData['parameters']['reportParameter'][0]['value'] = [document.claimer_id]
            jsonData['parameters']['reportParameter'][1]['value'] = [str(document.radicate_number)]

            headers = {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }

            response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
            dataResponse = response.json()
            sessionid = s.cookies.get('JSESSIONID')
            request_id = ""
            export_id = ""
            number_of_pages = 0
            fileName = ""

            try:
                request_id = dataResponse.get("requestId")
                export_id = dataResponse.get("exports")[0].get("id")
                number_of_pages = dataResponse.get("totalPages")
                fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
            except:
                print("An exception occurred invoke report jasper document 16")

            now = datetime.datetime.now()

            report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
            url_document= report_url+"?"+sessionid
            document.metadata_server_document = json.dumps(dataResponse)
            document.number_of_pages = number_of_pages
            urlData={"url": url_document}
            report= DocumentNonConcurrency.view_report(urlData)

            base_dir = ruta_archivos
            store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
            pathDocument = base_dir+store_path
            if not os.path.exists(pathDocument):
                os.makedirs(pathDocument)

            DocumentNonConcurrency.save_changes(document)
            name = "{documentId}-".format(documentId=document.document_id)+fileName
            document.url_document = store_path+name 

            with open(pathDocument+name, "wb") as code:
                try:
                    code.write(report)
                    document.document_size = os.path.getsize(pathDocument+name)
                    document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                    upload_file(store_path, name, pathDocument+name)
                except:
                    print("An exception occurred write report to disk")

            body_mail= body_mail.replace("$nombreSolicitante", ''.join([i if ord(i) < 128 else '' for i in user.full_name]))
            body_mail= body_mail.replace("$tipoDocumento",user.document_type)
            body_mail= body_mail.replace("$numDocumento",user.document_number)

            if user.email:
                body_mail= body_mail.replace("$correo",user.email)
            else:
                body_mail= body_mail.replace("$correo","Correo no registrado")

            textQuery1 = text("select nombre,documento,begin_date_subgroup,fecharadicado,horacertelec,tipodoc,numerodoc,numeroradicado,claimer_id,value, \
            viacitacion,pbnumber,pbfecha,numeroradicadoact,encabezadoact,fechavencio,horavencio,document_id \
            from (SELECT  CUD.full_name as nombre,cd.document_id as documento,sgc.begin_date_subgroup,to_char(CD.radication_date,'DD/MM/YYYY') as fecharadicado, \
            to_char(CD.radication_date, 'HH12:MI:SS') as horacertelec,CUD.document_type as tipodoc,CUD.document_number as numerodoc, \
            CD.radicate_number as numeroradicado,CD.claimer_id, DOM.VALUE,SGC.citation_type as viacitacion,CDP.radicate_number as pbnumber, \
            to_char(CDP.radication_date ,'DD/MM/YYYY') as pbfecha,ACT.aa_number as numeroradicadoact,ACT.description as encabezadoact,to_char(SGC.end_date_subgroup,'DD/MM/YYYY') as fechavencio, \
            '04:00 PM' as horavencio, CD.DOCUMENT_ID FROM phase_2.claimer_user_data CUD INNER JOIN phase_2.claimer_documents CD ON CD.claimer_ID = CUD.claimer_ID \
            left outer JOIN phase_2.citation CIT ON CIT.claimer_id = CUD.claimer_ID  and cit.document_id = cd.document_id INNER JOIN  CORE.DOMAIN DOM \
            ON CUD.DOCUMENT_TYPE = DOM.CODE and DOM.NAME='DOCUMENT TYPE' inner join (select claimer_id, max(sc.begin_date_subgroup) fecha_max \
            from phase_2.citation c inner join phase_2.subgroup_citation sc on sc.subgroup_id = c.subgroup_id \
            where c.claimer_id = :claimer_id group by claimer_id) f on f.claimer_id = cud.claimer_id INNER JOIN phase_2.subgroup_citation SGC \
            ON SGC.subgroup_id = CIT.subgroup_id and date(SGC.begin_date_subgroup) = date(f.fecha_max) and SGC.citation_type <> 'TEMPORAL INACTIVA' left outer JOIN phase_2.massive_publication MP \
            ON SGC.publication_id = MP.publication_id left outer JOIN phase_2.claimer_documents CDP \
            ON MP.generator_document_id = CDP.document_id INNER JOIN phase_2.administrative_act ACT \
            ON ACT.aa_id = CIT.aa_id WHERE  CUD.claimer_ID = :claimer_id \
            and  SGC.citation_type in ('CITACION MENSAJERIA','CITACION CORREO ELECTRONICO') and cd.document_id is not null \
            and CD.radicate_number is not null and CD.claimer_id is not null union SELECT  CUD.full_name as nombre,cdp.document_id as documento, sgc.begin_date_subgroup, \
            to_char(CDP.radication_date,'DD/MM/YYYY') as fecharadicado,to_char(CDP.radication_date, 'HH12:MI:SS') as horacertelec, CUD.document_type as tipodoc,CUD.document_number as numerodoc, \
            CDP.radicate_number as numeroradicado,cud.claimer_id, DOM.VALUE, SGC.citation_type as viacitacion,CDP.radicate_number as pbnumber, to_char(CDP.radication_date ,'DD/MM/YYYY') as pbfecha, \
            ACT.aa_number as numeroradicadoact,ACT.description as encabezadoact, to_char(SGC.end_date_subgroup,'DD/MM/YYYY') as fechavencio, \
            '04:00 PM' as horavencio, CDP.DOCUMENT_ID FROM phase_2.claimer_user_data CUD left outer JOIN phase_2.citation CIT ON CIT.claimer_id = CUD.claimer_ID  \
            INNER JOIN  CORE.DOMAIN DOM ON CUD.DOCUMENT_TYPE = DOM.CODE and DOM.NAME='DOCUMENT TYPE' inner join (  select claimer_id, max(sc.begin_date_subgroup) fecha_max \
            from phase_2.citation c inner join phase_2.subgroup_citation sc on sc.subgroup_id = c.subgroup_id where c.claimer_id = :claimer_id  \
            group by claimer_id) f on f.claimer_id = cud.claimer_id INNER JOIN phase_2.subgroup_citation SGC \
            ON SGC.subgroup_id = CIT.subgroup_id and date(SGC.begin_date_subgroup) = date(f.fecha_max) and SGC.citation_type <> 'TEMPORAL INACTIVA'left outer JOIN phase_2.massive_publication MP \
            ON SGC.publication_id = MP.publication_id left outer JOIN phase_2.claimer_documents CDP ON MP.generator_document_id = CDP.document_id INNER JOIN phase_2.administrative_act ACT \
            ON ACT.aa_id = CIT.aa_id WHERE  CUD.claimer_ID = :claimer_id) a \
            where documento is not null and horacertelec is not null and  numeroradicado is not null and claimer_id is not null and document_id is not null")
            
            no_concurrency = None

            try:
                no_concurrency = db.engine.execute(textQuery1, claimer_id=document.claimer_id).fetchall() 
            except Exception as e:
                print(e)

            asunto=u"Doña Juana le responde: "
            asunto = asunto +u"constancia de no concurrencia a diligencia de notificación personal de la Resolución No. $numActo, expedida por la Defensoría del Pueblo, a través de la cual “$encabezadoacto” Radicado de salida: $numRadicado"

            for r_no_concurrency in no_concurrency:
                print(r_no_concurrency)
                body_mail= body_mail.replace("$numActo",r_no_concurrency['numeroradicadoact'])
                body_mail= body_mail.replace("$encabezadoacto",r_no_concurrency['encabezadoact'])
                body_mail= body_mail.replace("$diaVencimiento",r_no_concurrency['fechavencio'])
                body_mail= body_mail.replace("$horaVencimiento",r_no_concurrency['horavencio'])
                body_mail= body_mail.replace("$viaCitacion",r_no_concurrency['viacitacion'])
                body_mail= body_mail.replace("$numRadicadoCita",r_no_concurrency['numeroradicado'])
                asunto = asunto.replace("$numActo",r_no_concurrency['numeroradicadoact'])
                asunto = asunto.replace("$encabezadoacto",r_no_concurrency['encabezadoact'])
                asunto = asunto.replace("$numRadicado",r_no_concurrency['numeroradicado'])

            if user.email and not user.email.strip()=='':
                try:
                    send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                except:
                    print("An exception occurred to send mail report")

            document.url_document = store_path+name
            DocumentNonConcurrency.save_changes(document)
            os.remove(pathDocument+name)

            claimerWorkflow = ClaimerWorkflow.query.filter(and_(ClaimerWorkflow.current_state.in_([7,10,11,12]),ClaimerWorkflow.end_date==None, ClaimerWorkflow.claimer_id==user.claimer_id)).first()

            claimerWorkflow.end_date = datetime.datetime.now()
            DocumentNonConcurrency.save_changes(claimerWorkflow)

            claimerWorkflowC = ClaimerWorkflow(
                claimer_id = claimerWorkflow.claimer_id,
                user_id = 1,
                current_state = 15,
                start_date = datetime.datetime.now(),        
                end_date = None,
                triggered_event_user = 0
            )

            DocumentNonConcurrency.save_changes(claimerWorkflowC)                    

    def view_report(report_download):
        auth = (usuario_jasper,contrasena_jasper)
        s1 = requests.Session()
        url = report_download.get('url').split("?")
        s1.cookies['JSESSIONID']=url[1]
        headersS = {"Accept": "application/json",
                "Content-Type": "application/xml"}
        ret = s1.get(url[0], headers = headersS, auth = auth)
        return ret.content

    def view_local_report(report_download):
        bytesReport=read_content_file(report_download)
        return bytesReport

    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(loggerName='concurrency_service')
            db.session.rollback()
            newLogger.error(e)

    def calculate_checksum(ret, id):
        hash = ""
        name = "p"+str(id)+".pdf"
        with open(name, "wb") as code:
            code.write(ret)
            hash = hashlib.md5(open(name, 'rb').read()).hexdigest()
        os.remove(name)
        return hash
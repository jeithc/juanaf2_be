import datetime
import hashlib
import io
import json
import os
import webbrowser
import time

import requests

from app.main import db
from app.main.service.logging_service import loggingBDError
from app.main.model.claimer_documents import Claimer_documents
from app.main.model.user import User,InitialPassword
from app.main.model.envio_masivo import EnvioMasivo
from app.main.util.mail import send_mail_report
from app.main.config import ruta_archivos, ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.service.s3_service import upload_file, read_content_file


class ReportJasperUserMigrate:
    
    def create_report(claimerId):
        body_mail=u'<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "/reports/Acta_Autorizacion_notificacion_electronica__Usuarios_migrados_",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]}
            ]
        }
        }"""

        typeReport = "1"

        user = User.query.filter_by(claimer_id=claimerId).first()
                
        initial_password = None
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            initial_password = InitialPassword.query.filter_by(claimer_id=user.claimer_id).first()

            documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            if not documentoEx:
                document = Claimer_documents(
                    claimer_id = user.claimer_id,
                    document_type_id = int(tr),
                    flow_document = "1",              
                    origin_document = origin_document,
                    document_state = True
                )

                ReportJasperUserMigrate.save_changes(document)        
                
                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                #se consulta el nombre del archivo en la tabla de documentos
                #document_jasper = Document_type.query.filter_by(id=typeReport).first()
                #jsonData['reportUnitUri'] = "/reports/{}".format(document_jasper.document_route)

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }

                response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
                except Exception as e:
                    print("An exception occurred invoke report jasper")

                now = datetime.datetime.now()

                ReportJasperUserMigrate.save_changes(user)

                report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                url_document= report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData={"url": url_document}

                report= ReportJasperUserMigrate.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperUserMigrate.save_changes(document)

                name = "{documentId}-".format(documentId=document.document_id)+fileName
                document.url_document = store_path+name 

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(pathDocument+name)
                        document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except Exception as e:
                        print("An exception occurred write report to disk")

                nombreUsuario = ""

                if user.first_name:
                    nombreUsuario=nombreUsuario+user.first_name+" "
                if user.middle_name:
                    nombreUsuario=nombreUsuario+user.middle_name+" "
                if user.maiden_name:
                    nombreUsuario=nombreUsuario+user.maiden_name+" "
                if user.last_name:
                    nombreUsuario=nombreUsuario+user.last_name+" "

                
                body_mail= body_mail.replace("$nombreusuario", ''.join([i if ord(i) < 128 else '' for i in nombreUsuario]))
                body_mail= body_mail.replace("$nombredoc",name)
                body_mail= body_mail.replace("$usuario",user.username)
                body_mail= body_mail.replace("$contrasena",initial_password.initial_password)

                if user.email:
                    body_mail= body_mail.replace("$email",user.email)
                else:
                    body_mail= body_mail.replace("$email","Correo no registrado")

                usuarioMasivo = EnvioMasivo.query.filter_by(claimer_id=user.claimer_id).first()

                if user.email and not user.email.strip()=='':
                    asunto=u"Doña Juana le responde: "
                    try:
                        if typeReport=="1":
                            asunto = asunto +u"Autorización de notificación personal electrónica"
                        #send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                        
                        if usuarioMasivo:
                            usuarioMasivo.estado = 'PROCESADO'
                            ReportJasperUserMigrate.save_changes(usuarioMasivo)
                    except:
                        usuarioMasivo.estado = 'SIN PROCESAR'
                        ReportJasperUserMigrate.save_changes(usuarioMasivo)
                        print("An exception occurred to send mail report")
                else:
                    print("Correo no enviado para usuario "+nombreUsuario+" "+user.claimer_id)

                document.url_document = store_path+name

                ReportJasperUserMigrate.save_changes(document)
                os.remove(pathDocument+name)
                return store_path+name
            else:
                return "Ya registró un documento anteriormente"

    def view_report(report_download):
        auth = (usuario_jasper,contrasena_jasper)
        s1 = requests.Session()
        url = report_download.get('url').split("?")
        s1.cookies['JSESSIONID']=url[1]
        headersS = {"Accept": "application/json",
                "Content-Type": "application/xml"}
        ret = s1.get(url[0], headers = headersS, auth = auth)
        return ret.content
    
    def view_local_report(report_download):
        bytesReport=read_content_file(report_download)
        return bytesReport

    
    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(loggerName='report_migrate_service')
            db.session.rollback()
            newLogger.error(e)


    def calculate_checksum(ret, id):
        hash = ""
        name = "p"+str(id)+".pdf"
        with open(name, "wb") as code:
            code.write(ret)
            hash = hashlib.md5(open(name, 'rb').read()).hexdigest()
        os.remove(name)
        return hash

    def create_report_internal(report_request):
        body_mail=u'<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "/reports/Acta_Autorizacion_notificacion_electronica__Usuarios_migrados_",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]}
            ]
        }
        }"""

        typeReport = "1"

        user = User.query.filter_by(claimer_id=report_request.get("claimerId")).first()
        
        initial_password = None
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            initial_password = InitialPassword.query.filter_by(claimer_id=user.claimer_id).first()

            documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            if documentoEx:
                document = documentoEx

                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }

                response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
                except Exception as e:
                    print("An exception occurred invoke report jasper")

                now = datetime.datetime.now()

                report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                url_document= report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData={"url": url_document}

                report= ReportJasperUserMigrate.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperUserMigrate.save_changes(document)

                name = "{documentId}-".format(documentId=document.document_id)+fileName
                document.url_document = store_path+name 

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(pathDocument+name)
                        document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except Exception as e:
                        print("An exception occurred write report to disk")

                nombreUsuario = ""

                if user.first_name:
                    nombreUsuario=nombreUsuario+user.first_name+" "
                if user.middle_name:
                    nombreUsuario=nombreUsuario+user.middle_name+" "
                if user.maiden_name:
                    nombreUsuario=nombreUsuario+user.maiden_name+" "
                if user.last_name:
                    nombreUsuario=nombreUsuario+user.last_name+" "

                
                body_mail= body_mail.replace("$nombreusuario", ''.join([i if ord(i) < 128 else '' for i in nombreUsuario]))
                body_mail= body_mail.replace("$nombredoc",name)
                body_mail= body_mail.replace("$usuario",user.username)
                body_mail= body_mail.replace("$contrasena",initial_password.initial_password)

                if user.email:
                    body_mail= body_mail.replace("$email",user.email)
                else:
                    body_mail= body_mail.replace("$email","Correo no registrado")

                usuarioMasivo = EnvioMasivo.query.filter_by(claimer_id=user.claimer_id).first()

                if user.email and not user.email.strip()=='':
                    asunto=u"Doña Juana le responde: "
                    try:
                        if typeReport=="1":
                            asunto = asunto +u"Autorización de notificación personal electrónica"
                        #send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                        
                        if usuarioMasivo:
                            usuarioMasivo.estado = 'REPROCESADO'
                            ReportJasperUserMigrate.save_changes(usuarioMasivo)
                    except:
                        usuarioMasivo.estado = 'SIN PROCESAR'
                        ReportJasperUserMigrate.save_changes(usuarioMasivo)
                        print("An exception occurred to send mail report")
                else:
                    print("Correo no enviado para usuario "+nombreUsuario+" "+user.claimer_id)

                document.url_document = store_path+name

                ReportJasperUserMigrate.save_changes(document)
                os.remove(pathDocument+name)
                return store_path+name

    def create_report_internal_recycle(claimerId):
        body_mail=u'<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "/reports/Acta_Autorizacion_notificacion_electronica__Usuarios_migrados_",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]}
            ]
        }
        }"""

        typeReport = "1"

        user = User.query.filter_by(claimer_id=claimerId).first()

        initial_password = None
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            initial_password = InitialPassword.query.filter_by(claimer_id=user.claimer_id).first()
            claimer_id_null = -1
            documentoEx = Claimer_documents.query.filter(Claimer_documents.claimer_id<=claimer_id_null).first()

            if documentoEx:
                document = documentoEx
                document.claimer_id = user.claimer_id
                document.document_type_id = int(tr)
                document.flow_document = "1"
                document.origin_document = origin_document

                ReportJasperUserMigrate.save_changes(document)

                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }

                response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
                except Exception as e:
                    print("An exception occurred invoke report jasper")

                now = datetime.datetime.now()

                report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                url_document= report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData={"url": url_document}

                report= ReportJasperUserMigrate.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasperUserMigrate.save_changes(document)

                name = "{documentId}-".format(documentId=document.document_id)+fileName
                
                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(pathDocument+name)
                        document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except Exception as e:
                        print("An exception occurred write report to disk")

                nombreUsuario = ""

                if user.first_name:
                    nombreUsuario=nombreUsuario+user.first_name+" "
                if user.middle_name:
                    nombreUsuario=nombreUsuario+user.middle_name+" "
                if user.maiden_name:
                    nombreUsuario=nombreUsuario+user.maiden_name+" "
                if user.last_name:
                    nombreUsuario=nombreUsuario+user.last_name+" "

                
                body_mail= body_mail.replace("$nombreusuario", ''.join([i if ord(i) < 128 else '' for i in nombreUsuario]))
                body_mail= body_mail.replace("$nombredoc",name)
                body_mail= body_mail.replace("$usuario",user.username)
                body_mail= body_mail.replace("$contrasena",initial_password.initial_password)

                if user.email:
                    body_mail= body_mail.replace("$email",user.email)
                else:
                    body_mail= body_mail.replace("$email","Correo no registrado")

                usuarioMasivo = EnvioMasivo.query.filter_by(claimer_id=user.claimer_id).first()

                if user.email and not user.email.strip()=='':
                    asunto=u"Doña Juana le responde: "
                    try:
                        if typeReport=="1":
                            asunto = asunto +u"Autorización de notificación personal electrónica"
                        #send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                        
                        if usuarioMasivo:
                            usuarioMasivo.estado = 'PROCESADO'
                            ReportJasperUserMigrate.save_changes(usuarioMasivo)
                    except:
                        usuarioMasivo.estado = 'SIN PROCESAR'
                        ReportJasperUserMigrate.save_changes(usuarioMasivo)
                        print("An exception occurred to send mail report")
                else:
                    print("Correo no enviado para usuario "+nombreUsuario+" "+user.claimer_id)

                document.url_document = store_path+name

                ReportJasperUserMigrate.save_changes(document)
                os.remove(pathDocument+name)
                return store_path+name
            else:
                return "Sin radicados huerfanos"

    def recreate_report(claimerId):
       
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "/reports/Acta_Autorizacion_notificacion_electronica__Usuarios_migrados_",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]}
            ]
        }
        }"""


        typeReport = "1"

        user = User.query.filter_by(claimerId=claimerId).first()

        if user:
            documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            if  documentoEx:
                
                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [str(documentoEx.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }

                response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                except Exception as e:
                    print("An exception occurred invoke report jasper")


                report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                url_document= report_url+"?"+sessionid
                urlData={"url": url_document}

                report= ReportJasperUserMigrate.view_report(urlData)

                base_dir = ruta_archivos
                ruta_local=documentoEx.url_document.split("/")
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=ruta_local[0], origen=ruta_local[1], tipologiaDoc=ruta_local[2], date= ruta_local[3] )
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                name = ruta_local[4]

                usuarioMasivo = EnvioMasivo.query.filter_by(claimer_id=user.claimer_id).first()

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        upload_file(store_path, name, pathDocument+name)
                        if usuarioMasivo:
                            usuarioMasivo.estado = 'REPROCESADO'
                            ReportJasperUserMigrate.save_changes(usuarioMasivo)
                    except Exception as e:
                        if usuarioMasivo:
                            usuarioMasivo.estado = 'PROCESADO'
                            ReportJasperUserMigrate.save_changes(usuarioMasivo)
                        print("An exception occurred write report to disk")

                time.sleep(2)
                os.remove(pathDocument+name)

    def send_mail_migrate(claimerId):
        body_mail=u'<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'

        typeReport = "1"

        user = User.query.filter_by(claimer_id=claimerId).first()
                
        initial_password = None

        if user:
            initial_password = InitialPassword.query.filter_by(claimer_id=user.claimer_id).first()
            documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            if documentoEx:
                report = ReportJasperUserMigrate.view_local_report(documentoEx.url_document)
                pathDocument = ruta_archivos
                ruta_local=documentoEx.url_document.split("/")

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                name = ruta_local[4]

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                    except Exception:
                        print("An exception occurred write report to disk")

                nombreUsuario = ""

                if user.first_name:
                    nombreUsuario=nombreUsuario+user.first_name+" "
                if user.middle_name:
                    nombreUsuario=nombreUsuario+user.middle_name+" "
                if user.maiden_name:
                    nombreUsuario=nombreUsuario+user.maiden_name+" "
                if user.last_name:
                    nombreUsuario=nombreUsuario+user.last_name+" "

                
                body_mail= body_mail.replace("$nombreusuario", ''.join([i if ord(i) < 128 else '' for i in nombreUsuario]))
                body_mail= body_mail.replace("$nombredoc",name)
                body_mail= body_mail.replace("$usuario",user.username)
                body_mail= body_mail.replace("$contrasena",initial_password.initial_password)

                usuarioMasivo = EnvioMasivo.query.filter_by(claimer_id=user.claimer_id).first()

                if user.email and user.valid_email:
                    body_mail= body_mail.replace("$email",user.email)
                    asunto=u"Doña Juana le responde: "
                    try:
                        if usuarioMasivo:
                            asunto = asunto +u"Autorización de notificación personal electrónica"
                            send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                            usuarioMasivo.estado = 'EMAIL_SEND'
                            ReportJasperUserMigrate.save_changes(usuarioMasivo)
                    except:
                        usuarioMasivo.estado = 'EMAIL_SEND_ERROR'
                        ReportJasperUserMigrate.save_changes(usuarioMasivo)
                        print("An exception occurred to send mail report")

                os.remove(pathDocument+name)
                print("Email send")
            else:
                print("There is no document to send.")
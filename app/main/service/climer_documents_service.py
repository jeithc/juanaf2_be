
from app.main import db
from app.main.model.claimer_documents import Claimer_documents
from app.main.model.user import User
from sqlalchemy.sql import text
from app.main.core.util.dto.document_dto import DocumentDTO
from app.main.core.service.general_service import manage_db_exception
from app.main import logger
from flask_restplus import marshal
from app.main.service.logging_service import loggingBDError
from app.main.core.model.users import Users
import datetime


def save_new_claimer(data, user_email):
    try:
        new_claimer = Claimer_documents(
            claimer_id=data['claimerId'],
            document_type_id=data['documentTypeId'],
            url_document=data['urlDocument'],
            expedient_entry_date=datetime.datetime.now(),
            flow_document=data['flowDocument'],
            number_of_pages=data['numberOfPages'],
            origin_document=data['originDocument'],
            document_checksum=data['documentChecksum'],
            document_state=True
        )

        if 'radicateNumber' in data:
            new_claimer.radicate_number = data['radicateNumber']
        if 'radicationDate' in data:
            new_claimer.radication_date = data['radicationDate']
        if 'documentSize' in data:
            new_claimer.document_size = data['documentSize']

        user_system = Users.query.filter_by(email=user_email).first()
        if user_system:
            new_claimer.user_id = user_system.id
        else:
            new_claimer.user_id = 9999

        db.session.add(new_claimer)
        db.session.commit()
        db.session.refresh(new_claimer)
        return marshal(new_claimer, DocumentDTO.document), 200
    except Exception as e:
        return manage_db_exception("Error en save_new_claimer#save_new_user - " +
                                   "Error al consultar el documento",
                                   'Ocurrió un error al guardar el documento.', e)


def claimer_update(data):
    try:
        claimer = Claimer_documents.query. \
            filter_by(document_id=data['document_id'], document_type_id=data['document_type_id']). \
            first()
    except Exception as e:
        return manage_db_exception("Error en claimer_update#save_new_user - " +
                                   "Error al actualizar el documento",
                                   'Ocurrió un error al actualizar el documento.', e)
    if claimer:
        claimer.url_document = data['url_document'],
        claimer.metadata_server_document = data['metadata_server_document']

        save_changes(claimer)

        response_object = {
            'status': 'success',
            'message': 'informacion del Reclamante actualizada con éxito',
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'User no exist in BD.',
        }
        return response_object


def claimer_search(claimer_id, document_type_id):
    claimer = Claimer_documents.query.filter_by(claimer_id=claimer_id,
                                                document_type_id=document_type_id).all()
    if claimer:

        return claimer
    else:
        response_object = {
            'status': 'fail',
            'message': 'No existen documentos de este tipo.',
        }
    return response_object, 409


def document_by_type_claimer(doc_type, claimer_id):
    logger.debug('Entro a climer_documents_service#document_by_type_claimer')
    try:
        document = Claimer_documents.query.\
            filter_by(document_type_id=doc_type, claimer_id=claimer_id).first()
        return marshal(document, DocumentDTO.document), 200
    except Exception as e:
        return manage_db_exception("Error en climer_documents_service#document_by_type_claimer - " +
                                   "Error al obtener documento",
                                   'Ocurrió un error al obtener el documento por email.', e)


def document_by_id(documentId):
    logger.debug('Entro a climer_documents_service#document_by_id')
    try:
        document = Claimer_documents.query.filter_by(document_id=documentId).first()
        return document, 200
    except Exception as e:
        return manage_db_exception("Error en climer_documents_service#document_by_type_claimer - " +
                                   "Error al obtener documento",
                                   'Ocurrió un error al obtener el documento por email.', e)


def document_by_radicate(radicate_number):
    logger.debug('Entro a climer_documents_service#document_by_radicate')
    try:
        document = Claimer_documents.query. \
          filter(Claimer_documents.radicate_number == radicate_number). \
          filter(Claimer_documents.document_state != False). \
          filter(Claimer_documents.claimer_id > 0). \
          first()
        return document, 200
    except Exception as e:
        return manage_db_exception("Error en climer_documents_service#document_by_radicate - " +
                                   "Error al obtener documento",
                                   'Ocurrió un error al consultar el documento.', e)


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='climer_documents_service')
        db.session.rollback()
        newLogger.error(e)



def get_radicate_documents(data):
    try:
        query_filter = ''
        if "claimerId" in data and data['claimerId'] != '':
            query_filter += f" and cud.claimer_id = '{data['claimerId']}' "
        if "documentType" in data and data['documentType'] != '':
            query_filter += f" and cud.document_type = '{data['documentType']}' "
        if "documentNumber" in data and data['documentNumber'] != '':
            query_filter += f" and cud.document_number = '{data['documentNumber']}' "
        if "firstName" in data and data['firstName'] != '':
            query_filter += f" and cud.first_name = '{data['firstName']}' "
        if "middleName" in data and data['middleName'] != '':
            query_filter += f" and cud.middle_name = '{data['middleName']}' "
        if "lastName" in data and data['lastName'] != '':
            query_filter += f" and cud.last_name = '{data['lastName']}' "
        if "maidenName" in data and data['maidenName'] != '':
            query_filter += f" and cud.maiden_name = '{data['maidenName']}' "
        if "aaNumber" in data and data['aaNumber'] != '':
            query_filter += f" and aac.aa_number = {data['aaNumber']} "

        query = text(
            "select  \
                cdo.document_id,  \
                crd.resource_type,  \
                cdo.radicate_number,  \
                cdo.radication_date,  \
                aac.description,  \
                crd.number_of_documents as document_quantity  \
            from  \
                phase_2.claimer_user_data cud,  \
                phase_2.claimer_documents cdo,  \
                phase_2.claimer_resource_document crd,  \
                phase_2.administrative_act aac  \
            where  \
                cud.claimer_id = cdo.claimer_id  \
                and cud.claimer_id = crd.claimer_id  \
                and crd.resource_document_id = cdo.document_id  \
                and crd.resource_document_id = cdo.document_id  \
                and crd.aa_id = aac.aa_id " + query_filter
        )

        try:
            print(data['perPage'])
            print(data['page'])
            result = db.engine.execute(query, per_page=data['perPage'], page=(
                int(data['page']) - 1) * data['perPage']).fetchall()
            db.session.close()
        except Exception as e:
            print(e)
            return manage_db_exception('Error filtro', e)
        print(f'QUERY\n{query}')
        print(f'RESULT\n{result}')

        return result
    except Exception as e:
        return manage_db_exception("Error en save_new_claimer#save_new_user - " +
                                   "Error al consultar el documento",
                                   'Ocurrió un error al guardar el documento.', e)

from app.main import db
from app.main.model.nat_geo import Nat_geo, Locality


def get_list():
    nat_geo= Nat_geo.query.order_by(Nat_geo.department, Nat_geo.municipality).all()
    return nat_geo

def get_list_locality():
    locality= Locality.query.order_by(Locality.locality, Locality.neighborhood).all()
    return locality



def save_changes(data):
    db.session.add(data)
    db.session.commit()

import logging
import os
import json
import logging.config
import logging.handlers
import sys


#_path = os.path.dirname(os.path.abspath(__file__))
_path = os.getcwd()+"/logs"
_filename= "audit_juanav2.log"
_filename_error= "log_error.log"
_filename_bd_error= "log_BD_error.log"
_filename_test= "log_test.log"


def configLogging(path=_path, filename=_filename, loggerName="", logLevel=logging.INFO):
    logger = None
    try:
        logging.basicConfig(filename=path+'/'+filename, format='%(asctime)s -  %(levelname)s - %(name)s - %(message)s')
        logger = logging.getLogger(loggerName)
        logger.setLevel(logLevel)
    except:
        print(sys.exc_info()[0])
    return logger

def configLoggingError(path=_path, filename=_filename_error, loggerName="", logLevel=logging.INFO):
    logger = None
    try:
        logging.basicConfig(filename=path+'/'+filename, format='%(asctime)s -  %(levelname)s - %(name)s - %(message)s')
        logger = logging.getLogger(loggerName)
        logger.setLevel(logLevel)
    except:
        print('error')
        print(sys.exc_info()[0])
    return logger


def loggingBDError(path=_path, filename=_filename_bd_error, loggerName="", logLevel=logging.ERROR):
    logger = None  
    logging.handlers.RotatingFileHandler(filename, maxBytes=2097152)
    try:
        logging.basicConfig(filename=path+'/'+filename, format='%(asctime)s -  %(levelname)s - %(name)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        logger = logging.getLogger(loggerName)
        logger.setLevel(logLevel)
    except:
        print('error')
        print(sys.exc_info()[0])
    return logger


def loggingTest(path=_path, filename=_filename_test, loggerName="", logLevel=logging.ERROR):
    logger = None  
    logging.handlers.RotatingFileHandler(filename, maxBytes=2097152)
    try:
        logging.basicConfig(filename=path+'/'+filename, format='%(asctime)s -  %(levelname)s - %(name)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        logger = logging.getLogger(loggerName)
        logger.setLevel(logLevel)
    except:
        print('error')
        print(sys.exc_info()[0])
    return logger

def main():
    print("python main function begin")

    newLogger = configLogging('/home/carlos/', 'juana_service.log', 'loggerName', logging.INFO)
    newLogger.info('NEW MESSAGE')

    print("python main function end")

if __name__ == '__main__':
    main()
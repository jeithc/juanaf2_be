import boto3
import logging
from io import BytesIO
from botocore.exceptions import ClientError
from app.main.config import s3_config, environment, environment_int

# from config import s3_config, environment

session = boto3.Session(
    aws_access_key_id=s3_config['aws_access_key_id'],

    aws_secret_access_key=s3_config['aws_secret_access_key']
)

client = boto3.client(
    's3',
    aws_access_key_id=s3_config['aws_access_key_id'],
    aws_secret_access_key=s3_config['aws_secret_access_key']
    # aws_session_token=SESSION_TOKEN,
)

environment_bucket = {
    'dev': "juana-dev-bucket",
    'qa': "juana-qa-bucket",
    'prod': "juana-prod-bucket",
    'int': "impresion",
    'j1': "juanaf1",
    'dev2': "juana-dev2-bucket",
}


def download_file(dest_store_path, dest_file_name):
    final_path = dest_store_path + "/" + dest_file_name
    logging.info(
        f'begin download_bin_file() PARAMS[ dest_store_path={dest_store_path}, dest_file_name={dest_file_name} ]')
    file_content = BytesIO(b'')
    try:
        dest_bucket_name = environment_bucket.get(environment)
        client.download_file(dest_bucket_name, final_path, dest_file_name)
        file_content = open(dest_file_name, "rb").read()
        logging.info('end download_bin_file()')
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e
    return file_content


def download_bin_file(dest_store_path, dest_file_name):
    final_path = dest_store_path + dest_file_name
    logging.info(
        f'begin download_bin_file() PARAMS[ dest_store_path={dest_store_path}, dest_file_name={dest_file_name} ]')
    file_content = BytesIO(b'')
    try:
        dest_bucket_name = environment_bucket.get(environment)
        tmp_filename = "tmp.down.txt"
        client.download_file(dest_bucket_name, final_path, tmp_filename)
        file_content = open(tmp_filename, "rb").read()
        logging.info('end download_bin_file()')
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e
    return file_content


def read_content_file(dest_store_path):
    file_content = None
    try:
        if(dest_store_path.count("/") > 1):
            dest_bucket_name = environment_bucket.get(environment)
        else:
            dest_bucket_name = environment_bucket.get("j1")
        obj = client.get_object(Bucket=dest_bucket_name, Key=dest_store_path)
        file_content = obj['Body'].read()
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e
    return file_content


def download_bin_file_nowork(dest_store_path, dest_file_name):
    final_path = dest_store_path + dest_file_name
    logging.info(
        f'begin download_bin_file() PARAMS[ dest_store_path={dest_store_path}, dest_file_name={dest_file_name} ]')
    file_content = BytesIO(b'')
    try:
        dest_bucket_name = environment_bucket.get(environment)
        resourceS3 = session.resource('s3')
        # s3_object =resourceS3.Bucket(dest_bucket_name).Object(final_path)
        # s3_object.download_fileobj(file_content)
        print("final path " + final_path)
        s3_object = resourceS3.Object(dest_bucket_name, final_path)
        s3_object.download_fileobj(file_content)
        # client.download_fileobj(file_content)

        logging.info('end download_bin_file()')
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e
    return file_content


def upload_file(dest_store_path, dest_file_name, local_file_path, environment = environment):
    final_path = dest_store_path + dest_file_name
    logging.info(
        f'begin upload_file() PARAMS[ dest_store_path={dest_store_path}, dest_file_name={dest_file_name}, local_file_path={local_file_path} ]')
    try:
        dest_bucket_name = environment_bucket.get(environment)
        # if client.get_object(Bucket=bucket, Key=file_key)
        resourceS3 = session.resource('s3')
        bucket_obj = resourceS3.Bucket(dest_bucket_name)
        bucket_obj.put_object(
            Bucket=dest_bucket_name,
            Body='',
            Key=dest_store_path
        )
        # print("final_path " + final_path)
        client.upload_file(local_file_path, dest_bucket_name, final_path)
        logging.info('end upload_file()')
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e
    return final_path


def put_object(dest_bucket_name, dest_object_name, src_data):
    """Add an object to an Amazon S3 bucket

    The src_data argument must be of type bytes or a string that references
    a file specification.

    :param dest_bucket_name: string
    :param dest_object_name: string
    :param src_data: bytes of data or string reference to file spec
    :return: True if src_data was added to dest_bucket/dest_object, otherwise
    False
    """

    # Construct Body= parameter
    if isinstance(src_data, bytes):
        object_data = src_data
    elif isinstance(src_data, str):
        try:
            object_data = open(src_data, 'rb')
            # possible FileNotFoundError/IOError exception
        except Exception as e:
            logging.error(e)
            return False
    else:
        logging.error('Type of ' + str(type(src_data)) +
                      ' for the argument \'src_data\' is not supported.')
        return False

    # Put the object
    s3 = boto3.client('s3')
    try:
        s3.put_object(Bucket=dest_bucket_name, Key=dest_object_name, Body=object_data)
    except ClientError as e:
        # AllAccessDisabled error == bucket not found
        # NoSuchKey or InvalidRequest error == (dest bucket/obj == src bucket/obj)
        logging.error(e)
        return False
    finally:
        if isinstance(src_data, str):
            object_data.close()
    return True


def list_objects_bucket(environmentS3):
    try:
        dest_bucket_name = environment_bucket.get(environmentS3)
        # if client.get_object(Bucket=bucket, Key=file_key)
        resourceS3 = session.resource('s3')
        bucket_obj = resourceS3.Bucket(dest_bucket_name)
        i = 0
        with open('S3_'+environmentS3+'.txt', 'w') as f:
            for bucket_element in bucket_obj.objects.all():
                try:
                    f.write("%s\n" % bucket_element.key)
                except:
                    print('Error al escribir '+ bucket_element.key)
                i = i + 1
                print(i)
        print("Total de folders: ", i)
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e


def copy_objects_bucket(package_number, date, key_document):
    try:
        orig_bucket_name = environment_bucket.get(environment)
        dest_bucket_name = environment_bucket.get(environment_int)
        resourceS3 = session.resource('s3')
        orig_key = key_document
        copy_source = {
            'Bucket': orig_bucket_name,
            'Key': orig_key
        }
        dest_bucket = resourceS3.Bucket(dest_bucket_name)
        dest_bucket.copy(copy_source,
                         "impresion/paquete_{package_number}_{date}/{document}".format(package_number=package_number, date=date,
                                                                             document=orig_key.split('/')[4]))
        print("Successfull copy " + orig_key.split('/')[4])
        ##PENDIENTE COLOCAR LA INSERCION DEL FLAG DE CONTROL AL DOCUMENTO COPIADO
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e
        print("Error copy " + key_document)


def clone_objects_bucket_rai(key_document):
    try:
        orig_bucket_name = environment_bucket.get(environment)
        
        dest_bucket_name = environment_bucket.get(environment_int)
        resourceS3 = session.resource('s3')
        orig_key = key_document
        copy_source = {
            'Bucket': orig_bucket_name,
            'Key': orig_key
        }
        dest_bucket = resourceS3.Bucket(dest_bucket_name)
        dest_bucket.copy(copy_source, orig_key)
        #print("Successfull copy " + orig_key.split('/')[4])
        ##PENDIENTE COLOCAR LA INSERCION DEL FLAG DE CONTROL AL DOCUMENTO COPIADO
        print('copiado {}'.format(key_document))
        return 'ok'
    except:
        # print("error " + str(e))
        print("Error copy {}".format(key_document))


if __name__ == '__main__':
    try:
        print(environment_bucket.get(environment))
        bucket = "juana-fase2-test"
        file_path = "/home/carlos/0VSCodePROJECTS/BACK/juanaf2_be/test.txt"
        # file_key = "test.up.txt"
        file_key = "test.txt"

        # client.upload_file(file_path, bucket, "dir1/" + file_key)        

        # upload_bin_file('dir1/dir2/dir3/', 'test.txt')        
        upload_file('dir1/dir2/dir3/', file_key, file_path)
        # content = download_bin_file('dir1/dir2/dir3/', file_key)
        # print(str(content))

        # resourceS3 = session.resource('s3')
        # resourceS3.Bucket(bucket).download_file(file_key, file_path)
    except Exception as e:
        print("error " + str(e))
        logging.error(e)
        raise e


def delete_object_bucket(document_path):
    try:
        bucket_name = environment_bucket.get(environment)

        list_objects = client.list_objects(Bucket=bucket_name, Prefix=document_path)

        for key in list_objects['Contents']:
            client.delete_object(Bucket=bucket_name, Key=key['Key'])

        response_object = {
            'status': 'success',
            'message': 'objeto eliminado del bucket correctamente',
        }

        return client.delete_object(Bucket=bucket_name, Key=s3_config['aws_secret_access_key']), 201

    except Exception as e:
        logging.error(e)


def copy_object_bucket(path_from, path_to, **bucket_to):
    try:
        bucket_name = environment_bucket.get(environment)
        copy_source = {
            'Bucket': bucket_name,
            'Key': path_from
        }
        if not bucket_to:
            bucket_to = bucket_name

        client.copy(copy_source, bucket_to, path_to)

        return path_to

    except Exception as e:
        logging.error(e)

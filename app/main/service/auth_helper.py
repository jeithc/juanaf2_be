from app.main.model.user import User, InitialPassword
from app.main.model.envio_masivo import EnvioMasivo
from app.main.service.user_service import save_changes
from app.main.util.password_generator import _create_password, password_encrypt
from app.main.service.logging_service import loggingBDError
from app.main.util.mail import *
from sqlalchemy import func
from app.main import db
import base64


class Auth:
    @staticmethod
    def login_user(data):
        try:
            decode_pass = base64.b64decode(data['password'])

            # fetch the user data
            user = User.query.filter(func.upper(
                User.username) == func.upper(data.get('username'))).first()
            
            if not user:
                response_object = {
                    'status': 'fail',
                    'message': 'Usuario o Contraseña Incorrectos.'
                }
                return response_object
            
            
            
            if user.cause_non_registration != 'NINGUNO' or  user.cause_non_registration is None :
                response_object = {
                    'status': 'fail',
                    'message': 'Acceso no permitido, usuario reportado como fallecido o menor de edad.'
                }
                return response_object

                
            if user and user.check_password(decode_pass):
                response_object = {
                    'status': 'success',
                    'id': user.claimer_id
                }
                return response_object, 200
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Usuario o Contraseña Incorrectos.'
                }
                return response_object
        except Exception as e:
            print(e)
            response_object = {
                'status': 'fail',
                'message': 'Intente de nuevo'
            }
            return response_object



    @staticmethod
    def logout_user(data):
        # if data:
        #     auth_token = data.split(" ")[1]
        # else:
        #     auth_token = ''
        # if auth_token:
        #     resp = User.decode_auth_token(auth_token)
        #     if not isinstance(resp, str):
        #         # mark the token as blacklisted
        #         return save_token(token=auth_token)
        #     else:
        #         response_object = {
        #             'status': 'fail',
        #             'message': resp
        #         }
        #         return response_object, 401
        # else:
        #     response_object = {
        #         'status': 'fail',
        #         'message': 'Provide a valid auth token.'
        #     }

        response_object = {
            'status': 'success',
            'message': 'ok'
        }
        return response_object, 200

    @staticmethod
    def get_logged_in_user(new_request):
        # get the auth token
        auth_token = new_request.headers.get('Authorization')
        if auth_token:
            resp = User.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                user = User.query.filter_by(id=resp).first()
                response_object = {
                    'status': 'success',
                    'data': {
                        'user_id': user.id,
                        'email': user.email,
                        'admin': user.admin,
                        'registered_on': str(user.registered_on)
                    }
                }
                return response_object, 200
            response_object = {
                'status': 'fail',
                'message': resp
            }
            return response_object
        else:
            response_object = {
                'status': 'fail',
                'message': 'Provide a valid auth token.'
            }
            return response_object

    @staticmethod
    def change_password(data_username, data_email):
        user = User.query.filter(func.upper(User.username) == func.upper(data_username),
                                 func.lower(User.email) == func.lower(data_email)).first()
        if user:
            password = _create_password(8)

            user.password_hash = user.password_encrypt(password)
            save_changes(user)
            send_mail(data_email, password)
            response_object = {
                'status': 'ok',
                'password': password,
            }
            return response_object, 200

        # return save_changes(new_user)
        # return generate_token(new_user)
        else:
            response_object = {
                'status': 'fail',
                'message': 'Usuario o correo no existe en sistema, por favor intente de nuevo.',
            }
            return response_object

    def update_password(data):
        user = User.query.filter_by(claimer_id=data['claimer_id']).first()
        if user:
            user.password_hash = user.password_encrypt(data['new_password'])
            save_changes(user)
            response_object = {
                'status': 'ok',
                'message': 'password change successful '
            }
            return response_object, 200

        # return save_changes(new_user)
        # return generate_token(new_user)
        else:
            response_object = {
                'status': 'fail',
                'message': 'user id no found in DB',
            }
            return response_object

    def password_hash_new(password):
        print(password)
        return password_encrypt(password)


    # def init_password():
    #     with open('passwordFile.csv', mode='w', newline='') as password_file:
    #         password_writer = csv.writer(password_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    #         for i in range(1,650000):
    #             password_i =_create_password(8)
    #             #password_h = password_encrypt(password_i)
    #             password_writer.writerow([i, password_i ])
    #             print(i)
    #     return

    def init_password():

        password = EnvioMasivo.query.all()
        i = 0
        for row in password:
            user = User.query.filter_by(claimer_id=row.claimer_id).first()
            query = InitialPassword.query.filter_by(claimer_id=row.claimer_id).first()
            password_init = query.initial_password
            print(password_init)
            pass_hash = password_encrypt(password_init)
            user.password_hash = pass_hash
            #save_changes(user)
            i = i + 1

        return i

    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(loggerName='init_password')
            db.session.rollback()
            newLogger.error(e)


def update_password_new(username, email):
    user = User.query.filter(func.upper(User.username) == func.upper(username),
                             func.lower(User.email) == func.lower(email)).first()
    if user:
        password = _create_password(8)

        html_body = "<p>Usted solicitó refrescar su contraseña en el sistema Doña Juana le responde " \
                    " </p> <p> Usuario: {} <br/>Contraseña: {} </p>" \
                    "<p>Se le advierte que la información de usuario y contraseña para el acceso a la plataforma " \
                    "es personal e intransferible, y en este sentido solamente usted es el responsable de su " \
                    "custodia y manejo, por lo que deberá impedir su divulgación por cualquier medio.</p>" \
                    "<p> Cordialmente, <br/> <br/> DOÑA JUANA LE RESPONDE.  <br/> </p>" \
                    "".format(username, password)

        html_body = format_html(html_body)
        html_body = html_body + get_email_footer()

        user.password_hash = user.password_encrypt(password)
        save_changes(user)
        send_mail_general(email, u"Doña Juana le responde - cambio de clave", html_body)
        response_object = {
            'status': 'ok',
            'password': password,
        }
        return response_object, 200

    # return save_changes(new_user)
    # return generate_token(new_user)
    else:
        response_object = {
            'status': 'fail',
            'message': 'Usuario o correo no existe en sistema, por favor intente de nuevo.',
        }
        return response_object

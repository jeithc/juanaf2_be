from app.main import db
from app.main.model.user import User, InitialPassword
from app.main.model.questions import Questions ,Answers, Answers_registration
from app.main.util.password_generator import  password_encrypt
from app.main.service.logging_service import loggingBDError
from sqlalchemy.sql import text
import random
import datetime

def get_list_questions():
    questions= Questions.query.order_by(Questions.visualization_order).filter(Questions.state =='A', Questions.id!=1).all()
    return questions

def get_list_options(user_id, question_id):
        #questions = db.session.query(func.phase_2.f_return_qa(data)).all()

        t = text("select * FROM phase_2.f_return_qa(:user_id, :question_id)")
        options=db.engine.execute(t, user_id=user_id, question_id=question_id).fetchall()
        db.session.close()

        if options:
            return options
        else:
            options = {
                'status': 'fail',
                'message': 'no question data for user',
            }      
        return options

def verify_answer(data):    
    try:
        answer=data['answer']
        query= Answers.query.filter(Answers.person_expedient_id ==data['personExpedientId'], \
            Answers.registration_question_id==data['idQuestion'], \
            Answers.answer.like(answer+'%')).first()

        if query:
            result =  True
            user = User.query.filter_by(claimer_id=data['claimerId']).first()
            if user:

                query= InitialPassword.query.filter(InitialPassword.claimer_id ==data['claimerId']).first()
                if query:
                    password_init = query.initial_password
                else:
                    password_init = 'raSja#s8'

                pass_hash = password_encrypt(password_init)
                user.password_hash = pass_hash
                user.approved_questions = True
                save_changes(user)
        else:
            result =  False

        #se guarda la traza en la tabla de registro de respuestas
        new_answers_registration = Answers_registration(
            answer_event_id = 123,
            answer_event_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            person_notification_id = data['personExpedientId'],
            registration_question_id = data['idQuestion'],
            answer = answer,
            question = data['question'],
            valid_answer = result
        )
        save_changes(new_answers_registration)

        return result
        

    except Exception as e:
        newLogger = loggingBDError(loggerName='verify_answer')
        newLogger.error(e)
        print(e)
        return 'query Error'





def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='question_service')
        db.session.rollback()
        newLogger.error(e)



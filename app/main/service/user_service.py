import json
from app.main import db
from sqlalchemy import func
from app.main.model.user import User, Expedients
from app.main.model.claimer_documents import Claimer_documents_view as cdv
from sqlalchemy.sql import text
from app.main.core.service import workflow_service
from app.main.core.service.general_service import manage_db_exception
from app.main.service.logging_service import loggingBDError
from flask_restplus import marshal
from app.main.util.dto import UserDto


def save_new_user(data):
  user = User.query.filter_by(document_number=data['documentNumber'], document_type=data['documentType']).first()
  if not user:
    try:
      new_claimer = User.init_from_dto(data)
      db.session.add(new_claimer)
      db.session.commit()
      db.session.refresh(new_claimer)
      return new_claimer
    except Exception as e:
      manage_db_exception("Error en #save_new",
                          "Error al guardar claimer", e)
  else:
    response_object = {
      'status': 'fail',
      'message': 'User already exists. Please Log in.',
    }
    return response_object


def user_update(data):
  print(data)
  user = User.query.filter_by(claimer_id=data['claimerId']).first()
  print(user)
  if user:
    if 'firstName' in data:
      user.first_name = data['firstName']

    if 'middleName' in data:
      user.middle_name = data['middleName']

    if 'lastName' in data:
      user.last_name = data['lastName']

    if 'maidenName' in data:
      user.maiden_name = data['maidenName']

    if 'documentType' in data:
      user.document_type = data['documentType']

    if 'documentNumber' in data:
      user.document_number = data['documentNumber']

    if 'email' in data:
      user.email = data['email']

    if 'idNatgeo' in data:
      if data['idNatgeo'] != 0:
        user.id_natgeo = data['idNatgeo']

    if 'idBogLocNeig' in data:
      if data['idBogLocNeig'] >= 0:
        user.id_bog_loc_neig = data['idBogLocNeig']

    if 'address' in data:
      user.address = data['address']

    if 'phone1' in data:
      user.phone1 = data['phone1']

    if 'phone2' in data:
      user.phone2 = data['phone2']
    else:
      user.phone2 = ''

    if 'urbanRuralLocation' in data:
      user.urban_rural_location = data['urbanRuralLocation']

    if 'authorizedNewsDataTreatment' in data:
      user.authorized_news_data_treatment = data['authorizedNewsDataTreatment']

    if 'alternateEmail' in data:
      user.alternate_email = data['alternateEmail']
    else:
      user.alternate_email = ''
    if 'country' in data:
      user.country = data['country']
    else:
      user.country = ''
    if 'city' in data:
      user.city = data['city']
    else:
      user.city = ''
    if 'dataUpdateRequired' in data:
      user.data_update_required = data['dataUpdateRequired']
    else:
      user.dataUpdateRequired = None

    if 'authorizeElectronicNotificati' in data:
      user.authorize_electronic_notificati = data['authorizeElectronicNotificati']

    if 'electronicAuthorizationDate' in data:
      user.electronic_authorization_date = data['electronicAuthorizationDate']

    if 'fullName' in data:
      user.full_name = data['fullName']

    save_changes(user)

    response_object = {
      'status': 'success',
      'message': 'Usuario actualizado con éxito',
    }
    return response_object, 201
    # return generate_token(new_user)
  else:
    response_object = {
      'status': 'fail',
      'message': 'User no exist in BD.',
    }
    return response_object, 204


def get_user_radicates(expedient_id):
  try:
    entero = int(expedient_id)
  except ValueError as e:
    print(e)
    return None
  try:
    t = text("select string_agg(radicate_number, ', ') from phase_2.expedient_person_search  eps \
                INNER JOIN phase_2.claimer_user_data cud on eps.expedient_id = cud.expedient_id\
                where claimer_id = :expedient_id")
    data = db.engine.execute(t, expedient_id=expedient_id).fetchone()
    db.session.close()
  except Exception as e:
    db.session.close()

  if data[0]:
    return data[0]
  else:
    return None


def get_all_users():
  return User.query.order_by(User.claimer_id.desc()).limit(30).all()


def get_users_by_filter(data):
  t = text('')
  if (data):
    data_c = {
      'doc_type': '',
      'doc_number': '',
      'first_name': '',
      'middle_name': '',
      'last_name': '',
      'maiden_name': '',
      'current_state': '',
      'radicate_number': '',
      'radicate_citation': '',
      'person_expedient_id': '',
      'page': 1,
      'perPage': 10,
      'orderCriteria': 'TYPE',
      'sortingOrder': 'DESC',
    }

    query_filter = " "
    join_filter = "LEFT JOIN phase_2.claimer_workflow cw on cud.claimer_id=cw.claimer_id and cw.end_date is null"
    state_filter = " "
    cw = " "

    copyOfData = dict(data)
    for (key, value) in copyOfData.items():
      data_c[key] = value

    if data_c['page'] == 1:
      data_c['page'] = 1

    if data_c['orderCriteria'] == 'NAME':
      data_c['orderCriteria'] = 'full_name'
    elif data_c['orderCriteria'] == 'TYPE':
      data_c['orderCriteria'] = 'document_type'
    else:
      data_c['orderCriteria'] = 'document_number'

    if data_c['doc_type'] != '':
      query_filter += " AND upper(cud.document_type) = upper('{}') ".format(data_c['doc_type'])

    if data_c['doc_number'] != '':
      query_filter += " AND cud.document_number = '{}' ".format(data_c['doc_number'])

    if data_c['first_name'] != '':
      query_filter += " AND upper(cud.first_name) = upper('{}') ".format(data_c['first_name'])

    if data_c['middle_name'] != '':
      query_filter += " AND upper(cud.middle_name) = upper('{}') ".format(data_c['middle_name'])

    if data_c['last_name'] != '':
      query_filter += " AND upper(cud.last_name) = upper('{}') ".format(data_c['last_name'])

    if data_c['maiden_name'] != '':
      query_filter += " AND upper(cud.maiden_name) = upper('{}') ".format(data_c['maiden_name'])

    if 'current_payment_state' in data_c and data_c['current_payment_state'] != '':
      query_filter += " AND cud.current_payment_state = '{}' ".format(data_c['current_payment_state'])

    if data_c['radicate_number'] != '':
      query_filter += " AND eps.radicate_number = '{}' ".format(data_c['radicate_number'])

    if data_c['person_expedient_id'] != '':
      query_filter += " AND eps.person_expedient_id = {} ".format(data_c['person_expedient_id'])

    if data_c['current_state'] != '':
      state_filter += " AND cw.current_state = {} ".format(data_c['current_state'])
      join_filter = " INNER JOIN phase_2.claimer_workflow cw on cud.claimer_id=cw.claimer_id and cw.end_date is null"

    if data_c['radicate_citation'] != '':
      join_filter += " INNER JOIN phase_2.claimer_documents cd on cud.claimer_id = cd.claimer_id"
      state_filter += " AND cd.radicate_number = '{}' ".format(data_c['radicate_citation'])

    t = text("SELECT DISTINCT \
            cud.expedient_id, cud.first_name, cud.middle_name, cud.last_name, cud.authorize_electronic_notificati, cud.maiden_name, cud.person_expedient_id, cud.non_former_claimer,\
            cud.full_name, cud.document_type, cud.document_number, cud.claimer_id, cud.approved_questions, cud.cause_non_registration, cud.registered_claimer ,coalesce(cw.current_state,0) current_state,\
            cud.current_payment_state \
            FROM phase_2.expedient_person_search eps\
            INNER JOIN phase_2.claimer_user_data cud on eps.expedient_id=cud.expedient_id\
            " + query_filter + join_filter + state_filter + " \
            order by " + data_c['orderCriteria'] + " " + data_c[
      'sortingOrder'] + ",  expedient_id   LIMIT :per_page OFFSET :page")

    try:
      data = db.engine.execute(t, per_page=data_c['perPage'], page=(data_c['page'] - 1) * data_c['perPage']).fetchall()
      db.session.close()
    except Exception as e:
      return manage_db_exception('Error filtro', e)

    return data

  else:
    response_object = {
      'status': 'fail',
      'message': 'no data to find',
    }
    return response_object, 204


# def get_users_by_filter_prueba(data):
#     if(data):
#         #datos para armar el orden y total de consulta
#         page =  data['page'] if data['page'] and data['page']!=1  else 0
#         del data["page"]
#         per_page = data['perPage'] if data['perPage']  else 10
#         del data["perPage"]
#         order_criteria = data['orderCriteria'] if data['orderCriteria'] else 'DOCUMENT'
#         del data["orderCriteria"]
#         sorting_order = data['sortingOrder']  if data['sortingOrder'] else 'DESC'
#         del data["sortingOrder"]


#         copyOfData = dict(data)
#         for (key, value) in copyOfData.items() :
#             if value == "":
#                 del data[key]


#         data=Expedients.query.distinct(Expedients.expedient_id).order_by(Expedients.doc_type.desc()).filter_by(**data).offset(page).limit(per_page).all()


#         if data:
#             return data
#         else:
#             response_object = {
#             'status': 'fail',
#             'message': 'no found',
#             }
#             return response_object, 404
#     else:
#         response_object = {
#             'status': 'fail',
#             'message': 'no data to find',
#         }
#         return response_object, 409


def get_total_by_filter(data):
  if (data):
    data_c = {
      'doc_type': '',
      'doc_number': '',
      'first_name': '',
      'middle_name': '',
      'last_name': '',
      'maiden_name': '',
      'radicate_number': '',
      'person_expedient_id': '',
      'current_state': '',
      'radicate_citation': ''

    }

    query_filter = " "
    join_filter = "LEFT JOIN phase_2.claimer_workflow cw on cud.claimer_id=cw.claimer_id and cw.end_date is null "
    state_filter = " "
    cw = " "

    copyOfData = dict(data)
    for (key, value) in copyOfData.items():
      data_c[key] = value

    if data_c['doc_type'] != '':
      query_filter += " AND upper(document_type) = upper('{}') ".format(data_c['doc_type'])

    if data_c['doc_number'] != '':
      query_filter += " AND document_number = '{}' ".format(data_c['doc_number'])

    if data_c['first_name'] != '':
      query_filter += " AND upper(cud.first_name) = upper('{}') ".format(data_c['first_name'])

    if data_c['middle_name'] != '':
      query_filter += " AND upper(cud.middle_name) = upper('{}') ".format(data_c['middle_name'])

    if data_c['last_name'] != '':
      query_filter += " AND upper(cud.last_name) = upper('{}') ".format(data_c['last_name'])

    if data_c['maiden_name'] != '':
      query_filter += " AND upper(cud.maiden_name) = upper('{}') ".format(data_c['maiden_name'])

    if 'current_payment_state' in data_c and data_c['current_payment_state'] != '':
      query_filter += " AND cud.current_payment_state = '{}' ".format(data_c['current_payment_state'])

    if data_c['radicate_number'] != '':
      query_filter += " AND radicate_number = '{}' ".format(data_c['radicate_number'])

    if data_c['person_expedient_id'] != '':
      query_filter += " AND eps.person_expedient_id = {} ".format(data_c['person_expedient_id'])

    if data_c['current_state'] != '':
      state_filter += " AND cw.current_state = {} ".format(data_c['current_state'])
      join_filter = " INNER JOIN phase_2.claimer_workflow cw on cud.claimer_id=cw.claimer_id and cw.end_date is null "

    if data_c['radicate_citation'] != '':
      join_filter += " INNER JOIN phase_2.claimer_documents cd on cud.claimer_id = cd.claimer_id"
      state_filter += " AND cd.radicate_number = ('{}') ".format(data_c['radicate_citation'])

    t = text("select count(*) AS total from(SELECT DISTINCT \
            cud.full_name, cud.document_type, cud.document_number, cud.claimer_id, cud.approved_questions \
            FROM phase_2.expedient_person_search eps\
            INNER JOIN phase_2.claimer_user_data cud on eps.expedient_id=cud.expedient_id\
            " + query_filter + join_filter + " " + state_filter + ")as cont")
    try:
      total = db.engine.execute(t).first()
      db.session.close()
      if total:
        return total[0]
      else:
        return 0
    except Exception as e:
      db.session.close()
      return 0
  else:
    total = 0
    return total, 204


def get_a_user(claimer_id):
  response_object = User.query.filter_by(claimer_id=claimer_id).first()

  if response_object:
    current_state = workflow_service.get_current_state(claimer_id)
    if current_state:
      response_object.current_state = current_state['currentState']
    return response_object
  else:
    response_object = {
      'status': 'fail',
      'message': 'user no found',
    }
    return response_object, 204


# def generate_token(user):
#     try:
#         # generate the auth token
#         auth_token = User.encode_auth_token(user.id)
#         response_object = {
#             'status': 'success',
#             'message': 'Successfully registered.',
#             'Authorization': auth_token.decode()
#         }
#         return response_object, 201
#     except Exception as e:
#         response_object = {
#             'status': 'fail',
#             'message': 'Some error occurred. Please try again.'
#         }
#         return response_object, 401

def isset(variable):
  return variable in locals() or variable in globals()


def get_user_documents(claimer_id):
  documents = cdv.query.filter_by(claimer_id=claimer_id).order_by(cdv.radication_date).all()
  if documents:
    return documents
  else:
    response_object = {
      'status': 'fail',
      'message': 'user no found',
    }
    return response_object, 204


def get_all_user_documents(claimer_id):
    try:
        query = "select vwdpc.*, dt.document_type, cd.document_format_initials, edf.document_order, edf.init_page, edf.end_page " \
                "from phase_2.vw_documents_per_claimer vwdpc " \
                "LEFT JOIN core.document_type dt ON vwdpc.document_type_id = dt.id " \
                "LEFT JOIN phase_2.claimer_documents cd ON vwdpc.document_id = cd.document_id " \
                "LEFT JOIN phase_2.expedient_document_folder edf ON vwdpc.document_id = edf.document_id " \
                "WHERE vwdpc.claimer_id = " + claimer_id + " ORDER BY edf.document_order"
        try:
            documents = db.engine.execute(query).fetchall()
            db.session.close()
        except Exception as e:
            return manage_db_exception('Error filtro', e)

        if documents:
            documents = [dict(row) for (row) in documents]
            return documents
        else:
            response_object = {
              'status': 'no_data_found',
              'message': 'No se encontraron resultados para esta búsqueda',
            }
            return response_object

    except Exception as e:
        return manage_db_exception("Error en user_service#get_all_user_documents - " +
                                   "Error al obtener los registros de la vista de documentos por claimer",
                                   'Ocurrió un error al obtener los documentos por claimer.', e)


def save_changes(data):
  db.session.add(data)
  try:
    db.session.commit()
  except Exception as e:
    newLogger = loggingBDError(loggerName='audit_service')
    db.session.rollback()
    newLogger.error(e)


# Trae todos los reclamantes que deben ser notificados por mensajería física
def get_users_for_fisical_citation(subgroup_id):
  # logger.debug('Entro a user_service#get_users_for_fisical_notification')
  try:
    # TODO: se debe hacer la consulta que se requiere PREGUNTAR TEMA DE LOS SUBGRUPOS Y cud.urban_rural_location
    query = text("select cud.claimer_id , cud.person_expedient_id ,cud.address, sc.aa_id,\
                    cud.id_natgeo , cud.urban_rural_location , cud.id_bog_loc_neig, sc.subgroup_id, sc.notification_deadlin\
                    from phase_2.claimer_user_data cud \
                    inner join phase_2.claimer_workflow cw on cud.claimer_id = cw.claimer_id \
                    inner join phase_2.citation c on c.claimer_id = cud.claimer_id\
                    inner join phase_2.subgroup_citation sc on sc.subgroup_id = c.subgroup_id\
                    where  cw.current_state = 18 and cw.end_date is null and c.subgroup_id = :subgroup_id \
                    and c.document_id is null")
    users = db.engine.execute(query, subgroup_id=subgroup_id).fetchall()
    db.session.close()
    users = json.dumps([dict(row) for (row) in users])
    return users
  except Exception as e:
    return manage_db_exception("Error en user_service#get_users_for_fisical_notification - " +
                               "Error al obtener los usuarios para notificacion fisica",
                               'Ocurrió un error al obtener los usuarios para notificacion fisica.', e)

def get_users_for_post_in_citation(claimer_id):
  try:
    query = text("select cud.claimer_id,cm.claimer_message_id, cd.document_id from phase_2.claimer_user_data cud\
                  inner join phase_2.claimer_documents cd on cud.claimer_id = cd.claimer_id\
                  inner join phase_2.claimer_message cm on cm.claimer_id = cud.claimer_id\
                  where cd.document_type_id = 23 and cud.claimer_id = :claimer_id")
    users = db.engine.execute(query, claimer_id=claimer_id).fetchone()
    db.session.close()
    #users = json.dumps([dict(row) for (row) in users])
    return users
  except Exception as e:
    return manage_db_exception("Error en user_service#get_users_for_post_in_citation - " +
                               "Error al obtener los usuarios para guardar en citation",
                               'Ocurrió un error al obtener los usuarios para guardar en citation.', e)


def get_users_for_fisical_notification():
  # logger.debug('Entro a user_service#get_users_for_fisical_notification')
  try:
    # TODO: se debe verificar que la entrega de citacion por mensajeria no fallo por direccion inexistente
    # o casa sin habitantes
    query = text("select cud.claimer_id , cud.person_expedient_id ,cud.address,\
                    cud.id_natgeo , cud.urban_rural_location , cud.id_bog_loc_neig\
                    from phase_2.claimer_user_data cud \
                    inner join phase_2.claimer_workflow cw on cud.claimer_id = cw.claimer_id \
                    where cud.address is not null and cud.email is null and cw.current_state = 15\
                    and cud.claimer_id not in (select cw.claimer_id from phase_2.claimer_workflow \
                    where cw.current_state = 8 and cw.end_date is not null)")
    users = db.engine.execute(query).fetchall()
    db.session.close()
    users = json.dumps([dict(row) for (row) in users])
    return users
  except Exception as e:
    return manage_db_exception("Error en user_service#get_users_for_fisical_notification - " +
                               "Error al obtener los usuarios para notificacion fisica",
                               'Ocurrió un error al obtener los usuarios para notificacion fisica.', e)
  try:
    db.session.commit()
  except Exception as e:
    newLogger = loggingBDError(loggerName='user_service')
    db.session.rollback()
    newLogger.error(e)


def get_users_fisical_notification_whit_address():
  # logger.debug('Entro a user_service#get_users_for_fisical_notification')
  try:
    query = text("select cud.claimer_id , cud.person_expedient_id ,cud.address,\
                    cud.id_natgeo , cud.urban_rural_location , cud.id_bog_loc_neig\
                    from phase_2.claimer_user_data cud \
                    inner join phase_2.claimer_workflow cw on cud.claimer_id = cw.claimer_id \
                    where cud.address is not null and cud.email is null and cw.current_state = 18")
    users = db.engine.execute(query).fetchall()
    db.session.close()
    users = json.dumps([dict(row) for (row) in users])
    return users
  except Exception as e:
    return manage_db_exception("Error en user_service#get_users_for_fisical_notification - " +
                               "Error al obtener los usuarios para notificacion fisica",
                               'Ocurrió un error al obtener los usuarios para notificacion fisica.', e)
  try:
    db.session.commit()
  except Exception as e:
    newLogger = loggingBDError(loggerName='user_service')
    db.session.rollback()
    newLogger.error(e)


def getTotalEmail(email):
  try:
    total = db.session.query(func.count(User.claimer_id)).filter(User.email == email)
    return {"total": total[0][0], "email": email}, 200
  except Exception as e:
    newLogger = loggingBDError(loggerName='getTotalEmail')
    db.session.rollback()
    newLogger.error(e)


def get_claimer_for_atril(doc_number):
    try:
        query = 'select cud.document_type tipo_documento, cud.document_number numero_documento, cud.full_name nombre, ' \
                'case when cn.claimer_notified is null then false  else cn.claimer_notified end notificado, ' \
                'cn.notification_type tipo_notificacion, ' \
                'to_char(cn.notification_date, \'DD/MM/YYYY - HH24:MI:SS\') fecha_notificacion, ' \
                'case when c.citation_id is null then false else true end citado, ' \
                'to_char(c.preferential_date, \'DD/MM/YYYY - HH24:MI:SS\') fecha_citacion ' \
                'from phase_2.claimer_user_data cud ' \
                'left join phase_2.claimer_notification cn on cn.claimer_id = cud.claimer_id ' \
                'left join phase_2.citation c on c.claimer_id = cud.claimer_id '
        query += " where cud.document_number = '{}'".format(doc_number)

        result = []

        response_object = db.engine.execute(text(query)).fetchall()
        db.session.close()

        if response_object:
          # result = json.dumps([dict(row) for (row) in response_object])
            for row in response_object:
                result.append(row)
            return marshal(result, UserDto.claimer_for_atril), 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'claimer no found',
            }

            return response_object, 204
    except Exception as e:
      newLogger = loggingBDError(loggerName='get_claimer_for_atril')
      db.session.rollback()
      newLogger.error(e)



def get_total_integral_search(data):
  if (data):
    data_c = {
      'doc_type': '',
      'doc_number': '',
      'first_name': '',
      'middle_name': '',
      'last_name': '',
      'maiden_name': '',
      'subjectivity_condition': '',
      'area_affected': '',
      'beneficiary': '',
      'aa': '',
      'non_former_claimer': ''

    }

    query_filter = " "
    join_filter = "LEFT JOIN phase_2.claimer_workflow cw on cud.claimer_id=cw.claimer_id and cw.end_date is null "
    state_filter = " "
    cw = " "

    copyOfData = dict(data)
    for (key, value) in copyOfData.items():
      data_c[key] = value

    if data_c['doc_type'] != '':
      query_filter += " AND upper(cud.document_type) = upper('{}') ".format(data_c['doc_type'])

    if data_c['doc_number'] != '':
      query_filter += " AND cud.document_number = '{}' ".format(data_c['doc_number'])

    if data_c['first_name'] != '':
      query_filter += " AND upper(cud.first_name) = upper('{}') ".format(data_c['first_name'])

    if data_c['middle_name'] != '':
      query_filter += " AND upper(cud.middle_name) = upper('{}') ".format(data_c['middle_name'])

    if data_c['last_name'] != '':
      query_filter += " AND upper(cud.last_name) = upper('{}') ".format(data_c['last_name'])

    if data_c['maiden_name'] != '':
      query_filter += " AND upper(cud.maiden_name) = upper('{}') ".format(data_c['maiden_name'])

    if data_c['beneficiary'] != '':
      query_filter += " AND fat.beneficiary = {} ".format(data_c['beneficiary'])

    if data_c['area_affected'] != '':
      query_filter += " AND fat.sub_group = '{}' ".format(data_c['area_affected'])

    if data_c['subjectivity_condition'] != '':
      query_filter += " AND upper(fat.sub_condition) = upper('{}') ".format(data_c['subjectivity_condition'])

    if data_c['aa'] != '':
      query_filter += " AND cud.claimer_id > 1 "

    if data_c['non_former_claimer'] != '':
      query_filter += " AND cud.non_former_claimer = {} ".format(data_c['non_former_claimer'])

    t = text("select count(cud.claimer_id) from phase_2.claimer_user_data cud " \
             "inner join phase_2.final_act_table fat on fat.expedient_id = cud.expedient_id" + query_filter)
    try:
      total = db.engine.execute(t).first()
      db.session.close()
      if total:
        return total[0]
      else:
        return 0
    except Exception as e:
      db.session.close()
      print(e)
      return 0
  else:
    total = 0
    return total, 204


def get_by_filter_integral_search(data):
  t = text('')
  if (data):
    data_c = {
      'doc_type': '',
      'doc_number': '',
      'first_name': '',
      'middle_name': '',
      'last_name': '',
      'maiden_name': '',
      'subjectivity_condition': '',
      'area_affected': '',
      'beneficiary': '',
      'aa': '',
      'non_former_claimer': '',
      'page': 1,
      'perPage': 10,
      'orderCriteria': 'TYPE',
      'sortingOrder': 'DESC',
    }

    query_filter = " "
    state_filter = " "
    cw = " "

    copyOfData = dict(data)
    for (key, value) in copyOfData.items():
      data_c[key] = value

    if data_c['page'] == 1:
      data_c['page'] = 1

    if data_c['orderCriteria'] == 'NAME':
      data_c['orderCriteria'] = 'full_name'
    elif data_c['orderCriteria'] == 'TYPE':
      data_c['orderCriteria'] = 'document_type'
    else:
      data_c['orderCriteria'] = 'document_number'

    if data_c['doc_type'] != '':
      query_filter += " AND upper(cud.document_type) = upper('{}') ".format(data_c['doc_type'])

    if data_c['doc_number'] != '':
      query_filter += " AND cud.document_number = '{}' ".format(data_c['doc_number'])

    if data_c['first_name'] != '':
      query_filter += " AND upper(cud.first_name) = upper('{}') ".format(data_c['first_name'])

    if data_c['middle_name'] != '':
      query_filter += " AND upper(cud.middle_name) = upper('{}') ".format(data_c['middle_name'])

    if data_c['last_name'] != '':
      query_filter += " AND upper(cud.last_name) = upper('{}') ".format(data_c['last_name'])

    if data_c['maiden_name'] != '':
      query_filter += " AND upper(cud.maiden_name) = upper('{}') ".format(data_c['maiden_name'])

    if data_c['beneficiary'] != '':
      query_filter += " AND fat.beneficiary = {} ".format(data_c['beneficiary'])

    if data_c['area_affected'] != '':
      query_filter += " AND fat.sub_group = '{}' ".format(data_c['area_affected'])

    if data_c['subjectivity_condition'] != '':
      query_filter += " AND upper(fat.sub_condition) = upper('{}') ".format(data_c['subjectivity_condition'])

    if data_c['aa'] != '':
      query_filter += " AND cud.claimer_id > 1  "

    if data_c['non_former_claimer'] != '':
      query_filter += " AND cud.non_former_claimer = {} ".format(data_c['non_former_claimer'])

    t = text("select cud.*,fat.sub_condition,sub_group,beneficiary,fat.id from phase_2.claimer_user_data cud \
            inner join phase_2.final_act_table fat on fat.expedient_id = cud.expedient_id "
             + query_filter + \
             "order by " + data_c['orderCriteria'] + " " + data_c[
               'sortingOrder'] + ",  expedient_id   LIMIT :per_page OFFSET :page")
    try:
      data = db.engine.execute(t, per_page=data_c['perPage'],
                               page=(data_c['page'] - 1) * data_c['perPage']).fetchall()

      db.session.close()
    except Exception as e:
      print(e)
      return manage_db_exception('Error filtro', e)

    if data:
      return data
    else:
      response_object = {
        'status': 'fail',
        'message': 'no found',
      }
      return response_object
  else:
    response_object = {
      'status': 'fail',
      'message': 'no data to find',
    }
    return response_object, 204

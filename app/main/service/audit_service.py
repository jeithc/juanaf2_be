import json
from app.main import db
from app.main.model.audit import Audit
from app.main.model.front_app_audit import FrontAudit
from app.main.model.user_audit import UserAudit
from app.main.service.logging_service import loggingBDError
import datetime
import jsonpickle
import socket

#logger = configLogging(loggerName='audit_service')

def save_new_audit_ini(request):
    user="N/A"
    if 'User-Id' in request.headers:
        if request.headers['User-Id']!='':
            user = request.headers['User-Id']
    try:
        if request.headers.environ['REQUEST_METHOD'] == 'GET':
            json_request= request.path
        else:
            json_request = json.dumps(request.json)
        if 'HTTP_X_FORWARDED_FOR' in request.headers.environ:
            ip=request.headers.environ['HTTP_X_FORWARDED_FOR']
        else:
            newLogger = loggingBDError(loggerName='audit_service_user')
            newLogger.error(request.headers.environ)

            ip = request.headers.environ['REMOTE_ADDR']
        # print(request.headers.environ['REMOTE_ADDR'])
        # print(user)
        # print(request.headers.environ['HTTP_USER_AGENT'])
        # print(request.headers.environ['REQUEST_METHOD'])
        # print(request.headers.environ['PATH_INFO'])
        # print(request.path)
        # print(request.json)
        # print(datetime.datetime.now() )

        new_audit = Audit(
            ip = ip,
            user = user,
            user_agent = request.headers.environ['HTTP_USER_AGENT'],
            method = request.headers.environ['REQUEST_METHOD'],
            service = request.url_rule.rule,
            data = json_request[:3999],
            type = 'BEFORE',
            last_updated_by = 'user-app'
        )
        save_changes(new_audit) 
        return 'ok'
    except Exception as e:
        print(e)


def save_new_audit_out(response, request):
    if 'User-Id' in request.headers:
        user = request.headers['User-Id']
    else:
        user="N/A"

    try:
        host_name = socket.gethostname() 
        host_ip = socket.gethostbyname(host_name) 
    except: 
        host_ip = 'ServerApp'

    #response= json.dumps(response)
    response = jsonpickle.encode(response)
    try:
        #print( user,request.headers.environ['REQUEST_METHOD'],request.headers.environ['PATH_INFO'],response[:990], datetime.datetime.now() )
        new_audit = Audit(
            ip = 'localhost',
            user = user,
            user_agent = host_ip,
            method = request.headers.environ['REQUEST_METHOD'],
            service = request.url_rule.rule,
            data = response[:3999],
            type = 'AFTER',
            last_updated_by = 'user-app'
        )
        save_changes(new_audit)
        return 'ok'
    except Exception as e:
            print(e)
            response_object = {
                'status': 'fail',
                'message': ' error to audit out Try again'
            }
            return response_object


def new_user_audit_init(request):
    try:
        if request.headers.environ['REQUEST_METHOD'] == 'GET':
            json_request= request.path
        else:
            json_request = json.dumps(request.json)
        if 'HTTP_X_FORWARDED_FOR' in request.headers.environ:
            ip = request.headers.environ['HTTP_X_FORWARDED_FOR']
        else:
            ip = request.headers.environ['REMOTE_ADDR']
        new_audit = UserAudit(
            ip = ip,
            user = request.headers['User-Id'],
            user_agent = request.headers.environ['HTTP_USER_AGENT'],
            method = request.headers.environ['REQUEST_METHOD'],
            service = request.url_rule.rule,
            data = json_request[:3999],
            type = 'BEFORE',
            last_updated_by = 'user-app'
        )
        save_changes(new_audit)
        return 'ok'
    except Exception as e:
        print(e)

def new_user_audit_finish(response, request):
    if 'HTTP_X_FORWARDED_FOR' in request.headers.environ:
        ip = request.headers.environ['HTTP_X_FORWARDED_FOR']
    else:
        ip = request.headers.environ['REMOTE_ADDR']

    if 'User-Id' in request.headers:
        user = request.headers['User-Id']
    else:
        user = "N/A"

    try:
        host_name = socket.gethostname() 
        host_ip = socket.gethostbyname(host_name) 
    except:
        host_ip = 'ServerApp'

    #response= json.dumps(response)
    response = jsonpickle.encode(response)
    
    try:
        #print( user,request.headers.environ['REQUEST_METHOD'],request.headers.environ['PATH_INFO'],response[:990], datetime.datetime.now() )
        new_audit = UserAudit(
            ip=ip,
            user = user,
            user_agent = host_ip,
            method = request.headers.environ['REQUEST_METHOD'],
            service = request.url_rule.rule,
            data = response[:3999],
            type = 'AFTER',
            last_updated_by = 'user-app'
        )
        save_changes(new_audit)
        return 'ok'
    except Exception as e:
            print(e)
            response_object = {
                'status': 'fail',
                'message': ' error to audit out Try again'
            }
            return response_object


def new_front_audit(request):
    try:
        new_audit = FrontAudit(
            event_date = datetime.datetime.now(),
            event_desc = json.dumps(request.json)
        )
        save_changes(new_audit) 
        return 'ok'
    except Exception as e:
        print(e)


def get_all_audits():
    return Audit.query.order_by(Audit.id.desc()).limit(30).all()


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='audit_service')
        db.session.rollback()
        newLogger.error(e)
import datetime
import hashlib
import io
import json
import os
import webbrowser
from sqlalchemy import text
import requests
from app.main.service.logging_service import loggingBDError
from app.main import db
from app.main.model.claimer_documents import Claimer_documents
from app.main.model.user import User,InitialPassword
from app.main.util.mail import send_mail_report
from app.main.config import ruta_archivos, ruta_jasper_server, usuario_jasper, contrasena_jasper
from app.main.service.s3_service import upload_file, read_content_file, copy_objects_bucket, clone_objects_bucket_rai
from xml.etree import ElementTree
from app.main.messaging.model.document_type import Document_type
from sqlalchemy import and_
from app.main.citation.model.citation import Citation, CitationManagement
from sqlalchemy.sql import text 

class ReportJasper:
    def create_report(report_request):
        body_mail='<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]}
            ]
        }
        }"""

        typeReport = report_request.get("reportType")

        user = User.query.filter_by(claimer_id=int(report_request.get("claimerId"))).first()
        
        initial_password = None
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            initial_password = InitialPassword.query.filter_by(claimer_id=user.claimer_id).first()

            documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            if not documentoEx:
                document = Claimer_documents(
                    claimer_id = user.claimer_id,
                    document_type_id = int(tr),
                    flow_document = "1",              
                    origin_document = origin_document,
                    document_state = True
                )

                ReportJasper.save_changes(document)        
                
                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                #se consulta el nombre del archivo en la tabla de documentos
                document_jasper = Document_type.query.filter_by(id=typeReport).first()

                jsonData['reportUnitUri'] = "/reports/{}".format(document_jasper.document_route)

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]

                user.registered_claimer = True

                ReportJasper.save_changes(user)

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }

                response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
                except:
                    print("An exception occurred invoke report jasper")

                now = datetime.datetime.now()

                if typeReport=="1":
                    user.authorize_electronic_notificati = True
                    user.electronic_authorization_date = now 

                report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                url_document= report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData={"url": url_document}

                report= ReportJasper.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasper.save_changes(document)

                name = "{documentId}-".format(documentId=document.document_id)+fileName
                document.url_document = store_path+name 

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(pathDocument+name)
                        document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except:
                        print("An exception occurred write report to disk")

                nombreUsuario = ""

                if user.first_name:
                    nombreUsuario=nombreUsuario+user.first_name+" "
                if user.middle_name:
                    nombreUsuario=nombreUsuario+user.middle_name+" "
                if user.maiden_name:
                    nombreUsuario=nombreUsuario+user.maiden_name+" "
                if user.last_name:
                    nombreUsuario=nombreUsuario+user.last_name+" "

                body_mail= body_mail.replace("$nombreusuario", ''.join([i if ord(i) < 128 else '' for i in nombreUsuario]))
                body_mail= body_mail.replace("$nombredoc",name)
                body_mail= body_mail.replace("$usuario",user.username)
                body_mail= body_mail.replace("$contrasena",initial_password.initial_password)

                if user.email:
                    body_mail= body_mail.replace("$email",user.email)
                else:
                    body_mail= body_mail.replace("$email","Correo no registrado")

                if user.email and not user.email.strip()=='':
                    asunto=u"Doña Juana le responde: "
                    try:
                        if typeReport=="1":
                            asunto = asunto +u"Autorización de notificación personal electrónica"
                        elif typeReport=="4":
                            asunto = asunto +u"Constancia de registro y actualización de datos"
                        send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                    except:
                        print("An exception occurred to send mail report")

                document.url_document = store_path+name

                ReportJasper.save_changes(document)
                os.remove(pathDocument+name)
                return store_path+name
            else:
                return "Ya registró un documento anteriormente"

    def view_report(report_download):
        auth = (usuario_jasper,contrasena_jasper)
        s1 = requests.Session()
        url = report_download.get('url').split("?")
        s1.cookies['JSESSIONID']=url[1]
        headersS = {"Accept": "application/json",
                "Content-Type": "application/xml"}
        ret = s1.get(url[0], headers = headersS, auth = auth)

        return ret.content

    def view_local_report(report_download):
        bytesReport=read_content_file(report_download)
        return bytesReport

    def save_changes(data):
        db.session.add(data)
        try:
            db.session.commit()
        except Exception as e:
            newLogger = loggingBDError(loggerName='jasper_serv')
            db.session.rollback()
            newLogger.error(e)


    def calculate_checksum(ret, id):
        hash = ""
        name = "p"+str(id)+".pdf"
        with open(name, "wb") as code:
            code.write(ret)
            hash = hashlib.md5(open(name, 'rb').read()).hexdigest()
        os.remove(name)
        return hash
      
    def create_report_internal(report_request):
        body_mail='<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type"><style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");ol{margin:0;padding:0}table td,table th{padding:0}.c0{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:italic}.c1{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:12pt;font-family:"Trebuchet MS";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify;height:11pt}.c5{padding-top:0pt;padding-bottom:0pt;line-height:1.0;orphans:2;widows:2;text-align:justify}.c2{font-size:12pt;font-family:"Trebuchet MS";font-style:italic;font-weight:700}.c8{font-size:12pt;font-family:"Trebuchet MS";font-weight:700}.c7{background-color:#ffffff;max-width:441.9pt;padding:70.8pt 85pt 70.8pt 85pt}.c10{color:#000000;text-decoration:none;vertical-align:baseline}.c3{font-size:12pt;font-family:"Trebuchet MS";font-weight:400}.c11{font-style:normal}.c6{background-color:#00ffff}.c9{font-style:italic}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:"Georgia";line-height:1.0791666666666666;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{margin:0;color:#000000;font-size:11pt;font-family:"Calibri"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:"Calibri";line-height:1.0791666666666666;page-break-after:avoid;orphans:2;widows:2;text-align:left}</style></head><body class="c7"><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Hola </span><span>$nombreusuario</span><span class="c1">, </span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Bienvenido a DO&Ntilde;A JUANA LE RESPONDE. Su registro se ha realizado exitosamente.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3">Para su conocimiento y fines pertinentes adjuntamos al presente correo electr&oacute;nico el documento: &ldquo;</span><span>$nombredoc</span><span class="c8">&rdquo;</span><span class="c1">. Igualmente le recordamos que los datos de acceso a su cuenta son los siguientes:</span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c8">Usuario:</span><span class="c3">&nbsp;</span><span>$usuario</span></p><p class="c5"><span class="c8">Contrase&ntilde;a:</span><span class="c3">&nbsp;</span><span>$contrasena</span></p><p class="c5"><span class="c8">Correo electr&oacute;nico:</span><span class="c3">&nbsp;</span><span>$email</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Se le advierte que la informaci&oacute;n de usuario y contrase&ntilde;a para el acceso a la plataforma es personal e intransferible, y en este sentido solamente usted es el responsable de su custodia y manejo, por lo que deber&aacute; impedir su divulgaci&oacute;n por cualquier medio.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c1">Cordialmente, </span></p><p class="c4"><span class="c1"></span></p><p class="c5" id="h.gjdgxs"><span class="c8 c10 c11">DO&Ntilde;A JUANA LE RESPONDE.</span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c4"><span class="c1"></span></p><p class="c5"><span class="c3 c9">Por favor, </span><span class="c2">no responda a este mensaje</span><span class="c3 c9 c10">, ya que este buz&oacute;n de correo electr&oacute;nico no se encuentra habilitado para recibir mensajes, pues solamente tiene funciones de distribuci&oacute;n. Si requiere m&aacute;s informaci&oacute;n, acuda a los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c3">La Defensor&iacute;a del pueblo </span><span class="c8">nunca le solicitar&aacute;</span><span class="c3">&nbsp;sus datos personales o sus credenciales de acceso a la plataforma </span><span class="c8">DO&Ntilde;A JUANA LE RESPONDE</span><span class="c3">&nbsp;mediante v&iacute;nculos de correo electr&oacute;nico. En caso de recibir alguno, rep&oacute;rtelo de inmediato a trav&eacute;s de los canales de atenci&oacute;n de la Defensor&iacute;a del Pueblo.</span></p><p class="c4"><span class="c0"></span></p><p class="c5"><span class="c2">Aviso de confidencialidad:</span><span class="c3">&nbsp;Este mensaje est&aacute; dirigido para ser usado por su destinatario(a) exclusivamente y puede contener informaci&oacute;n confidencial y/o reservada o protegida legalmente. Si usted no es el destinatario, le comunicamos que cualquier distribuci&oacute;n o reproducci&oacute;n de este, o de cualquiera de sus anexos, est&aacute; estrictamente prohibida. Si usted ha recibido este mensaje por error, por favor notif&iacute;quenos inmediatamente y elimine su texto original, incluidos los anexos, o destruya cualquier reproducci&oacute;n de este.</span></p></body></html>'
        auth = (usuario_jasper,contrasena_jasper)
        
        # Init session so we have no need to auth again and again:
        s = requests.Session()  

        data = """{
        "reportUnitUri": "",
        "async": "false",
        "freshData": "false",
        "saveDataSnapshot": "false",
        "outputFormat": "pdf",
        "interactive": "true",
        "ignorePagination": "false",
        "parameters": {
            "reportParameter": [
                {"name": "param_radicate_number","value": [""]},
                {"name": "param_claimer_id","value": [""]},
                {"name": "param_subgroup_id","value": [""]}
            ]
        }
        }"""

        typeReport = report_request.get("reportType")

        user = User.query.filter_by(claimer_id=int(report_request.get("claimerId"))).first()
        
        initial_password = None
        origin_document = "SISTEMA"
        tr = typeReport

        if user:
            if typeReport == "1" or typeReport ==  "2":
                initial_password = InitialPassword.query.filter_by(claimer_id=user.claimer_id).first()

            documentoEx = Claimer_documents.query.filter_by(claimer_id=user.claimer_id, document_type_id=int(typeReport)).first()

            if documentoEx:
                document = documentoEx
                
                jsonData = json.loads(data)
                url = ruta_jasper_server+"reportExecutions"

                document_jasper = Document_type.query.filter_by(id=typeReport).first()

                jsonData['reportUnitUri'] = "/reports/{}".format(document_jasper.document_route)

                jsonData['outputFormat'] = "pdf"
                jsonData['parameters']['reportParameter'][0]['value'] = [str(document.radicate_number)]
                jsonData['parameters']['reportParameter'][1]['value'] = [user.claimer_id]

                if typeReport == '23':
                    jsonData['parameters']['reportParameter'][2]['value'] = [report_request.get("subgroupId")]

                if typeReport == "1" or typeReport ==  "2":
                    user.registered_claimer = True
                    ReportJasper.save_changes(user)

                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                }

                print(json.dumps(jsonData))

                response = s.post(url, data=json.dumps(jsonData), headers=headers, auth=auth)
                dataResponse = response.json()
                sessionid = s.cookies.get('JSESSIONID')
                request_id = ""
                export_id = ""
                number_of_pages = 0
                fileName = ""

                try:
                    request_id = dataResponse.get("requestId")
                    export_id = dataResponse.get("exports")[0].get("id")
                    number_of_pages = dataResponse.get("totalPages")
                    fileName = dataResponse.get("exports")[0].get("outputResource").get("fileName")
                except:
                    print("An exception occurred invoke report jasper")

                now = datetime.datetime.now()

                if typeReport=="1":
                    user.authorize_electronic_notificati = True
                    user.electronic_authorization_date =  document.radication_date

                report_url = ruta_jasper_server+"reportExecutions/{request_id}/exports/{export_id}/outputResource".format(request_id=request_id, export_id=export_id)
                url_document= report_url+"?"+sessionid
                document.metadata_server_document = json.dumps(dataResponse)
                document.number_of_pages = number_of_pages
                urlData={"url": url_document}

                report= ReportJasper.view_report(urlData)

                base_dir = ruta_archivos
                store_path = "{claimerId}/{origen}/{tipologiaDoc}/{date}/".format(claimerId=user.claimer_id, origen=origin_document, tipologiaDoc=typeReport, date= now.strftime('%Y%m%d') )
                pathDocument = base_dir+store_path

                if not os.path.exists(pathDocument):
                    os.makedirs(pathDocument)

                ReportJasper.save_changes(document)

                name = "{documentId}-".format(documentId=document.document_id)+fileName
                document.url_document = store_path+name 

                with open(pathDocument+name, "wb") as code:
                    try:
                        code.write(report)
                        document.document_size = os.path.getsize(pathDocument+name)
                        document.document_checksum = hashlib.md5(open(pathDocument+name, 'rb').read()).hexdigest()
                        upload_file(store_path, name, pathDocument+name)
                    except:
                        print("An exception occurred write report to disk")

                if typeReport== "1" or typeReport ==  "2":

                    nombreUsuario = ""

                    if user.first_name:
                        nombreUsuario=nombreUsuario+user.first_name+" "
                    if user.middle_name:
                        nombreUsuario=nombreUsuario+user.middle_name+" "
                    if user.maiden_name:
                        nombreUsuario=nombreUsuario+user.maiden_name+" "
                    if user.last_name:
                        nombreUsuario=nombreUsuario+user.last_name+" "

                    body_mail= body_mail.replace("$nombreusuario", ''.join([i if ord(i) < 128 else '' for i in nombreUsuario]))
                    body_mail= body_mail.replace("$nombredoc",name)
                    body_mail= body_mail.replace("$usuario",user.username)
                    body_mail= body_mail.replace("$contrasena",initial_password.initial_password)
                    if user.email:
                        body_mail= body_mail.replace("$email",user.email)
                    else:
                        body_mail= body_mail.replace("$email","Correo no registrado")

                    if user.email and not user.email.strip()=='':
                        asunto=u"Doña Juana le responde: "
                        try:
                            if typeReport=="1":
                                asunto = asunto +u"Autorización de notificación personal electrónica"
                            elif typeReport=="4":
                                asunto = asunto +u"Constancia de registro y actualización de datos"
                            send_mail_report(asunto,body_mail,pathDocument+name,user.email,name)
                        except:
                            print("An exception occurred to send mail report")

                document.url_document = store_path+name

                ReportJasper.save_changes(document)
                os.remove(pathDocument+name)
                return store_path+name

    def copy_objects_bucket(document, subgroup, fecha):
        # documentos = Claimer_documents.query.filter(and_(Claimer_documents.claimer_id.in_(
        #     (600090,602341,604940,608873,611432,602341,606924,606155,602201,600746,6100023,607628)
        #     ),Claimer_documents.document_type_id.in_((23)))).all()

        query= text('select cd.* from phase_2.claimer_documents cd \
        inner join phase_2.citation c on c.claimer_id = cd.claimer_id \
        where c.subgroup_id = :subgroup and cd.document_type_id = :document')
        documentos = db.engine.execute(query.execution_options(autocommit=True), subgroup=subgroup, document=document).fetchall()
        db.session.close()

        # documentos = Claimer_documents.query.join(CitationManagement, CitationManagement.claimer_id==Claimer_documents.claimer_id)\
        #     .filter(CitationManagement.subgroup_id == subgroup, Claimer_documents.document_type_id == document).all()

        for documento in documentos:
            copy_objects_bucket(subgroup,fecha, documento.url_document)
        return "Copy S3 Successfull"

    def copy_objects_bucket_protopre(typeDocument):
        # documentos = Claimer_documents.query.filter(Claimer_documents.document_type_id.in_((typeDocument.get("typeDocument")))).all()

        query = 'select * from tempwork.paso_documentos_bucket_20191030 where url_document is not null'
        documentos=db.engine.execute(text(query).execution_options(autocommit=True)).fetchall()
        for documento in documentos:
            clone_objects_bucket_rai(documento.url_document)
        return "Clone S3 Successfull"

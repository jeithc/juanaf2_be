from app.main.model.notorious_facts_places import NotoriousFactsPlaces


def get_list():
    notorious_facts_places_list = []
    try:
        notorious_facts_places_list = NotoriousFactsPlaces.query.order_by(NotoriousFactsPlaces.notorious_facts_name).all()
    except Exception as e:
        print(e)
    return notorious_facts_places_list

from datetime import datetime
from app.main import db
from app.main import logger
from app.main.notification.util.dto.attorney_dto import AttorneyDto
from app.main.notification.model.attorney import Attorney
from app.main.radication.model.claimer_resource_document import ClaimerResourceDocument


def save_new_attorney(data):
    print(data)
    logger.debug("come in  #save_new_attrorney")
    attorney = Attorney(

        document_type=data['documentType'],
        document_number=data['documentNumber'],
        professional_card_number=data['professionalCardNumber'],
        attorney_type=data['attorneyType'],
        first_name=data['firstName'],
        last_name=data['lastName'],
        middle_name=data['middleName'],
        maiden_name=data['maidenName'],
        phone_1=data['phone1'],
        phone_2=data['phone2'],
        id_natgeo=data['natgeoId'],
        id_bog_loc_neig=data['bogLocNeigId'],
        urban_rural_location=data['urbanRuralLocation'],
        created_at=datetime.now(),
        updated_at=datetime.now(),
        email=data['email'],
        country=data['country'],
        city=data['city']

    )

    if 'address' in data and data['address']:
        attorney.address = data['address']

    try:
        db.session.add(attorney)
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': 'attorney almacenado correctamente ',
            'attorney_id': attorney.attorney_id
        }
        return response_object, 201
    except Exception as e:
        logger.error('Error al guardar attorney' + str(e))
        db.session.rollback()


def update_attorney(data):
    logger.debug('come in #update_attorney')


def get_all_attorneys():
    logger.debug('come in #get_attorney')
    attorneys = Attorney.query.all()
    return attorneys


def get_attorney_by_id(resource_id):
    logger.debug('come in #get_attorney_by_id')
    attorney = Attorney.query.join(
        ClaimerResourceDocument, Attorney.attorney_id == ClaimerResourceDocument.attorney_id
    ).filter(ClaimerResourceDocument.resource_id == resource_id).first()
    return attorney

from app.main import Flask
from app.main import db
from app.main import logger
from app.main.notification.model.claimerWithdrawal import ClaimerWithdrawal
from app.main.core.service.general_service import manage_db_exception


def save_new_claimer_whitedrawal(data):
    claimer_whitedrawal = ClaimerWithdrawal()

    if 'claimerId' in data and data['claimerId']:
        claimer_whitedrawal.claimer_id = data['claimerId']
    if 'claimerWithdrawalDate' in data and data['claimerWithdrawalDate']:
        claimer_whitedrawal.claimer_withdrawal_date = data['claimerWithdrawalDate']
    if 'userId' in data and data['userId']:
        claimer_whitedrawal.user_id = data['userId']
    if 'documentWithdrawalId' in data and data['documentWithdrawalId']:
        claimer_whitedrawal.document_withdrawal_id = data['documentWithdrawalId']
    if 'withdrawalReason' in data and data['withdrawalReason']:
        claimer_whitedrawal.withdrawal_reason = data['withdrawalReason']
    if 'numberOfAnnexes' in data and data['numberOfAnnexes']:
        claimer_whitedrawal.number_of_annexes = data['numberOfAnnexes']
    if 'numberOfPages' in data and data['numberOfPages']:
        claimer_whitedrawal.number_of_pages = data['numberOfPages']

    try:
        db.session.add(claimer_whitedrawal)
        db.session.commit()
        # print(claimer_whitedrawal)
        response_object = {
            'status': 'success',
            'message': 'claimer_notification almacenado correctamente',
        }
        return response_object, 200

    except Exception as e:
        db.session.rollback()
        return manage_db_exception('Error en # save_new_claimer_whitedrawal - ',
            'Error al guardar save_new_claimer_whitedrawal', e) 

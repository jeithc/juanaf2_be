from app.main import db
from app.main import logger
import datetime
from sqlalchemy import and_, text
import io
import os
import base64
from app.main.config import temp_server_path
from app.main.service.jasper_serv import ReportJasper
from app.main.notification.model.claimer_notification import ClaimerNotification
from app.main.citation.model.aa import Aa
from app.main.model.claimer_documents import Claimer_documents
from app.main.notification.service import attorney_service
from app.main.service import climer_documents_service
from app.main.core.service import report_service
from app.main.core.service import file_service
from app.main.service import user_service
from app.main.core.util.dto.document_dto import DocumentDTO
from flask_restplus import marshal
from app.main.notification.model.reprocces_notification import ReprocessNotification
from app.main.service.logging_service import loggingBDError, configLoggingError
from app.main.core.model.users import Users
from app.main.core.service.workflow_service import make_claimer_transition
from app.main.core.model.claimer_workflow import ClaimerWorkflow
from app.main.model.user import User
from app.main.mail.service.build_message_helper import email_metadata, build_body_mail
from app.main.mail.service.mail_service import send_email_user_multidocument
from app.main.model.claimer_documents import Claimer_documents_view as cdv
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.core.service.general_service import manage_db_exception
from app.main.core.model.users import Users
from app.main.radication.service.radicates_table_service import get_next_business_days


def save_claimer_notification(data, user_email):
    logger.debug('come in save_claimer_notification')

    claimer_notification = ClaimerNotification()

    if 'attorneyDocumentId' in data and data['attorneyDocumentId']:
        claimer_notification.url_attorney_document = data['attorneyDocumentId']
    if 'notificationDocumentId' in data and data['notificationDocumentId']:
        claimer_notification.notification_document_id = data['notificationDocumentId']
    if 'signNotificationDocumentId' in data and data['signNotificationDocumentId']:
        claimer_notification.sign_notification_document_id = data['signNotificationDocumentId']
    if 'claimerId' in data and data['claimerId']:
        claimer_notification.claimer_id = data['claimerId']
    if 'notificationType' in data and data['notificationType']:
        claimer_notification.notification_type = data['notificationType']
    if 'hasAttorney' in data and data['hasAttorney']:
        claimer_notification.has_attorney = data['hasAttorney']
    if 'notificationDate' in data and data['notificationDate']:
        claimer_notification.notification_date = data['notificationDate']
    if 'aaId' in data:
        claimer_notification.aa_id = data['aaId']
    if 'waiverOfTerms' in data:
        claimer_notification.waiver_of_terms = data['waiverOfTerms']

    user_system = Users.query.filter_by(email=user_email).first()
    if user_system:
        claimer_notification.user_id = user_system.id
    else:
        claimer_notification.user_id = 9999

    try:

        if claimer_notification.waiver_of_terms:
            claimer_notification.waiver_of_terms_date = datetime.datetime.today()

        if 'attorney' in data and data['attorney']:
            attorney = attorney_service.save_new_attorney(data['attorney'])
            claimer_notification.attorney_id = attorney[0]['attorney_id']

        save_changes(claimer_notification)

        response_object = {
            'status': 'success',
            'message': 'claimer_notification almacenado correctamente',
        }

        return response_object, 201

    except Exception as e:
        db.session.rollback()
        return logger.error('Error al guardar claimer_notification ' + str(e)), 500


def get_claimer_notification(claimer_id, aa_id):
    logger.debug('come in #get_claimer_notification')
    try:
        claimer_notification = ClaimerNotification.query.filter(
            and_(ClaimerNotification.claimer_id == claimer_id, ClaimerNotification.aa_id == aa_id,
                 ClaimerNotification.claimer_notified!=False)).first()
        return claimer_notification
    except Exception as e:
        return logger.error('Error al consultar claimer_notification ' + str(e)), 500


def updated_claimer_notification(data):
    logger.debug('come in #updated_claimer_notification')

    try:
        reprocess= ReprocessNotification.query.filter_by(claimer_id = data['claimerId'], reprocess_type = True).first()

        if reprocess:

            query= text('select max(notification_id) as max from phase_2.claimer_notification\
                where claimer_id = :id and aa_id = :aaid')
            maxid = db.engine.execute(query.execution_options(autocommit=True),  id=data['claimerId'], aaid= data['aaId']).fetchone()
            db.session.close()

            notification = ClaimerNotification.query.filter(ClaimerNotification.notification_id == maxid[0]).first()
        else:
            notification = ClaimerNotification.query. \
            filter(ClaimerNotification.claimer_id == data['claimerId'],
                   ClaimerNotification.aa_id == data['aaId']).first()
            if not notification:
                response_object = {
                    'status': 'fail',
                    'message': 'claimer_notification no existe'
                }
                return response_object, 500

        notification.url_attorney_document = data['attorneyDocumentId']
        notification.sign_notification_document_id = data['signNotificationDocumentId']
        notification.notification_document_id = data['notificationDocumentId']
        notification.notification_date = datetime.datetime.today()
        notification.claimer_notified = True
        if 'waiverOfTerms' in data:
            notification.waiver_of_terms = data['waiverOfTerms']

        if notification.waiver_of_terms:
            notification.waiver_of_terms_date = datetime.datetime.today()

        save_changes(notification)
        response_object = {
            'status': 'success',
            'message': 'claimer_notification actualizado correctamente'
        }
        return response_object, 201

    except Exception as e:
        db.session.rollback()
        return logger.error('Error al actualizar claimer_notification' + str(e)), 500


def generate_document(data):
    logger.debug('come in #generate_document')

    try:

        document = Claimer_documents(
            claimer_id=data['claimerId'],
            document_type_id=data['documentTypeId'],
            expedient_entry_date=data['expedientEntryDate'],
            flow_document=data['flowDocument'],
            number_of_pages=data['numberOfPages'],
            origin_document=data['originDocument'],
            document_state=True
        )

        save_changes(document)

        user = Users.query.filter_by(id=data['userId']).first()

        # generate report,
        params = {'param_claimer_id': document.claimer_id,
                  'param_radicate_number': document.radicate_number,
                  'param_user_name': '{} {}'.format(user.names, user.surnames),
                  'param_user_document': user.identification_number
                  }

        report = report_service.create_report(report_service.get_report_route(
            document.document_type_id), params, 'pdf')

        # document.url_document = "/doc_noti.pdf"
        # document.number_of_pages = report.number_of_pages
        # document.metadata_server_document = report['metadata_server_document']

        file_service.save_report_file(report, document.document_id)
        db.session.refresh(document)

        document = climer_documents_service.document_by_id(document.document_id)[0]

        return marshal(document, DocumentDTO.document), 200

    except Exception as e:
        db.session.rollback()
        return logger.error('Error al generar el documento' + str(e)), 500


def save_reprocess(data):
    # se busca el claimer id que hace el reproceso
    document= Claimer_documents.query.filter_by(document_id=data['documentId']).first()
    # se verifica la notificación al usuario
    notification=ClaimerNotification.query.filter(ClaimerNotification.claimer_id == document.claimer_id,
    ClaimerNotification.aa_id ==data['aaId'], ClaimerNotification.claimer_notified.is_(True)).first()
    # notification = ClaimerNotification.query.filter(ClaimerNotification.claimer_id ==document.claimer_id,        ClaimerNotification.aa_id==data.aa_id).first()

    if notification:

        data_workflow = {
            'claimerId': document.claimer_id,
            'nextState': 21,
            'nextUser': 0,
            'logUser': data['userId'],
        }
        make_claimer_transition(data_workflow)
        
        if (data['reprocessCausal']=='3'):
            # user = update(User).where(User.claimer_id==document.claimer_id).values(cause_non_registration='PERSONA FALLECIDA')
            user= User.query.filter_by(claimer_id=document.claimer_id).first()
            user.cause_non_registration='PERSONA FALLECIDA'
            save_changes(user)
            #si es fallecido lo muevo a nuevo estado 
            data_workflow = {
                'claimerId': document.claimer_id,
                'nextState': 15,
                'nextUser': 0,
                'logUser': data['userId'],
            }
            make_claimer_transition(data_workflow)

        reprocess = ReprocessNotification(
            document_id= data['documentId'],
            reprocess_causal= data['reprocessCausal'],
            aa_id=data['aaId'],
            user_id= data['userId'],
            claimer_id= document.claimer_id,
            old_notification_date = notification.notification_date,
            previous_notification_id = notification.notification_id,
            reprocess_type = True
        )
        save_changes(reprocess)

        # se guarda en false la notificación al usuario
        notification.claimer_notified=False
        save_changes(notification)

        return 'ok'
    else:
        return 'claimer no aparece notificado'


def send_notification(workflow_state, email_id, limit):
    # verifico que el claimer este en el estado necesario
    # claimers = User.query.join(ClaimerWorkflow, ClaimerWorkflow.claimer_id == User.claimer_id)\
    #     .filter(ClaimerWorkflow.current_state == workflow_state,
    #     ClaimerWorkflow.end_date == None, User.authorize_electronic_notificati == True).all()
    # verifico los claimers que estan en estado necesario y no sse les ha enviado mail

    # query= text('select cud.* from phase_2.claimer_user_data cud\
    #     inner join phase_2.claimer_workflow cw on cud.claimer_id = cw.claimer_id\
    #     where cw.current_state = :state and cw.end_date is null and\
    #     cud.claimer_id not in (select claimer_id from phase_2.claimer_message cm\
    #     where cm.claimer_message_mode = :mode and CM.claimer_message_type = :type)')
    
    query= text('select  cud.* from (	select cw.claimer_id	from phase_2.claimer_workflow  cw	\
        where cw.current_state = 2 and cw.end_date is null ) cw left outer join (select claimer_id 	 \
        from phase_2.claimer_message cm  where cm.claimer_message_mode = :mode and CM.claimer_message_type = :type) cm on  \
        cw.claimer_id = cm.claimer_id inner join phase_2.claimer_user_data cud on cud.claimer_id = cw.claimer_id where cm.claimer_id is null')
    claimers = db.engine.execute(query.execution_options(autocommit=True), mode='ELECTRONICO', type='NOTIFICATION').fetchall()
    db.session.close()

    result = {
        'send': 0,
        'no_send': 0,
        'list_no_send': [],
        'result_send': []
    }
    if claimers:
        # saco los datos de metadata para el mail necesario
        data_send_mail = email_metadata(email_id)
        if data_send_mail:
            # recorro el proceso para llamar a cada claimer
            sendmails = []
            result_send = []
            data_body = data_send_mail.copy()
            for claimer in claimers:
                data_send_mail = data_body.copy()

                # creo el reporte
                # ReportJasperMasivo.create_report(13, claimer.claimer_id)
                # se construye el body cambiando los datos necesarios
                data_send_mail['body']=build_body_mail(data_send_mail['body'],email_id, claimer)
                data_add = {
                    'claimer': claimer,
                }
                data_send_mail.update(data_add)
                print(data_send_mail)
                # SE ENVIÁ EL MAIL CON LOS DATOS DE data_send_mail
                documents = documents_s3(claimer.claimer_id)
                send_result = ''
                print('salio del documents_s3 en linea 263 claimer_notification_service')
                if documents != 'error':
                    send_result = send_email_user_multidocument(
                        data_send_mail, documents)
                else:
                    sendmails.append(claimer.claimer_id)
                if (send_result != '' and send_result['code'] == 200):
                    delete_claimer(claimer.claimer_id)
                    result['send'] = result['send'] + 1
                    result_send.append(send_result['message'])
                else:
                    result_send.append(send_result['message'])
                    result['no_send'] = result['no_send'] + 1
            result['list_no_send'] = sendmails
            result['result_send'] = result_send
            return result
        else:
            return "error al consultar datos de mail o adjuntar documentos"
    else:
        return 'no se encontraron usuarios para envió'  


def delete_claimer(claimer_id):
    query= text('delete from tempwork.claimer_state_2_to_notification where claimer_id = :id')
    db.engine.execute(query.execution_options(autocommit=True), id=claimer_id)
    db.session.close()
    return True
    

def resend_notification(workflow_state, email_id):
    # verifico que el claimer este en el estado necesario
    query= text('select cud.* from phase_2.claimer_user_data cud\
        inner join phase_2.claimer_message cm on cm.claimer_id = cud.claimer_id\
        where cm.claimer_message_mode = :mode and cm.claimer_message_type = :type')
    claimers = db.engine.execute(query.execution_options(autocommit=True),  mode='ELECTRONICO', type= 'NOTIFICATION').fetchall()
    db.session.close()

    result = {
        'send': 0,
        'no_send': 0,
        'list_no_send': [],
        'result_send': []
    }
    if claimers:
        # saco los datos de metadata para el mail necesario
        data_send_mail = email_metadata(email_id)
        if data_send_mail:
            # recorro el proceso para llamar a cada claimer
            sendmails = []
            result_send = []
            data_body = data_send_mail.copy()
            for claimer in claimers:
                data_send_mail = data_body.copy()
                notificated = ClaimerNotification.query.filter(ClaimerNotification.notification_type =='CONDUCTA CONCLUYENTE',
                    ClaimerNotification.claimer_id ==claimer.claimer_id).first()
                
                if not notificated:
                    # se construye el body cambiando los datos necesarios
                    data_send_mail['body']=build_body_mail(data_send_mail['body'],email_id, claimer)
                    data_add = {
                        'claimer': claimer,
                    }
                    data_send_mail.update(data_add)
                    print(data_send_mail)
                    # SE ENVIÁ EL MAIL CON LOS DATOS DE data_send_mail
                    documents = documents_s3(claimer.claimer_id)
                    send_result = ''
                    if documents != 'error':
                        send_result = send_email_user_multidocument(
                            data_send_mail, documents, True)
                    else:
                        sendmails.append(claimer.claimer_id)
                    if send_result['code'] == 200:
                        result['send'] = result['send'] + 1
                        result_send.append(send_result['message'])
                    else:
                        result_send.append(send_result['message'])
                        result['no_send'] = result['no_send'] + 1
                result['list_no_send'] = sendmails
                result['result_send'] = result_send
            return result
        else:
            return "error al consultar datos de mail o adjuntar documentos"
    else:
        return 'no se encontraron usuarios para envió'








def documents_s3(user):
    try:
        db_documents = cdv.query.filter(
            cdv.claimer_id == user, cdv.document_type_id.in_([31, 32])).all()
        print(db_documents)

        documents_mail = []
        id_documents = []
        for document in db_documents:
            # if document.document_type_id == 13:
            #     notification = ClaimerNotification(
            #         claimer_id=document.claimer_id,
            #         notification_type='NOTIFICATION',
            #         has_attorney=False,
            #         notification_date=datetime.datetime.now(),
            #         notification_document_id=document.document_id,
            #         claimer_notified=True
            #     )
            #     result = save_changes(notification)

            id_documents.append(document.document_id)
            # TODO query claimer_documents, all documents are pdf
            path, filename = get_filename_parts(document)
            b64_file = s3_documents_download(path, filename)
            print('creo el base 64 linea 372 claimer_notification_service')
            if b64_file != 'error':
                documents_mail.append(b64_file)
            else:
                return 'error'
                # return documents
        return {'documents_mail': documents_mail,
                'id_documents': id_documents}
    except Exception as e:
        newLogger = configLoggingError(
            loggerName='claimer notification service-documents_s3')
        newLogger.error(e)
        print(e)
        return 'error'


def get_filename_parts(document):
    full_path = document.url_document
    filename_start = full_path.rfind("/")
    return full_path[:filename_start], full_path[filename_start + 1:]


def s3_documents_download(path, filename):
    try:
        dataurl = "{}/{}".format(path, filename)  # pathReport
        print(dataurl)
        input_file = ReportJasper.view_local_report(dataurl)
        now = datetime.datetime.now()
        path = '-3/SISTEMA/MAIL/{}'.format(now.strftime('%Y%m%d'))
        base_path = temp_server_path
        local_path = base_path +  path
        if not os.path.exists(local_path):
            os.makedirs(local_path)
        with open("{}/{}".format(local_path, filename), 'wb') as f:
            f.write(input_file)
            encoded_string = str(
                base64.b64encode(open("{}/{}".format(local_path, filename), "rb").read()).decode("ascii"))
            data = {
                "name": filename,
                "data": encoded_string
            }
        try:
            if os.path.exists("{}/{}".format(local_path, filename)):
                os.remove("{}/{}".format(local_path, filename)) 
        except: 
            print('error al borrar')
        return data
    except Exception as e:
        newLogger = configLoggingError(
            loggerName='claimer notification service-download s3 document')
        newLogger.error(e)
        print(e)
        return 'error'


def generate_patch(document):
    now = datetime.datetime.now()
    path = os.path.sep.join(
        [-3, document.origin_document.strip(), str(document.document_type_id), now.strftime('%Y%m%d'), ''])


def conclusive_behavior_notify(data):
    #se agrega para evitar conducta concluyente
    return True
    logger.debug('come in conclusive_notify')
    aa = Aa.query.filter_by(aa_id=1).first()

    try:

        document = Claimer_documents(
            claimer_id=data['claimerId'],
            document_type_id=data['documentTypeId'],
            expedient_entry_date=datetime.datetime.now(),
            flow_document=1,
            origin_document='SISTEMA',
            document_state=True
        )

        if 'userId' in data:
            user = Users.query.filter_by(id=data['userId']).first()
            if not user:
                user_id = 0
            else:
                user_id = user.id
        else:
            user_id = 0

        save_changes(document)
        db.session.refresh(document)

        if 'resourceId' in data:
            resource_id = data['resourceId']
        else:
            resource_id = ''

        # generate report,
        params = {'param_claimer_id': document.claimer_id,
                  'param_radicate_number': document.radicate_number,
                  'param_aa_number': aa.aa_number,
                  'param_aa_description': aa.description,
                  'param_aa_expedition_date': aa.expedition_date.strftime("%Y"),
                  'param_resource_id': resource_id
                  }

        report = report_service.create_report(report_service.get_report_route(
            document.document_type_id), params, 'pdf')

        if report is not None:
            file_service.save_report_file(report, document.document_id)

            document = climer_documents_service.document_by_id(document.document_id)[
                0]
            notify = ClaimerNotification(
                claimer_id=data['claimerId'],
                notification_type='CONDUCTA CONCLUYENTE',
                has_attorney=False,
                notification_date=datetime.datetime.now(),
                notification_document_id=document.document_id,
                claimer_notified=True,
                aa_id=1,
                user_id=10000,
            )
            if data['documentTypeId'] == 17:
                notify.waiver_of_terms = True
                notify.waiver_of_terms_date = datetime.datetime.now()

            save_changes(notify)
            workflow_params = {
                'claimerId': data['claimerId'],
                'nextState': 14,
                'nextUser': user_id,
                'logUser': user_id
            }
            make_claimer_transition(workflow_params)

            return True

    except Exception as e:
        return manage_db_exception("Error en claimer_notification_service#conclusive_behavior_notify - " +
                                   "Error al notificar por conducta conluyente",
                                   'Error al notificar por conducta conluyente.', e)


def save_changes(data):
    db.session.add(data)
    try:
        db.session.commit()
    except Exception as e:
        newLogger = loggingBDError(loggerName='claimer notification service')
        db.session.rollback()
        newLogger.error(e)


def waiverOfTerms(claimer):
    try:
        notification = ClaimerNotification.query.filter_by(
            claimer_id=claimer['claimerId']).first()

        if notification is None:
            data = {
                'claimerId': claimer['claimerId'],
                'documentTypeId': 17,
            }
            conclusive_behavior_notify(data)

        else:
            document = Claimer_documents(
                claimer_id=claimer['claimerId'],
                flow_document=2,
                number_of_pages=1,
                origin_document='SISTEMA',
                document_state=True,
                document_type_id=20
            )
            notification.waiver_of_terms = True
            notification.waiver_of_terms_date = datetime.datetime.now()
            save_changes(notification)
            save_changes(document)
            db.session.refresh(document)

            params = {
                'param_claimer_id': claimer['claimerId'],
                'param_radicate_number': document.radicate_number
            }
            report = report_service.create_report(report_service.get_report_route(
                document.document_type_id), params, 'pdf')

            file_service.save_report_file(report, document.document_id)

        data_workflow = {
            'claimerId': claimer['claimerId'],
            'nextState': 22,
            'nextUser': 0,
            'logUser': 0,
        }

        return make_claimer_transition(data_workflow), 200

    except Exception as e:
        db.session.rollback()
        return logger.error('Error al generar el documento ' + str(e)), 500



def save_exceptional(data):
    # se busca el claimer id que hace el reproceso
    document= Claimer_documents.query.filter_by(document_id=data['documentId']).first()

    notification=ClaimerNotification.query.filter(ClaimerNotification.claimer_id == document.claimer_id,
    ClaimerNotification.aa_id ==data['aa_id']).first()

    exceptional = ReprocessNotification(
        document_id=data['documentId'],
        reprocess_causal=data['reprocessCausal'],
        aa_id=data['aa_id'],
        user_id= data['userId'],
        claimer_id = document.claimer_id,
        old_notification_date = notification.notification_date,
        previous_notification_id = notification.notification_id,
        reprocess_type = False

    )
    save_changes(exceptional)

    # claimerWorkflow = ClaimerWorkflow.query.filter(and_(ClaimerWorkflow.current_state.in_(
    # [3]),ClaimerWorkflow.end_date==None,ClaimerWorkflow.claimer_id==document.claimer_id)).first()
    
    if notification: 
        notification.claimer_notified = True
        datenow= datetime.datetime.now()
        date_notification=get_next_business_days(datenow,1)
        notification.notification_date = date_notification[0].business_day_date
        # notification.notification_document_id = document.document_id
    else:
        notification = ClaimerNotification(
            claimer_id = document.claimer_id,
            notification_type='RECONTEO',
            has_attorney = False,
            notification_date = data['newDateNotificacion'],
            notification_document_id = document.document_id, 
            aa_id = 1,
            claimer_notified = True
        )

    save_changes(notification)  

    # if claimerWorkflow:
    #     claimerWorkflow.end_date = datetime.datetime.now()
    #     save_changes(claimerWorkflow)

    # claimerWorkflowC = ClaimerWorkflow(
    #     claimer_id = document.claimer_id,
    #     user_id = 1,
    #     current_state = 3,
    #     start_date = datetime.datetime.now(),        
    #     end_date = None,
    #     triggered_event_user = 0
    # )

    # save_changes(claimerWorkflowC)         

    # nextstate =3

    # data_workflow = {
    #     'claimerId': document.claimer_id,
    #     'nextState': nextstate,
    #    'nextUser': 0,
    #     'logUser': 0,
    # }

    # make_claimer_transition(data_workflow)

    return 'ok'
        
        


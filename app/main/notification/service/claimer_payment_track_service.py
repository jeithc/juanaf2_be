from app.main import logger, db
from datetime import datetime
from app.main.core.service.general_service import manage_db_exception
from app.main.notification.model.claimer_payment_track import ClaimerPaymentTrack


def make_payment_transition(data):
    logger.debug('Entro a claimer_payment_state_service#make_payment_transition')

    claimer_id = data['claimerId']
    next_state = data['paymentState']
    user_id = data['userId']

    try:
        current_state = get_current_state(claimer_id)

        if not current_state:
            response_object = {
              'status': 'fail',
              'message': 'El expediente no está registrado en ningun estado.'
            }
            return response_object, 200

        if current_state.payment_state == next_state:
            response_object = {
              'status': 'success',
              'message': 'El expediente ya esta en ese estado.'
            }
            return response_object, 200

        return advance_state(claimer_id, next_state, current_state.payment_state, user_id)

    except Exception as e:
        return manage_db_exception("Error en claimer_payment_state_service#make_payment_transition - " +
                                 "Error al consultar el estado de pago solicitante",
                                 'Ocurrió un error al consultar el estado de pago del solicitante.', e)


def get_current_state(claimer_id):
    logger.debug('Entro a claimer_payment_state_service#get_current_state')

    try:
        payment_state = ClaimerPaymentTrack.query \
          .filter(ClaimerPaymentTrack.claimer_id == claimer_id).filter(ClaimerPaymentTrack.end_date==None).first()

        if not payment_state:
            return {}

        return payment_state

    except Exception as e:
        return manage_db_exception("Error en claimer_payment_state_service#get_current_state - " +
                                   "Error al consultar el estado de pago solicitante",
                                   'Ocurrió un error al consultar el estado de pago del solicitante.', e)


def advance_state(claimer_id, next_state, current_state, user_id):
    logger.debug('Entro a claimer_payment_state_service#advance_state')

    try:
        payment_state = ClaimerPaymentTrack.query.filter(
          ClaimerPaymentTrack.claimer_id == claimer_id).filter(ClaimerPaymentTrack.payment_state == current_state).filter(
            ClaimerPaymentTrack.end_date.is_(None)).first()

        payment_state.end_date = datetime.now()
    except Exception as e:
        return manage_db_exception("Error en expedient_state_service#advance_state - " +
                                   "Error al actualizar el estado de pago solicitante",
                                   'Ocurrió un error al actualizar el estado de pago del solicitante.', e)

    try:
        new_payment_state = ClaimerPaymentTrack(
          claimer_id=claimer_id,
          payment_state=next_state,
          start_date=datetime.now(),
          user_id=user_id
        )

        db.session.add(new_payment_state)
        db.session.commit()

        response_object = {
          'status': 'success',
          'message': 'El estado de pago se avanzó exitosamente.'
        }
        return response_object, 200
    except Exception as e:
        return manage_db_exception("Error en claimer_payment_state_service#advance_state - " +
                                   "Error al crear el nuevo estado de pago del solicitante",
                                   'Ocurrió un error al crear el nuevo estado de pago del solicitante.', e)

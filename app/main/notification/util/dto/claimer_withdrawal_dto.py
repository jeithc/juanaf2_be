from flask_restplus import Namespace , fields

class ClaimerWhitedrawalDto():

    api = Namespace('claimer_whitedrawal', description='operaciones relacionadas con la tabla claimer_whitedrawal')

    claimerWhitedrawal = api.model('claimer_whitedrawal', {
        'claimerWithdrawalId': fields.Integer(attribute='claimer_withdrawal_id', description="id del desistimiento"),
        'claimerId': fields.Integer(attribute='claimer_id', description="id del solicitante"),
        'claimerWithdrawalDate': fields.Date(attribute='claimer_withdrawal_date', description="fecha radicacion desistimiento"),
        'userId': fields.Date(attribute='user_id', description="id usuario interno que radica"),
        'documentWithdrawalId': fields.Date(attribute='document_withdrawal_id', description="codigo del documento de solicitud de desistimiento"),
        'withdrawalReason': fields.Date(attribute='withdrawal_reason', description="razon del desistimiento"),
        'numberOfAnnexes': fields.Integer(attribute='number_of_annexes', description="numero de anexos del desistimiento"),
        'numberOfPages': fields.Integer(attribute='number_of_pages', description="numero de paginas del adjunto del desistimiento")
    })
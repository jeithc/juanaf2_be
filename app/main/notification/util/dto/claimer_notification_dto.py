from flask_restplus import Namespace , fields
from app.main.notification.util.dto.attorney_dto import AttorneyDto


class ClaimerNotificationDto():

    api = Namespace('claimer_notification' , description='operaciones relacionadas con la tabla claimer_notification')

    claimerNotification = api.model('claimer_notification' , {
        'notificationId' : fields.Integer(attribute='notification_id' , description= "id del solicitante"),
        'claimerId' : fields.Integer(attribute='claimer_id' , description= "id del solicitante"),
        'notificationType' : fields.String(attribute='notification_type' , description= "tipo de la notificacion"),
        'hasAttorney' : fields.Integer(attribute='has_attorney' , description= "Attributo que indica si tiene apoderado o no"),
        'attorney' : fields.Nested(AttorneyDto.attorney , description=" id apoderado de la notificacion"),
        'notificationDate': fields.Date(attribute='notification_date', description="fecha de la notificacion"),
        'attorneyDocumentId': fields.Integer(attribute='url_attorney_document', description="id documento del apoderado"),
        'notificationDocumentId': fields.Integer(attribute='notification_document_id', description="id acta notificacion generada por el sistema"),
        'signNotificationDocumentId': fields.Integer(attribute='sign_notification_document_id', description="id documento firmado"),
        'aaId': fields.Integer(attribute='aa_id', description="id acto administrativo"),
        'waiverOfTerms': fields.Boolean(attribute='waiver_of_terms', description="Renuncia a terminos"),
        'waiverOfTermsDate': fields.Date(attribute='waiver_of_terms_date', description="Fecha de Renuncia a terminos"),
        'userId': fields.Integer(attribute='user_id', descripcion='Id usuario Dona Juana')
    })


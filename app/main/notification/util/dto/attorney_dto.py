from flask_restplus import Namespace , fields

class AttorneyDto:
    api = Namespace('attorney' , description='Operaciones relacionadas con la tabla Attorney')

    attorney = api.model('attorney' , {
        'attorneyId' : fields.Integer(attribute='attorney_id' , description="id del apoderado"),
        'firstName' : fields.String(attribute='first_name' , description="primer nombre del apoderado"),
        'lastName' : fields.String(attribute='last_name' , description="primer apellido del apoderado"),
        'middleName': fields.String(attribute='middle_name', description="segundo nombre del apoderado"),
        'maidenName': fields.String(attribute='maiden_name', description="segundo apellido del apoderado"),
        'documentType': fields.String(attribute='document_type', description="tipo de documento del apoderado"),
        'documentNumber': fields.String(attribute='document_number', description="numero documento del apoderado"),
        'professionalCardNumber': fields.String(attribute='professional_card_number', description="numero de tarjeta profesional del apoderado"),
        'attorneyType': fields.String(attribute='attorney_type', description="segundo apellido del apoderado"),
        'email': fields.String(description="segundo apellido del apoderado"),
        'phone1': fields.String(attribute='phone_1', description="primer telefono del apoderado"),
        'phone2': fields.String(attribute='phone_2', description="segundo telefono del apoderado"),
        'natgeoId': fields.String(attribute='id_natgeo', description="id depatamento y municipio"),
        'bogLocNeigId': fields.String(attribute='id_bog_loc_neig', description="id localidad y barrio"),
        'urbanRuralLocation' : fields.String(attribute='urban_rural_location' , description="tipo de direccion rural/urbana"),
        'address' : fields.String(attribute='address' , description="direccion de attorney"),
        'createdAt': fields.String(attribute='created_at' , description="fecha de creacion de apoderado"),
        'updatedAt': fields.String(attribute='updated_at' , description="fecha de la ultima actualizacion del apoderado"),
        'country': fields.String(attribute='country' , description="Pais apoderado"),
        'city': fields.String(attribute='city' , description="Ciudad apoderado"),

    })

    attorney_id = api.model('attorney_id' , {
        'attorneyId' : fields.Integer(attribute='attorney_id' , description="id del apoderado")
    })

from flask_restplus import Namespace, fields


class ClaimerPaymentTrackDTO:
    api = Namespace('payment', description='Operaciones relacionadas con los estados de pago')

    claimer_payment_track = api.model('payment_state', {
        'claimerPaymentTrackId': fields.Integer(attribute='claimer_payment_track_id'),
        'claimerId': fields.Integer(attribute='claimer_id'),
        'paymentState': fields.String(attribute='payment_state'),
        'startDate': fields.Date(attribute='start_date'),
        'endDate': fields.Date(attribute='end_date'),
        'userId': fields.Integer(attribute='user_id')
    })

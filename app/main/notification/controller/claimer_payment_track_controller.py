from flask_restplus import Resource

from app.main.notification.service.claimer_payment_track_service import make_payment_transition
from app.main.util.decorator import audit_init, audit_finish, login_app
from app.main.notification.util.dto.claimer_payment_track_dto import ClaimerPaymentTrackDTO
from flask import request


api = ClaimerPaymentTrackDTO.api


@api.route('/claimer_payment_track')
class PaymentState(Resource):
    #@login_app
    @audit_init
    @audit_finish
    @api.expect(ClaimerPaymentTrackDTO.claimer_payment_track, validate=False)
    @api.doc('Avanzar estado de pago')
    def post(self):
        """Avanzar estado de pago"""
        data = request.json
        return make_payment_transition(data)

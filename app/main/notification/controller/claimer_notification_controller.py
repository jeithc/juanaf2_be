from flask import request
from flask_restplus import Resource
from app.main.notification.util.dto.claimer_notification_dto import ClaimerNotificationDto
from app.main.core.util.dto.document_dto import DocumentDTO
from app.main.notification.service import claimer_notification_service
from app.main.util.decorator import audit_init, audit_finish

api = ClaimerNotificationDto.api


@api.route('/claimer_notification')
class ClaimerNotificationList(Resource):

    @api.doc('create claimer_notification')
    @api.expect(ClaimerNotificationDto.claimerNotification, validate=False)
    @audit_init
    @audit_finish
    def post(self):
        """ create claimer_notification"""
        data = request.json
        user_email = request.headers.environ['HTTP_USER_ID']
        return claimer_notification_service.save_claimer_notification(data = data, user_email = user_email)
    
    @api.doc('update claimer_notification')
    @api.expect(ClaimerNotificationDto.claimerNotification , validate=False)
    @audit_init
    @audit_finish
    def put(self):
        """ update claimer_notification"""
        data = request.json
        return claimer_notification_service.updated_claimer_notification(data = data)


@api.route('/claimer_notification/<claimer_id>/<aa_id>')
class NotificationByClaimer(Resource):
    
    @api.doc('get claimer_notification')
    @api.marshal_with(ClaimerNotificationDto.claimerNotification , 200)
    @audit_init
    @audit_finish
    def get(self , claimer_id , aa_id):
        """get claimer_notification"""
        return claimer_notification_service.get_claimer_notification(claimer_id = claimer_id , aa_id = aa_id)


@api.route('/generate_document')
class GenerateDocument(Resource):

    @api.doc('post generate_document')
    @api.expect(DocumentDTO.document, validate=False)
    @audit_init
    @audit_finish
    def post(self):
        """get generate_document"""
        data = request.json
        return claimer_notification_service.generate_document(data)


@api.route('/claimer_notification/reprocess')
class Reprocess(Resource):
    @api.doc('save reprocess reason')
    @audit_init
    @audit_finish
    def post(self):
        """activate reprocess user"""
        data = request.json           
        return claimer_notification_service.save_reprocess(data)

@api.route('/waiver_of_terms')
class WaiverOfTerms(Resource):
    @api.doc('WaiverOfTerms of claimer')
    @audit_init
    @audit_finish
    def post(self):
        """WaiverOfTerms of claimer"""
        data = request.json
        return claimer_notification_service.waiverOfTerms(claimer=data)

@api.route('/notify_users/<limit>')
@api.param('limit', 'limit consulta')
class Reprocess(Resource):
    @api.doc('notify all users')
    @audit_init
    @audit_finish
    def get(self, limit):
        """process notify users """            
        return claimer_notification_service.send_notification(2,4, limit)


@api.route('/resend_notify_users/')
class Reprocess(Resource):
    @api.doc('resend notify all users')
    @audit_init
    @audit_finish
    def get(self):
        """process resend notify users """            
        return claimer_notification_service.resend_notification(2,2)


@api.route('/notify_conclusive_behavior')
@api.response(204, "Error al notificar")
class ConclusiveBehavior(Resource):
    @api.doc('Notificar por conducta conclusiva')
    # @login_app
    @audit_init
    @audit_finish
    def post(self):
        """view radicates claimer"""

        data = request.json
        return claimer_notification_service.conclusive_behavior_notify(data)










from flask import request
from flask_restplus import Resource
from app.main.notification.util.dto.attorney_dto import AttorneyDto
from app.main.notification.service import attorney_service
from app.main.util.decorator import audit_init, audit_finish


api = AttorneyDto.api


@api.route('/attorney')
class AttorneyList(Resource):
    @api.doc('create new Attorney')
    @api.expect(AttorneyDto.attorney, validate=False)
    @audit_init
    @audit_finish
    def post(self):
        """ Create new Attorney """
        data = request.json
        return attorney_service.save_new_attorney(data=data)
    
    @api.doc('get list attorneys')
    @api.marshal_with(AttorneyDto.attorney, 200)
    @audit_init
    @audit_finish
    def get(self):
        """ get list attorneys """
        return attorney_service.get_all_attorneys()


@api.route('/attorney_by_resource_id/<resource_id>')
class AttorneyList(Resource):
    @api.doc('get attorney by id')
    @api.marshal_with(AttorneyDto.attorney, 200)
    @audit_init
    @audit_finish
    def get(self, resource_id):
        """ get list attorneys """
        return attorney_service.get_attorney_by_id(resource_id)


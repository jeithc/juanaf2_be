from flask import request
from flask import json
from app.main import logger
from flask_restplus import Resource
from app.main.messaging.service import claimer_message_service
from app.main.messaging.util.dto.claimer_message_dto import ClaimerMessageDto
from app.main.service import user_service
from app.main.service import climer_documents_service
from app.main.core.service import report_notification_massive_service
from app.main.messaging.service import message_address_service
from app.main.core.service import workflow_service
from app.main.core.service.report_notification_massive_service import ReportJasperMasivo
from app.main.util.decorator import audit_init, audit_finish


api = ClaimerMessageDto.api


@api.route('/notification/fisical_notification')
class ClaimerNotificationFisicalUsers(Resource):
	@audit_init
	@audit_finish
	def get(self):
		users = user_service.get_users_for_fisical_notification()
		lista = json.loads(users)
		for user in lista:
			data = workflow_service.get_current_state(user['claimer_id'])
			if  data['currentState'] == 15:
    		
				dataMct = {
					'claimerId' : user['claimer_id'],
					'nextState' : 18,
					'nextUser' : 0,
					'logUser' : '0'
			 	}
				
				workflow_service.make_claimer_transition(dataMct)
			
		else:
    				
			users = user_service.get_users_fisical_notification_whit_address()
			lista = json.loads(users)
			for user in lista:
				data2 = workflow_service.get_current_state(user['claimer_id'])
				if data2['currentState'] == 18:
				
					dataMa = {
								'address' : user['address'],
								'idNatgeo' : user['id_natgeo'],
								'urbanRuralLocation' : user['urban_rural_location'],
								'idBogLocNeig' : user['id_bog_loc_neig'],
								'country' : 'COLOMBIA'
							}

					ma = message_address_service.save_new_message_address(dataMa)
						
					dataCm = {
						'personExpedientId' : user['person_expedient_id'],
						'claimerId' : user['claimer_id'],
						'aaId' : '1',
						'claimerMessageMode' : 'FISICO',
						'claimerMessageType' : 'NOTIFICACION',
						'addressId' : ma[0].address_id,
					}
					claimer_message_service.save_new_message(dataCm)
					ReportJasperMasivo.create_report(8, user['claimer_id'])
				# TODO servicio de H26 guardar resultados de empresa de mensajeria
		return json.loads(users)
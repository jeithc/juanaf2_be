from flask import request
from flask_restplus import Resource
from app.main.notification.util.dto.claimer_withdrawal_dto import ClaimerWhitedrawalDto
from app.main.notification.service import claimer_whitedrawal_service


api = ClaimerWhitedrawalDto.api


@api.route('/claimer_whitedrawal')
class ClaimerWhiteDrawalList(Resource):

    @api.doc('create claimer_whitedrawal')
    #@api.expect(ClaimerWhitedrawalDto.claimerWhitedrawal, validate=False)
    def post(self):
        """ create claimer_whitedrawal """
        data = request.json
        print (request.json)
        return claimer_whitedrawal_service.save_new_claimer_whitedrawal(data=data)
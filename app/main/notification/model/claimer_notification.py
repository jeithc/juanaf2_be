from app.main import db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship


class ClaimerNotification(db.Model):
    """claimerNotification model"""
    __tablename__ = 'claimer_notification'
    __table_args__ = {'schema': 'phase_2'}

    notification_id = db.Column(db.Integer , primary_key=True , autoincrement=True)
    claimer_id = db.Column(db.Integer)
    notification_type = db.Column(db.String(100))
    has_attorney = db.Column(db.Boolean)
    attorney_id = db.Column(db.Integer , ForeignKey('phase_2.attorney.attorney_id'))
    notification_date = db.Column(db.Date)
    url_attorney_document = db.Column(db.Integer, ForeignKey('phase_2.claimer_documents.document_id'))
    notification_document_id = db.Column(db.Integer, ForeignKey('phase_2.claimer_documents.document_id'))
    sign_notification_document_id = db.Column(db.Integer,ForeignKey('phase_2.claimer_documents.document_id'))
    aa_id = db.Column(db.Integer)
    claimer_notified = db.Column(db.Boolean)
    attorney = relationship("Attorney", back_populates="claimer_notification")
    waiver_of_terms = db.Column(db.Boolean)
    waiver_of_terms_date = db.Column(db.Date)
    user_id = db.Column(db.Integer)
    claimer_message_id = db.Column(db.Integer)



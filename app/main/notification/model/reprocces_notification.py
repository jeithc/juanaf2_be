from app.main import db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship


class ReprocessNotification(db.Model):
    """Reprocess table model"""
    __tablename__ = 'reprocess_notification'
    __table_args__ = {'schema': 'phase_2'}

    reprocess_notification_id = db.Column(db.Integer , primary_key=True , autoincrement=True)
    document_id = db.Column(db.Integer)
    reprocess_causal = db.Column(db.String(100))
    aa_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    claimer_id = db.Column(db.Integer)
    old_notification_date = db.Column(db.DateTime)
    previous_notification_id = db.Column(db.Integer)
    reprocess_type = db.Column(db.Boolean)
   
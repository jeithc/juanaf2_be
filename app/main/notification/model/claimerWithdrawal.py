from app.main import db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship


class ClaimerWithdrawal(db.Model):
    "claimerWithdrawal model"
    __tablename__ = 'claimer_withdrawal'
    __table_args__ = {'schema': 'phase_2'}

    claimer_withdrawal_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    claimer_id = db.Column(db.Integer)
    claimer_withdrawal_date = db.Column(db.Date)
    user_id = db.Column(db.Integer)
    document_withdrawal_id = db.Column(db.Integer)
    withdrawal_reason = db.Column(db.Integer)
    number_of_annexes = db.Column(db.Integer)
    number_of_pages = db.Column(db.Integer)
from app.main import db
from sqlalchemy.orm import relationship


class Attorney(db.Model):
    "attorney Model for claimer"
    __tablename__ = "attorney"
    __table_args__ = {'schema' : 'phase_2'}

    attorney_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    document_type = db.Column(db.String(100))
    document_number = db.Column(db.String(100))
    professional_card_number = db.Column(db.String(100))
    attorney_type = db.Column(db.String(100))
    first_name = db.Column(db.String(128))
    last_name = db.Column(db.String(128))
    middle_name = db.Column(db.String(128))
    maiden_name = db.Column(db.String(128))
    phone_1 = db.Column(db.String(64))
    phone_2 = db.Column(db.String(64))
    id_natgeo = db.Column(db.Integer)
    id_bog_loc_neig = db.Column(db.Integer)
    urban_rural_location = db.Column(db.String(1))
    address = db.Column(db.String(255))
    created_at = db.Column(db.Date)
    updated_at = db.Column(db.Date)
    email = db.Column(db.String(100))
    country = db.Column(db.String(100))
    city = db.Column(db.String(100))
    claimer_notification = relationship("ClaimerNotification" , back_populates="attorney")





from app.main import db


class ClaimerPaymentTrack(db.Model):
    """ClaimerPaymentTrack model"""
    __tablename__ = 'claimer_payment_track'
    __table_args__ = {'schema': 'phase_2'}

    claimer_payment_track_id = db.Column(db.Integer, primary_key=True)
    claimer_id = db.Column(db.Integer)
    payment_state = db.Column(db.String)
    start_date = db.Column(db.Integer)
    end_date = db.Column(db.Integer)
    user_id = db.Column(db.Integer)


def __repr__(self):
    return "<ClaimerExpedientState '{}'>".format(self.ces_id)

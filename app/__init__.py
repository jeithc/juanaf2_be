from app.main.substantiation.controller.questionaire_controller import api as questionaire_controller_ns
from app.main.substantiation.controller.resource_proof_request_controller import api as resource_proof_request_ns
from app.main.substantiation.controller.expedient_state_controller import api as expedient_state_controller_ns
from flask_restplus import Api
from flask import Blueprint

from .main.controller.user_controller import api as juana_ns
from .main.controller.report_controller import api as report_ns
from .main.controller.auth_controller import api as auth_ns
from .main.controller.domain_controller import api as domain_ns
from .main.controller.nat_geo_controller import api as nat_geo_ns
from .main.controller.notorious_facts_places_controller import api as notorious_facts_places_ns
from .main.controller.questions_controller import api as questions_ns
from .main.controller.news_controller import api as news_ns
from .main.controller.concurrency_controller import api as news_ns

from app.main.core.controller.user_controller import api as users_ns
from app.main.core.controller.role_controller import api as roles_ns
from app.main.core.controller.authentication_controller import api as authentication_ns
from app.main.core.controller.audit_controller import api as audit_ns
from app.main.core.controller.workflow_controller import api as workflow_ns
from app.main.core.controller.massive_controller import api as notification_ns
from app.main.core.controller.claimer_citation_controller import api as citation_ns
from app.main.core.controller.stage_parameters_controller import api as stage_parameters_ns
from app.main.core.controller.app_params_controller import api as app_params_controller_ns
from app.main.notification.controller.attorney_controller import api as attorney_ns
from app.main.notification.controller.claimer_notification_controller import api as claimer_notification_ns
from app.main.core.controller.document_controler import api as document_ns
from app.main.messaging.controller.claimer_message_controller import api as claimer_message_controller_ns
from app.main.messaging.controller.message_address_controller import api as message_address_ns
from app.main.citation.controller.message_fisical_citation_controller import api as message_fisical_citation_ns
from app.main.core.controller.administrative_act_controller import api as administrative_act_ns
from app.main.notification.controller.message_fisical_notification_controller import api as message_fisical_notification_ns
from app.main.citation.controller.subgroup_controller import api as subgroup_controller_ns
from app.main.citation.controller.place_controller import api as place_controller_ns
from app.main.citation.controller.aa_controller import api as aa_controller_ns
from app.main.citation.controller.citation_controller import api as citation_ms
from app.main.notification.controller.claimer_whitedrawal_controller import api as claimer_whitedrawal_ns
from app.main.controller.documents_controller import api as radicate_document_ns
from app.main.radication.controller.resource_documents_controller import api as radication_ns
from app.main.messaging.controller.document_type_controller import api as document_type_ns
from app.main.messaging.controller.document_typology_controller import api as document_typology_ns
from app.main.radication.controller.radicates_table_controller import api as radicates_table_ns
from app.main.core.electronic_signature.controller.signature_controller import api as signature_ns
from app.main.radication.controller.radication_attorney_controller import api as radication_attorney_ns
from app.main.core.controller.frame_list_controller import api as frame_list_ns
from app.main.citation.controller.subgroup_controller import api_publication as publication_api_ns
from app.main.core.controller.notification_controller import api as notification_serv_ns
from app.main.crud_claimer.controller.update_claimer_controller import api as update_claimer_ns
from app.main.mail.controller.mail_controller import api as mail_ns


from app.main.core.controller.test_mail_verify import api as test_mail_verify_ns

from app.main.documental.controller.document_box_controller import api as document_box_ns
from app.main.documental.controller.document_folder_controller import api as document_folder_ns
from app.main.documental.controller.request_controller import api as request_ns
from app.main.documental.controller.claimer_physcal_expedient_controller import api as claimer_physcal_expedient_ns
from app.main.documental.controller.third_person_controller import api as third_person_ns
from app.main.documental.controller.physical_expedient_loan_controller import api as expedient_loan_ns
from app.main.core.controller.testing_expedient_controller import api as testing_expedient_ns

from app.main.substantiation.controller.task_assignment_controller import api as substantiation_ns
from app.main.core.controller.serie_subserie_controller import api as serie_subserie_ns
from app.main.core.controller.serie_subserie_attributes_controller import api as serie_subserie_attributes_ns
from app.main.core.controller.serie_subserie_metadata_controller import api as serie_subserie_metadata_ns

from app.main.substantiation.controller.res_document_serie_subserie_controller import api as \
    res_document_serie_subserie_ns
from app.main.substantiation.controller.res_answer_per_document_controller import api as res_answer_per_document_ns
from app.main.substantiation.controller.claimer_resource_state_controller import api as claimer_resource_state_ns
from app.main.substantiation.controller.subserie_correction_request_controller import api as \
    subserie_correction_request_ns
from app.main.substantiation.controller.res_auto_proof_document_controller import api as \
    res_auto_proof_document_controller_ns
from app.main.substantiation.controller.resource_proof_request_controller import api as resource_proof_request_ns
from app.main.substantiation.controller.questionaire_controller import api as questionaire_controller_ns
from app.main.substantiation.controller.resolutive_numeral_controller import api as resolutive_numeral_ns
from app.main.substantiation.controller.document_section_controller import api as document_section_ns

from app.main.juana1.controller.juana1_controller import api as juana1_ns

from app.main.genereate_resolution.controller.response_controller import api as response_ns
from app.main.substantiation.controller.total_expedient_controller import api as total_expedient_ns

from app.main.notification.controller.claimer_payment_track_controller import api as payment_state_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='BackEnd Juana Fase 2',
          version='1.0.0',
          description='Webservices for project Juana Fase 2'
          )

api.add_namespace(juana_ns)
api.add_namespace(auth_ns)
api.add_namespace(report_ns)
api.add_namespace(domain_ns)
api.add_namespace(nat_geo_ns)
api.add_namespace(notorious_facts_places_ns)
api.add_namespace(questions_ns)
api.add_namespace(news_ns)

api.add_namespace(roles_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(users_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(authentication_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(workflow_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(stage_parameters_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(app_params_controller_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(audit_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(frame_list_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(testing_expedient_ns, path='/juanaf2/v1.0.0/core')

api.add_namespace(attorney_ns, path='/juanaf2/v1.0.0/notification')
api.add_namespace(claimer_notification_ns, path='/juanaf2/v1.0.0/notification')
api.add_namespace(notification_serv_ns,
                  path='/juanaf2/v1.0.0/claimer_notification')
api.add_namespace(citation_ns, path='/juanaf2/v1.0.0/citation')

api.add_namespace(document_ns, path='/juanaf2/v1.0.0/claimer_document')

api.add_namespace(notification_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(claimer_message_controller_ns,
                  path='/juanaf2/v1.0.0/messaging')
api.add_namespace(message_address_ns, path='/juanaf2/v1.0.0/messaging')
api.add_namespace(message_fisical_citation_ns, path='/juanaf2/v1.0.0/citation')
api.add_namespace(administrative_act_ns,
                  path='/juanaf2/v1.0.0/administrative_act')

api.add_namespace(message_fisical_notification_ns,
                  path='/juanaf2/v1.0.0/notification')
api.add_namespace(subgroup_controller_ns, path='/juanaf2/v1.0.0/citation')
api.add_namespace(subgroup_controller_ns, path='/juanaf2/v1.0.0/citation')
api.add_namespace(place_controller_ns, path='/juanaf2/v1.0.0/citation')
api.add_namespace(publication_api_ns, path='/juanaf2/v1.0.0/citation')
api.add_namespace(aa_controller_ns, path='/juanaf2/v1.0.0/citation')

api.add_namespace(citation_ms, path='/juanaf2/v1.0.0/citation')

api.add_namespace(test_mail_verify_ns,
                  path='/juanaf2/v1.0.0/mail_verify_status')
api.add_namespace(update_claimer_ns, path='/juanaf2/v1.0.0/update_claimer')

api.add_namespace(claimer_whitedrawal_ns, path='/juanaf2/v1.0.0/nofication')

api.add_namespace(radicate_document_ns, path='/juanaf2/v1.0.0/document')
api.add_namespace(radicates_table_ns, path='/juanaf2/v1.0.0/radication')
api.add_namespace(mail_ns, path='/juanaf2/v1.0.0/mail')

api.add_namespace(radication_ns, path='/juanaf2/v1.0.0/radication')


api.add_namespace(document_type_ns, path='/juanaf2/v1.0.0/document_type')
api.add_namespace(document_typology_ns,
                  path='/juanaf2/v1.0.0/document_typology')
api.add_namespace(radication_ns, path='/juanaf2/v1.0.0/radication')
api.add_namespace(radicates_table_ns, path='/juanaf2/v1.0.0/radication')
api.add_namespace(radication_attorney_ns, path='/juanaf2/v1.0.0/radication')

api.add_namespace(document_box_ns, path='/juanaf2/v1.0.0/documental')
api.add_namespace(document_folder_ns, path='/juanaf2/v1.0.0/documental')
api.add_namespace(request_ns, path='/juanaf2/v1.0.0/documental')
api.add_namespace(claimer_physcal_expedient_ns,path='/juanaf2/v1.0.0/documental')
api.add_namespace(third_person_ns, path='/juanaf2/v1.0.0/documental')
api.add_namespace(expedient_loan_ns, path='/juanaf2/v1.0.0/documental/physical_expedient_loan')

api.add_namespace(substantiation_ns, path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(signature_ns, path='/juanaf2/v1.0.0/signature')
api.add_namespace(questionaire_controller_ns, path='/juanaf2/v1.0.0/substantiation')

api.add_namespace(serie_subserie_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(serie_subserie_attributes_ns, path='/juanaf2/v1.0.0/core')
api.add_namespace(serie_subserie_metadata_ns, path='/juanaf2/v1.0.0/core')

api.add_namespace(expedient_state_controller_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(res_auto_proof_document_controller_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(res_document_serie_subserie_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(res_answer_per_document_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(claimer_resource_state_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(subserie_correction_request_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(resource_proof_request_ns,path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(resolutive_numeral_ns, path='/juanaf2/v1.0.0/substantiation')

api.add_namespace(juana1_ns, path='/juanaf2/v1.0.0/juana1')
api.add_namespace(document_section_ns, path='/juanaf2/v1.0.0/substantiation')
api.add_namespace(response_ns, path='/juanaf2/v1.0.0/response_resolution')
api.add_namespace(total_expedient_ns, path='/juanaf2/v1.0.0/substantiation')

api.add_namespace(payment_state_ns, path='/juanaf2/v1.0.0/payment')




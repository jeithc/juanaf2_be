#### FLASK RESTFUL API BOILER-PLATE WITH JWT

### Terminal commands

    Initial installation: pip install -r requirements.txt

    To use development server: export FLASK_ENV=development

    To run application: python manage.py run



### Viewing the app ###

    Open the following url on your browser to view swagger documentation
    http://127.0.0.1:8080/


### Using Postman ####

    Authorization header is in the following format:

    Key: Authorization
    Value: "token_generated_during_login"

    For testing authorization, url for getting all user requires an admin token while url for getting a single
    user by public_id requires just a regular authentication.

### Full description and guide ###
https://medium.freecodecamp.org/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563


### Contributing
If you want to contribute to this flask restplus boilerplate, clone the repository and just start making pull requests.

```
https://github.com/cosmic-byte/flask-restplus-boilerplate.git
```
